//
//  AnoutUsViewController.swift
//  SYM
//
//  Created by Sagar Pandit on 10/11/21.
//  Copyright © 2021 AM. All rights reserved.
//
import UIKit
import AVFoundation


class AboutUsViewController: UIViewController {
    //MARK: IBOUtlets
    @IBOutlet var blackOverlay: UIView!
    
    var player: AVPlayer?



    override func viewDidLoad() {
        super.viewDidLoad()
        guard let path = Bundle.main.path(forResource: "welcome", ofType:"mov") else {
            return
        }
        // begin implementing the avplayer
        // "https://player.vimeo.com/video/425784803"
        player = AVPlayer(url: URL(fileURLWithPath: path))
        player?.actionAtItemEnd = .none
        player?.isMuted = false
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        
        playerLayer.frame = CGRect(x: blackOverlay.frame.minX-15, y: blackOverlay.frame.minY, width: self.view.frame.width*0.9, height: 200)
        playerLayer.videoGravity = .resizeAspectFill
        blackOverlay.layer.masksToBounds = true
        blackOverlay.layer.addSublayer(playerLayer)
        blackOverlay.translatesAutoresizingMaskIntoConstraints = false
        player?.play()
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: .main) { [weak self] _ in
            self?.player?.play()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        player?.play()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        player?.pause()
    }
    
    func playerItemDidReachEnd() {
        player!.seek(to: CMTime.zero)
    }
    
    @objc func loopVideo() {
        player?.seek(to: CMTime.zero)
        player?.play()
    }
}

