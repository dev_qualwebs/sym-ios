//
//  ViewController.swift
//  SYM
//
//  Created by AM on 18/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavTitle(title: "")
        self.setNavigationBar("back", nil)
        self.navigationController?.isNavigationBarHidden = false
        UINavigationBar.appearance().barTintColor = primaryColor
       UINavigationBar.appearance().tintColor = .white
        self.handleNavigation()
    }
    
    func handleNavigation(){
        if let token = UserDefaults.standard.value(forKey: UD_TOKEN) as? String{
            
            Router.DriverHomeVC()
        }
        else{
            
            Router.entryHomeVC()
        }
    }
}

