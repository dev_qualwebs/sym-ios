//
//  ForgotPasswordViewController.swift
//  SYM
//
//  Created by qw on 13/07/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPasswordViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var emailAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var otpView: UIStackView!
    @IBOutlet weak var otpField: SkyFloatingLabelTextField!
    @IBOutlet weak var newPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var submitButton: CustomButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavTitle(title: "Forgot Password")
        setNavigationBar("back", "none")
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {

    }
    
    func sendOtp(){
       ActivityIndicator.show(view: self.view)
       SessionManager.shared.methodForApiCalling(url: U_BASE + U_FORGOT_PASSWORD, method: .post, parameter: ["email": self.emailAddress.text ?? ""], objectClass: Response.self, requestCode: U_FORGOT_PASSWORD, userToken: nil) { (response) in
           ActivityIndicator.hide()
           self.otpView.isHidden = false
           self.otpField.text = ""
           self.newPassword.text = ""
           
          self.emailAddress.isUserInteractionEnabled = false
            Singleton.shared.showToast(text: "Please enter OTP sent to your email address", color: errorRed)
       }
    }
    
    //MARK: IBAction
    @IBAction func submitAction(_ sender: Any) {
        if(submitButton.titleLabel?.text == "Send OTP"){
            if(emailAddress.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter your email address", color: errorRed)
            }else if !(self.isValidEmail(testStr:emailAddress.text ?? "")){
                Singleton.shared.showToast(text: "Enter your email address", color: errorRed)
            }else {
                self.sendOtp()
                self.submitButton.setTitle("Reset", for: .normal)
            }
            
        }else {
            if(self.otpField.text!.isEmpty){
               Singleton.shared.showToast(text: "Enter OTP", color: errorRed)
            }else if(self.newPassword.text!.isEmpty){
               Singleton.shared.showToast(text: "Enter new password", color: errorRed)
            }else {
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_RESET_PASSWORD, method: .post, parameter: ["otp": self.otpField.text ?? "","password":self.newPassword.text ?? ""], objectClass: Response.self, requestCode: U_RESET_PASSWORD, userToken: nil) { (response) in
                    ActivityIndicator.hide()
                    self.otpView.isHidden = false
                    self.otpField.text = ""
                    self.newPassword.text = ""
                   self.emailAddress.isUserInteractionEnabled = false
                     Singleton.shared.showToast(text: "Password Reset Successfully", color: successGreen)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    @IBAction func resendOtpAction(_ sender: Any) {
        self.otpView.isHidden = true
         self.submitButton.setTitle("Send OTP", for: .normal)
        self.emailAddress.isUserInteractionEnabled = true
    }
    
    
}
