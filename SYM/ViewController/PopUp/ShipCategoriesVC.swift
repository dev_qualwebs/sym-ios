//
//  ShipCategoriessVC.swift
//  SYM
//
//  Created by AM on 04/12/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

protocol SelectedCategories {
    func currentSelectedCategories(id: Set<Int>,categories:[GetCategories])
    func currentSelectedSubCategories(id: Set<Int>,categories:[GetSubCategories])
}

class ShipCategoriesVC: UIViewController {
    
    //Mark: IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headingLabel: DesignableUILabel!
    
    //Mark: PROPERTIES
    var categoryData = [GetCategories]()
    var categoryDelegate:SelectedCategories? = nil
    var selectedCategories = [GetCategories]()
    var selectedSubCategories = [GetSubCategories]()
    var subcategories = [GetSubCategories]()
    var selectedCategory = Set<Int>()
    var isSubcategory = false
    var headingText = "Select Subcategory"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if((self.headingText ?? "") != ""){
            self.headingLabel.text = self.headingText
        }
    }
    
    //MARK: IBActions
    @IBAction func doneBtnPressed(_ sender: Any) {
        if(isSubcategory){
            self.categoryDelegate?.currentSelectedSubCategories(id: self.selectedCategory, categories: self.selectedSubCategories)
        }else{
            self.categoryDelegate?.currentSelectedCategories(id: self.selectedCategory, categories: self.selectedCategories)
        }
        dismiss(animated: true, completion: nil)
    }
    
    
}

extension ShipCategoriesVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isSubcategory){
            return subcategories.count
        }else{
            return self.categoryData.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ShipCategoriesCell", for: indexPath) as! ShipCategoriesCell
        if(isSubcategory){
            let val = self.subcategories[indexPath.row]
            for cat in self.selectedCategory {
                if(val.id == cat){
                    self.selectedSubCategories.append(val)
                    cell.cellImage.image = #imageLiteral(resourceName: "GreyTick")
                }
            }
            cell.lblCategories.text = val.category
            
        }else {
            let val = self.categoryData[indexPath.row]
            for cat in self.selectedCategory {
                if(val.id == cat){
                    self.selectedCategories.append(val)
                    cell.cellImage.image = #imageLiteral(resourceName: "GreyTick")
                }
            }
            cell.lblCategories.text = val.category
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ShipCategoriesCell
        if(cell.cellImage.image == #imageLiteral(resourceName: "GreyTick")){
            cell.cellImage.image = #imageLiteral(resourceName: "GreyBox")
            if(isSubcategory){
                self.selectedCategory.remove(self.subcategories[indexPath.row].id ?? 0)
                                
                self.selectedSubCategories.filter{
                    $0.id != self.subcategories[indexPath.row].id ?? 0
                }
                
                for i in 0..<self.selectedSubCategories.count {
                    if self.subcategories[indexPath.row].id == self.selectedSubCategories[i].id {
                        self.selectedSubCategories.remove(at: i)
                        break
                    }
                }
            }else{
                self.selectedCategories.filter{
                    $0.id != self.categoryData[indexPath.row].id ?? 0
                }
                
                self.selectedCategory.remove(self.categoryData[indexPath.row].id ?? 0)
            }
            
        }else{
            cell.cellImage.image = #imageLiteral(resourceName: "GreyTick")
            if(isSubcategory){
                self.selectedSubCategories.append(self.subcategories[indexPath.row])
                self.selectedCategory.insert(self.subcategories[indexPath.row].id ?? 0)
            }else{
                self.selectedCategories.append(self.categoryData[indexPath.row])
                self.selectedCategory.insert(self.categoryData[indexPath.row].id ?? 0)
            }
            
        }
    }
}

class ShipCategoriesCell: UITableViewCell{
    @IBOutlet weak var lblCategories: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    
    var deleteButton:(()-> Void)? = nil
    
    @IBAction func deleteAction(_ sender: Any) {
        if let deleteButton = self.deleteButton{
            deleteButton()
        }
    }
}
