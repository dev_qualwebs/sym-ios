//
//  EcommerceCategoryDetailViewController.swift
//  SYM
//
//  Created by qw on 18/09/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
import iOSDropDown

class EcommerceCategoryDetailViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var attachmentCell: UICollectionView!
   // @IBOutlet weak var productName: DesignableUILabel!
    @IBOutlet weak var productName: UIButton!
    @IBOutlet weak var productPrice: DesignableUILabel!
    @IBOutlet weak var productDetails: DesignableUILabel!
    @IBOutlet weak var quantityDropdown: DropDown!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    var productData = GetProductResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar("back",nil)
        for i in 0..<(self.productData.quantity ?? 0) {
            self.quantityDropdown.optionArray.append("\(i+1)")
        }
        self.quantityDropdown.text = "1"
        self.setNavTitle(title: self.productData.product_name ?? "")
        //self.productName.text = self.productData.product_name
        self.productName.setTitle(self.productData.product_name, for: .normal)
        self.productPrice.text = "$" + (self.productData.price ?? "")
        self.productDetails.text = self.productData.description
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        APICallingViewController.shared.getCartData { (data) in
            self.setNavigationBar("back", nil)
            
        }
    }
    
    //MARK: IBAction
    @IBAction func addToCartAction(_ sender: Any) {
        let alert = UIAlertController(title: "Add Item to cart", message: nil, preferredStyle: .alert)
        let actionOne = UIAlertAction(title: "Yes", style: .default) { (action) in
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "product_id":self.productData.id ?? 0,
                "quantity":self
                    .quantityDropdown.text
            ]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE+U_ADD_TO_CART, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_TO_CART, userToken: nil) { (response) in
                Singleton.shared.showToast(text:response.message ?? "", color: successGreen)
              
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
        
        let actionTwo = UIAlertAction(title: "No", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(actionTwo)
        alert.addAction(actionOne)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func buyNowAction(_ sender: Any) {
        self.addToCartAction(self)
    }
    
    @IBAction func nameAction(_ sender: Any) {
       // self.openUrl(urlStr: self.productData.product_link ?? "")
    }
    

}

extension EcommerceCategoryDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.pageControl.numberOfPages = self.productData.product_images?.count ?? 0
        return self.productData.product_images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCell", for: indexPath) as! AttachmentCell
        if((self.productData.product_images?.count ?? 0) > 0){
            cell.attachmentImage.sd_setImage(with: URL(string:U_IMAGE + (self.productData.product_images![indexPath.row].image ?? "")), placeholderImage: nil)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.attachmentCell.frame.width)-5, height: self.attachmentCell.frame.height)
    }
}
