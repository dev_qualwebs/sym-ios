//
//  DriverHomeVC.swift
//  SYM
//
//  Created by AM on 25/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit


protocol DashboardReloadData {
    func reloadData()
}

class DriverHomeVC: UIViewController , DashboardReloadData{
    func reloadData() {
        lblUserName.text = "Hi, \(Singleton.shared.userType.first_name ?? "")" + " " +  "\(Singleton.shared.userType.last_name ?? "")"
        self.view.isUserInteractionEnabled = true
        self.userImgView.sd_setImage(with: URL(string:U_IMAGE + (Singleton.shared.userType.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "placeholder"))
    }
    
    
    //Mark: IBOUTLETS
    @IBOutlet weak var lblUserName: DesignableUILabel!
    @IBOutlet weak var userImgView: ImageView!
    @IBOutlet weak var totalShipment: DesignableUILabel!
    @IBOutlet weak var totalDispute: DesignableUILabel!
    @IBOutlet weak var totalMiles: DesignableUILabel!
    @IBOutlet weak var totalReview: DesignableUILabel!
    @IBOutlet weak var inProgressshipment: DesignableUILabel!
    @IBOutlet weak var progressView: StepProgressView!
    
    var dashboardData = DashboardData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.isUserInteractionEnabled = true
        self.getCards()
        self.getDashboardData()
        EditProfileVC.dahboardReloadDelegate = self
        self.progressView.viewShipment={
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.navigationController?.pushViewController(myVC, animated: true)
         }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lblUserName.text = "Hi, \(Singleton.shared.userType.first_name ?? "")" + " " +  "\(Singleton.shared.userType.last_name ?? "")"
    }
  
    override func viewDidAppear(_ animated: Bool) {
        self.view.isUserInteractionEnabled = true
        self.userImgView.sd_setImage(with: URL(string:U_IMAGE + (Singleton.shared.userType.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        self.setNavigationBar("menu", nil)
        self.setNavTitle(title: "Dashboard")
      
       if(Singleton.shared.userType.role != 1){
       self.progressView.showAnimatingDots()
       self.progressView.showHideView={
           if(Singleton.shared.progressShipment?.count == 0){
               self.progressView.isHidden = true
           }else{
               for val in Singleton.shared.progressShipment!{
                   if(val.driver_status == 5){
                       self.progressView.isHidden = false
                   }
               }
           }
       }
       }
    }
    
    func getCards(){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_DRIVER_SAVED_CARDS, method: .post, parameter: nil, objectClass: GetCard.self, requestCode: U_GET_DRIVER_SAVED_CARDS, userToken: nil) { (response) in
            Singleton.shared.savedCardData = response.response
            self.view.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func totalShipmentAction(_ sender: Any) {
        K_CURRENT_CATEGORY = 0
        NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_CURRENT_TAB), object: nil,userInfo: ["index":1])
    }
    
    @IBAction func totalDisputeAction(_ sender: Any) {
        K_CURRENT_CATEGORY = 6
        NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_CURRENT_TAB), object: nil,userInfo: ["index":1])
    }
    
    @IBAction func totalMilesAction(_ sender: Any) {
    }
    
    @IBAction func totalReviewsAction(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_CURRENT_TAB), object: nil,userInfo: ["index":3])
    }
    
    @IBAction func inProgressAction(_ sender: Any) {
        K_CURRENT_CATEGORY = 4
        NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_CURRENT_TAB), object: nil,userInfo: ["index":1])
    }
    
    func getDashboardData() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_DASHBOARD_DATA, method: .get, parameter: nil, objectClass: GetDashboardData.self, requestCode: U_DASHBOARD_DATA, userToken: nil) { (response) in
            self.dashboardData  = response.response
            self.totalReview.text = "\(self.dashboardData.total_reviews ?? 0)"
            self.totalMiles.text = "\(self.dashboardData.total_distance ?? 0)"
            self.totalDispute.text = "\(self.dashboardData.total_disputes ?? 0)"
            self.totalShipment.text = "\(self.dashboardData.total_shipment ?? 0)"
            self.inProgressshipment.text = "\(self.dashboardData.inprogress_shipment ?? 0)"
            ActivityIndicator.hide()
            self.view.isUserInteractionEnabled = true
        }
    }

   
}
