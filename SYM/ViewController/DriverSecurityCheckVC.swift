//
//  DriverSecurityCheckVC.swift
//  SYM
//
//  Created by AM on 09/12/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import iOSDropDown

class DriverSecurityCheckVC: UIViewController, UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField==self.ssnTxtField){
            let currentCharacterCount = self.ssnTxtField.text?.count ?? 0
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
            }
            if(currentCharacterCount < 9){
                return true
            } else {
                return false
            }
        }
        return true
    }
    
    
    
    //Mark: IBOUTLETS
    @IBOutlet weak var firstNameTxtField: UITextField!
    @IBOutlet weak var lastNameTxtField: UITextField!
    @IBOutlet weak var phoneTxtField: UITextField!
    @IBOutlet weak var lblStateResidence: DropDown!
    @IBOutlet weak var lblDateofBirth: UILabel!
    @IBOutlet weak var otpTxtfield: UITextField!
    @IBOutlet weak var ssnTxtField: UITextField!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var popupLabel: DesignableUILabel!
    
    //Mark: Properties
    var state = [String]()
    var stateID = Int()
    var stateResidence = [GetStateResidenceDetails]()
    var isComingDirect = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavTitle(title: "Safety Check")
        self.setNavigationBar("back", nil)
        self.firstNameTxtField.text = "\(Singleton.shared.userDetail.first_name ?? "")"
        self.lastNameTxtField.text = "\(Singleton.shared.userDetail.last_name ?? "")"
        self.phoneTxtField.text = "\(Singleton.shared.userDetail.phone ?? "")"
        self.stateResidence = Singleton.shared.stateNames
        self.stateResidenceData()
        self.lblStateResidence.didSelect { (selectedString, index, id) in
            print(selectedString, index, id)
            self.stateID = self.stateResidence[index].id ?? 0
        }
        self.ssnTxtField.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(dateAdd(_:)), name: NSNotification.Name(N_Date_Picker), object: nil)
    }
    
    @objc func dateAdd(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_Date_Picker), object: nil)
        self.lblDateofBirth.text = "\(K_SELECTED_DATE)"
        NotificationCenter.default.addObserver(self, selector: #selector(dateAdd(_:)), name: NSNotification.Name(N_Date_Picker), object: nil)
    }
    
    
    
    func stateResidenceData(){
        for i in 0...self.stateResidence.count-1{
            self.lblStateResidence.optionArray.append(self.stateResidence[i].state ?? "")
        }
        
    }
    
    @IBAction func dateBtnPressed(_ sender: Any) {
        let dPVC = storyboard?.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        dPVC.picker_type = 3
        let date = Calendar.current.date(byAdding: .year, value: -16, to: Date())
        dPVC.maxDate = date
        
        self.present(dPVC, animated: true, completion: nil)
    }
    
    @IBAction func sendOtpBtnPressed(_ sender: Any) {
        if phoneTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter phone number", color: errorRed)
        } else{
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEND_OTP, method: .get, parameter: nil, objectClass: GetSecurityOtpResponse.self, requestCode: U_SEND_OTP, userToken: nil) { (responseData) in

            }
        }
    }
    
    @IBAction func popupOkayAction(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
    
    @IBAction func showDetailsAction(_ sender: UIButton) {
        if(sender.tag == 0){
        self.popupLabel.text = "Sentence Formatting: As part of our safety screening process we require all drivers to undergo a basic background screening. This helps your customers have peace-of-mind when trusting their valuables to you. It's quick and simple, and we just ask for some basic information from you.\n\nAfter you've submitted the information, we'll add a 'background certified' icon to your driver profile page, giving your customers the important peace-of-mind the need!"
        } else if(sender.tag == 1){
            self.popupLabel.text = "As an additional safety feature, we'll send a verification code to your phone number. Please enter your number below, and then input the special verification code in the box below."
        }  else if(sender.tag == 2){
            self.popupLabel.text = "You should receive the code via SMS in few moments."
        }  else if(sender.tag == 3){
            self.popupLabel.text = "As part of our safety check, we work with Clearstar who perform safe and secure background."
        }
        self.popupView.isHidden = false
    }
    
    
    
    @IBAction func submitBtnPressed(_ sender: Any) {
        if firstNameTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter first name", color: errorRed)
        }
        else if lastNameTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter last name", color: errorRed)
        }
        else if lblStateResidence.text!.isEmpty{
            Singleton.shared.showToast(text: "Please select state", color: errorRed)
        }
        else if lblDateofBirth.text!.isEmpty || lblDateofBirth.text! == "Date of Birth"{
            Singleton.shared.showToast(text: "Please select date", color: errorRed)
        }
        else if phoneTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter phone number", color: errorRed)
        }
//        else if otpTxtfield.text!.isEmpty{
//            Singleton.shared.showToast(text: "Please enter OTP")
//        }
        else if ssnTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter social security number", color: errorRed)
        }
        else{
            let dob = self.convertDateToTimestamp(lblDateofBirth.text ?? "", to: "MM-dd-yyyy")
            let param:[String:Any] = [
                "dob": self.convertTimestampToDate(dob, to: "MM/dd/YYYY"),
                "first_name":firstNameTxtField.text ?? "",
                "last_name":lastNameTxtField.text ?? "",
                "phone":phoneTxtField.text ?? "",
                "otp":otpTxtfield.text ?? "",
                "ssn_number":ssnTxtField.text ?? "",
                "state":stateID,
                "verification_id":"1",
                "country_code": "+1"
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DRIVER_SECURITY_CHECK, method: .post, parameter: param, objectClass: Response.self, requestCode: U_DRIVER_SECURITY_CHECK, userToken: nil) { (responseData) in
                Singleton.shared.showToast(text: "Successfully submitted driver security check", color: successGreen)
                if(self.isComingDirect){
                    let myVC = self.storyboard!.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                    self.navigationController?.pushViewController(myVC, animated: true)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
}
