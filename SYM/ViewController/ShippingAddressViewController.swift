//
//  ShippingAddressViewController.swift
//  SYM
//
//  Created by qw on 29/10/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class ShippingAddressViewController: UIViewController, AddressAdded {
    func reloadAddressTable() {
        Singleton.shared.addressData = []
        self.getShipmentAddress()
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var orderTable: UITableView!
    @IBOutlet weak var noAddressLabel: UILabel!
    
    
    var addressData = [GetAddressResponse]()
    var isSelectAddress = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavTitle(title: "Shipping Address")
        self.setNavigationBar("back", nil)
        if(self.addressData.count == 0){
            self.getShipmentAddress()
        }
    }
    
    func getShipmentAddress(){
        ActivityIndicator.show(view: self.view)
        Singleton.shared.addressData = []
        APICallingViewController.shared.getShippingAddress { (address) in
            self.addressData = address
            self.orderTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func addAddressAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        myVC.addressDelegate = self
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}

extension ShippingAddressViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addressData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableCell") as! AddressTableCell
        let val = self.addressData[indexPath.row]
        cell.userAddress.text = val.address
        cell.deleteButton = {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_REMOVE_SHIPPING_ADDRESS , method: .post, parameter: ["id": val.id], objectClass: SuccessResponse.self, requestCode: U_REMOVE_SHIPPING_ADDRESS, userToken: nil) { (response) in
                Singleton.shared.showToast(text: response.message ?? "", color: successGreen)
                ActivityIndicator.hide()
                
                self.getShipmentAddress()
            }
        }
        
        cell.editButton = {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
            myVC.addressDelegate = self
            myVC.addressDetail = val
            myVC.isEditAddress = true
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        
        return cell
    }
    
}

class AddressTableCell: UITableViewCell {
    //MARK: IBOUtlets
    @IBOutlet weak var userAddress: DesignableUILabel!
    @IBOutlet weak var editView: UIStackView!
    @IBOutlet weak var tickImage: UIImageView!
    @IBOutlet weak var street: DesignableUILabel!
    @IBOutlet weak var addressType: DesignableUILabel!
    
    var deleteButton:(()-> Void)? = nil
    var editButton:(()-> Void)? = nil
    var tickButton:(()-> Void)? = nil
    
    //MARK: IBActions
    @IBAction func deleteAction(_ sender: Any) {
        if let deleteButton = self.deleteButton {
            deleteButton()
        }
    }
    
    @IBAction func editAction(_ sender: Any) {
        if let editButton = self.editButton {
            editButton()
        }
    }
    
    @IBAction func tickAction(_ sender: Any) {
        if let tickButton = self.tickButton {
            tickButton()
        }
    }
}
