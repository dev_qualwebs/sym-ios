//
//  UserRouteVC.swift
//  SYM
//
//  Created by AM on 22/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import Alamofire

class UserRouteVC: UIViewController,HandleUserRoute{
    func handleUserAdress(address: HandleAddress) {
        if(placeName == "PickUp"){
            self.pickUpLatitude = address.latitude ?? ""
            self.pickUpLongitude = address.longitude ?? ""
            self.pickUpCity = address.city ?? ""
            let building = ((address.building ?? "" != "") ? ", ": "") + (address.building ?? "")
            let cityComma = ((address.city ?? "" != "") ? ", ":"") + (address.city ?? "")

            self.pickUpCityTxtField.text = address.street
            self.pickUpZipCode = address.zip ?? ""
            self.pickupZipCodeTxtField.text = address.zip ?? ""
            self.pickUpState = address.state ?? ""
            
            self.pickUpHouseNoField.text = address.house ?? ""
            self.FromHouse = address.house ?? ""
            self.FromBuilding = address.building ?? ""
            self.pickUpBuildingNameField.text = address.building ?? ""
            self.FromStreet = address.street ?? ""
            self.pickUpStrretLineField.text = address.street ?? ""
            self.fromName = (address.house ?? "") + building + cityComma
            
            handleMapView()
        } else {
            self.dropOffLatitude = address.latitude ?? ""
            self.dropOffLongitude = address.longitude ?? ""
            self.dropOffCity = address.city ?? ""
            let building = ((address.building ?? "" != "") ? ", ": "") + (address.building ?? "")
            let cityComma = ((address.city ?? "" != "") ? ", ":"") + (address.city ?? "")
            
            self.drpOffCityTxtField.text = address.street
            self.dropOffZipCode = address.zip ?? ""
            self.dropOffZipCodeTxtField.text = address.zip ?? ""
            self.dropOffState = address.state ?? ""
            
            self.dropHouseNoField.text = address.house ?? ""
            self.toHouse = address.house ?? ""
            self.dropBuildingNameField.text = address.building ?? ""
            self.toBuilding = address.building ?? ""
            self.toStreet = address.street ?? ""
            self.dropStreetLineField.text = address.street ?? ""
            self.toName = address.street ?? ""
            
            handleMapView()
        }
    }
    
    
    
    
    //Mark: IBOUTLETS
    @IBOutlet weak var pickUpCityTxtField: UITextField!
    @IBOutlet weak var drpOffCityTxtField: UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var saveButtonView: View!
    @IBOutlet weak var pickupZipCodeTxtField: UITextField!
    @IBOutlet weak var dropOffZipCodeTxtField: UITextField!
    
    @IBOutlet weak var pickUpHouseNoField: UITextField!
    @IBOutlet weak var pickUpBuildingNameField: UITextField!
    @IBOutlet weak var pickUpStrretLineField: UITextField!
    
    @IBOutlet weak var dropHouseNoField: UITextField!
    @IBOutlet weak var dropBuildingNameField: UITextField!
    @IBOutlet weak var dropStreetLineField: UITextField!
    @IBOutlet weak var distanceLabel: UILabel!
    
    //Mark: IBOUTLETS
    var fromName = String()
    var toName = String()
    var pickUpLatitude = String()
    var pickUpLongitude = String()
    var dropOffLatitude = String()
    var dropOffLongitude = String()
    var pickUpState = String()
    var pickUpCity = String()
    var pickUpZipCode = String()
    var dropOffState = String()
    var dropOffCity = String()
    var dropOffZipCode = String()
    var placeName = String()
    var lookingFor = String()
    var category = String()
    var subCategory = [Int]()
    var discription = String()
    var isEditJob = false
    
    var FromHouse = String()
    var FromBuilding = String()
    var FromStreet = String()
    var toHouse = String()
    var toBuilding = String()
    var toStreet = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddressBottomViewController.addressDelegate = self
        setNavTitle(title: "1")
        setNavigationBar("back", "none")
        if UserDefaults.standard.value(forKey: UD_TOKEN) as? String != nil {
            self.saveButtonView.isHidden = K_DRAFT_OR_EDIT == 1 ? true:false
        }
        self.initializeView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    
    func initializeView(){
        
        if(Singleton.shared.postYourShipment.from_house_no != "" && Singleton.shared.postYourShipment.from_city != ""){
            self.pickUpCityTxtField.text = Singleton.shared.postYourShipment.from_house_no + ", " + Singleton.shared.postYourShipment.from_building + (Singleton.shared.postYourShipment.from_building != "" ? ", ": "") + Singleton.shared.postYourShipment.from_city}
        
        if(Singleton.shared.postYourShipment.to_house_no != "" && Singleton.shared.postYourShipment.to_city != ""){
            self.drpOffCityTxtField.text = Singleton.shared.postYourShipment.to_house_no + ", " + Singleton.shared.postYourShipment.to_building + (Singleton.shared.postYourShipment.to_building != "" ? ", ":"") + Singleton.shared.postYourShipment.to_city
        }
        
        self.pickupZipCodeTxtField.text = Singleton.shared.postYourShipment.from_zipcode
        self.dropOffZipCodeTxtField.text = Singleton.shared.postYourShipment.to_zipcode
        self.pickUpLatitude = Singleton.shared.postYourShipment.from_latitude
        self.pickUpLongitude = Singleton.shared.postYourShipment.from_longitude
        
        self.pickUpHouseNoField.text = Singleton.shared.postYourShipment.from_house_no
        self.pickUpBuildingNameField.text = Singleton.shared.postYourShipment.from_building
        self.pickUpStrretLineField.text = Singleton.shared.postYourShipment.from_street
        self.dropHouseNoField.text = Singleton.shared.postYourShipment.to_house_no
        self.dropBuildingNameField.text = Singleton.shared.postYourShipment.to_building
        self.dropStreetLineField.text = Singleton.shared.postYourShipment.to_street
        
        self.FromHouse = Singleton.shared.postYourShipment.from_house_no
        self.FromBuilding = Singleton.shared.postYourShipment.from_building
        self.FromStreet = Singleton.shared.postYourShipment.from_street
        self.toHouse = Singleton.shared.postYourShipment.to_house_no
        self.toBuilding = Singleton.shared.postYourShipment.to_building
        self.toStreet = Singleton.shared.postYourShipment.to_street
        
        self.fromName = Singleton.shared.postYourShipment.from_name
        self.toName = Singleton.shared.postYourShipment.to_name
        
        self.dropOffLatitude = Singleton.shared.postYourShipment.to_latitude
        self.dropOffLongitude = Singleton.shared.postYourShipment.to_longitude
        self.pickUpCity = Singleton.shared.postYourShipment.from_city
        self.pickUpZipCode = Singleton.shared.postYourShipment.from_zipcode
        self.pickUpState = Singleton.shared.postYourShipment.from_state
        self.dropOffState = Singleton.shared.postYourShipment.to_state
        self.dropOffCity = Singleton.shared.postYourShipment.to_city
        self.dropOffZipCode = Singleton.shared.postYourShipment.to_zipcode
        handleMapView()
    }
    
    func handleMapView(){
        if !(pickUpCityTxtField.text!.isEmpty){
            if !(drpOffCityTxtField.text!.isEmpty){
                let sourceCoordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(pickUpLatitude) as? CLLocationDegrees ?? 0.0, longitude: CLLocationDegrees(pickUpLongitude) as? CLLocationDegrees ?? 0.0)
                let destinationCoordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(dropOffLatitude) as? CLLocationDegrees ?? 0.0, longitude: CLLocationDegrees(dropOffLongitude) as? CLLocationDegrees ?? 0.0)
                
                let marker = GMSMarker(position: destinationCoordinate)
                let markerImage = UIImage(named: "Red-Pin")
                marker.icon = markerImage
                marker.map = mapView
                let startLOC =  CLLocation(latitude:CLLocationDegrees(pickUpLatitude) as? CLLocationDegrees ?? 0.0, longitude:CLLocationDegrees(pickUpLongitude) as? CLLocationDegrees ?? 0.0)
                let endLOC =  CLLocation(latitude:CLLocationDegrees(dropOffLatitude) as? CLLocationDegrees ?? 0.0, longitude: CLLocationDegrees(dropOffLongitude) as? CLLocationDegrees ?? 0.0)
                getPolylineRoute(from: startLOC, to: endLOC)
            }else{
                let sourceCoordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(pickUpLatitude) as? CLLocationDegrees ?? 0.0, longitude: CLLocationDegrees(pickUpLongitude) as? CLLocationDegrees ?? 0.0)
                let marker = GMSMarker(position: sourceCoordinates)
                let camera = GMSCameraPosition.camera(withLatitude: Double(pickUpLatitude) as? CLLocationDegrees ?? 0.0, longitude: Double(pickUpLongitude) as? CLLocationDegrees ?? 0.0, zoom: 16)
                let markerImage = UIImage(named: "Green-Pin")
                marker.icon = markerImage
                marker.map = mapView
                mapView?.camera = camera
                mapView?.animate(to: camera)
            }
        }
        
        if(self.isEditJob){
            let sourceCoordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(pickUpLatitude) as? CLLocationDegrees ?? 0.0, longitude: CLLocationDegrees(pickUpLongitude) as? CLLocationDegrees ?? 0.0)
            let destinationCoordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(dropOffLatitude) as? CLLocationDegrees ?? 0.0, longitude: CLLocationDegrees(dropOffLongitude) as? CLLocationDegrees ?? 0.0)
            getPolylineRoute(from: CLLocation(latitude: sourceCoordinates.latitude, longitude: sourceCoordinates.longitude), to: CLLocation(latitude: destinationCoordinate.latitude, longitude: destinationCoordinate.longitude))
            let marker = GMSMarker(position: sourceCoordinates)
            let camera = GMSCameraPosition.camera(withLatitude: Double(pickUpLatitude) as? CLLocationDegrees ?? 0.0, longitude: Double(pickUpLongitude) as? CLLocationDegrees ?? 0.0, zoom: 16)
            let markerImage = UIImage(named: "Green-Pin")
            marker.icon = markerImage
            marker.map = mapView
            mapView?.camera = camera
            mapView?.animate(to: camera)
            
            let dmarker = GMSMarker(position: destinationCoordinate)
            let dmarkerImage = UIImage(named: "Red-Pin")
            dmarker.icon = dmarkerImage
            dmarker.map = mapView
        }
    }
    
    func getPolylineRoute(from source:CLLocation,to destination:CLLocation){
        let origin  = "\(pickUpLatitude),\(pickUpLongitude)"
        let dest = "\(dropOffLatitude),\(dropOffLongitude)"
        let parameter = "json?origin=\(origin)&destination=\(dest)"
        let strUrl = "https://maps.googleapis.com/maps/api/directions/" + parameter + "&key=" + K_GOOGLE_API_KEY
        
        self.mapView.clear()
        let sourceCoordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(pickUpLatitude) as? CLLocationDegrees ?? 0.0, longitude: CLLocationDegrees(pickUpLongitude) as? CLLocationDegrees ?? 0.0)
        let marker = GMSMarker(position: sourceCoordinates)
        let camera = GMSCameraPosition.camera(withLatitude: Double(pickUpLatitude) as? CLLocationDegrees ?? 0.0, longitude: Double(pickUpLongitude) as? CLLocationDegrees ?? 0.0, zoom: 16)
        let markerImage = UIImage(named: "Green-Pin")
        marker.icon = markerImage
        marker.map = mapView
        let destinationCoordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(dropOffLatitude) as? CLLocationDegrees ?? 0.0, longitude: CLLocationDegrees(dropOffLongitude) as? CLLocationDegrees ?? 0.0)
        let dmarker = GMSMarker(position: destinationCoordinate)
        let dmarkerImage = UIImage(named: "Red-Pin")
        dmarker.icon = dmarkerImage
        dmarker.map = mapView
        mapView?.camera = camera
        mapView?.animate(to: camera)
        Singleton.shared.postYourShipment.distance = "\(Int((source.distance(from: destination)/1609)))"
        Singleton.shared.draftData.distance = Singleton.shared.postYourShipment.distance
        self.distanceLabel.text = "  Distance :- \(Int((source.distance(from: destination)/1609))) miles  "
        AF.request(URLRequest(url: URL(string: strUrl)!)).responseJSON(completionHandler: { (response) in
            print("luffy",response.value)
            let json = response.value as? [String:Any]
            if let routes = json?["routes"] as? [[String:Any]]{
                for route in routes {
                    let routeOverviewPolyline = route["overview_polyline"] as! [String:Any]
                    let points = routeOverviewPolyline["points"]
                    let path = GMSPath.init(fromEncodedPath: points! as! String)
                    let polyline = GMSPolyline.init(path: path)
                    polyline.strokeWidth = 4
                    polyline.strokeColor = .darkGray
                    let bounds = GMSCoordinateBounds(path: path!)
                    self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50))
                    polyline.map = self.mapView
                }
            }
        })
    }
    
    @IBAction func pickUPCityTapped(_ sender: Any) {
        placeName = "PickUp"
        pickUpCityTxtField.resignFirstResponder()
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddressBottomViewController") as! AddressBottomViewController
        myVC.isPickup = true
        //        myVC.isEditJob = self.isEditJob
        //        if(self.isEditJob){
        var address = HandleAddress()
        address.latitude = self.pickUpLatitude
        address.longitude = self.pickUpLongitude
        address.city = self.pickUpCity
        address.zip = self.pickupZipCodeTxtField.text ?? ""
        address.state = self.pickUpState
        address.house = self.pickUpHouseNoField.text ?? ""
        address.building = self.pickUpBuildingNameField.text ?? ""
        address.street = self.pickUpStrretLineField.text ?? ""
        myVC.address = address
        //        }
        self.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func dropOffCityTapped(_ sender: Any) {
        placeName = "DropOff"
        drpOffCityTxtField.resignFirstResponder()
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddressBottomViewController") as! AddressBottomViewController
        myVC.isPickup = false
        
        var address = HandleAddress()
        address.latitude = self.dropOffLatitude
        address.longitude = self.dropOffLongitude
        address.city = self.dropOffCity
        address.zip  =   self.dropOffZipCodeTxtField.text ?? ""
        address.state = self.dropOffState
        address.house = self.dropHouseNoField.text ?? ""
        address.building = self.dropBuildingNameField.text ?? ""
        address.street = self.dropStreetLineField.text ?? ""
        myVC.address = address
        
        self.present(myVC, animated: true, completion: nil)
    }
    
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        self.navigateToNextScreen(saveInfo: false)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil {
            Singleton.shared.postYourShipment.from_name = self.fromName
            Singleton.shared.postYourShipment.from_latitude = pickUpLatitude
            Singleton.shared.postYourShipment.from_longitude = pickUpLongitude
            Singleton.shared.postYourShipment.from_city = pickUpCity
            Singleton.shared.postYourShipment.from_state = pickUpState
            Singleton.shared.postYourShipment.from_zipcode = pickUpZipCode
            Singleton.shared.postYourShipment.to_name = self.toName
            Singleton.shared.postYourShipment.to_latitude = dropOffLatitude
            Singleton.shared.postYourShipment.to_longitude = dropOffLongitude
            Singleton.shared.postYourShipment.to_city = dropOffCity
            Singleton.shared.postYourShipment.to_state = dropOffState
            Singleton.shared.postYourShipment.to_zipcode = dropOffZipCode
            
            Singleton.shared.postYourShipment.from_house_no = self.pickUpHouseNoField.text ?? ""
            Singleton.shared.postYourShipment.from_building = self.pickUpBuildingNameField.text ?? ""
            Singleton.shared.postYourShipment.from_street = self.pickUpStrretLineField.text ?? ""
            Singleton.shared.postYourShipment.to_house_no = self.dropHouseNoField.text ?? ""
            Singleton.shared.postYourShipment.to_building = self.dropBuildingNameField.text ?? ""
            Singleton.shared.postYourShipment.to_street = self.dropStreetLineField.text ?? ""
            
            Singleton.shared.showToast(text: "Saved as draft.", color: successGreen)
            Singleton.shared.saveShipmentData(data: Singleton.shared.postYourShipment, screen: "1")
        } else {
            var draftId : Int?
            if Singleton.shared.postYourShipment.id == 0 {
                draftId = nil
            } else {
                draftId = Singleton.shared.postYourShipment.id
            }
            
            ActivityIndicator.show(view: self.view)
            let param : [String:Any] = [
                "draft_shipment_id": draftId,
                "from_state": self.pickUpState,
                "to_state":self.dropOffState,
                "from_name": self.fromName,
                "to_name": self.toName,
                "from_longitude": self.pickUpLongitude,
                "from_latitude": self.pickUpLatitude,
                "to_longitude": self.dropOffLongitude,
                "to_latitude": self.dropOffLatitude,
                "from_city": self.pickUpCity,
                "to_city": self.dropOffCity,
                "from_zipcode": self.pickUpZipCode,
                "to_zipcode": self.dropOffZipCode,
                "from_house_no" : self.pickUpHouseNoField.text,
                "from_building" : self.pickUpBuildingNameField.text,
                "from_street" : self.pickUpStrretLineField.text,
                "to_house_no" : self.dropHouseNoField.text,
                "to_building" : self.dropBuildingNameField.text,
                "to_street" : self.dropStreetLineField.text,
                "description" : UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.discription : Singleton.shared.postYourShipment.discription,
                "category_id": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.category_id : Singleton.shared.postYourShipment.category_id,
                "lookingFor": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.lookingFor : Singleton.shared.postYourShipment.lookingFor,
                "sub_category_id": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.sub_category_id : Singleton.shared.postYourShipment.sub_category_id,
                "image" : UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.image : Singleton.shared.postYourShipment.image
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SAVE_DRAFT_SHIPMENT, method: .post, parameter: param, objectClass: SaveDraftResponse.self, requestCode: U_SAVE_DRAFT_SHIPMENT, userToken: nil) { (response) in
                Singleton.shared.showToast(text: "Saved as draft.", color: successGreen)
                ActivityIndicator.hide()
            }
        }
    }
    
    func navigateToNextScreen(saveInfo:Bool){
        if pickUpCityTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter Pick Up address", color: errorRed)
        }
        else if drpOffCityTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter Dropp Off address", color: errorRed)
        }else if pickupZipCodeTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter Pick up zip code", color: errorRed)
        }else if dropOffZipCodeTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter Dropp off zip code", color: errorRed)
        }else if(drpOffCityTxtField.text == pickUpCityTxtField.text){
            Singleton.shared.showToast(text: "Address cannot be same.", color: errorRed)
        }else{
            let uRVC = storyboard!.instantiateViewController(withIdentifier: "UserPhotosUploadVC") as! UserPhotosUploadVC
            if UserDefaults.standard.value(forKey: UD_TOKEN) as? String != nil {
                Singleton.shared.postYourShipment.from_name = self.fromName
                Singleton.shared.postYourShipment.from_latitude = pickUpLatitude
                Singleton.shared.postYourShipment.from_longitude = pickUpLongitude
                Singleton.shared.postYourShipment.from_city = pickUpCity
                Singleton.shared.postYourShipment.from_state = pickUpState
                Singleton.shared.postYourShipment.from_zipcode = pickUpZipCode
                Singleton.shared.postYourShipment.to_name = self.toName
                Singleton.shared.postYourShipment.to_latitude = dropOffLatitude
                Singleton.shared.postYourShipment.to_longitude = dropOffLongitude
                Singleton.shared.postYourShipment.to_city = dropOffCity
                Singleton.shared.postYourShipment.to_state = dropOffState
                Singleton.shared.postYourShipment.to_zipcode = dropOffZipCode
                
                Singleton.shared.postYourShipment.from_house_no = self.pickUpHouseNoField.text ?? ""
                Singleton.shared.postYourShipment.from_building = self.pickUpBuildingNameField.text ?? ""
                Singleton.shared.postYourShipment.from_street = self.pickUpStrretLineField.text ?? ""
                Singleton.shared.postYourShipment.to_house_no = self.dropHouseNoField.text ?? ""
                Singleton.shared.postYourShipment.to_building = self.dropBuildingNameField.text ?? ""
                Singleton.shared.postYourShipment.to_street = self.dropStreetLineField.text ?? ""
                
            } else {
                Singleton.shared.draftData.from_name = self.fromName
                Singleton.shared.draftData.from_latitude = pickUpLatitude
                Singleton.shared.draftData.from_longitude = pickUpLongitude
                Singleton.shared.draftData.from_city = pickUpCity
                Singleton.shared.draftData.from_state = pickUpState
                Singleton.shared.draftData.from_zipcode = pickUpZipCode
                Singleton.shared.draftData.to_name = self.toName
                Singleton.shared.draftData.to_latitude = dropOffLatitude
                Singleton.shared.draftData.to_longitude = dropOffLongitude
                Singleton.shared.draftData.to_city = dropOffCity
                Singleton.shared.draftData.to_state = dropOffState
                Singleton.shared.draftData.to_zipcode = dropOffZipCode
                
                Singleton.shared.draftData.from_house_no = self.pickUpHouseNoField.text ?? ""
                Singleton.shared.draftData.from_building = self.pickUpBuildingNameField.text ?? ""
                Singleton.shared.draftData.from_street = self.pickUpStrretLineField.text ?? ""
                Singleton.shared.draftData.to_house_no = self.dropHouseNoField.text ?? ""
                Singleton.shared.draftData.to_building = self.dropBuildingNameField.text ?? ""
                Singleton.shared.draftData.to_street = self.dropStreetLineField.text ?? ""
                
                Singleton.shared.postYourShipment.from_name = self.fromName
                Singleton.shared.postYourShipment.from_latitude = pickUpLatitude
                Singleton.shared.postYourShipment.from_longitude = pickUpLongitude
                Singleton.shared.postYourShipment.from_city = pickUpCity
                Singleton.shared.postYourShipment.from_state = pickUpState
                Singleton.shared.postYourShipment.from_zipcode = pickUpZipCode
                Singleton.shared.postYourShipment.to_name = self.toName
                Singleton.shared.postYourShipment.to_latitude = dropOffLatitude
                Singleton.shared.postYourShipment.to_longitude = dropOffLongitude
                Singleton.shared.postYourShipment.to_city = dropOffCity
                Singleton.shared.postYourShipment.to_state = dropOffState
                Singleton.shared.postYourShipment.to_zipcode = dropOffZipCode
                
                Singleton.shared.postYourShipment.from_house_no = self.pickUpHouseNoField.text ?? ""
                Singleton.shared.postYourShipment.from_building = self.pickUpBuildingNameField.text ?? ""
                Singleton.shared.postYourShipment.from_street = self.pickUpStrretLineField.text ?? ""
                Singleton.shared.postYourShipment.to_house_no = self.dropHouseNoField.text ?? ""
                Singleton.shared.postYourShipment.to_building = self.dropBuildingNameField.text ?? ""
                Singleton.shared.postYourShipment.to_street = self.dropStreetLineField.text ?? ""
            }
            let coordinateFrom = CLLocation(latitude: CLLocationDegrees(pickUpLatitude) as! CLLocationDegrees, longitude: CLLocationDegrees(pickUpLongitude) as! CLLocationDegrees)
            let coordinateTo = CLLocation(latitude: CLLocationDegrees(dropOffLatitude) as! CLLocationDegrees, longitude: CLLocationDegrees(dropOffLongitude) as! CLLocationDegrees)
            let distance = coordinateFrom.distance(from: coordinateTo)
            uRVC.isEditJob = self.isEditJob
            
            NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_CURRENT_INDEX), object: nil, userInfo: ["index":2])
        }
    }
}
