//
//  SingleShipmentVC.swift
//  SYM
//
//  Created by AM on 22/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import ImageSlideshow

class SingleShipmentVC: UIViewController,UITextViewDelegate {
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView == self.refundTextView){
            if(textView.text == ""){
                self.refundTextView.text = "Type your reason here..."
                self.refundTextView.textColor = .lightGray
            }
        }
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView == self.refundTextView){
            if(textView.text == "Type your reason here..."){
                self.refundTextView.text = ""
                self.refundTextView.textColor = .black
            }
        }
    }
    
    //Mark: IBOUTLETS
    @IBOutlet weak var postedDate: DesignableUILabel!
    @IBOutlet weak var sourceAddress: DesignableUILabel!
    @IBOutlet weak var destinationAddress: DesignableUILabel!
    @IBOutlet weak var estimatedPrice: DesignableUILabel!
    @IBOutlet weak var categoryDetail: DesignableUILabel!
    @IBOutlet weak var totalDistance: DesignableUILabel!
    @IBOutlet weak var attachmentCollection: UICollectionView!
    @IBOutlet weak var awardedTo: DesignableUILabel!
    @IBOutlet weak var currentStatus: DesignableUILabel!
    @IBOutlet weak var totalBidsPlaced: DesignableUILabel!
    @IBOutlet weak var customerDescription: DesignableUILabel!
    @IBOutlet weak var headingText: DesignableUILabel!
    @IBOutlet weak var pickupDate: DesignableUILabel!
    @IBOutlet weak var dropoffDate: DesignableUILabel!
    @IBOutlet weak var shipmentWeight: DesignableUILabel!
    @IBOutlet weak var attachmentCount: DesignableUILabel!
    @IBOutlet weak var subcategoryName: DesignableUILabel!
    @IBOutlet weak var noAttachment: UILabel!
    @IBOutlet weak var rateDriverButton: UIButton!
    @IBOutlet weak var trackShipmentButton: UIButton!
    @IBOutlet weak var fullScreenImage: ImageView!
    @IBOutlet weak var fullScreenView: UIView!
    @IBOutlet weak var estimatedPriceLabel: DesignableUILabel!
    @IBOutlet weak var awardedView: UIView!
    @IBOutlet weak var imageCollectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewDriverBidsView: UIView!
    @IBOutlet weak var requestRefundButton: UIButton!
    @IBOutlet weak var refundTextView: UITextView!
    @IBOutlet weak var refundModalView: UIView!
    
    
    var id = Int()
    var shipmentData = GetUserShipmentData()
    var shipmentStatus = Int()
    var isCancelled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavTitle(title: "Shipment Details")
        setNavigationBar("back", nil)
        
        self.refundTextView.delegate = self
        self.refundTextView.textColor = .lightGray
        self.refundTextView.text = "Type your reason here..."
        
        self.viewDriverBidsView.isHidden = self.isCancelled
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.subcategoryName.text = ""
        self.getSingleShipments()
    }
    
    func getSingleShipments(){
        ActivityIndicator.show(view: self.view)
        let param:[String:Any] = ["shipment_id":id]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_CUSTOMER_SINGLE_SHIPMENT, method: .post, parameter: param, objectClass: CustomerSingleShipment.self, requestCode: U_CUSTOMER_SINGLE_SHIPMENT, userToken: nil) { (response) in
            self.shipmentData = response.response
            if(self.shipmentData.lookingFor == 1){
                self.shipmentStatus = self.shipmentData.driver_status ?? 0
            }else if(self.shipmentData.lookingFor == 2){
                self.shipmentStatus = self.shipmentData.loader_status ?? 0
            }else if(self.shipmentData.lookingFor == 3){
                self.shipmentStatus = self.shipmentData.driver_status ?? 0
            }else{
                self.shipmentStatus = self.shipmentData.driver_status ?? 0
            }
            self.initializeView()
        }
    }
    
    func initializeView(){
        self.postedDate.text = "Posted on: " +  "\(self.convertTimestampToDate(self.shipmentData.created_date ?? 0, to: "EEE, dd-MMM-yy, h:mm a"))"
        self.sourceAddress.text = self.shipmentData.from_name
        self.destinationAddress.text = self.shipmentData.to_name
        
        self.awardedView.isHidden = true
        for i in 0..<(shipmentData.driver_bids?.count ?? 0){
            if shipmentData.driver_bids?[i].driver_accepted == 1 || shipmentData.driver_bids?[i].customer_accepted == 1 {
                self.awardedView.isHidden = false
            }
        }
        
        if self.shipmentData.price == nil {
            self.estimatedPrice.isHidden = true
            self.estimatedPriceLabel.isHidden = true
        } else {
            self.estimatedPrice.text = "$\(self.shipmentData.price ?? "0")"
            self.estimatedPrice.isHidden = false
            self.estimatedPriceLabel.isHidden = false
        }
        let cat = Singleton.shared.categoryData.filter{
            $0.id == self.shipmentData.category_id
        }
        if(cat.count > 0){
            self.categoryDetail.text = cat[0].category
        }
        self.totalBidsPlaced.text = "\(self.shipmentData.driver_bids?.count ?? 0)"
        self.currentStatus.text = ""
        self.totalDistance.text = (self.shipmentData.distance ?? "0") + " miles"
        self.attachmentCollection.delegate = self
        self.attachmentCollection.dataSource = self
        self.attachmentCollection.reloadData()
        if(shipmentStatus == 1){
            self.currentStatus.text = "Waiting for drivers to place bid"
            self.awardedTo.text = "Not awarded yet"
        }else if(shipmentStatus == 2){
            self.awardedTo.text = "Not awarded yet"
            self.currentStatus.text = "Bid placed"
        }else if(shipmentStatus == 3){
            self.awardedTo.text = (self.shipmentData.driver?.first_name ?? "") + " " + (self.shipmentData.driver?.last_name ?? "")
            self.currentStatus.text = "Bid awarded by you"
        }else if(shipmentStatus == 4){
            self.requestRefundButton.isHidden = false
            self.currentStatus.text = "Driver accepted"
            self.awardedTo.text = (self.shipmentData.driver?.first_name ?? "") + " " + (self.shipmentData.driver?.last_name ?? "")
        }else if(shipmentStatus == 5){
            self.requestRefundButton.isHidden = false
            self.currentStatus.text = "Shipment is in progress"
            self.awardedTo.text = (self.shipmentData.driver?.first_name ?? "") + " " + (self.shipmentData.driver?.last_name ?? "")
        }else if(shipmentStatus == 6){
            self.requestRefundButton.isHidden = false
            self.currentStatus.text = "Shipment completed"
            if(self.shipmentData.is_driver_rated != 1){
                self.rateDriverButton.isHidden = false
                self.awardedTo.text = (self.shipmentData.driver?.first_name ?? "") + " " + (self.shipmentData.driver?.last_name ?? "")
            }
        }
        
        if self.shipmentData.lookingFor == 1 {
            self.headingText.text = "Looking for driver for the shipment below: "
        } else if self.shipmentData.lookingFor == 2 {
            self.headingText.text = "Looking for loader for the shipment below: "
        } else if self.shipmentData.lookingFor == 3 {
            self.headingText.text = "Looking for driver & loader for the shipment below: "
        }
        
        self.attachmentCount.text = "\(self.shipmentData.image_list?.count ?? 0)"
        self.shipmentWeight.text = "\(self.shipmentData.weight ?? 0)" + " lbs."
        if(self.shipmentData.image_list?.count == 0){
            self.noAttachment.isHidden = false
        }else {
            self.noAttachment.isHidden = true
        }
        
        for i in 0..<self.shipmentData.subcategory_name.count {
            if(i == (self.shipmentData.subcategory_name.count - 1)){
            self.subcategoryName.text = (self.subcategoryName.text ?? "")  + (self.shipmentData.subcategory_name[i].category ?? "")
            } else {
            self.subcategoryName.text = (self.subcategoryName.text ?? "")  + (self.shipmentData.subcategory_name[i].category ?? "") + ", "
            }
        }
        self.pickupDate.text = "\(self.convertTimestampToDate(self.shipmentData.pickup_date ?? 0, to: "EEE, dd-MMM-yy, h:mm a"))"
        self.dropoffDate.text = "\(self.convertTimestampToDate(self.shipmentData.dropoff_date ?? 0, to: "EEE, dd-MMM-yy, h:mm a"))"
        self.customerDescription.text = self.shipmentData.description
        
        if(self.shipmentStatus == 7){
            self.requestRefundButton.isHidden = false
            self.headingText.text = "Shipment disputed!"
            self.headingText.textColor = .red
        }
        
        if(self.shipmentStatus == 8){
            self.requestRefundButton.isHidden = true
            self.headingText.text = "Shipment cancelled! Refund requested."
            self.headingText.textColor = .red
            if(self.shipmentData.refund_shipment_request?.status ?? 0 == 1){
                self.headingText.text = "Shipment cancelled! Refund accepted by admin."
                self.headingText.textColor = primaryColor
            } else if(self.shipmentData.refund_shipment_request?.status ?? 0 == 2){
                self.headingText.text = "Shipment cancelled! Refund rejected by admin."
                self.headingText.textColor = primaryColor
            }
        }
    }
    
    //MARK:IBACtions
    
    @IBAction func refundRequestAction(_ sender: Any) {
        self.refundModalView.isHidden = false
    }
    
    
    @IBAction func refundAction(_ sender: UIButton) {
        if(sender.tag == 0){
            if(self.refundTextView.text == "" || self.refundTextView.text == "Type your reason here..."){
                Singleton.shared.showToast(text: "Please enter your reason for refund!", color: errorRed)
                return
            }
            
            self.refundModalView.isHidden = true
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_REFUND_REQUEST, method: .post, parameter: ["shipment_id": self.shipmentData.id ?? 0, "reason" : self.refundTextView.text ?? ""], objectClass: SuccessResponse.self, requestCode: U_REFUND_REQUEST, userToken: nil) { (response) in
                ActivityIndicator.hide()
                self.refundTextView.text = "Type your reason here..."
                self.refundTextView.textColor = .lightGray
                self.shipmentStatus = 8
                self.shipmentData.loader_status = 8
                self.shipmentData.driver_status = 8
                self.initializeView()
                Singleton.shared.showToast(text: "Successfully submitted refund request", color: successGreen)
            }
        } else {
            self.refundModalView.isHidden = true
            self.refundTextView.text = "Type your reason here..."
            self.refundTextView.textColor = .lightGray
        }
    }
    
    
    @IBAction func trackShipmentAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        myVC.shipmentStatus = self.shipmentStatus
        myVC.pickup_from = self.shipmentData.from_name ?? ""
        myVC.pickup_to = self.shipmentData.to_name ?? ""
        myVC.pickUpLatitude = self.shipmentData.from_latitude ?? ""
        myVC.pickUpLongitude = self.shipmentData.from_longitude ?? ""
        myVC.dropOffLatitude = self.shipmentData.to_latitude ?? ""
        myVC.dropOffLongitude = self.shipmentData.to_longitude ?? ""
        if(self.shipmentData.lookingFor == 1){
            myVC.shipmentStatus = self.shipmentData.driver_status ?? 0
        }else if(self.shipmentData.lookingFor == 2){
            myVC.shipmentStatus = self.shipmentData.loader_status ?? 0
        }else if(self.shipmentData.lookingFor == 3){
            myVC.shipmentStatus = self.shipmentData.driver_status ?? 0
        }else{
            myVC.shipmentStatus = self.shipmentData.driver_status ?? 0
        }
        let val = self.shipmentData
        
        for i in 0..<(self.shipmentData.driver_bids?.count ?? 0) {
            if (self.shipmentData.driver_bids?[i].driver_accepted ?? 0) == 1 {
                myVC.driverData = self.shipmentData.driver_bids?[i].driver ?? GetDriverProfileDetails()
            }
        }
        myVC.shipmentDetail = GetAllCommonShipment(id: val.id, user_id: val.user_id, category_id: val.category_id, description: val.description, from_name: val.from_name, from_latitude: val.from_latitude, from_longitude: val.from_longitude, from_city: val.from_city, from_state: val.from_state, from_zipcode: val.from_zipcode, to_name: val.to_name, to_latitude: val.to_latitude, to_longitude: val.to_longitude, to_state: val.to_state, to_city: val.to_city, to_zipcode: val.to_zipcode, pickup_date: val.pickup_date, dropoff_date: val.dropoff_date, price: val.price, multiple_item: val.multiple_item, set_budget: val.set_budget, distance: val.distance, weight: val.weight, driver_status: val.driver_status, loader_status: val.loader_status, lookingFor: val.lookingFor, completed_driver_id: val.completed_driver_id, completed_loader_id: val.completed_loader_id, driver_completed_at: val.driver_completed_at, loader_completed_at: val.loader_completed_at, driver_id: val.driver_id, image_list: val.image_list)
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func rateDriverAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RatingViewController") as! RatingViewController
        myVC.shipmentId = self.shipmentData.id ?? 0
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func driverBidAction(_ sender: Any) {
        if((self.shipmentData.driver_bids?.count ?? 0) == 0){
            Singleton.shared.showToast(text: "No bid placed", color: errorRed)
        } else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverBidsViewController") as! DriverBidsViewController
            myVC.shipmentData = self.shipmentData
            myVC.shipmentStatus = self.shipmentStatus
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.fullScreenView.isHidden = true
    }
}

extension SingleShipmentVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.imageCollectionHeightConstraint.constant = (self.shipmentData.image_list?.count ?? 0) > 0 ? 100 : 0
        return self.shipmentData.image_list?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCell", for: indexPath) as! AttachmentCell
        cell.attachmentImage.sd_setImage(with: URL(string:U_IMAGE + (self.shipmentData.image_list?[indexPath.row].image ?? "").replacingOccurrences(of: " ", with: "%20")), placeholderImage: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageSlider = ImageSlideshow()
        var input = [SDWebImageSource]()
        for val in 0..<(self.shipmentData.image_list?.count ?? 0) {
            input.append(SDWebImageSource(urlString: U_IMAGE + (self.shipmentData.image_list?[val].image ?? "").replacingOccurrences(of: " ", with: "%20"))!)
        }
        imageSlider.setImageInputs(input)
        let fullScreenController = imageSlider.presentFullScreenController(from: self)
        
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
}


class AttachmentCell: UICollectionViewCell{
    //MARK:IBOutlets
    @IBOutlet weak var attachmentImage: UIImageView!
}

