//
//  UserDashBoardVC.swift
//  SYM
//
//  Created by AM on 22/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController

protocol EditShipmentStatus {
    func changeShipmentData(status:Int)
}

var selectedShipment = Int()

class UserDashBoardVC: UIViewController,EditShipmentStatus,UITextViewDelegate {
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView == self.refundTextView){
            if(textView.text == ""){
                self.refundTextView.text = "Type your reason here..."
                self.refundTextView.textColor = .lightGray
            }
        }
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView == self.refundTextView){
            if(textView.text == "Type your reason here..."){
                self.refundTextView.text = ""
                self.refundTextView.textColor = .black
            }
        }
    }
    
    
    
    func changeShipmentData(status:Int){
        if status <= self.getUserShipment.count {
            self.getUserShipment[selectedShipment].driver_status = status
            self.getUserShipment[selectedShipment].loader_status = status
            self.getData()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView == self.tableView){
            if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height) && !isLoadingList){
                self.isLoadingList = true
                self.loadMoreItemsForList()
            }
        }
    }
    
    func loadMoreItemsForList(){
        currentPage += 1
        self.getData()
    }
    
    //Mark: IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    @IBOutlet weak var tabbar: RAMAnimatedTabBarItem!
    @IBOutlet weak var welcomeUserLabel: DesignableUILabel!
    @IBOutlet weak var totalShipmentLabel: DesignableUILabel!
    @IBOutlet weak var completedShipmentLabel: DesignableUILabel!
    @IBOutlet weak var refundModalView: UIView!
    @IBOutlet weak var refundTextView: UITextView!
    
    //Mark: Properties
    var getUserShipment = [GetUserShipmentData]()
    var refreshControl = UIRefreshControl()
    var driverData = GetDriverProfileDetails()
    var currentPage = 1
    var isLoadingList = false
    var selectedIndex = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refundTextView.delegate = self
        self.refundTextView.textColor = .lightGray
        self.refundTextView.text = "Type your reason here..."
        DriverBidsViewController.editShipmentStatus = self
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: NSNotification.Name(N_RELOAD_DASHBOARD_DATA), object: nil)
        tableView.addSubview(refreshControl)
        tableView.estimatedRowHeight = 250
        tableView.rowHeight = UITableView.automaticDimension
        setNavigationBar("menu",nil)
        getData()
        getCards()
        self.view.isUserInteractionEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.isUserInteractionEnabled = true
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        setNavTitle(title: "Dashboard")
        self.welcomeUserLabel.text = "Hi, \(Singleton.shared.userType.first_name ?? "")"
        self.view.isUserInteractionEnabled = true
    }
    
    @objc func refresh() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_RELOAD_DASHBOARD_DATA), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: NSNotification.Name(N_RELOAD_DASHBOARD_DATA), object: nil)
        self.refreshControl.endRefreshing()
        self.currentPage = 1
        self.getData()
    }
    
    func getData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_CUSTOMER_ALL_SHIPMENTS + "?offset=" + "\(self.currentPage)", method: .get, parameter: nil, objectClass: GetUserShipmentResponse.self, requestCode: U_CUSTOMER_ALL_SHIPMENTS, userToken: nil) { (responseData) in
            if(self.getUserShipment.count == 0 || self.currentPage == 1 ){
                self.getUserShipment = responseData.response.data
                self.isLoadingList = false
            } else if responseData.response.data.count > 0{
                for val in responseData.response.data {
                    self.getUserShipment.append(val)
                }
                self.isLoadingList = false
            }
            
            if(responseData.response.data.count == 0){
                if self.currentPage > 1 {
                    self.currentPage -= 1
                }
            }
            self.totalShipmentLabel.text = "Total Shipments: " + "\(responseData.response.total_shipments ?? 0)"
            self.completedShipmentLabel.text = "Completed Shipments: " + "\(responseData.response.total_completed_shipments ?? 0)"
            self.tableView.reloadData()
            self.view.isUserInteractionEnabled = true
            ActivityIndicator.hide()
        }
    }
    
    func getCards(){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_DRIVER_SAVED_CARDS, method: .post, parameter: nil, objectClass: GetCard.self, requestCode: U_GET_DRIVER_SAVED_CARDS, userToken: nil) { (response) in
            Singleton.shared.savedCardData = response.response
            self.view.isUserInteractionEnabled = true
        }
    }
    
    
    @IBAction func refundAction(_ sender: UIButton) {
        if(sender.tag == 0){
            if(self.refundTextView.text == "" || self.refundTextView.text == "Type your reason here..."){
                Singleton.shared.showToast(text: "Please enter your reason for refund!", color: errorRed)
                return
            }
            
            self.refundModalView.isHidden = true
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_REFUND_REQUEST, method: .post, parameter: ["shipment_id": self.getUserShipment[selectedIndex].id ?? 0, "reason" : self.refundTextView.text ?? ""], objectClass: SuccessResponse.self, requestCode: U_REFUND_REQUEST, userToken: nil) { (response) in
                self.refundTextView.text = "Type your reason here..."
                ActivityIndicator.hide()
                self.refundTextView.textColor = .lightGray
                self.getUserShipment[self.selectedIndex].loader_status = 8
                self.getUserShipment[self.selectedIndex].driver_status = 8
                UIView.performWithoutAnimation {
                    self.tableView.reloadData()
                }
                Singleton.shared.showToast(text: "Successfully submitted refund request", color: successGreen)
            }
        } else {
            self.refundModalView.isHidden = true
            self.refundTextView.text = "Type your reason here..."
            self.refundTextView.textColor = .lightGray
        }
    }
    
}

extension UserDashBoardVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.getUserShipment.count == 0){
            self.noDataLabel.isHidden = false
        }else {
            self.noDataLabel.isHidden = true
        }
        return getUserShipment.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserDashBoardTableCell", for: indexPath) as! UserDashBoardTableCell
//        let data = getUserShipment[indexPath.row]
        var shipmentStatus = Int()
        if(getUserShipment[indexPath.row].lookingFor == 1){
            shipmentStatus = getUserShipment[indexPath.row].driver_status ?? 0
        }else if(getUserShipment[indexPath.row].lookingFor == 2){
            shipmentStatus = getUserShipment[indexPath.row].loader_status ?? 0
        }else if(getUserShipment[indexPath.row].lookingFor == 3){
            shipmentStatus = getUserShipment[indexPath.row].driver_status ?? 0
        }else{
            shipmentStatus = getUserShipment[indexPath.row].driver_status ?? 0
        }
        
        cell.awardedView.isHidden = true
        for i in 0..<(getUserShipment[indexPath.row].driver_bids?.count ?? 0){
            if getUserShipment[indexPath.row].driver_bids?[i].driver_accepted == 1 || getUserShipment[indexPath.row].driver_bids?[i].customer_accepted == 1 {
                cell.awardedView.isHidden = false
            }
        }
        
        cell.shipmentTitleView.isHidden = getUserShipment[indexPath.row].shipment_title == nil ? true : false
        
        if(getUserShipment[indexPath.row].shipment_title != nil){
            cell.shipmentTitle.text = getUserShipment[indexPath.row].shipment_title ?? ""
        }
        
        cell.lblSource.text = "\(getUserShipment[indexPath.row].from_name ?? "")"
        cell.startDate.text = "Date: " + (self.convertTimestampToDate(getUserShipment[indexPath.row].pickup_date ?? 0, to: "dd-MM-yyyy"))
        cell.endDate.text = "Date: " + (self.convertTimestampToDate(getUserShipment[indexPath.row].dropoff_date ?? 0, to: "dd-MM-yyyy"))
        
        cell.postedDate.text = self.convertTimestampToDate(getUserShipment[indexPath.row].created_date ?? 0, to: "EEE, dd-MMM-yy")
        
        cell.distance.text = "\(getUserShipment[indexPath.row].distance ?? "0") miles"
        cell.rehireButtonView.isHidden = true
        cell.refundButton.isHidden = true
        let dropDate = Date(timeIntervalSince1970: TimeInterval((getUserShipment[indexPath.row].dropoff_date ?? 0)))
        if(shipmentStatus == 1){
            cell.currentStatus.text = "Open for Bid"
            cell.editShipmentButton.setTitle("Edit Shipment", for: .normal)
            cell.editShipmentButton.isHidden = false
        }else if(shipmentStatus == 2) {
            cell.currentStatus.text = "Bid Placed"
            cell.editShipmentButton.setTitle("Edit Shipment", for: .normal)
            cell.editShipmentButton.isHidden = false
        }else if(shipmentStatus == 3){
            if((getUserShipment[indexPath.row].driver_bids?.count ?? 0) == 1){
                if(getUserShipment[indexPath.row].driver_bids?[0].driver_accepted == 2){
                    cell.currentStatus.text = "Bid awarded by you"
                    cell.editShipmentButton.setTitle("Edit Shipment", for: .normal)
                    cell.editShipmentButton.isHidden = false
                }else {
                    cell.currentStatus.text = "Bid accepted"
                    cell.editShipmentButton.isHidden = true
                }
            }else {
                cell.currentStatus.text = "Bid awarded by you"
                cell.editShipmentButton.setTitle("Edit Shipment", for: .normal)
            }
        }else if(shipmentStatus == 4){
            cell.refundButton.isHidden = false
            cell.currentStatus.text = "Driver accepted"
            cell.editShipmentButton.isHidden = true
        }else if(shipmentStatus == 5){
            cell.refundButton.isHidden = false
            cell.currentStatus.text = "Shipment is in progress"
            cell.editShipmentButton.isHidden = true
        }else if(shipmentStatus == 6){
            cell.refundButton.isHidden = false
            cell.rehireButtonView.isHidden = false
            cell.currentStatus.text = "Shipment completed"
            cell.editShipmentButton.isHidden = true
        } else if (shipmentStatus == 7) {
            cell.refundButton.isHidden = false
            cell.currentStatus.text = "Shipment disputed"
            cell.editShipmentButton.isHidden = true
        } else if(shipmentStatus == 8) {
            cell.refundButton.isHidden = true
            cell.editShipmentButton.isHidden = true
            cell.currentStatus.text = "Refund requested"
            
            if(getUserShipment[indexPath.row].refund_shipment_request?.status ?? 0 == 1){
                cell.currentStatus.text = "Shipment cancelled! Refund accepted by admin."
            } else if(getUserShipment[indexPath.row].refund_shipment_request?.status ?? 0 == 2){
                cell.currentStatus.text = "Shipment cancelled! Refund rejected by admin."
            }
        }
        
        cell.lblDestination.text = "\(getUserShipment[indexPath.row].to_name ?? "")"
        cell.lblTtlBids.text = "\(getUserShipment[indexPath.row].driver_bids_count ?? 0)"
        cell.shipmentButton = {
            selectedShipment = indexPath.row
            let sSVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
            sSVC.id = self.getUserShipment[indexPath.row].id ?? 0
            self.navigationController?.pushViewController(sSVC, animated: true)
        }
        
        cell.editButton = {
            let data = self.getUserShipment[indexPath.row]
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! PageViewController
            //controller.isEditJob = true
            var imageData = [String]()
            for val in data.image_list!{
                imageData.append(val.image ?? "")
            }
            var subcategory = [Int]()
            for val in data.subcategory_name {
                subcategory.append(val.id ?? 0)
            }
            K_DRAFT_OR_EDIT = 1
            K_DRIVER_REHIRE_ID = 0
            Singleton.shared.postYourShipment = GetPostYourShipmentData(id: data.id ?? 0, category_id: data.category_id ?? 0, discription: data.description ?? "", from_name: data.from_name ?? "", from_latitude: data.from_latitude ?? "", from_longitude: data.from_longitude ?? "", from_city: data.from_city ?? "", from_state: data.from_state ?? "0", from_zipcode: "\(data.from_zipcode ?? 0)", to_name: data.to_name ?? "", to_latitude: data.to_latitude ?? "", to_longitude: data.to_longitude ?? "", to_city: data.to_city ?? "", to_state: data.to_state ?? "0", to_zipcode: "\(data.to_zipcode ?? 0)", lookingFor: data.lookingFor ?? 0, sub_category_id: subcategory, set_budget: data.set_budget ?? 0, multiple_item: "0", distance: "\(data.distance ?? "0")", weight: data.weight ?? 0, price: data.price ?? "0", image: imageData, pickup_date: data.pickup_date ?? 0, dropoff_date: data.dropoff_date ?? 0)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        cell.refund = {
            self.selectedIndex = indexPath.row
            self.refundModalView.isHidden = false
        }
        
        
        cell.rehireButton = {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! PageViewController
            Singleton.shared.postYourShipment = GetPostYourShipmentData()
            K_DRAFT_OR_EDIT = 2
            if(self.getUserShipment[indexPath.row].lookingFor == 2){
                K_DRIVER_REHIRE_ID = self.getUserShipment[indexPath.row].completed_loader_id ?? 0
            } else {
                K_DRIVER_REHIRE_ID = self.getUserShipment[indexPath.row].completed_driver_id ?? 0
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        return cell
    }
}


class UserDashBoardTableCell: UITableViewCell{
    //MARK: IBOutlets
    @IBOutlet weak var lblSource: DesignableUILabel!
    @IBOutlet weak var lblDestination: DesignableUILabel!
    
    @IBOutlet weak var currentStatus: DesignableUILabel!
    @IBOutlet weak var postedDate: DesignableUILabel!
    @IBOutlet weak var distance: DesignableUILabel!
    @IBOutlet weak var lblTtlBids: DesignableUILabel!
    @IBOutlet weak var category: DesignableUILabel!
    @IBOutlet weak var editShipmentButton: CustomButton!
    @IBOutlet weak var shipmentComplete: CustomButton!
    
    @IBOutlet weak var startDate: DesignableUILabel!
    @IBOutlet weak var endDate: DesignableUILabel!
    @IBOutlet weak var rehireButtonView: CustomButton!
    @IBOutlet weak var shipmentTitleView: UIView!
    @IBOutlet weak var shipmentTitle: UILabel!
    @IBOutlet weak var awardedView: UIView!
    @IBOutlet weak var refundButton: UIButton!
    
    
    
    var shipmentButton:(()-> Void)? = nil
    var editButton:(()-> Void)? = nil
    var completeButton:(()-> Void)? = nil
    var rehireButton:(()-> Void)? = nil
    var refund:(()-> Void)? = nil
    
    
    //MARK: IBAction
    @IBAction func shipmentAction(_ sender: Any) {
        if let ship = self.shipmentButton {
            ship()
        }
    }
    
    @IBAction func editShipment(_ sender: Any) {
        if let edit = self.editButton {
            edit()
        }
    }
    
    @IBAction func rehireButton(_ sender: Any) {
        if let rehireButton = self.rehireButton {
            rehireButton()
        }
    }
    
    @IBAction func refundAction(_ sender: Any) {
        if let refund = self.refund {
            refund()
        }
    }
    
    
    
    @IBAction func completeAction(_ sender: Any) {
        if let completeButton = self.completeButton {
            completeButton()
        }
    }
}
