//
//  RateAndReviewVC.swift
//  SYM
//
//  Created by AM on 22/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import Cosmos

class RateAndReviewVC: UIViewController ,SelectFromPicker{
    func selectedPickerData(val: String, pos: Int) {
        if val == "Rating - High To Low" {
            sort = "desc"
        }else if val == "Rating - Low To High" {
            sort = "asc"
        }
        
        self.refresh()
        
    }
    

    //Mark: IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    @IBOutlet weak var progressView: StepProgressView!
    @IBOutlet weak var popupImage: ImageView!
    @IBOutlet weak var popupDate: DesignableUILabel!
    @IBOutlet weak var popupRating: CosmosView!
    @IBOutlet weak var popupReviewText: DesignableUILabel!
    @IBOutlet weak var popupFrom: DesignableUILabel!
    @IBOutlet weak var popupTo: DesignableUILabel!
    @IBOutlet weak var popupName: DesignableUILabel!
    @IBOutlet weak var popupView: UIView!
    
    var ratingData = Ratings()
    var refreshControl = UIRefreshControl()
    var sort = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableView.automaticDimension
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        self.refresh()
        
        self.progressView.viewShipment={
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.navigationController?.pushViewController(myVC, animated: true)
         }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setNavTitle(title: "Rating & Reviews")
        if(Singleton.shared.userType.role != 1){
            self.progressView.showAnimatingDots()
        self.progressView.showHideView={
            if(Singleton.shared.progressShipment?.count == 0){
                self.progressView.isHidden = true
            }else{
                for val in Singleton.shared.progressShipment!{
                    if(val.driver_status == 5){
                        self.progressView.isHidden = false
                    }
                }
            }
        }
        }
    }
   
    
    @objc func refresh() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_RATINGS + "?offset=\(1)" + "&order_by=" + sort, method: .get, parameter: nil, objectClass: GetRating.self, requestCode: U_GET_RATINGS, userToken: nil) { (response) in
            ActivityIndicator.hide()
            self.ratingData = response.response
            self.tableView.reloadData()
        }
        self.refreshControl.endRefreshing()
    }
    
    @IBAction func sortByAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerData.append("Rating - High To Low")
        myVC.pickerData.append("Rating - Low To High")
        myVC.pickerDelegate = self
        myVC.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func popupHideAction(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
    
}

extension RateAndReviewVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.ratingData.review_rating.count == 0){
            self.noDataLabel.isHidden = false
        }else {
            self.noDataLabel.isHidden = true
        }
        return self.ratingData.review_rating.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RateAndReviewTableCell", for: indexPath) as! RateAndReviewTableCell
        let val = self.ratingData.review_rating[indexPath.row]
        cell.userName.text = (val.user_details?.first_name ?? "") + " " + (val.user_details?.last_name ?? "")
        let date = self.convertDateToTimestamp(val.created_at ?? "",to: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        cell.date.text = self.convertTimestampToDate(date ?? 0, to: "MM-dd-yyyy")
        cell.userImage.sd_setImage(with: URL(string:U_IMAGE + (val.user_details?.profile_image ?? "")), placeholderImage:UIImage(named:"CATLEY_LAKEMAN-Russell"))
        
        cell.reviewText.text = "Review:- " +  (val.review ?? "")
        print("number")
        print(cell.reviewText.calculateMaxLines())
        if cell.reviewText.calculateMaxLines() > 1 {
           cell.reviewText.numberOfLines = 1
           cell.viewMoreButton.isHidden = false
        } else {
            cell.viewMoreButton.isHidden = true
        }
        
        cell.rating.rating = Double(val.rating ?? 0)
        cell.category.text = val.from_name
        cell.address.text = val.to_name
        
        cell.viewButton = {
            let val = self.ratingData.review_rating[indexPath.row]
            self.popupName.text = (val.user_details?.first_name ?? "") + " " + (val.user_details?.last_name ?? "")
            let date = self.convertDateToTimestamp(val.created_at ?? "",to: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            self.popupDate.text = self.convertTimestampToDate(date ?? 0, to: "MM-dd-yyyy")
            self.popupImage.sd_setImage(with: URL(string:U_IMAGE + (val.user_details?.profile_image ?? "")), placeholderImage:UIImage(named:"CATLEY_LAKEMAN-Russell"))
            self.popupReviewText.text = (val.review ?? "")
            self.popupRating.rating = Double(val.rating ?? 0)
            self.popupFrom.text = val.from_name
            self.popupTo.text = val.to_name
            self.popupView.isHidden = false
        }
       
        return cell
    }
}

class RateAndReviewTableCell: UITableViewCell{
    //MARK:IBOUtlets
    @IBOutlet weak var userName: DesignableUILabel!
    @IBOutlet weak var date: DesignableUILabel!
    @IBOutlet weak var reviewText: DesignableUILabel!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var category: DesignableUILabel!
    @IBOutlet weak var address: DesignableUILabel!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var viewMoreButton: UIButton!
    var viewButton:(()-> Void)? = nil
    
    @IBAction func viewMoreAction(_ sender: Any) {
        if let viewButton = self.viewButton {
            viewButton()
        }
    }
    
}
