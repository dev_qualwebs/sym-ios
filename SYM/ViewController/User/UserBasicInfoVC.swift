
//
//  UserBasicInfoVC.swift
//  SYM
//
//  Created by AM on 22/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import iOSDropDown

class UserBasicInfoVC: UIViewController, UITextViewDelegate,SelectedCategories {
    
    //MAKE DROPDOWN FAST GO INSIDE DROPDOWN AND SET VALUE OF DELAY TO 0 AND WITH DURATION TO 0.2
    
    //Mark: IBOUTLETS
    @IBOutlet weak var lookingFrTxtField: DropDown!
    @IBOutlet weak var selectCtgryTxtField: DropDown!
    @IBOutlet weak var lblChooseType: UILabel!
    @IBOutlet weak var shipmentTxtField: UITextView!
    @IBOutlet weak var selectSubCtgryTxtField: DropDown!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    
    //Mark: Properties
    var singleDraft = SingleDraft()
    var selectedCategoryId = Int()
    var selectedSubcategories = [GetSubCategories]()
    var getCategoryResponse = [GetCategories]()
    var subcategoriesData = Set<Int>()
    var isEditJob = false
    var selectedRole = Int()
    var shipmentId : Int?
    var draftImagePaths = [String]()
    var shipmentData = GetUserShipmentData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.value(forKey: UD_TOKEN) as? String != nil {
            self.navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
                if vc.isKind(of: SignUpVC.self) || vc.isKind(of: SignInVC.self) || vc.isKind(of: EntryViewController.self)
                {
                    return true
                }
                else
                {
                    return false
                }
            })
        }
        setNavTitle(title: "0")
        setNavigationBar("back", "none")
        getCategory()
        self.shipmentTxtField.delegate = self
        self.shipmentTxtField.textColor = .lightGray
        self.shipmentTxtField.text = "Please write a brief description of your shipment. Incldude as many as detail as possible."
        lookingFrTxtField.optionArray = ["Driver","Loader","Both"]
        lookingFrTxtField.didSelect { (selectedString, index, id) in
            self.selectedRole = index + 1
            self.lookingFrTxtField.text = "\(selectedString)"
        }
        
        self.shipmentId = Singleton.shared.postYourShipment.id == 0 ? nil:Singleton.shared.postYourShipment.id
        
        if UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil {
            Singleton.shared.getShimentData()
            self.shipmentId = Singleton.shared.postYourShipment.id
            self.subcategoriesData = Set(Singleton.shared.postYourShipment.sub_category_id)
            self.initializeView()
        } else {
            self.saveButton.isHidden = K_DRAFT_OR_EDIT == 1 ? true:false
            if K_DRAFT_OR_EDIT == 1{
                //self.lookingFrTxtField.text = "Driver"
                getSingleShipment()
            } else {
                getSingleDraft()
            }
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //   self.saveAction(self)
    }
    
    
    
    func initializeView(){
        self.selectedCategoryId = Singleton.shared.postYourShipment.category_id
        self.selectedRole = Singleton.shared.postYourShipment.lookingFor
        if(self.selectedRole == 1){
            self.lookingFrTxtField.text = "Driver"
        }else if(self.selectedRole == 2){
            self.lookingFrTxtField.text = "Loader"
        }else if(self.selectedRole == 3){
            self.lookingFrTxtField.text = "Both"
        }
        
        if Singleton.shared.postYourShipment.discription != ""{
            shipmentTxtField.text = Singleton.shared.postYourShipment.discription
            self.shipmentTxtField.textColor = .black
        }
        self.handleCategory()
    }
    
    func getSingleShipment(){
        if self.shipmentId != nil && self.shipmentId != 0 {
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = ["shipment_id":self.shipmentId]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_CUSTOMER_SINGLE_SHIPMENT, method: .post, parameter: param, objectClass: CustomerSingleShipment.self, requestCode: U_CUSTOMER_SINGLE_SHIPMENT, userToken: nil) { (response) in
                self.shipmentData = response.response
                
                self.draftImagePaths = []
                
                for i in 0..<(self.shipmentData.image_list?.count ?? 0){
                    self.draftImagePaths.append(self.shipmentData.image_list?[i].image ?? "")
                }
                // var val = Singleton.shared.postYourShipment
                Singleton.shared.postYourShipment.id = self.shipmentData.id ?? 0
                Singleton.shared.postYourShipment.lookingFor = self.shipmentData.lookingFor ?? 0
                Singleton.shared.postYourShipment.category_id = self.shipmentData.category_id ?? 0
                Singleton.shared.postYourShipment.discription = self.shipmentData.description ?? ""
                Singleton.shared.postYourShipment.from_state = self.shipmentData.from_state ?? ""
                Singleton.shared.postYourShipment.to_state = self.shipmentData.to_state ?? ""
                Singleton.shared.postYourShipment.from_name = self.shipmentData.from_name ?? ""
                Singleton.shared.postYourShipment.to_name = self.shipmentData.to_name ?? ""
                Singleton.shared.postYourShipment.from_house_no = self.shipmentData.from_house_no ?? ""
                Singleton.shared.postYourShipment.from_building = self.shipmentData.from_building ?? ""
                Singleton.shared.postYourShipment.from_street = self.shipmentData.from_street ?? ""
                Singleton.shared.postYourShipment.to_house_no = self.shipmentData.to_house_no ?? ""
                Singleton.shared.postYourShipment.to_building = self.shipmentData.to_building ?? ""
                Singleton.shared.postYourShipment.to_street = self.shipmentData.to_street ?? ""
                Singleton.shared.postYourShipment.from_longitude = self.shipmentData.from_longitude ?? ""
                Singleton.shared.postYourShipment.from_latitude = self.shipmentData.from_latitude ?? ""
                Singleton.shared.postYourShipment.to_longitude = self.shipmentData.to_longitude ?? ""
                Singleton.shared.postYourShipment.to_latitude = self.shipmentData.to_latitude ?? ""
                Singleton.shared.postYourShipment.from_city = self.shipmentData.from_city ?? ""
                Singleton.shared.postYourShipment.to_city = self.shipmentData.to_city ?? ""
                Singleton.shared.postYourShipment.from_zipcode = String(self.shipmentData.from_zipcode ?? 0)
                Singleton.shared.postYourShipment.to_zipcode = String(self.shipmentData.to_zipcode ?? 0)
                Singleton.shared.postYourShipment.image = self.draftImagePaths
                self.initializeView()
                ActivityIndicator.hide()
            }
        }
    }
    
    
    func getSingleDraft(){
        if self.shipmentId != nil && self.shipmentId != 0 {
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SINGLE_DRAFT + "\(Singleton.shared.postYourShipment.id )", method: .get, parameter: nil, objectClass: GetSingleDraftResponse.self, requestCode: U_GET_SINGLE_DRAFT, userToken: nil) { (response) in
                
                self.singleDraft = response.response ?? SingleDraft()
                
                self.draftImagePaths = []
                
                for i in 0..<(self.singleDraft.draft_images?.count ?? 0){
                    self.draftImagePaths.append(self.singleDraft.draft_images?[i].image ?? "")
                }
                // var val = Singleton.shared.postYourShipment
                Singleton.shared.postYourShipment.id = self.singleDraft.id ?? 0
                Singleton.shared.postYourShipment.lookingFor = self.singleDraft.lookingFor ?? 0
                Singleton.shared.postYourShipment.category_id = self.singleDraft.category_id ?? 0
                Singleton.shared.postYourShipment.discription = self.singleDraft.description ?? ""
                Singleton.shared.postYourShipment.from_state = String(self.singleDraft.from_state ?? 0)
                Singleton.shared.postYourShipment.to_state = String(self.singleDraft.to_state ?? 0)
                Singleton.shared.postYourShipment.from_name = self.singleDraft.from_name ?? ""
                Singleton.shared.postYourShipment.to_name = self.singleDraft.to_name ?? ""
                Singleton.shared.postYourShipment.from_longitude = self.singleDraft.from_longitude ?? ""
                Singleton.shared.postYourShipment.from_latitude = self.singleDraft.from_latitude ?? ""
                Singleton.shared.postYourShipment.to_longitude = self.singleDraft.to_longitude ?? ""
                Singleton.shared.postYourShipment.to_latitude = self.singleDraft.to_latitude ?? ""
                Singleton.shared.postYourShipment.from_city = self.singleDraft.from_city ?? ""
                Singleton.shared.postYourShipment.to_city = self.singleDraft.to_city ?? ""
                Singleton.shared.postYourShipment.from_zipcode = String(self.singleDraft.from_zipcode ?? 0)
                Singleton.shared.postYourShipment.to_zipcode = String(self.singleDraft.to_zipcode ?? 0)
                
                Singleton.shared.postYourShipment.from_house_no = self.singleDraft.from_house_no ?? ""
                Singleton.shared.postYourShipment.from_building = self.singleDraft.from_building ?? ""
                Singleton.shared.postYourShipment.from_street = self.singleDraft.from_street ?? ""
                Singleton.shared.postYourShipment.to_house_no = self.singleDraft.to_house_no ?? ""
                Singleton.shared.postYourShipment.to_building = self.singleDraft.to_building ?? ""
                Singleton.shared.postYourShipment.to_street = self.singleDraft.to_street ?? ""
                
                Singleton.shared.postYourShipment.image = self.draftImagePaths
                Singleton.shared.postYourShipment.pickup_date = self.singleDraft.pickup_date ?? 0
                Singleton.shared.postYourShipment.dropoff_date = self.singleDraft.dropoff_date ?? 0
                
                self.initializeView()
                ActivityIndicator.hide()
            }
        }
    }
    
    func getCategory(){
        if(Singleton.shared.categoryData.count > 0){
            self.getCategoryResponse = Singleton.shared.categoryData
            for i in 0...self.getCategoryResponse.count-1{
                self.selectCtgryTxtField.optionArray.append(self.getCategoryResponse[i].category ?? "")
            }
            self.selectCtgryTxtField.didSelect { (selectedString, index, id) in
                self.selectCtgryTxtField.text = "\(selectedString)"
                // self.lblChooseType.text = "Choose the type of \(selectedString)"
                self.selectedCategoryId = self.getCategoryResponse[index].id ?? 0
                self.selectedSubcategories.removeAll()
                self.subcategoriesData.removeAll()
                self.selectSubCtgryTxtField.text = ""
                self.selectedSubcategories = self.getCategoryResponse[index].sub_categories!
            }
            self.handleCategory()
        }else{
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CATEGORIES, method: .get, parameter: nil, objectClass: GetCategoriesResponse.self, requestCode: U_GET_CATEGORIES, userToken: nil) { (response) in
                Singleton.shared.categoryData = response.response
                self.getCategoryResponse = response.response
                if(self.getCategoryResponse.count > 0){
                    
                    for i in 0...self.getCategoryResponse.count-1{
                        self.selectCtgryTxtField.optionArray.append(self.getCategoryResponse[i].category ?? "")
                    }
                    
                    self.selectCtgryTxtField.didSelect { (selectedString, index, id) in
                        self.selectCtgryTxtField.text = "\(selectedString)"
                        // self.lblChooseType.text = "Choose the type of \(selectedString)"
                        self.selectedCategoryId = self.getCategoryResponse[index].id ?? 0
                        self.selectedSubcategories.removeAll()
                        self.subcategoriesData.removeAll()
                        self.selectSubCtgryTxtField.text = ""
                        self.selectedSubcategories = self.getCategoryResponse[index].sub_categories!
                        ActivityIndicator.hide()
                    }
                    self.handleCategory()
                }
            }
        }

    }
    
    func handleCategory(){
        for i in 0..<self.getCategoryResponse.count {
            if self.selectedCategoryId == self.getCategoryResponse[i].id {
                self.selectCtgryTxtField.text = self.getCategoryResponse[i].category ?? ""
            }
        }
        
        
        let category = self.getCategoryResponse.filter{
            $0.id == Singleton.shared.postYourShipment.category_id
        }
        
        
        if(category.count > 0){
            self.selectCtgryTxtField.text = category[0].category
            self.selectedSubcategories = category[0].sub_categories!
        }
        if UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil {
            for val in 0..<(self.selectedSubcategories.count){
                for i in self.subcategoriesData {
                    if(self.selectedSubcategories[val].id == i){
                        if(i == (self.selectedSubcategories.count - 1)){
                            self.selectSubCtgryTxtField.text = (self.selectSubCtgryTxtField.text ?? "") + (self.selectedSubcategories[val].category ?? "")
                        } else {
                        self.selectSubCtgryTxtField.text = (self.selectSubCtgryTxtField.text ?? "") + (self.selectedSubcategories[val].category ?? "") + ", "
                        }
                        self.subcategoriesData.insert(i ?? 0)
                    }
                }
            }
        } else if K_DRAFT_OR_EDIT != 1{
            for val in 0..<(self.singleDraft.draft_subcategories?.count ?? 0 ){
                for i in 0..<self.selectedSubcategories.count{
                if((self.singleDraft.draft_subcategories?[val].subcategory_id ?? 0) == self.selectedSubcategories[i].id){
                    if(val == ((self.singleDraft.draft_subcategories?.count ?? 0) - 1)){
                        self.selectSubCtgryTxtField.text = (self.selectSubCtgryTxtField.text ?? "") + (self.selectedSubcategories[i].category ?? "")
                    } else {
                    self.selectSubCtgryTxtField.text = (self.selectSubCtgryTxtField.text ?? "") + (self.selectedSubcategories[i].category ?? "") + ", "
                    }
                        self.subcategoriesData.insert(self.selectedSubcategories[i].id ?? 0)
                    }
                }
            }
            
        } else {
            for val in 0..<(self.shipmentData.subcategory_name.count){
                for i in 0..<self.selectedSubcategories.count{
                    if((self.shipmentData.subcategory_name[val].id ?? 0) == self.selectedSubcategories[i].id){
                        if(val == (self.shipmentData.subcategory_name.count - 1)){
                            self.selectSubCtgryTxtField.text = (self.selectSubCtgryTxtField.text ?? "") + (self.selectedSubcategories[i].category ?? "")
                        } else {
                        self.selectSubCtgryTxtField.text = (self.selectSubCtgryTxtField.text ?? "") + (self.selectedSubcategories[i].category ?? "") + ", "
                        }
                        self.subcategoriesData.insert(self.selectedSubcategories[i].id ?? 0)
                    }
                }
            }
            
        }
    }
    
    func currentSelectedCategories(id: Set<Int>, categories: [GetCategories]) {
        
    }
    
    func currentSelectedSubCategories(id: Set<Int>, categories: [GetSubCategories]) {
        self.subcategoriesData = id
        self.selectSubCtgryTxtField.text = ""
//        for val in categories {
//            self.selectSubCtgryTxtField.text = (self.selectSubCtgryTxtField.text ?? "") + (val.category ?? "") + ", "
//        }
        for i in 0..<categories.count {
            if(i == (categories.count - 1)){
            self.selectSubCtgryTxtField.text = (self.selectSubCtgryTxtField.text ?? "") + (categories[i].category ?? "")
            } else {
            self.selectSubCtgryTxtField.text = (self.selectSubCtgryTxtField.text ?? "") + (categories[i].category ?? "") + ", "
            }
        }
       
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    @IBAction func choseSubctgryBtPressed(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ShipCategoriesVC") as! ShipCategoriesVC
        myVC.isSubcategory = true
        myVC.categoryDelegate = self
        myVC.headingText = "Select Subcategory"
        myVC.subcategories = self.selectedSubcategories
        myVC.selectedCategory = self.subcategoriesData
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.subcategories.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        self.navigateToAddressFill(saveInfo: false)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil {
            // var val = Singleton.shared.postYourShipment
            Singleton.shared.postYourShipment.id = self.shipmentId ?? 0
            Singleton.shared.postYourShipment.lookingFor = self.selectedRole
            Singleton.shared.postYourShipment.discription =  (shipmentTxtField.text ?? "" == "Please write a brief description of your shipment. Incldude as many as detail as possible.") ? "" : shipmentTxtField.text ?? ""
            Singleton.shared.postYourShipment.category_id = self.selectedCategoryId
            Singleton.shared.postYourShipment.sub_category_id = Array(self.subcategoriesData)
            Singleton.shared.postYourShipment.category_id = self.selectedCategoryId
            Singleton.shared.showToast(text: "Saved as draft.", color: successGreen)
            Singleton.shared.saveShipmentData(data: Singleton.shared.postYourShipment, screen: "0")
        } else {
            
            if self.shipmentId == 0 {
                self.shipmentId = nil
            }
            
            ActivityIndicator.show(view: self.view)
            let param : [String:Any] = [
                "draft_shipment_id": self.shipmentId,
                "description" : self.shipmentTxtField.text,
                "category_id": selectedCategoryId,
                "lookingFor": selectedRole,
                "sub_category_id": Array(self.subcategoriesData),
                "from_state": self.singleDraft.from_state,
                "to_state":self.singleDraft.to_state,
                "from_name": self.singleDraft.from_name,
                "to_name": self.singleDraft.to_name,
                "from_longitude": self.singleDraft.from_longitude,
                "from_latitude": self.singleDraft.from_latitude,
                "to_longitude": self.singleDraft.to_longitude,
                "to_latitude": self.singleDraft.to_latitude,
                "from_city": self.singleDraft.from_city,
                "to_city": self.singleDraft.to_city,
                "from_zipcode": self.singleDraft.from_zipcode,
                "to_zipcode": self.singleDraft.to_zipcode,
                "image" : self.draftImagePaths,
                "from_house_no" : self.singleDraft.from_house_no,
                "from_building" : self.singleDraft.from_building,
                "from_street" : self.singleDraft.from_street,
                "to_house_no" : self.singleDraft.to_house_no,
                "to_building" : self.singleDraft.to_building,
                 "to_street" : self.singleDraft.to_street
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SAVE_DRAFT_SHIPMENT, method: .post, parameter: param, objectClass: SaveDraftResponse.self, requestCode: U_SAVE_DRAFT_SHIPMENT, userToken: nil) { (response) in
                Singleton.shared.postYourShipment.id = response.response?.draft_shipment_id ?? 0
                self.shipmentId = response.response?.draft_shipment_id
                
                Singleton.shared.showToast(text: "Saved as draft.", color: successGreen)
                
                ActivityIndicator.hide()
            }
        }
    }
    
    
    func navigateToAddressFill(saveInfo:Bool){
        if lookingFrTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please fill all fields", color: errorRed)
        }
        else if selectCtgryTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please fill all fields", color: errorRed)
        }
        else if subcategoriesData.count == 0 {
            Singleton.shared.showToast(text: "Please fill all fields", color: errorRed)
        }
        else if shipmentTxtField.text!.isEmpty || shipmentTxtField.text == "Please write a brief description of your shipment. Incldude as many as detail as possible." {
            Singleton.shared.showToast(text: "Please fill all fields", color: errorRed)
        } else{
            let uRVC = storyboard!.instantiateViewController(withIdentifier: "UserRouteVC") as! UserRouteVC
            if UserDefaults.standard.value(forKey: UD_TOKEN) as? String != nil {
                Singleton.shared.postYourShipment.id = self.shipmentId ?? 0
                Singleton.shared.postYourShipment.lookingFor = self.selectedRole
                Singleton.shared.postYourShipment.discription = shipmentTxtField.text ?? ""
                Singleton.shared.postYourShipment.category_id = self.selectedCategoryId
                Singleton.shared.postYourShipment.sub_category_id = Array(self.subcategoriesData)
                Singleton.shared.postYourShipment.category_id = self.selectedCategoryId
            } else {
                Singleton.shared.draftData.id = self.shipmentId ?? 0
                Singleton.shared.draftData.lookingFor = self.selectedRole
                Singleton.shared.draftData.discription = shipmentTxtField.text ?? ""
                Singleton.shared.draftData.category_id = self.selectedCategoryId
                Singleton.shared.draftData.sub_category_id = Array(self.subcategoriesData)
                Singleton.shared.draftData.category_id = self.selectedCategoryId
                
                Singleton.shared.postYourShipment.id = self.shipmentId ?? 0
                Singleton.shared.postYourShipment.lookingFor = self.selectedRole
                Singleton.shared.postYourShipment.discription = shipmentTxtField.text ?? ""
                Singleton.shared.postYourShipment.category_id = self.selectedCategoryId
                Singleton.shared.postYourShipment.sub_category_id = Array(self.subcategoriesData)
                Singleton.shared.postYourShipment.category_id = self.selectedCategoryId
                //            uRVC.draftData = self.draftData
            }
            uRVC.isEditJob = self.isEditJob
            
            NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_CURRENT_INDEX), object: nil, userInfo: ["index":1])
            
            
        }
    }
    
}

extension UserBasicInfoVC{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Please write a brief description of your shipment. Incldude as many as detail as possible."){
            self.shipmentTxtField.text = ""
            self.shipmentTxtField.textColor = .black
        }else {
            self.shipmentTxtField.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == ""){
            self.shipmentTxtField.text = "Please write a brief description of your shipment. Incldude as many as detail as possible."
            self.shipmentTxtField.textColor = .lightGray
        }else {
            self.shipmentTxtField.textColor = .black
        }
    }
}


class SubCategoryTableCell: UITableViewCell{
    
    @IBOutlet weak var lblSubctgryName: DesignableUILabel!
    
    var indexpath:IndexPath?
    var removeItem: (()-> Void)? = nil
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        if let removeItem = self.removeItem {
            removeItem()
        }
    }
}
