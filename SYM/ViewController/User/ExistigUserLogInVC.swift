//
//  ExistigUserLogInVC.swift
//  SYM
//
//  Created by AM on 04/12/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class ExistigUserLogInVC: UIViewController {
    
    //Mark: IBOUTLETS
    @IBOutlet weak var firstNameTxtField: DesignableUITextField!
    @IBOutlet weak var lastNameTxtField: DesignableUITextField!
    @IBOutlet weak var phoneNumbrTxtField: DesignableUITextField!
    @IBOutlet weak var emailTxtField: DesignableUITextField!
    @IBOutlet weak var passwordTxtField: DesignableUITextField!
    @IBOutlet weak var confirmPsswrdTxtField: DesignableUITextField!
    @IBOutlet weak var EnterPasswordEyeImage: UIImageView!
    @IBOutlet weak var confirmPasswordEyeImage: UIImageView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var createAccountButton: CustomButton!
    
    //Mark: Properties
    var pickUpDate = String()
    var dropOffDate = String()
    var didSelectTime = Date()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.phoneNumbrTxtField.delegate = self
        self.phoneNumbrTxtField.keyboardType = .phonePad
        self.confirmPasswordEyeImage.image = UIImage(named: "invisible")
        self.EnterPasswordEyeImage.image = UIImage(named: "invisible")
        self.setNavTitle(title: "Post your Move")
        setNavigationBar("back", nil)
        self.hideIndicator()
    }
    
    
    func showIndicator(){
        self.createAccountButton.setTitle("", for: .normal)
        self.indicatorView.isHidden = false
        self.indicatorView.color = .white
        self.indicatorView.startAnimating()
        self.createAccountButton.isUserInteractionEnabled = false
    }
    
    func hideIndicator(){
        self.createAccountButton.setTitle("CREATE ACCOUNT", for: .normal)
        self.indicatorView.isHidden = true
        self.indicatorView.color = .white
        self.indicatorView.stopAnimating()
        self.createAccountButton.isUserInteractionEnabled = true
    }
    
    
    
    @IBAction func hideShowEnterPasswordAction(_ sender: Any) {
        if self.EnterPasswordEyeImage.image == UIImage(named: "invisible") {
            self.EnterPasswordEyeImage.image = UIImage(named: "eye")
            self.passwordTxtField.isSecureTextEntry = false
        } else {
            self.EnterPasswordEyeImage.image = UIImage(named: "invisible")
            self.passwordTxtField.isSecureTextEntry = true
        }
    }
    
    @IBAction func hideShowCOnfirmPasswordAction(_ sender: Any) {
            if self.confirmPasswordEyeImage.image == UIImage(named: "invisible") {
                self.confirmPasswordEyeImage.image = UIImage(named: "eye")
                self.confirmPsswrdTxtField.isSecureTextEntry = false
            } else {
                self.confirmPasswordEyeImage.image = UIImage(named: "invisible")
                self.confirmPsswrdTxtField.isSecureTextEntry = true
            }
    }
    
    @IBAction func loginBtnPressed(_ sender: Any) {
        
        if firstNameTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter first name", color: errorRed)
        }
        else if lastNameTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter last name", color: errorRed)
        }
        else if phoneNumbrTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter phone Number", color: errorRed)
        }
        else if emailTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter email", color: errorRed)
        }
        else if passwordTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter password", color: errorRed)
        }
        else if confirmPsswrdTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter confirm password", color: errorRed)
        }
        else{
            
            //delay for preventing multiple taps
             if Date().timeIntervalSince(self.didSelectTime) < 1.0 {
                     self.didSelectTime = Date()
                     return
                 }
            
              self.didSelectTime = Date()
            
            
            var number = self.phoneNumbrTxtField.text ?? ""
            let fcmToken = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) as? String
            number = number.replacingOccurrences(of: "+1", with: "")
            number = number.replacingOccurrences(of: "(", with: "")
            number = number.replacingOccurrences(of: ")", with: "")
            number = number.replacingOccurrences(of: " ", with: "")
            let param:[String:Any] = [
                "category_id":Singleton.shared.draftData.category_id
                ,"description":Singleton.shared.draftData.discription
                ,"from_name":Singleton.shared.draftData.from_name
                ,"from_latitude":Singleton.shared.draftData.from_latitude
                ,"from_longitude":Singleton.shared.draftData.from_longitude
                ,"from_city":Singleton.shared.draftData.from_city ?? Singleton.shared.draftData.from_state
                ,"from_state":Singleton.shared.draftData.from_state
                ,"from_zipcode":Singleton.shared.draftData.from_zipcode
                ,"to_name":Singleton.shared.draftData.to_name
                ,"to_latitude":Singleton.shared.draftData.to_latitude
                ,"to_longitude":Singleton.shared.draftData.to_longitude
                ,"to_city":Singleton.shared.draftData.to_city ?? Singleton.shared.draftData.to_state
                ,"to_state":Singleton.shared.draftData.to_state
                ,"to_zipcode":Singleton.shared.draftData.to_zipcode
                ,"pickup_date":Singleton.shared.draftData.pickup_date
                ,"dropoff_date":Singleton.shared.draftData.dropoff_date
                ,"lookingFor":Singleton.shared.draftData.lookingFor
                ,"sub_category_id":Singleton.shared.draftData.sub_category_id,
                 "first_name":firstNameTxtField.text ?? "",
                 "last_name":lastNameTxtField.text ?? "",
                 "email":emailTxtField.text ?? "",
                 "password":passwordTxtField.text ?? "",
                "from_house_no" :Singleton.shared.draftData.from_house_no,
                "from_building" :Singleton.shared.draftData.from_building,
                "from_street" :Singleton.shared.draftData.from_street,
                  "to_house_no" :   Singleton.shared.draftData.to_house_no ,
                "to_building" : Singleton.shared.draftData.to_building ,
                  "to_street" :Singleton.shared.draftData.to_street,
                 "type":1,
                 "country_code":"+1",
                 "image":Singleton.shared.draftData.image,
                 "password_confirmation":confirmPsswrdTxtField.text ?? "",
                 "phone":number,
                 "set_budget":Singleton.shared.draftData.set_budget,
                 "city_distance":Singleton.shared.draftData.distance,
                 "weight":Singleton.shared.draftData.weight,
                 "price":Singleton.shared.draftData.price,
                 "firebase_token":fcmToken ?? "",
                 "platform":2
            ]
            self.showIndicator()
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_NEW_SHIPMENT, method: .post, parameter: param, objectClass: GetPostShipmentAndRegister.self, requestCode: U_ADD_NEW_SHIPMENT, userToken: nil) { (responseData) in
                self.hideIndicator()
                Singleton.shared.draftData = GetPostYourShipmentData()
                UserDefaults.standard.set(responseData.response?.token ?? "", forKey: UD_TOKEN)
                  Singleton.shared.showToast(text: "Your Shipment Posted Successfully!", color: successGreen)
                  Singleton.shared.userType.role = 1
                UserDefaults.standard.removeObject(forKey: UD_SHIPMENT_DATA)
                Singleton.shared.userType.username = responseData.response?.username
                Singleton.shared.userType.first_name = responseData.response?.first_name
                Singleton.shared.userType.phone = responseData.response?.phone ?? ""
                Singleton.shared.userType.last_name = responseData.response?.last_name
                Singleton.shared.userType.user_id = responseData.response?.user_id
                Singleton.shared.saveTypeOfUser(data: Singleton.shared.userType)
                    let uRVC = self.storyboard!.instantiateViewController(withIdentifier: "UserTabViewController")
                    self.navigationController?.pushViewController(uRVC, animated: true)
            }
            
        }
        
    }

}


extension ExistigUserLogInVC : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
       if (textField == phoneNumbrTxtField) {
        if(textField.text == ""){
            self.phoneNumbrTxtField.text = "+1"
        }
       }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == phoneNumbrTxtField) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.phoneNumbrTxtField.text = self.phoneNumbrTxtField.text?.applyPatternOnNumbers(pattern:"+x (xxx) xxx-xxxxx", replacmentCharacter: "x")
            return newLength <= 17
        }else {
            return true
        }
    }
    
}
