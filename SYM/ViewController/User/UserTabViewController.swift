//
//  UserTabViewController.swift
//  SYM
//
//  Created by qw on 15/03/21.
//  Copyright © 2021 AM. All rights reserved.
//

import UIKit
//import RAMAnimatedTabBarController
import MaterialComponents.MaterialBottomNavigation

class UserTabViewController : UITabBarController,MDCBottomNavigationBarDelegate {
    
    let bottomNavBar = MDCBottomNavigationBar()
    var dashboardViewControllers: [UIViewController] = []
    let tabBarItem1 = UITabBarItem( title: "Home", image: UIImage(named: "home(5)"), tag: 0 )
    let tabBarItem2 = UITabBarItem( title: "Draft",   image: UIImage(named: "tracking-1"), tag: 1 )
    let tabBarItem3 = UITabBarItem( title: "Messages", image: UIImage(named: "email(5)"), tag: 2 )
    let tabBarItem4 = UITabBarItem( title: "Rate/Review", image: UIImage(named: "star"), tag: 3 )
    var isFirstTime = true
    
        override func viewDidLoad() {
            super.viewDidLoad()
            tabBarItem.accessibilityValue = "10 unread emails"
            dashboardViewControllers = [self.newColoredViewController(controller: "UserDashBoardVC"),
                                              self.newColoredViewController(controller: "ShipmentsVC"),
                                              self.newColoredViewController(controller: "DriverMessagesVC"),
                                              self.newColoredViewController(controller: "RateAndReviewVC")]
            
            self.viewControllers = dashboardViewControllers
            self.selectedViewController = self.viewControllers![0]
            setNavigationBar("menu", "notification")
            self.bottomNavBar.unselectedItemTintColor = .lightGray
            self.bottomNavBar.selectedItemTintColor = primaryColor
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.changeTab(_:)), name: NSNotification.Name(N_CHANGE_CURRENT_TAB), object: nil)
        }
    
    private func newColoredViewController(controller: String)->UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controller)
    }
    
    func bottomNavigationBar(_ bottomNavigationBar: MDCBottomNavigationBar, didSelect item: UITabBarItem) {
            print("did select item \(item.tag)")
        self.selectedViewController = self.viewControllers![item.tag]
        }
    
        override func viewWillAppear(_ animated: Bool) {
            self.bottomNavBar.items = [ tabBarItem1, tabBarItem2, tabBarItem3 ,tabBarItem4]
            if self.isFirstTime == true {
                self.selectedIndex = 1
                self.bottomNavBar.selectedItem = self.bottomNavBar.items[1]
                self.selectedIndex = 0
                self.bottomNavBar.selectedItem = self.bottomNavBar.items[0]
                self.isFirstTime = false
            }
            self.navigationController?.isNavigationBarHidden = false
        }
//    override func viewWillAppear(_ animated: Bool) {
//        bottomNavBar.items = [ tabBarItem1, tabBarItem2, tabBarItem3 ,tabBarItem4]
//        if isFirstTime == true {
//            self.selectedIndex = 1
//            self.bottomNavBar.selectedItem = self.bottomNavBar.items[1]
//            self.selectedIndex = 0
//            self.bottomNavBar.selectedItem = self.bottomNavBar.items[0]
//            self.isFirstTime = false
//        }
//        self.navigationController?.isNavigationBarHidden = false
//    }
        @objc func changeTab(_ notif: Notification){
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_CHANGE_CURRENT_TAB), object: nil)
            if let index = notif.userInfo?["index"] as? Int{
                self.selectedIndex = index
            }
            NotificationCenter.default.addObserver(self, selector: #selector(self.changeTab(_:)), name: NSNotification.Name(N_CHANGE_CURRENT_TAB), object: nil)
        }
    
    init()
     {
         super.init(nibName: nil, bundle: nil)
         commonBottomNavigationTypicalUseSwiftExampleInit()
     }

     @available(*, unavailable)
     required init?(coder aDecoder: NSCoder)
     {
         super.init(coder: aDecoder)
         commonBottomNavigationTypicalUseSwiftExampleInit()
     }

     // Bottom Bar Customization
     func commonBottomNavigationTypicalUseSwiftExampleInit()
     {
         view.backgroundColor = .lightGray
         view.addSubview(bottomNavBar)

         // Always show bottom navigation bar item titles.
         bottomNavBar.titleVisibility = .selected
         bottomNavBar.alignment = .justifiedAdjacentTitles
         bottomNavBar.elevation = ShadowElevation(rawValue: 10)

         // Cluster and center the bottom navigation bar items.
         bottomNavBar.alignment = .centered

         // Add items to the bottom navigation bar.
         
//         bottomNavBar.items = [ tabBarItem1, tabBarItem2, tabBarItem3 ,tabBarItem4]

         // Select a bottom navigation bar item.
         bottomNavBar.selectedItem = tabBarItem1;
         bottomNavBar.delegate = self
         
     }


     override func viewWillLayoutSubviews()
     {
         super.viewWillLayoutSubviews()
         layoutBottomNavBar()
     }

     #if swift(>=3.2)
     @available(iOS 11, *)
     override func viewSafeAreaInsetsDidChange()
     {
         super.viewSafeAreaInsetsDidChange()
         layoutBottomNavBar()
     }
     #endif

     // Setting Bottom Bar
     func layoutBottomNavBar()
     {
         let size = bottomNavBar.sizeThatFits(view.bounds.size)
         let bottomNavBarFrame = CGRect( x: 0,
                                         y: view.bounds.height - size.height,
                                         width: size.width,
                                         height: size.height)
         bottomNavBar.frame = bottomNavBarFrame
     }

 }
