//
//  AddressBottomViewController.swift
//  SYM
//
//  Created by Sagar Pandit on 14/09/22.
//  Copyright © 2022 AM. All rights reserved.
//

import UIKit
import GooglePlaces


protocol HandleUserRoute {
    func handleUserAdress(address:HandleAddress)
}

class AddressBottomViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cityLabel: DesignableUILabel!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var houseLabel: DesignableUILabel!
    @IBOutlet weak var houseField: DesignableUITextField!
    @IBOutlet weak var zipcodeLabel: DesignableUILabel!
    @IBOutlet weak var zipcodeField: DesignableUITextField!
    @IBOutlet weak var buildingLabel: DesignableUILabel!
    @IBOutlet weak var buildingField: UITextField!
    @IBOutlet weak var streetLabel: DesignableUILabel!
    @IBOutlet weak var streetField: UITextField!
    
    var isPickup = true
    var address = HandleAddress()
    var isEditJob = false
    
    static var addressDelegate : HandleUserRoute? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
//        if(self.isEditJob){
        initValues()
//        }
    }
    
   func initView(){
       self.titleLabel.text = self.isPickup == true ? "Pickup Address" : "Drop Off Address"
       self.cityLabel.text = self.isPickup  == true ? "Pickup City" : "Drop Off City"
       self.houseLabel.text = "House Number"
       self.zipcodeLabel.text = "Zipcode"
       self.buildingLabel.text = "Building Name"
       self.streetLabel.text = "Address Line (Optional)"
       
       self.cityField.placeholder = self.isPickup ? "Pickup City" : "Drop Off City"
       self.houseField.placeholder = "House No."
       self.zipcodeField.text = self.isPickup ? "Pickup Zipcode" : "Drop Off Zipcode"
       self.buildingField.placeholder = "Building Name"
       self.cityField.text = self.isPickup ? "Pickup City" : "Drop Off City"
       
    }
    
    func initValues(){
        self.cityField.text = address.city ?? ""
        self.houseField.text = address.house ?? ""
        self.buildingField.text = address.building ?? ""
        self.streetField.text = address.street ?? ""
        self.zipcodeField.text = address.zip ?? ""
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func CitySearchAction(_ sender: Any) {
        self.cityField.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        filter.country = "USA"
        acController.autocompleteFilter = filter
        acController.delegate = self as! GMSAutocompleteViewControllerDelegate
        present(acController, animated: true, completion: nil)
    }

    @IBAction func saveAction(_ sender: Any) {
        if(self.cityField.text == "" ){
            Singleton.shared.showToast(text: "Please Enter City", color: errorRed)
        } else if(self.houseField.text == "" ){
            Singleton.shared.showToast(text: "Please Enter Houese Number", color: errorRed)
        }  else if(self.zipcodeField.text == "" ){
            Singleton.shared.showToast(text: "Please Enter Zipcode", color: errorRed)
        } else {
            self.address.city = self.cityField.text ?? ""
            self.address.house = self.houseField.text ?? ""
            self.address.building = self.buildingField.text ?? ""
            self.address.street = self.streetField.text ?? ""
            self.address.zip = self.zipcodeField.text ?? ""
            AddressBottomViewController.addressDelegate?.handleUserAdress(address: self.address)
            self.dismiss(animated: true)
        }
    }
    

}


extension AddressBottomViewController: GMSAutocompleteViewControllerDelegate{
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        Singleton.shared.showToast(text: error.localizedDescription, color: errorRed)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {

        address.latitude = "\(place.coordinate.latitude)"
        address.longitude = "\(place.coordinate.longitude)"
            let geoCoder = CLGeocoder()

            let location = CLLocation(latitude: place.coordinate.latitude,  longitude: place.coordinate.longitude)
            geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
                guard let placeMark = placemarks?.first else { return }
//                if let locationName = placeMark.subAdministrativeArea {
//                    self.address.name = locationName
//                }
                self.streetField.text = place.formattedAddress
                
                if let city = placeMark.addressDictionary?["City"] as? String {
                    self.cityField.text = city
                }
                
            
                
                if let house = placeMark.subThoroughfare {
                    self.houseField.text = house
                }
                
                if let building = placeMark.thoroughfare {
                    self.buildingField.text = building
                }
            
                if let zipCode = placeMark.postalCode {
                    self.zipcodeField.text = "\(zipCode)"
                }
                if let state = placeMark.administrativeArea {
                    let pickState = Singleton.shared.stateNames.filter{
                        $0.code == state
                    }
                    if(pickState.count > 0){
                        self.address.state = pickState[0].state ?? ""
                    }
                }
            }
            
            dismiss(animated: true, completion: nil)
    }
}


struct HandleAddress : Codable {
var city:String?
var house:String?
var building:String?
var street:String?
var zip:String?
var latitude:String?
var longitude:String?
var state : String?
}
