//
//  UserFinishingUpVC.swift
//  SYM
//
//  Created by AM on 22/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class UserFinishingUpVC: UIViewController {
    
    //Mark: IBOUTLETS
    @IBOutlet weak var pickUpTxtField: UITextField!
    @IBOutlet weak var dropOffTxtField: UITextField!
    @IBOutlet weak var openImgView: UIImageView!
    @IBOutlet weak var strictImgView: UIImageView!
    @IBOutlet weak var strictPriceView: View!
    @IBOutlet weak var strictPriceTxtField: DesignableUITextField!
    @IBOutlet weak var priceTop: NSLayoutConstraint!
    
    @IBOutlet weak var priceLabel: DesignableUILabel!
    @IBOutlet weak var termsPolicyTop: NSLayoutConstraint!
    @IBOutlet weak var saveButton: CustomButton!
    @IBOutlet weak var noSelectionBorderView: View!
    @IBOutlet weak var noSelectionView: View!
    @IBOutlet weak var yesSelectionBorderView: View!
    @IBOutlet weak var yesSelectionView: View!
    
    
    //Mark: Properties
    var pickerType = 1
    var isEditJob = false
    var pickDate = Int()
    var dropDate = Int()
//    var draftData = GetPostYourShipmentData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavTitle(title: "Share Your Move")
        setNavigationBar("back", "none")
        strictPriceTxtField.isHidden = true
        strictPriceView.isHidden = true
        self.priceLabel.isHidden = true
        priceTop.constant = CGFloat(20)
        openImgView.image = UIImage(named: "GreyTick")
        yesSelectionView.backgroundColor = UIColor.lightGray
        yesSelectionBorderView.borderColor = UIColor.lightGray
        noSelectionView.backgroundColor = brownColor
        noSelectionBorderView.borderColor = brownColor
        
        if UserDefaults.standard.value(forKey: UD_TOKEN) as? String != nil {
            self.saveButton.isHidden = K_DRAFT_OR_EDIT == 1 ? true:false
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(dateAdd(_:)), name: NSNotification.Name(N_Date_Picker), object: nil)
        //if(self.isEditJob){
        self.initializeView()
        // }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       // self.saveAction(self)
    }
    
    func initializeView(){
        if(Singleton.shared.postYourShipment.pickup_date != 0){
            self.pickUpTxtField.text = self.convertTimestampToDate(Singleton.shared.postYourShipment.pickup_date, to: "yyyy-MM-dd")
        }
        self.pickDate = Singleton.shared.postYourShipment.pickup_date
        self.dropDate = Singleton.shared.postYourShipment.dropoff_date
        if(Singleton.shared.postYourShipment.dropoff_date != 0){
            self.dropOffTxtField.text = self.convertTimestampToDate(Singleton.shared.postYourShipment.dropoff_date, to: "yyyy-MM-dd")
        }
        if(Singleton.shared.postYourShipment.set_budget == 0){
            self.openforImgBtn(self)
        }else if(Singleton.shared.postYourShipment.set_budget == 1){
            self.strictBudgtImgBtn(self)
        }
        self.strictPriceTxtField.text = "\(Singleton.shared.postYourShipment.price)"
    }
    
    @objc func dateAdd(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_Date_Picker), object: nil)
        if pickerType == 1{
            self.pickUpTxtField.text = "\(K_SELECTED_DATE)"
            self.pickDate = self.convertDateToTimestamp(K_SELECTED_DATE, to: "yyyy-MM-dd")
            self.dropDate = 0
            self.dropOffTxtField.text = ""
        }
        else if pickerType == 2{
            self.dropOffTxtField.text = "\(K_SELECTED_DATE)"
            self.dropDate = self.convertDateToTimestamp(K_SELECTED_DATE, to: "yyyy-MM-dd")
        }
        NotificationCenter.default.addObserver(self, selector: #selector(dateAdd(_:)), name: NSNotification.Name(N_Date_Picker), object: nil)
    }
    
    @IBAction func pickUpDtBtnPressed(_ sender: Any) {
        let dPVC = storyboard?.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        let myDate = Calendar.current.date(byAdding: .day, value: 2, to: Date())!
        dPVC.minDate = myDate
        pickerType = 1
        dPVC.picker_type = 1
        self.present(dPVC, animated: true, completion: nil)
    }
    
    @IBAction func drpOffBtnPressed(_ sender: Any) {
        if(self.pickDate == 0){
            Singleton.shared.showToast(text: "Select pickup date", color: errorRed)
        }else {
            let dPVC = storyboard?.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
            
            var myDate = Date(timeIntervalSince1970: TimeInterval(exactly:  Double(self.pickDate))!)
            myDate = Calendar.current.date(byAdding: .day, value: 1, to: myDate)!
//            dPVC.minDate = Date()
            dPVC.minDate = myDate
//            Date(timeIntervalSince1970: TimeInterval(self.pickDate))
            pickerType = 2
            dPVC.picker_type = 2
            self.present(dPVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func openforImgBtn(_ sender: Any) {
        openImgView.image = UIImage(named: "GreyTick")
        strictImgView.image = UIImage(named: "GreyBox")
        strictPriceTxtField.isHidden = true
        self.priceLabel.isHidden = true
        strictPriceView.isHidden = true
        priceTop.constant = CGFloat(20)
        yesSelectionView.backgroundColor = UIColor.lightGray
        yesSelectionBorderView.borderColor = UIColor.lightGray
        noSelectionView.backgroundColor = brownColor
        noSelectionBorderView.borderColor = brownColor
    }
    
    @IBAction func strictBudgtImgBtn(_ sender: Any) {
        strictImgView.image = UIImage(named: "GreyTick")
        openImgView.image = UIImage(named: "GreyBox")
        strictPriceTxtField.isHidden = false
        self.priceLabel.isHidden = false
        strictPriceView.isHidden = false
        priceTop.constant = CGFloat(87)
        yesSelectionView.backgroundColor = brownColor
        yesSelectionBorderView.borderColor = brownColor
        noSelectionView.backgroundColor = UIColor.lightGray
        noSelectionBorderView.borderColor = UIColor.lightGray
    }
    
    @IBAction func saveAction(_ sender: Any) {
        
        if UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil {
            Singleton.shared.postYourShipment.pickup_date = self.pickDate
            Singleton.shared.postYourShipment.dropoff_date = self.dropDate
            Singleton.shared.postYourShipment.price = self.strictPriceTxtField.text ?? "0"
            Singleton.shared.showToast(text: "Saved as draft.", color: greenColor)
            Singleton.shared.saveShipmentData(data: Singleton.shared.postYourShipment, screen: "3")
        } else {
            
            var draftId : Int?
            if Singleton.shared.postYourShipment.id == 0 {
                draftId = nil
            } else {
                draftId = UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.id : Singleton.shared.postYourShipment.id
            }
            Singleton.shared.postYourShipment.pickup_date = self.pickDate
            Singleton.shared.postYourShipment.dropoff_date = self.dropDate
            Singleton.shared.postYourShipment.price = self.strictPriceTxtField.text ?? "0"
            ActivityIndicator.show(view: self.view)
            var param : [String:Any] = [
                "draft_shipment_id": draftId,
                "from_state": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_state : Singleton.shared.postYourShipment.from_state,
                "to_state": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_state : Singleton.shared.postYourShipment.to_state,
                "from_name": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_name : Singleton.shared.postYourShipment.from_name,
                "to_name": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_name : Singleton.shared.postYourShipment.to_name,
                "from_longitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_longitude : Singleton.shared.postYourShipment.from_longitude,
                "from_latitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_latitude : Singleton.shared.postYourShipment.from_latitude,
                "to_longitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_longitude :Singleton.shared.postYourShipment.to_longitude,
                "to_latitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_latitude : Singleton.shared.postYourShipment.to_latitude,
                "from_city": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_city : Singleton.shared.postYourShipment.from_city,
                "to_city": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_city : Singleton.shared.postYourShipment.to_city,
                "from_zipcode": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_zipcode : Singleton.shared.postYourShipment.from_zipcode,
                "to_zipcode": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_zipcode : Singleton.shared.postYourShipment.to_zipcode,
                "description" : UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.discription : Singleton.shared.postYourShipment.discription,
                "category_id": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.category_id : Singleton.shared.postYourShipment.category_id,
                "lookingFor": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.lookingFor : Singleton.shared.postYourShipment.lookingFor,
                "sub_category_id": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.sub_category_id : Singleton.shared.postYourShipment.sub_category_id,
                "from_house_no" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_house_no : Singleton.shared.postYourShipment.from_house_no,
                "from_building" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_building : Singleton.shared.postYourShipment.from_building,
                "from_street" : UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_street : Singleton.shared.postYourShipment.from_street,
                  "to_house_no" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_house_no : Singleton.shared.postYourShipment.to_house_no,
                "to_building" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_building : Singleton.shared.postYourShipment.to_building,
                  "to_street" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_street : Singleton.shared.postYourShipment.to_street,
                "image" : UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.image : Singleton.shared.postYourShipment.image,
                "set_budget" : Singleton.shared.postYourShipment.set_budget,
                "pickup_date": Singleton.shared.postYourShipment.pickup_date,
                "dropoff_date":Singleton.shared.postYourShipment.dropoff_date
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SAVE_DRAFT_SHIPMENT, method: .post, parameter: param, objectClass: SaveDraftResponse.self, requestCode: U_SAVE_DRAFT_SHIPMENT, userToken: nil) { (response) in
                Singleton.shared.postYourShipment.id = response.response?.draft_shipment_id ?? 0
               Singleton.shared.showToast(text: "Saved as draft.", color: successGreen)
                ActivityIndicator.hide()
            }
        }
    }
    
    
    @IBAction func finishBtnPressed(_ sender: Any) {
        if pickUpTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter pickup date", color: errorRed)
        }else if dropOffTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter drop off date", color: errorRed)
        }else if openImgView.image == UIImage(named: "GreyTick"){
            let fcmToken = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) as? String
            if UserDefaults.standard.value(forKey: UD_TOKEN) as? String != nil {
                ActivityIndicator.show(view: self.view)
                var  param:[String:Any] = [
                    "category_id": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.category_id : Singleton.shared.postYourShipment.category_id,
                    "description": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.discription : Singleton.shared.postYourShipment.discription,
                    "from_name": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_name : Singleton.shared.postYourShipment.from_name,
                    "from_latitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_latitude : Singleton.shared.postYourShipment.from_latitude,
                    "from_longitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_longitude : Singleton.shared.postYourShipment.from_longitude,
                    "from_city": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_city : Singleton.shared.postYourShipment.from_city,
                    "from_state": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_state : Singleton.shared.postYourShipment.from_state,
                    "from_zipcode": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_zipcode : Singleton.shared.postYourShipment.from_zipcode,
                    "to_name": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_name : Singleton.shared.postYourShipment.to_name,
                    "to_latitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_latitude : Singleton.shared.postYourShipment.to_latitude,
                    "to_longitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_longitude : Singleton.shared.postYourShipment.to_longitude,
                    "to_city": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_city : Singleton.shared.postYourShipment.to_city,
                    "to_state": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_state : Singleton.shared.postYourShipment.to_state,
                    "to_zipcode": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_zipcode : Singleton.shared.postYourShipment.to_zipcode,
                    "pickup_date": self.pickDate,
                    "dropoff_date":self.dropDate,
                    "lookingFor": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.lookingFor : Singleton.shared.postYourShipment.lookingFor,
                    "sub_category_id": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.sub_category_id : Singleton.shared.postYourShipment.sub_category_id,
                    "from_house_no" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_house_no : Singleton.shared.postYourShipment.from_house_no,
                    "from_building" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_building : Singleton.shared.postYourShipment.from_building,
                    "from_street" : UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_street : Singleton.shared.postYourShipment.from_street,
                      "to_house_no" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_house_no : Singleton.shared.postYourShipment.to_house_no,
                    "to_building" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_building : Singleton.shared.postYourShipment.to_building,
                      "to_street" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_street : Singleton.shared.postYourShipment.to_street,
                    "image": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.image : Singleton.shared.postYourShipment.image ?? [],
                    "city_distance": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.distance : Singleton.shared.postYourShipment.distance,
                    "size_unit": 2,
                    "type": 1,
                    "platform": 1,
                    "set_budget" : 0,
                    "country_code": "+1",
                    "unit": 1,
                ]
                
                if(K_DRIVER_REHIRE_ID != 0 && K_DRIVER_REHIRE_ID != nil){
                param["driver_id"] = K_DRIVER_REHIRE_ID
                }
                
                
                if(K_DRAFT_OR_EDIT == 1 && K_IS_REPOST_CANCELLED == 0){
                    param["shipment_id"] = Singleton.shared.postYourShipment.id
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_EDIT_SHIPMENT, method: .post, parameter: param, objectClass: Response.self, requestCode: U_EDIT_SHIPMENT, userToken: nil) { (responseData) in
                        ActivityIndicator.hide()
                        let uRVC = self.storyboard!.instantiateViewController(withIdentifier: "UserTabViewController")
                        Singleton.shared.showToast(text: "Your shipment has been edited successfully", color: successGreen)
                        NotificationCenter.default.post(name: NSNotification.Name(N_RELOAD_DASHBOARD_DATA), object: nil)
                        self.navigationController?.pushViewController(uRVC, animated: true)
                    }
                    
                }else{
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_NEW_SHIPMENT, method: .post, parameter: param, objectClass: GetPostShipmentAndRegister.self, requestCode: U_ADD_NEW_SHIPMENT, userToken: nil) { (responseData) in
                        ActivityIndicator.hide()
                        Singleton.shared.showToast(text: "Your Shipment Posted Successfully!", color: successGreen)
                        K_DRIVER_REHIRE_ID = 0
                        K_IS_REPOST_CANCELLED = 0
                        let uRVC = self.storyboard!.instantiateViewController(withIdentifier: "UserTabViewController")
                        NotificationCenter.default.post(name: NSNotification.Name(N_RELOAD_DASHBOARD_DATA), object: nil)
                        self.navigationController?.pushViewController(uRVC, animated: true)
                    }
                }
            }
            else{
                Singleton.shared.showToast(text: "Please provide some more info", color: errorRed)
                let uRVC = self.storyboard!.instantiateViewController(withIdentifier: "ExistigUserLogInVC") as! ExistigUserLogInVC
                let todayDate = self.convertTimestampToDate(Int(Date().timeIntervalSince1970), to: "yyyy-MM-dd")
                Singleton.shared.draftData.pickup_date = self.convertDateToTimestamp(self.pickUpTxtField.text ?? todayDate, to: "yyyy-MM-dd")
                
                Singleton.shared.draftData.dropoff_date = self.convertDateToTimestamp(self.dropOffTxtField.text ?? todayDate, to: "yyyy-MM-dd")
                Singleton.shared.draftData.set_budget = 0
                Singleton.shared.draftData.price = "0"
                
                self.navigationController?.pushViewController(uRVC, animated: true)
            }
        }
        else if strictImgView.image == UIImage(named: "GreyTick"){
            if strictPriceTxtField.text!.isEmpty{
                Singleton.shared.showToast(text: "Please enter you Budget", color: errorRed)
            }
            else{
                let fcmToken = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) as? String
                if UserDefaults.standard.value(forKey: UD_TOKEN) as? String != nil {
                    var draftId : Int?
                    if Singleton.shared.postYourShipment.id == 0 {
                        draftId = nil
                    } else {
                        draftId = Singleton.shared.postYourShipment.id
                    }
                    ActivityIndicator.show(view: self.view)
                    var param:[String:Any] = [
                        "category_id": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.category_id : Singleton.shared.postYourShipment.category_id,
                        "description": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.discription : Singleton.shared.postYourShipment.discription,
                        "from_name": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_name : Singleton.shared.postYourShipment.from_name,
                        "from_latitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_latitude : Singleton.shared.postYourShipment.from_latitude,
                        "from_longitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_longitude : Singleton.shared.postYourShipment.from_longitude,
                        "from_city": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_city : Singleton.shared.postYourShipment.from_city,
                        "from_state": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_state : Singleton.shared.postYourShipment.from_state,
                        "from_zipcode": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_zipcode : Singleton.shared.postYourShipment.from_zipcode,
                        "to_name": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_name : Singleton.shared.postYourShipment.to_name,
                        "to_latitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_latitude : Singleton.shared.postYourShipment.to_latitude,
                        "to_longitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_longitude : Singleton.shared.postYourShipment.to_longitude,
                        "to_city": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_city : Singleton.shared.postYourShipment.to_city,
                        "to_state": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_state : Singleton.shared.postYourShipment.to_state,
                        "to_zipcode": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_zipcode : Singleton.shared.postYourShipment.to_zipcode,
                        "pickup_date": self.pickDate,
                        "dropoff_date":self.dropDate,
                        "lookingFor": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.lookingFor : Singleton.shared.postYourShipment.lookingFor,
                        "sub_category_id": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.sub_category_id : Singleton.shared.postYourShipment.sub_category_id,
                        "from_house_no" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_house_no : Singleton.shared.postYourShipment.from_house_no,
                        "from_building" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_building : Singleton.shared.postYourShipment.from_building,
                        "from_street" : UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_street : Singleton.shared.postYourShipment.from_street,
                          "to_house_no" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_house_no : Singleton.shared.postYourShipment.to_house_no,
                        "to_building" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_building : Singleton.shared.postYourShipment.to_building,
                          "to_street" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_street : Singleton.shared.postYourShipment.to_street,
                        "image": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? (Singleton.shared.draftData.image ?? []) : (Singleton.shared.postYourShipment.image ?? []),
                        "city_distance": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.distance : Singleton.shared.postYourShipment.distance,
                        "size_unit": 2,
                        "type": 1,
                        "platform": 1,
                        "set_budget" : 1,
                        "country_code": "+1",
                        "unit": 1,
                        "price":self.strictPriceTxtField.text,
                    ]
                    
                    if(K_DRIVER_REHIRE_ID != 0 && K_DRIVER_REHIRE_ID != nil){
                    param["driver_id"] = K_DRIVER_REHIRE_ID
                    }
                    
                    if(K_DRAFT_OR_EDIT == 1 && K_IS_REPOST_CANCELLED == 0){
                        param["shipment_id"] = UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.id : Singleton.shared.postYourShipment.id
                        SessionManager.shared.methodForApiCalling(url: U_BASE + U_EDIT_SHIPMENT, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ADD_NEW_SHIPMENT, userToken: nil) { (responseData) in
                            ActivityIndicator.hide()
                            Singleton.shared.showToast(text: "Your Shipment Posted Successfully!", color: successGreen)
                            let uRVC = self.storyboard!.instantiateViewController(withIdentifier: "UserTabViewController")
                            Singleton.shared.showToast(text: "Your shipment has been edited successfully", color: successGreen)
                            NotificationCenter.default.post(name: NSNotification.Name(N_RELOAD_DASHBOARD_DATA), object: nil)
                            self.navigationController?.pushViewController(uRVC, animated: true)
                        }
                    }else{
                        SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_NEW_SHIPMENT, method: .post, parameter: param, objectClass: GetPostShipmentAndRegister.self, requestCode: U_ADD_NEW_SHIPMENT, userToken: nil) { (responseData) in
                            ActivityIndicator.hide()
                            K_DRIVER_REHIRE_ID = 0
                            K_IS_REPOST_CANCELLED = 0
                            Singleton.shared.showToast(text: "Your Shipment Posted Successfully!", color: successGreen)
                            let uRVC = self.storyboard!.instantiateViewController(withIdentifier: "UserTabViewController")
                            NotificationCenter.default.post(name: NSNotification.Name(N_RELOAD_DASHBOARD_DATA), object: nil)
                            self.navigationController?.pushViewController(uRVC, animated: true)
                        }
                    }
                }
                else{
                    Singleton.shared.showToast(text: "Please provide some more info", color: errorRed)
                    let uRVC = self.storyboard!.instantiateViewController(withIdentifier: "ExistigUserLogInVC") as! ExistigUserLogInVC
                    let todayDate = self.convertTimestampToDate(Int(Date().timeIntervalSince1970), to: "yyyy-MM-dd")
                    Singleton.shared.draftData.pickup_date = self.convertDateToTimestamp(self.pickUpTxtField.text ?? todayDate, to: "yyyy-MM-dd")
                    Singleton.shared.draftData.dropoff_date = self.convertDateToTimestamp(self.dropOffTxtField.text ?? todayDate, to: "yyyy-MM-dd")
                    Singleton.shared.draftData.set_budget = 1
                    Singleton.shared.draftData.price = self.strictPriceTxtField.text ?? "0"
                    self.navigationController?.pushViewController(uRVC, animated: true)
                }
            }
        }
    }
}
