//
//  UserPhotosUploadVC.swift
//  SYM
//
//  Created by AM on 22/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SDWebImage
import ImageSlideshow

class UserPhotosUploadVC: UIViewController {
    //Mark: IBOutlets
    @IBOutlet weak var imageTable: ContentSizedTableView!
    @IBOutlet weak var saveButton: CustomButton!
    @IBOutlet weak var popupView: UIStackView!
    @IBOutlet weak var imageFullHideShow: UIView!
    @IBOutlet weak var fullSingleImage: ImageView!
    
    var imageName = ""
    var imagePath = [String]()
    let picker = UIImagePickerController()
    var isEditJob = false
    var removeImageIndex = Int()
//    var draftData = GetPostYourShipmentData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavTitle(title: "Post you Shipment")
        if UserDefaults.standard.value(forKey: UD_TOKEN) as? String != nil {
            self.saveButton.isHidden = K_DRAFT_OR_EDIT == 1 ? true:false
        }
        setNavigationBar("back", "none")
        //  if(isEditJob){
         self.initializeView()
        //  }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // handleMapView()
      //  self.initializeView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       // self.saveAction(self)
    }
    
    func initializeView(){
        
        if (Singleton.shared.postYourShipment.image?.count ?? 0) > 0 {
           // self.imagePath = []
            self.imagePath = Singleton.shared.postYourShipment.image!
        }
        imageTable.reloadData()
    }
    
    func uploadImage(){
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
        }
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK:IBActions
    @IBAction func letsWrapBtnPressed(_ sender: Any) {
        if UserDefaults.standard.value(forKey: UD_TOKEN) as? String != nil {
            Singleton.shared.postYourShipment.image = self.imagePath
        } else {
            Singleton.shared.draftData.image = self.imagePath
            
            Singleton.shared.postYourShipment.image = self.imagePath
        }
        let uRVC = storyboard!.instantiateViewController(withIdentifier: "UserFinishingUpVC") as! UserFinishingUpVC
        uRVC.isEditJob = self.isEditJob
//        uRVC.draftData = self.draftData
       //
       
        // self.navigationController?.pushViewController(uRVC, animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_CURRENT_INDEX), object: nil, userInfo: ["index":3])
    }
    
    @IBAction func cancelImageAction(_ sender: Any) {
        self.imageFullHideShow.isHidden = true
    }
    
    
    @IBAction func addImageAction(_ sender: Any) {
        self.picker.delegate = self
        self.uploadImage()
    }
    
    
    @IBAction func okayPopupAction(_ sender: UIButton) {
        self.popupView.isHidden = true
        
        if sender.tag == 1 {
            Singleton.shared.postYourShipment.image?.remove(at: self.removeImageIndex)
            self.imagePath.remove(at: self.removeImageIndex)
            self.imageTable.reloadData()
        }
    }
    
    
    
    @IBAction func saveAction(_ sender: Any) {
        if UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil {
            Singleton.shared.postYourShipment.image = self.imagePath
            Singleton.shared.showToast(text: "Saved as draft.", color: successGreen)
            Singleton.shared.saveShipmentData(data: Singleton.shared.postYourShipment, screen: "2")
        } else {
            
            var draftId : Int?
            if Singleton.shared.postYourShipment.id == 0 {
                draftId = nil
            } else {
                
                draftId = UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.id : Singleton.shared.postYourShipment.id
            }
            Singleton.shared.postYourShipment.image = self.imagePath
            
            ActivityIndicator.show(view: self.view)
            var param : [String:Any] = [
                "draft_shipment_id": draftId,
                "from_state": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_state : Singleton.shared.postYourShipment.from_state,
                "to_state": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_state : Singleton.shared.postYourShipment.to_state,
                "from_name": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_name : Singleton.shared.postYourShipment.from_name,
                "to_name": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_name : Singleton.shared.postYourShipment.to_name,
                "from_longitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_longitude : Singleton.shared.postYourShipment.from_longitude,
                "from_latitude": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_latitude : Singleton.shared.postYourShipment.from_latitude,
                "to_longitude":  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_longitude : Singleton.shared.postYourShipment.to_longitude,
                "to_latitude":  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_latitude : Singleton.shared.postYourShipment.to_latitude,
                "from_city":  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_city : Singleton.shared.postYourShipment.from_city,
                "to_city":  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_city : Singleton.shared.postYourShipment.to_city,
                "from_zipcode":  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_zipcode : Singleton.shared.postYourShipment.from_zipcode,
                "to_zipcode": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_zipcode : Singleton.shared.postYourShipment.to_zipcode,
                "description" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.discription : Singleton.shared.postYourShipment.discription,
                "category_id": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.category_id : Singleton.shared.postYourShipment.category_id,
                "lookingFor": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.lookingFor : Singleton.shared.postYourShipment.lookingFor,
                "sub_category_id": UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.sub_category_id : Singleton.shared.postYourShipment.sub_category_id,
                "from_house_no" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_house_no : Singleton.shared.postYourShipment.from_house_no,
                "from_building" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_building : Singleton.shared.postYourShipment.from_building,
                "from_street" : UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.from_street : Singleton.shared.postYourShipment.from_street,
                  "to_house_no" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_house_no : Singleton.shared.postYourShipment.to_house_no,
                "to_building" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_building : Singleton.shared.postYourShipment.to_building,
                  "to_street" :  UserDefaults.standard.value(forKey: UD_TOKEN) as? String == nil ? Singleton.shared.draftData.to_street : Singleton.shared.postYourShipment.to_street,
                "image" : self.imagePath
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SAVE_DRAFT_SHIPMENT, method: .post, parameter: param, objectClass: SaveDraftResponse.self, requestCode: U_SAVE_DRAFT_SHIPMENT, userToken: nil) { (response) in
                Singleton.shared.postYourShipment.id = response.response?.draft_shipment_id ?? 0
                Singleton.shared.showToast(text: "Saved as draft.", color: successGreen)
                ActivityIndicator.hide()
            }
        }
    }
}

extension UserPhotosUploadVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.imagePath.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShipCategoriesCell") as! ShipCategoriesCell
        cell.cellImage.sd_setImage(with: URL(string:U_IMAGE + (self.imagePath[indexPath.row] ?? "")), placeholderImage: nil)
        cell.deleteButton = {
//            Singleton.shared.postYourShipment.image?.remove(at: indexPath.row)
//            self.imagePath.remove(at: indexPath.row)
//            self.imageTable.reloadData()
            self.removeImageIndex = indexPath.row
            self.popupView.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let imageSlider = ImageSlideshow()
        var input = [SDWebImageSource]()
        let clickedImage = U_IMAGE + self.imagePath[indexPath.item]
        input.append(SDWebImageSource(urlString: clickedImage)!)
        for val in self.imagePath {
            if (U_IMAGE + val) != clickedImage {
            input.append(SDWebImageSource(urlString: U_IMAGE + val)!)
            }
        }
        imageSlider.setImageInputs(input)
        let fullScreenController = imageSlider.presentFullScreenController(from: self)
        
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    
}

extension UserPhotosUploadVC: UIImagePickerControllerDelegate ,UINavigationControllerDelegate  {
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        let imageData:NSData = selectedImage.pngData()! as NSData
        
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if let cropImage = selectedImage as? UIImage {
            let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
            let params = [
                "file": data!
            ] as [String : Any]
            
            // HTTP Request for upload Picture
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.makeMultipartRequest(url: U_BASE + U_UPLOAD_IMAGE, fileData: data!, param: params, objectClass: UploadImage.self, fileName: imageName) { (path) in
                var url = (path as! UploadImage).response.path ?? ""
                self.imagePath.append(url) //(response as! UploadImage).response?.url ?? ""
                self.imageTable.reloadData()
                ActivityIndicator.hide()
            }
        }
        picker.dismiss(animated: true)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    func presentCameraSettings() {
        let alertController = UIAlertController(title: "Allow Camera to take photos",
                                                message: "Camera access is denied",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default){
            _ in
        })
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })
        self.present(alertController, animated: true)
    }
    
}
