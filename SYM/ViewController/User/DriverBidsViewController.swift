//
//  DriverBidsViewController.swift
//  SYM
//
//  Created by qw on 10/09/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
import iOSDropDown

class DriverBidsViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var filterBy: DropDown!
    @IBOutlet weak var sortby: DropDown!
    @IBOutlet weak var tableView: ContentSizedTableView!
    @IBOutlet weak var alertBoxLabel: DesignableUILabel!
    @IBOutlet weak var alertBoxStackView: UIStackView!
    @IBOutlet weak var disputeAlertStackView: UIStackView!
    @IBOutlet weak var reasonTextView: UITextView!
    @IBOutlet weak var filterView: View!
    @IBOutlet weak var sortView: View!
    
    
    var shipmentData = GetUserShipmentData()
    var bidData = [AppliedBidDetail]()
    var shipmentStatus = Int()
    var selectedSortby = Int()
    var selectedFilter = Int()
    var selectedShipmentId = Int()
    var selectedDriverId = Int()
    var alertRedirection = Int()
    var notificationRedirection = false
    var awardedDriverId = Int()
    var isShipmentAwarded = false
    
    static var editShipmentStatus : EditShipmentStatus? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reasonTextView.delegate = self
        self.filterBy.optionArray = ["All Rating", "1 Rating", "2 Rating", "3 Rating", "4 Rating", "5 Rating"]
        self.sortby.optionArray = ["Relevance", "Low to High", "High to Low"]
        self.filterBy.didSelect { (val, index, id) in
            
            self.selectedFilter = index
            self.driverBidSearch()
        }
        
        self.sortby.didSelect { (val, index, id) in
            self.selectedSortby = index
            self.driverBidSearch()
        }
        
        if Singleton.shared.userType.role == 1 {
            self.filterView.isHidden = false
            self.sortView.isHidden = false
        } else {
            self.filterView.isHidden = true
            self.sortView.isHidden = true
        }
        
        if notificationRedirection == false {
            if Singleton.shared.userType.role == 1 {
                self.driverBidSearch()
            }
        } else {
            self.getSingleShipments()
        }
        
        self.setNavTitle(title: "Driver Bids")
        self.setNavigationBar("back", nil)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.estimatedRowHeight = 250
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func driverBidSearch(){
        let param = [
            "shipment_id":self.shipmentData.id,
            "price_sort" : self.selectedSortby,
            "rating" : self.selectedFilter
        ] as? [String:Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_CUSTOMER_SEARCH_BID + "", method: .post, parameter: param, objectClass: GetBidSearchDetail.self, requestCode: U_CUSTOMER_SEARCH_BID, userToken: nil) { (response) in
            self.bidData = response.response ?? []
            
            
            for i in self.bidData{
                if i.driver_accepted == 1 || i.customer_accepted == 1 {
                    if(i.driver_accepted != 2 && i.customer_accepted != 2){
                    self.awardedDriverId = i.driver_id ?? 0
                    self.isShipmentAwarded = true
                    }
                }
            }
            
            self.tableView.reloadData()
        }
    }
    
    func getSingleShipments(){
        ActivityIndicator.show(view: self.view)
        let param:[String:Any] = ["shipment_id":self.shipmentData.id]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_CUSTOMER_SINGLE_SHIPMENT, method: .post, parameter: param, objectClass: CustomerSingleShipment.self, requestCode: U_CUSTOMER_SINGLE_SHIPMENT, userToken: nil) { (response) in
            self.shipmentData = response.response
            if(self.shipmentData.lookingFor == 1){
                self.shipmentStatus = self.shipmentData.driver_status ?? 0
            }else if(self.shipmentData.lookingFor == 2){
                self.shipmentStatus = self.shipmentData.loader_status ?? 0
            }else if(self.shipmentData.lookingFor == 3){
                self.shipmentStatus = self.shipmentData.driver_status ?? 0
            }else{
                self.shipmentStatus = self.shipmentData.driver_status ?? 0
            }
            self.driverBidSearch()
        }
    }
    
    @IBAction func okayAction(_ sender: Any) {
        self.alertBoxStackView.isHidden = true
        if self.alertRedirection == 1 {
            var statusKey = Int()
            
            if self.shipmentData.lookingFor == 1 {
                statusKey = 1
            } else if self.shipmentData.lookingFor == 2 {
                statusKey = 2
            } else if self.shipmentData.lookingFor == 3 {
                statusKey = 3
            }
            
            if Singleton.shared.savedCardData.count > 0 {
                ActivityIndicator.show(view: self.view)
                let param:[String:Any] = [
                    "shipment_id": self.selectedShipmentId, //val?.shipment_id
                    "driver_id": self.selectedDriverId, //val?.driver?.id
                    "user_response": 1,
                    "status_key":"\(statusKey)"
                ]
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_ACCEPT_REJECT_BID_CUSTOMER, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ACCEPT_REJECT_BID_CUSTOMER, userToken: nil) { (response) in
                    ActivityIndicator.hide()
                    Singleton.shared.showToast(text: "Bid awarded successfully", color: successGreen)
                    DriverBidsViewController.editShipmentStatus?.changeShipmentData(status: 3)
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        } else if alertRedirection == 2 {
            self.disputeAlertStackView.isHidden = false
        } else if alertRedirection == 3 {
            if(self.shipmentStatus == 5){
                let dropDate = Date(timeIntervalSince1970: TimeInterval((self.shipmentData.dropoff_date ?? 0)))
//                if(self.shipmentStatus == 5 && (Int(dropDate.onlyDate!.timeIntervalSince1970) <= Int(Date().onlyDate!.timeIntervalSince1970))){
                    ActivityIndicator.show(view: self.view)
                    var param = [
                        "shipment_id": self.selectedShipmentId,
                        "driver_id":self.selectedDriverId
                    ]
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_CUSTOMER_COMPLETE_SHIPMENT , method: .post, parameter: param, objectClass: Response.self, requestCode: U_CUSTOMER_COMPLETE_SHIPMENT, userToken: nil) { (response) in
                        ActivityIndicator.hide()
                        Singleton.shared.showToast(text: "Shipment completed Successfully", color: successGreen)
                        self.shipmentStatus = 6
                        self.tableView.reloadData()
                    }
//                } else {
//                    Singleton.shared.showToast(text: "Sorry you can't mark shipment as complete before the completion date.")
//                }
            }
        } else if alertRedirection == 4{
            var statusKey = Int()
            
            if self.shipmentData.lookingFor == 1 {
                statusKey = 1
            } else if self.shipmentData.lookingFor == 2 {
                statusKey = 2
            } else if self.shipmentData.lookingFor == 3 {
                statusKey = 3
            }
            
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "shipment_id": self.selectedShipmentId, //val?.shipment_id
                "driver_id": self.selectedDriverId, //val?.driver?.id
                "user_response": 2,
                "status_key":"\(statusKey)"
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ACCEPT_REJECT_BID_CUSTOMER, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ACCEPT_REJECT_BID_CUSTOMER, userToken: nil) { (response) in
                ActivityIndicator.hide()
                Singleton.shared.showToast(text: "Bid rejected successfully", color: successGreen)
                self.driverBidSearch()
            }
        }
    }
    @IBAction func disputeAction(_ sender: Any) {
        if self.reasonTextView.text == "Your reason" || self.reasonTextView.text == "" {
            Singleton.shared.showToast(text: "Please mention your reason", color: errorRed)
        } else {
            self.disputeAlertStackView.isHidden = true
            
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "shipment_id": self.selectedShipmentId, //val?.shipment_id
                "driver_id": self.selectedDriverId, //val?.driver?.id
                "reason": self.reasonTextView.text
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_CUSTOMER_DISPUTE, method: .post, parameter: param, objectClass: Response.self, requestCode: U_CUSTOMER_DISPUTE, userToken: nil) { (response) in
                ActivityIndicator.hide()
                DriverBidsViewController.editShipmentStatus?.changeShipmentData(status: 7)
                self.shipmentStatus = 7
                Singleton.shared.showToast(text: "Dispute report done", color: successGreen)
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func disputeCancelAction(_ sender: Any) {
        self.disputeAlertStackView.isHidden = true
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.alertBoxStackView.isHidden = true
    }
    
}

extension DriverBidsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return self.shipmentData.driver_bids?.count ?? 0
        return bidData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserSingleShipmentCell", for: indexPath) as! UserSingleShipmentCell
        
        var val = self.bidData[indexPath.row]
        
        cell.rateNowText.isHidden = true
        cell.rateNow.isHidden = true
        
        
        
        if val.sponsored == 1 {
            cell.featuredLabel.isHidden = false
        } else {
            cell.featuredLabel.isHidden = true
        }
        
        if(self.isShipmentAwarded == true){
            if(self.awardedDriverId == val.driver_id) {
                cell.markedDisputeButtonView.setTitle("  Marked as Dispute  ", for: .normal)
                cell.disputeButton.isHidden = true
                
                if(shipmentStatus == 2){
                    cell.awardBtn.setTitle("Want to award this Bid?", for: .normal)
                    cell.markedDisputeButtonView.isHidden = true
                    cell.markedDisputeHeight.constant = 0
                    cell.disputeButton.isHidden = true
                    cell.chatBtn.isHidden = true
                    cell.rejectButtonView.isHidden = false
                }else if(shipmentStatus == 3){
                    cell.rejectButtonView.isHidden = true
                    cell.markedDisputeButtonView.isHidden = true
                    cell.markedDisputeHeight.constant = 0
                    cell.chatBtn.isHidden = true
                    if(val.driver_accepted == 2){
                        cell.awardBtn.setTitle("Driver Rejected Bid", for: .normal)
                        cell.awardBtn.backgroundColor = .lightGray
                    }else if(val.driver_accepted == 0 && val.customer_accepted == 0){
                        cell.awardBtn.setTitle("Want to award this Bid?", for: .normal)
                    }else if(val.driver_accepted == 0  && val.customer_accepted == 1){
                        cell.awardBtn.setTitle("Awaiting for service provider's acceptance", for: .normal)
                    }else if(val.driver_accepted == 1){
                        cell.awardBtn.setTitle("Awaiting to start", for: .normal)
                        cell.chatBtn.isHidden = false
                        cell.disputeButton.isHidden = false
                    }
                }else if(shipmentStatus == 4){
                    cell.rejectButtonView.isHidden = true
                    if(val.driver_accepted == 2){
                        cell.awardBtn.setTitle("Driver Rejected Bid", for: .normal)
                        cell.awardBtn.backgroundColor = .lightGray
                        cell.chatBtn.isHidden = true
                    } else {
                        cell.disputeButton.isHidden = false
                        cell.chatBtn.isHidden = false
                        cell.awardBtn.setTitle("In progress", for: .normal)
                        cell.markedDisputeButtonView.isHidden = true
                        cell.markedDisputeHeight.constant = 0
                    }
                }else if(shipmentStatus == 5){
                    cell.rejectButtonView.isHidden = true
                    if(val.driver_accepted == 2){
                        cell.awardBtn.setTitle("Driver Rejected Bid", for: .normal)
                        cell.awardBtn.backgroundColor = .lightGray
                        cell.chatBtn.isHidden = true
                    } else {
                        cell.disputeButton.isHidden = false
                        cell.awardBtn.setTitle("Mark as Complete", for: .normal)
                        cell.markedDisputeButtonView.isHidden = true
                        cell.markedDisputeHeight.constant = 0
                        cell.chatBtn.isHidden = false
                    }
                }else if(shipmentStatus == 6){
                    cell.rejectButtonView.isHidden = true
                    if(val.driver_accepted == 2){
                        cell.awardBtn.setTitle("Driver Rejected Bid", for: .normal)
                        cell.awardBtn.backgroundColor = .lightGray
                        cell.chatBtn.isHidden = true
                    } else {
                        cell.disputeButton.isHidden = false
                        cell.markedDisputeButtonView.isHidden = true
                        cell.markedDisputeHeight.constant = 0
                        //  cell.awardBtn.isHidden = true
                        cell.chatBtn.isHidden = false
                        cell.awardBtn.setTitle("Shipment completed", for: .normal)
                        cell.rateNow.isHidden = false
                        cell.rateNowText.isHidden = false
                        cell.awardBtn.isUserInteractionEnabled = false
                    }
                } else if (shipmentStatus == 7) {
                    cell.rejectButtonView.isHidden = true
                    if(val.driver_accepted == 2){
                        cell.awardBtn.setTitle("Driver Rejected Bid", for: .normal)
                        cell.awardBtn.backgroundColor = .lightGray
                    } else {
                        cell.disputeButton.isHidden = true
                        cell.awardBtn.isHidden = true
                        cell.chatBtn.isHidden = true
                        cell.markedDisputeButtonView.isHidden = false
                        cell.markedDisputeHeight.constant = 30
                    }
                } else if (self.shipmentStatus == 1){
                    if(val.driver_accepted == 2){
                        cell.rejectButtonView.isHidden = true
                        cell.awardBtn.setTitle("Driver Rejected", for: .normal)
                        cell.awardBtn.backgroundColor = .lightGray
                        cell.markedDisputeButtonView.isHidden = true
                        cell.disputeButton.isHidden = true
                        cell.markedDisputeHeight.constant = 0
                        cell.chatBtn.isHidden = true
                    }  else if(val.customer_accepted == 2){
                        cell.markedDisputeButtonView.setTitle("Rejected", for: .normal)
                        cell.markedDisputeButtonView.isHidden = false
                        cell.markedDisputeHeight.constant = 30
                        cell.rejectButtonView.isHidden = true
                        cell.awardBtn.isHidden = true
                        cell.chatBtn.isHidden = true
                    } else {
                        cell.rejectButtonView.isHidden = false
                        cell.awardBtn.setTitle("Want to award this Bid?", for: .normal)
                        cell.markedDisputeButtonView.isHidden = true
                        cell.markedDisputeHeight.constant = 0
                        cell.disputeButton.isHidden = true
                        cell.awardBtn.isHidden = false
                        cell.chatBtn.isHidden = false
                        cell.rejectButtonView.isHidden = false
                    }
                }
                
            } else {
                cell.awardBtn.isHidden = true
                cell.markedDisputeButtonView.isHidden = true
                cell.markedDisputeHeight.constant = 0
                cell.chatBtn.isHidden = true
                cell.disputeButton.isHidden = true
                cell.rejectButtonView.isHidden = true
             //implement on android
                if(self.bidData[indexPath.row].customer_accepted == 2){
                    cell.markedDisputeButtonView.setTitle("Rejected", for: .normal)
                    cell.markedDisputeButtonView.isHidden = false
                    cell.markedDisputeHeight.constant = 30
                }
            }
        } else {
            if(self.shipmentStatus == 2){
                cell.rejectButtonView.isHidden = false
                cell.awardBtn.setTitle("Want to award this Bid?", for: .normal)
                cell.markedDisputeButtonView.isHidden = true
                cell.markedDisputeHeight.constant = 0
                cell.disputeButton.isHidden = true
                
                 if(val.customer_accepted == 2) {
                    cell.markedDisputeButtonView.setTitle("Rejected", for: .normal)
                    cell.markedDisputeButtonView.isHidden = false
                    cell.markedDisputeHeight.constant = 30
                    cell.rejectButtonView.isHidden = true
                     cell.awardBtn.isHidden = true
                     cell.chatBtn.isHidden = true
                } else {
                    cell.markedDisputeButtonView.setTitle("Marked as dispute", for: .normal)
                }
            } else if(shipmentStatus == 3 ){
                cell.rejectButtonView.isHidden = true
                cell.markedDisputeButtonView.isHidden = true
                cell.markedDisputeHeight.constant = 0
                cell.chatBtn.isHidden = true
                if(val.driver_accepted == 2){
                    cell.awardBtn.setTitle("Driver Rejected Bid", for: .normal)
                    cell.awardBtn.backgroundColor = .lightGray
                }else if(val.driver_accepted == 0 && val.customer_accepted == 0){
                    cell.awardBtn.setTitle("Want to award this Bid?", for: .normal)
                }else if(val.driver_accepted == 0  && val.customer_accepted == 1){
                    cell.awardBtn.setTitle("Awaiting for service provider's acceptance", for: .normal)
                }else if(val.driver_accepted == 1){
                    cell.awardBtn.setTitle("Awaiting to start", for: .normal)
                    cell.chatBtn.isHidden = false
                    cell.disputeButton.isHidden = false
                }
                
            } else if (self.shipmentStatus == 1){
                cell.rejectButtonView.isHidden = true
                if(val.driver_accepted == 2){
                    cell.awardBtn.setTitle("Driver Rejected", for: .normal)
                    cell.awardBtn.backgroundColor = .lightGray
                    cell.markedDisputeButtonView.isHidden = true
                    cell.disputeButton.isHidden = true
                    cell.markedDisputeHeight.constant = 0
                    cell.chatBtn.isHidden = true
                }   else if(val.customer_accepted == 2){
                    cell.markedDisputeButtonView.setTitle("Rejected", for: .normal)
                    cell.markedDisputeButtonView.isHidden = false
                    cell.markedDisputeHeight.constant = 30
                    cell.rejectButtonView.isHidden = true
                    cell.awardBtn.isHidden = true
                    cell.chatBtn.isHidden = true
                } else {
                    cell.rejectButtonView.isHidden = false
                    cell.awardBtn.setTitle("Want to award this Bid?", for: .normal)
                    cell.markedDisputeButtonView.isHidden = true
                    cell.markedDisputeHeight.constant = 0
                    cell.disputeButton.isHidden = true
                    cell.awardBtn.isHidden = false
                    cell.chatBtn.isHidden = false
                }
            }
        }
        
        if(self.shipmentStatus == 8){
            cell.awardBtn.isHidden = true
            cell.markedDisputeButtonView.isHidden = true
            cell.markedDisputeHeight.constant = 0
            cell.chatBtn.isHidden = true
            cell.disputeButton.isHidden = true
            cell.rejectButtonView.isHidden = true
        }
        
        cell.userImage.sd_setImage(with: URL(string:U_IMAGE + (val.driver?.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        cell.username.text = (val.driver?.first_name ?? "") + " " + (val.driver?.last_name ?? "")
        cell.bidValue.text = "$\(val.amount ?? "")"
        cell.userDescription.text = val.message
        if(val.customer_accepted == 1){
            //   self.awardedTo.text = (val?.driver?.first_name ?? "") + " " + (val?.driver?.last_name ?? "")
        }
        if(val.placingAs == 1){
            cell.userOccupation.text = "Driver"
        }else if(val.placingAs == 2){
            cell.userOccupation.text = "Loader"
        }else if(val.placingAs == 3){
            cell.userOccupation.text = "Driver & Loader"
        }
        
        cell.totalShipment.text = "\(val.total_complete_shipment ?? 0)"
        
        cell.disputeAction = {
            self.selectedDriverId = val.driver_id ?? 0
            self.selectedShipmentId = val.shipment_id ?? 0
            self.alertRedirection = 2
            self.alertBoxLabel.text = "Are you sure to raise a dispute?"
            self.alertBoxStackView.isHidden = false
        }
        
        cell.shipmentButton = {
            if(cell.awardBtn.titleLabel?.text == "Awaiting for Acceptance"){
                
            }else if(cell.awardBtn.titleLabel?.text == "Driver Rejected Bid"){
                
            }else if(cell.awardBtn.titleLabel?.text == "Want to award this Bid?"){
                if(Singleton.shared.savedCardData.count > 0){
                self.selectedShipmentId = val.shipment_id ?? 0
                self.selectedDriverId = val.driver?.id ?? 0
                var amount = (val.driver?.last_name ?? "") + " with amount $" + String(val.amount ?? "")
                self.alertBoxLabel.text = "Do you want to accept the bid of " + (val.driver?.first_name ?? "") + " " +  amount
                self.alertBoxStackView.isHidden = false
                self.alertRedirection = 1
                } else {
                    Singleton.shared.showToast(text: "Please add a card first.", color: errorRed)
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "PaymentMethodVC") as! PaymentMethodVC
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            } else if (cell.awardBtn.titleLabel?.text == "Mark as Complete"){
                self.selectedDriverId = val.driver_id ?? 0
                self.selectedShipmentId = val.shipment_id ?? 0
                self.alertRedirection = 3
                self.alertBoxLabel.text = "Mark this shipment as completed?"
                self.alertBoxStackView.isHidden = false
            }
            
        }
        
        cell.rejectButton = {
                self.selectedShipmentId = val.shipment_id ?? 0
                self.selectedDriverId = val.driver?.id ?? 0
                self.alertBoxLabel.text = "Do you want to reject the bid ?"
                self.alertBoxStackView.isHidden = false
                self.alertRedirection = 4
        }

        
        cell.chatButton = {
            if(cell.awardBtn.titleLabel?.text != "Driver Rejected Bid"){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                myVC.jobDetail = ChatDetail(sender_id: self.shipmentData.user_id ?? 0, receiver_id: val.driver?.id ?? 0, receiver_name: (val.driver?.first_name ?? "") + " " + (val.driver?.last_name ?? ""), sender_name: (Singleton.shared.userType.first_name ?? "") + " " +  (Singleton.shared.userType.last_name ?? ""), sender_image: (Singleton.shared.userType.profile_image ?? ""), receiver_image: val.driver?.profile_image, shipment_id: val.shipment_id ?? 0)
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
        
        cell.reviewButton = {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RateAndReviewVC") as! RateAndReviewVC
            self.navigationController?.present(myVC, animated: true)
        }
        
        cell.rateNowButton = {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RatingViewController") as! RatingViewController
            myVC.shipmentId = val.shipment_id ?? 0
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        
        return cell
    }
}

extension DriverBidsViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if self.reasonTextView.text == "Your reason" {
            self.reasonTextView.text = ""
        }
    }
}


class UserSingleShipmentCell: UITableViewCell{
    //MARK:IBOutlets
    @IBOutlet weak var totalBid: DesignableUILabel!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var username: DesignableUILabel!
    @IBOutlet weak var bidValue: DesignableUILabel!
    @IBOutlet weak var totalShipment: DesignableUILabel!
    @IBOutlet weak var userOccupation: DesignableUILabel!
    @IBOutlet weak var userDescription: DesignableUILabel!
    @IBOutlet weak var awardBtn: CustomButton!
    @IBOutlet weak var chatBtn: CustomButton!
    @IBOutlet weak var disputeButton: UIButton!
    @IBOutlet weak var markedDisputeButtonView: CustomButton!
    @IBOutlet weak var featuredLabel: UILabel!
    @IBOutlet weak var rateNow: UIButton!
    @IBOutlet weak var markedDisputeHeight: NSLayoutConstraint!
    @IBOutlet weak var rateNowText: UILabel!
    @IBOutlet weak var rejectButtonView: UIButton!
    
    var shipmentButton: (()-> Void)? = nil
    var chatButton: (() -> Void)? = nil
    var reviewButton: (() -> Void)? = nil
    var disputeAction: (() -> Void)? = nil
    var rateNowButton: (() -> Void)? = nil
    var rejectButton: (() -> Void)? = nil
    
    //MARK: IBActions
    @IBAction func awardBidAction(_ sender: Any) {
        if let shipmentButton = self.shipmentButton {
            shipmentButton()
        }
    }
    
    @IBAction func disputeButtonAction(_ sender: Any) {
        if let disputeAction = self.disputeAction {
            disputeAction()
        }
    }
    
    @IBAction func chatAction(_ sender: Any) {
        if let chatButton = self.chatButton {
            chatButton()
        }
    }
    
    @IBAction func reviewAction(_ sender: Any) {
        if let reviewButton = self.reviewButton {
            reviewButton()
        }
    }
    
    @IBAction func rateAction(_ sender: Any) {
        if let rateNowButton = self.rateNowButton {
            rateNowButton()
        }
    }
    
    @IBAction func rejectAction(_ sender: Any) {
        if let rejectButton = self.rejectButton {
            rejectButton()
        }
    }
    
    
}
