//
//  EntryViewController.swift
//  SYM
//
//  Created by AM on 22/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class EntryViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //APICallingViewController.shared.getCategory()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    @IBAction func postShipmentBtPressed(_ sender: Any) {
        let bIVC = storyboard!.instantiateViewController(withIdentifier: "PageViewController")
        if Singleton.shared.postYourShipment.id != nil {
            K_DRAFT_OR_EDIT = 2
        } else {
            K_DRAFT_OR_EDIT = 1
        }
        K_DRIVER_REHIRE_ID = 0
        self.navigationController?.pushViewController(bIVC, animated: true)
    }
    
    @IBAction func loginBtnPressed(_ sender: Any) {
        
        let sIVC = storyboard!.instantiateViewController(withIdentifier: "SignInVC")
        self.navigationController?.pushViewController(sIVC, animated: true)
    }
    
    @IBAction func aboutUsAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        self.present(myVC, animated: true, completion: nil)
    }
    
}
