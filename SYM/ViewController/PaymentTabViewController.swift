//
//  PaymentTabViewController.swift
//  SYM
//
//  Created by qw on 07/09/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit


class PaymentTabViewController: UIViewController,PageViewScroll {
    
    //MARK: IBOutlets
    @IBOutlet weak var viewPosted: UIView!
    @IBOutlet weak var viewReceived: UIView!
    @IBOutlet weak var labelPosted: DesignableUILabel!
    @IBOutlet weak var labelReceived: DesignableUILabel!
    @IBOutlet weak var documentVerifiedImage: UIImageView!
    @IBOutlet weak var navigationTitle: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationTitle.backgroundColor = primaryColor
        self.setNavTitle(title: "Payment Methods")
        self.setNavigationBar("back", nil)
        self.postedAction(self)
        PaymentPageViewController.pageDelegate = self
    }
 
    
    func changeCurrentPage(page: Int) {
        if(page == 0){
            self.labelPosted.textColor = .black
            self.labelReceived.textColor = .lightGray
            self.viewPosted.isHidden = false
            self.viewReceived.isHidden = true
        }else {
            
            self.labelPosted.textColor = .lightGray
            self.labelReceived.textColor = .black
            self.viewPosted.isHidden = true
            self.viewReceived.isHidden = false
        }
    }
    //MARK: IBActions
    @IBAction func receivedAction(_ sender: Any) {
        let pageViewController = PaymentPageViewController.dataSource as! PaymentPageViewController
        self.labelPosted.textColor = .lightGray
        self.labelReceived.textColor = .black
        self.viewPosted.isHidden = true
        self.viewReceived.isHidden = false
        pageViewController.setControllerSecond()
    }
    
    @IBAction func postedAction(_ sender: Any) {
        let pageViewController = PaymentPageViewController.dataSource as! PaymentPageViewController
        self.labelPosted.textColor = .black
        self.labelReceived.textColor = .lightGray
        self.viewPosted.isHidden = false
        self.viewReceived.isHidden = true
        pageViewController.setControllerFirst()
    }
}



