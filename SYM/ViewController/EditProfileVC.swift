//
//  EditProfileVC.swift
//  SYM
//
//  Created by AM on 21/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import iOSDropDown
import CarbonKit
import GoogleMaps
import GooglePlaces

class EditProfileVC: UIViewController,SelectedCategories{
    func currentSelectedSubCategories(id: Set<Int>, categories: [GetSubCategories]) {
    }
    
    //Mark: IBOUTLETS
    @IBOutlet weak var firstNameTxtField: DesignableUITextField!
    @IBOutlet weak var lastNameTxtField: DesignableUITextField!
    @IBOutlet weak var userNameTxtField: DesignableUITextField!
    @IBOutlet weak var dobTxtField: DesignableUITextField!
    @IBOutlet weak var emailTxtField: DesignableUITextField!
    @IBOutlet weak var addressTxtField: DesignableUITextField!
    @IBOutlet weak var cityTxtField: DesignableUITextField!
    @IBOutlet weak var cityCollectionView: UICollectionView!
    @IBOutlet weak var stateTxtField: DropDown!
    @IBOutlet weak var zipTxtField: DesignableUITextField!
    @IBOutlet weak var phoneTxtField: DesignableUITextField!
    @IBOutlet weak var dbaTxtField: DesignableUITextField!
    @IBOutlet weak var einTxtField: DesignableUITextField!
    @IBOutlet weak var usdotTxtField: DesignableUITextField!
    
    @IBOutlet weak var experienceTxtField: UITextView!
    @IBOutlet weak var describeTxtField: UITextView!
    @IBOutlet weak var insuranceTxtField: DesignableUITextField!
    @IBOutlet weak var roleTxtField: DropDown!
    @IBOutlet weak var driverDetailStack: UIStackView!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var categoryTable: DynamicHeightTableView!
    @IBOutlet weak var userAddress: DesignableUITextField!
    @IBOutlet weak var viewUserAddress: UIView!
    @IBOutlet weak var cityCOllectionHeight: NSLayoutConstraint!
    
    
    //Mark: PROPERTIES
    var role = [String]()
    var selectedCategory = Set<Int>()
    var imagePath = String()
    var imageName = String()
    var type = Int()
    var picker = UIImagePickerController()
    var shipCategory = [GetCategories]()
    var profileData = GetDriverProfileDetailsData()
    var selectedStateId = Int()
    var selectedCities = [String]()
    var selectedGooglePlace = 1
    static var tabChangeDelegate : ChangeProfileTab? = nil
    static var dahboardReloadDelegate : DashboardReloadData? = nil
    var isFirstTime = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.zipTxtField.delegate = self
        self.phoneTxtField.delegate = self
        self.picker.delegate = self
        
        setNavTitle(title: "Profile")
        setNavigationBar("back", nil)
        
        if(((Singleton.shared.userType.profile_image ?? "").contains("http"))){
            self.userImage.sd_setImage(with: URL(string: (Singleton.shared.userType.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        }else {
            self.userImage.sd_setImage(with: URL(string:U_IMAGE + (Singleton.shared.userType.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        }
        
        if(Singleton.shared.userType.role == 1){
            
            self.driverDetailStack.isHidden = true
            getCustomerData()
        }else {
            
            self.getProfileData()
        }
        
    }
    
    func getCustomerData(){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CUSTOMER_PROFILE, method: .get, parameter: nil, objectClass: GetCustomerProfile.self, requestCode: U_GET_CUSTOMER_PROFILE, userToken: nil) { (response) in
            let user = response.response
            self.userNameTxtField.text = user?.username
            self.firstNameTxtField.text = user?.first_name
            self.lastNameTxtField.text = user?.last_name
            self.emailTxtField.text = user?.email
            self.phoneTxtField.text = user?.phone?.applyPatternOnNumbers(pattern:"+x (xxx) xxx-xxxxx", replacmentCharacter: "x")
            self.userAddress.text =  Singleton.shared.userType.address
            self.imagePath = user?.profile_image ?? ""
            
            Singleton.shared.userType.role = 1
            Singleton.shared.userType.username = user?.username
            Singleton.shared.userType.first_name = user?.first_name
            Singleton.shared.userType.phone = user?.phone
            Singleton.shared.userType.last_name = user?.last_name
            Singleton.shared.userType.address = user?.address
            Singleton.shared.userType.user_id = user?.id
            Singleton.shared.userType.profile_image = user?.profile_image
            Singleton.shared.saveTypeOfUser(data: Singleton.shared.userType)
            
        }
    }
    
    
    func getProfileData(){
        if let token = UserDefaults.standard.value(forKey: UD_TOKEN) as? String{
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_PROFILE_DETAILS, method: .get, parameter: nil, objectClass: GetDriverProfileDetailsResponse.self, requestCode: U_PROFILE_DETAILS, userToken: nil) { (response) in
                if(response.response.count > 0){
                    Singleton.shared.userDetail = response.response[0]
                    self.profileData = Singleton.shared.userDetail
                    Singleton.shared.userType.role = 2
                    Singleton.shared.userType.username = Singleton.shared.userDetail.username
                    Singleton.shared.userType.first_name = Singleton.shared.userDetail.first_name
                    Singleton.shared.userType.phone = Singleton.shared.userDetail.phone
                    Singleton.shared.userType.last_name = Singleton.shared.userDetail.last_name
                    Singleton.shared.userType.address = Singleton.shared.userDetail.address
                    Singleton.shared.userType.user_id = Singleton.shared.userDetail.id
                    Singleton.shared.userType.profile_image = Singleton.shared.userDetail.profile_image
                    Singleton.shared.saveTypeOfUser(data: Singleton.shared.userType)
                    
                    self.shipCategory = Singleton.shared.categoryData
                    self.categoryTable.reloadData()
                    
                    EditProfileVC.dahboardReloadDelegate?.reloadData()
                    self.initView()
                    
                    if(response.response[0].security_check == 0){
                        self.isFirstTime = false
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverSecurityCheckVC") as! DriverSecurityCheckVC
                        self.navigationController?.pushViewController(myVC, animated: true)
                    }
                }
            }
        }
    }
    
    func currentSelectedCategories(id: Set<Int>, categories: [GetCategories]) {
        self.selectedCategory = id
        self.shipCategory = categories
        self.categoryTable.reloadData()
    }
    
    func initView(){
        self.driverDetailStack.isHidden = false
        self.profileData = Singleton.shared.userDetail
        self.shipCategory = Singleton.shared.userDetail.category
        self.shipCategory.map { val in
            self.selectedCategory.insert(val.id ?? 0)
        }
        if(self.profileData.role == 1){
            roleTxtField.text = "Driver"
            self.type = 1
        }else if(self.profileData.role == 2){
            roleTxtField.text = "Loader"
            self.type = 2
        }else if(self.profileData.role == 4){
            roleTxtField.text = "Both"
            self.type = 3
        }
        roleTxtField.optionArray = ["Driver","Loader","Both"]
        roleTxtField.didSelect { (selectedString, id, index) in
            if(selectedString == "Driver"){
                self.type = 1
            }else if(selectedString == "Loader"){
                self.type = 2
            }else {
                self.type = 3
            }
            self.roleTxtField.text = selectedString
        }
        self.stateTxtField.optionArray = []
        for val in Singleton.shared.stateNames{
            self.stateTxtField.optionArray.append(val.state ?? "")
        }
        self.stateTxtField.didSelect { (string, id, index) in
            self.stateTxtField.text =  string
            self.selectedStateId = Singleton.shared.stateNames[id].id ?? 0
        }
        
        self.userNameTxtField.text = profileData.username
        self.firstNameTxtField.text = profileData.first_name
        self.lastNameTxtField.text = profileData.last_name
        self.emailTxtField.text = profileData.email
        self.addressTxtField.text = profileData.address
        self.cityTxtField.text = profileData.city
        self.selectedCities = profileData.loader_city ?? []
        self.cityCollectionView.reloadData()
        let state = Singleton.shared.stateNames.filter{
            $0.id == profileData.state
        }
        if(state.count > 0){
            self.selectedStateId = state[0].id ?? 0
            self.stateTxtField.text = state[0].state
        }
        self.type = (profileData.role ?? 2)
        if(self.type == 1){
            self.roleTxtField.text = "Driver"
        }else if(self.type == 2){
            self.roleTxtField.text = "Loader"
        }else if(self.type == 3){
            self.roleTxtField.text = "Both"
        }
        self.zipTxtField.text = profileData.zipcode
        self.phoneTxtField.text = profileData.phone?.applyPatternOnNumbers(pattern:"+x (xxx) xxx-xxxxx", replacmentCharacter: "x")
        self.dbaTxtField.text = profileData.business_name
        self.usdotTxtField.text = profileData.usdot_number ?? ""
        self.einTxtField.text = profileData.ein_number ?? ""
        self.experienceTxtField.text = profileData.experience ?? ""
        self.describeTxtField.text = profileData.description
        self.insuranceTxtField.text = profileData.insurance_company
        
    }
    
    @objc func dateAdd(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_Date_Picker), object: nil)
        self.dobTxtField.text = "\(K_SELECTED_DATE)"
    }
    
    //MARK: IBActions
    
    @IBAction func addressAction(_ sender: Any) {
        selectedGooglePlace = 2
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue:UInt(GMSPlaceField.name.rawValue) |
                                                  UInt(GMSPlaceField.placeID.rawValue) |
                                                  UInt(GMSPlaceField.coordinate.rawValue) |
                                                  GMSPlaceField.addressComponents.rawValue |
                                                  GMSPlaceField.formattedAddress.rawValue)
        autocompleteController.placeFields = fields
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        autocompleteController.autocompleteFilter = filter
        // Display the autocomplete view controller.
        autocompleteController.modalPresentationStyle = .overFullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
    
    @IBAction func dobAction(_ sender: Any) {
        let dPVC = storyboard?.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        let date = Calendar.current.date(byAdding: .year, value: -16, to: Date())
        dPVC.maxDate = date
        dPVC.picker_type = 1
        NotificationCenter.default.addObserver(self, selector: #selector(dateAdd(_:)), name: NSNotification.Name(N_Date_Picker), object: nil)
        self.present(dPVC, animated: true, completion: nil)
    }
    
    @IBAction func uploadImage(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func updateAction(_ sender: Any) {
        
        if(firstNameTxtField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter First Name", color: errorRed)
        }else if(lastNameTxtField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter last Name", color: errorRed)
        }else if(emailTxtField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Email Address", color: errorRed)
        }else if !(self.isValidEmail(testStr: self.emailTxtField.text ?? "")){
            Singleton.shared.showToast(text: "Enter valid Email Address", color: errorRed)
        } else if(self.addressTxtField.text!.isEmpty && Singleton.shared.userType.role != 1){
            Singleton.shared.showToast(text: "Enter valid Address", color: errorRed)
        } else if(selectedStateId == 0  && Singleton.shared.userType.role != 1){
            Singleton.shared.showToast(text: "Please Select your State", color: errorRed)
        } else if(cityTxtField.text!.isEmpty  && Singleton.shared.userType.role != 1){
            Singleton.shared.showToast(text: "Enter valid city", color: errorRed)
        }  else if(zipTxtField.text!.isEmpty  && Singleton.shared.userType.role != 1){
            Singleton.shared.showToast(text: "Enter valid zip code", color: errorRed)
        }  else if(phoneTxtField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter valid phone number", color: errorRed)
        }  else if(experienceTxtField.text!.isEmpty  && Singleton.shared.userType.role != 1){
            Singleton.shared.showToast(text: "Enter your experience", color: errorRed)
        }   else if(describeTxtField.text!.isEmpty  && Singleton.shared.userType.role != 1){
            Singleton.shared.showToast(text: "Enter your vehicle description", color: errorRed)
        }   else if(insuranceTxtField.text!.isEmpty  && Singleton.shared.userType.role != 1){
            Singleton.shared.showToast(text: "Enter insurance company", color: errorRed)
        }   else if(selectedCities.count == 0  && Singleton.shared.userType.role != 1){
            Singleton.shared.showToast(text: "Please select atleast 1 city", color: errorRed)
        } else{
            var param = [String:Any]()
            var url  = String()
            var number = self.phoneTxtField.text ?? ""
            number = number.replacingOccurrences(of: "+1", with: "")
            number = number.replacingOccurrences(of: "(", with: "")
            number = number.replacingOccurrences(of: ")", with: "")
            number = number.replacingOccurrences(of: " ", with: "")
            number = number.replacingOccurrences(of: "-", with: "")
            number = number.replacingOccurrences(of: "+", with: "")
            if(Singleton.shared.userType.role == 1){
                url = U_BASE + U_EDIT_CUSTOMER_PROFILE
                param = [
                    "first_name":self.firstNameTxtField.text ?? "",
                    "phone":number,
                    "country_code":"+1",
                    "profile_image":self.imagePath,
                    "dob": self.dobTxtField.text ?? "",
                    "last_name":self.lastNameTxtField.text ?? "",
                    "address":self.userAddress.text ?? "",
                ]
            }else {
                url = U_BASE + U_EDIT_DRIVER_PROFILE
                if(self.stateTxtField.text!.isEmpty){
                    Singleton.shared.showToast(text: "Enter State Name", color: errorRed)
                    return
                }
                
                param = [
                    "first_name":self.firstNameTxtField.text ?? "",
                    "dob": self.dobTxtField.text ?? "",
                    "profile_image":self.imagePath,
                    "phone":number,
                    "country_code":"+1",
                    "last_name":self.lastNameTxtField.text,
                    "address":self.addressTxtField.text,
                    "city": self.cityTxtField.text,
                    "state":self.selectedStateId,
                    "zipcode": self.zipTxtField.text,
                    "type":self.type,
                    "city_name": cityTxtField.text ?? "",
                    "loader_city":self.selectedCities ?? [],
                    "category": Array(self.selectedCategory),
                    "experience": self.experienceTxtField.text,
                    "business_name" :self.dbaTxtField.text,
                    "usdot_number":self.usdotTxtField.text,
                    "ein_number":self.einTxtField.text,
                    "description" :self.describeTxtField.text,
                    "insurance_company": self.insuranceTxtField.text
                ]
            }
            
            SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: url, userToken: nil) { (response) in
                Singleton.shared.showToast(text: "Profile updated successfully", color: successGreen)
                if(Singleton.shared.userType.role == 1){
                    self.getCustomerData()
                }else {
                    if(K_COMING_DIRECT_AFTER_SIGNUP_DRIVER == 1 || K_COMING_DIRECT_AFTER_SIGNUP_DRIVER == 2){
                        EditProfileVC.tabChangeDelegate?.changeTab()
                    }
                    self.getProfileData()
                }
            }
        }
    }
    
    @IBAction func searchCityAction(_ sender: Any) {
        selectedGooglePlace = 1
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue:UInt(GMSPlaceField.name.rawValue) |
                                                  UInt(GMSPlaceField.placeID.rawValue) |
                                                  UInt(GMSPlaceField.coordinate.rawValue) |
                                                  GMSPlaceField.addressComponents.rawValue |
                                                  GMSPlaceField.formattedAddress.rawValue)
        autocompleteController.placeFields = fields
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .city
        autocompleteController.autocompleteFilter = filter
        // Display the autocomplete view controller.
        autocompleteController.modalPresentationStyle = .overFullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
    @IBAction func shipDropDown(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ShipCategoriesVC") as! ShipCategoriesVC
        myVC.categoryDelegate = self
        myVC.categoryData = Singleton.shared.categoryData
        myVC.headingText = "Select Category"
        myVC.selectedCategory = self.selectedCategory
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
}

extension EditProfileVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.shipCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryTableCell") as! SubCategoryTableCell
        cell.lblSubctgryName.text = self.shipCategory[indexPath.row].category
        cell.removeItem = {
            self.shipCategory = self.shipCategory.filter{
                $0.id != self.shipCategory[indexPath.row].id
            }
            
            self.selectedCategory = []
            self.shipCategory.map { val in
                self.selectedCategory.insert(val.id ?? 0)
            }
            
            self.categoryTable.reloadData()
        }
        return cell
    }
}

extension EditProfileVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            self.imageName = selectedImageName
        }else {
            self.imageName = "image.jpg"
        }
        let imageData:NSData = selectedImage.pngData()! as NSData
        
        //save in user default
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if let cropImage = selectedImage as? UIImage {
            let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
            let params = [
                "file": data!,
            ] as [String : Any]
            // HTTP Request for upload Picture
            ActivityIndicator.show(view: self.view)
            var url = String()
            SessionManager.shared.makeMultipartRequest(url: U_BASE + U_UPLOAD_IMAGE, fileData: data!, param: params, objectClass: UploadImage.self, fileName: imageName) { (path) in
                self.imagePath  = path.response.path ?? ""
                self.userImage.image = cropImage
                ActivityIndicator.hide()
            }
        }
        picker.dismiss(animated: true)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    func presentCameraSettings() {
        let alertController = UIAlertController(title: "Allow Camera to take photos",
                                                message: "Camera access is denied",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default){
            _ in
        })
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })
        self.present(alertController, animated: true)
    }
}

extension EditProfileVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == phoneTxtField) {
            if(textField.text == ""){
                self.phoneTxtField.text = "+1"
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == phoneTxtField) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.phoneTxtField.text = self.phoneTxtField.text?.applyPatternOnNumbers(pattern:"+x (xxx) xxx-xxxxx", replacmentCharacter: "x")
            return newLength <= 18
        }else if (textField == zipTxtField) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 6
        }else if (textField == usdotTxtField) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 8
        }else if (textField == einTxtField) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 10
        }else {
            return true
        }
    }
    
}

extension EditProfileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        let isEven = (self.selectedCities.count%2) == 0
        self.cityCOllectionHeight.constant = isEven ? CGFloat(self.selectedCities.count/2 * 35) : CGFloat((self.selectedCities.count/2 * 35)+35)
        return self.selectedCities.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CityCell", for: indexPath) as! CityCell
        cell.layer.cornerRadius = 10
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.cityName.text = self.selectedCities[indexPath.row]
        
        cell.cancel = {
            self.selectedCities.remove(at: indexPath.item)
            self.cityCollectionView.reloadData()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.cityCollectionView.frame.width/2)-5, height: 35)
    }
}


extension EditProfileVC: GMSAutocompleteViewControllerDelegate ,GMSMapViewDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
        if selectedGooglePlace == 1 {
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: place.coordinate.latitude , longitude: place.coordinate.longitude)
            geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
                print("Response GeoLocation : \(placemarks)")
                var placeMark: CLPlacemark!
                placeMark = placemarks?[0]
                if let country = placeMark.addressDictionary!["Country"] as? String {
                    if let city = placeMark.addressDictionary!["City"] as? String {
                        if(!(self.selectedCities.contains(city))){
                            self.selectedCities.append(city)
                            self.cityCollectionView.reloadData()
                        }
                    }
                }
            })
        } else if selectedGooglePlace == 2 {
            self.addressTxtField.text = place.formattedAddress ?? ""
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
            geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
                guard let placeMark = placemarks?.first else { return }
                
                self.stateTxtField.text = ""
                
                if let city = placeMark.addressDictionary?["City"] as? String {
                    self.cityTxtField.text = city
                }
                
                if let state = placeMark.locality {
                    for locality in Singleton.shared.stateNames {
                        if(locality.state?.lowercased() == state.lowercased()){
                            self.stateTxtField.text =  locality.state
                            self.selectedStateId = locality.id ?? 0
                        }
                    }
                }
                
                
                if let zipCode = placeMark.postalCode {
                    self.zipTxtField.text = "\(zipCode)"
                }
            }
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

class CityCell : UICollectionViewCell {
    
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    
    var cancel: (() -> Void)? = nil
    
    //MARK: IBActions
    @IBAction func cancelAction(_ sender: Any) {
        if let cancel = self.cancel {
            cancel()
        }
    }
    
}
