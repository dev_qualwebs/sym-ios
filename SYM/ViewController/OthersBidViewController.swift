//
//  OthersBidViewController.swift
//  SYM
//
//  Created by Sagar Pandit on 28/09/22.
//  Copyright © 2022 AM. All rights reserved.
//

import UIKit
import iOSDropDown

class OthersBidViewController: UIViewController {

    @IBOutlet weak var otherBidsTable: ContentSizedTableView!
//    @IBOutlet weak var filterDropDown: DropDown!
//    @IBOutlet weak var sortDropDown: DropDown!
    
    var other_bids:[AppliedBidDetail]?
    var selectedSortby = Int()
    var selectedFilter = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavTitle(title: "Other Bids")
        setNavigationBar("back", nil)
        
//        self.filterDropDown.optionArray = ["All Rating", "1 Rating", "2 Rating", "3 Rating", "4 Rating", "5 Rating"]
//        self.sortDropDown.optionArray = ["Relevance", "Low to High", "High to Low"]
//        self.filterDropDown.didSelect { (val, index, id) in
//
//            self.selectedFilter = index
////            self.driverBidSearch()
//        }
        
//        self.sortDropDown.didSelect { (val, index, id) in
//            self.selectedSortby = index
////            self.driverBidSearch()
//        }
        
        // Do any additional setup after loading the view.
    }

}


extension OthersBidViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.other_bids?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserSingleShipmentCell", for: indexPath) as! UserSingleShipmentCell
        let val = self.other_bids?[indexPath.row]
        cell.userImage.sd_setImage(with: URL(string:U_IMAGE + (val?.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        cell.username.text = (val?.first_name ?? "") + " "  + (val?.last_name ?? "")
        cell.bidValue.text = "$\(val?.amount ?? "")"
        if(val?.driver?.role == 1){
            cell.userOccupation.text = "Driver"
        }else if(val?.driver?.role == 2){
            cell.userOccupation.text = "Loader"
        }else if(val?.driver?.role == 3){
            cell.userOccupation.text = "Driver & Loader"
        }
        return cell
    }
}
