//
//  AddCardViewController.swift
//  SYM
//
//  Created by qw on 07/05/20.
//  Copyright © 2020 AM. All rights reserved.
//



import UIKit
import SkyFloatingLabelTextField
import Stripe

protocol AddCard {
    func addCard()
}

class AddCardViewController: UIViewController, UITextFieldDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var cardName: SkyFloatingLabelTextField!
    @IBOutlet weak var cardNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var expiryMonth: SkyFloatingLabelTextField!
    @IBOutlet weak var cvvText: SkyFloatingLabelTextField!
    @IBOutlet weak var expiryYear: SkyFloatingLabelTextField!
    
    @IBOutlet weak var cardNumberView: View!
    @IBOutlet weak var cardNameView: View!
    @IBOutlet weak var expiryMonthView: View!
    @IBOutlet weak var expiryYearView: View!
    @IBOutlet weak var cardCVVView: View!
    
    @IBOutlet weak var saveCardButton: CustomButton!
    
    
    @IBOutlet weak var visaImage: UIImageView!
    // @IBOutlet weak var visaTickImage: UIImageView!
    
    
    @IBOutlet weak var tickImage: ImageView!
    
    var isCardInvalid = false
    var cardDelegate: AddCard? = nil
    var shipmentData = GetDriverSingleShipment()
    var isAddCard = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(isAddCard){
            self.saveCardButton.setTitle("Add Card", for: .normal)
        }else {
            self.saveCardButton.setTitle("Proceed", for: .normal)
        }
        self.setNavTitle(title: "Add New Card")
        self.setNavigationBar("back", "none")
        self.cardNumber.delegate = self
        self.cardName.delegate = self
        self.expiryMonth.delegate = self
        self.expiryYear.delegate = self
        self.cvvText.delegate = self
        self.cardNumberView.layer.borderWidth = 1
        self.cardNameView.layer.borderWidth = 1
        self.expiryMonthView.layer.borderWidth = 1
        self.expiryYearView.layer.borderWidth = 1
        self.cardCVVView.layer.borderWidth = 1
        self.tickImage.image = #imageLiteral(resourceName: "GreyTick")
        self.expiryYear.placeholder = "YYYY"
    }
    
    
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        if(self.cardName.text!.isEmpty){
            Singleton.shared.showToast(text:"Please enter card holder's name", color: errorRed)
        }else if(self.isCardInvalid){
            Singleton.shared.showToast(text:"Entered card details are invalid", color: errorRed)
        }else if(self.cardNumber.text!.isEmpty){
            Singleton.shared.showToast(text:"Please enter card number", color: errorRed)
        }else if(self.expiryMonth.text!.isEmpty){
            Singleton.shared.showToast(text:"Please enter expiry month", color: errorRed)
        }else if(self.expiryYear.text!.isEmpty){
            Singleton.shared.showToast(text:"Please enter expiry year", color: errorRed)
        }else if(self.expiryYear.text!.count < 4){
            Singleton.shared.showToast(text:"Please valid expiry year", color: errorRed)
        }else if(self.cvvText.text!.isEmpty){
            Singleton.shared.showToast(text:"Please enter your cvv", color: errorRed)
        }else if(self.cvvText.text!.count < 3){
            Singleton.shared.showToast(text:"Please enter valid cvv", color: errorRed)
        }else {
            let numText = (self.cardNumber.text ?? "0").replacingOccurrences(of: " ", with: "")
            if(self.isAddCard){
                ActivityIndicator.show(view: self.view)
            
                let config = STPPaymentConfiguration.shared
                let stripePaymentCards = STPCardParams()
                stripePaymentCards.name = self.cardName.text ?? ""
                stripePaymentCards.expMonth = UInt(Int(self.expiryMonth.text ?? "0") ?? 0)
                stripePaymentCards.expYear = UInt(Int(self.expiryYear.text ?? "0") ?? 0)
                stripePaymentCards.cvc = self.cvvText.text ?? ""
                stripePaymentCards.number = numText
                
                let stpApiClient = STPAPIClient.init(configuration: config)
                stpApiClient.createToken(withCard: stripePaymentCards) {(token,error) in
                    if(error == nil){
                        
                        let card = token?.allResponseFields["card"] as? [String:Any] ?? [String:Any]()
                        let cardParam : [String:Any] = [
                            "last4" : card["last4"] as? String ?? "",
                            "exp_month" : card["exp_month"] as? Int ?? 0,
                            "exp_year" : card["exp_year"] as? Int ?? 0,
                            "object" : card["object"] as? String ?? "",
                            "funding" : card["funding"] as? String ?? "",
                            "cvc_check" : card["cvc_check"] as? String ?? "",
                            "id" : card["id"] as? String ?? "",
                            "name" : card["name"] as? String ?? "",
                            "address_country" : card["country"] as? String ?? "",
                            "country" : card["country"] as? String ?? "",
                            "brand" : card["brand"] as? String ?? "",
                        ]
                        
                        let param:[String:Any] = [
                            "is_default": 1,
                            "nonce" : [
                                "id": token?.tokenId ?? "",
                                "object": token?.allResponseFields["object"] as? String ?? "",
                                "client_ip": token?.allResponseFields["client_ip"] as? String  ?? "",
                                "created": token?.allResponseFields["created"] as? Int  ?? "",
                                "livemode": token?.livemode  ?? false,
                                "type": token?.allResponseFields["type"] as? String  ?? "",
                                "used": token?.allResponseFields["used"] as? Bool  ?? false,
                                "card" : cardParam as? [String:Any]
                            ]
                        ]
                        
                        SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_CARD, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_CARD, userToken: nil) { (response) in
                            ActivityIndicator.hide()
                            Singleton.shared.showToast(text: response.message ?? "", color: successGreen)
                            self.cardDelegate?.addCard()
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    } else {
                        ActivityIndicator.hide()
                        Singleton.shared.showToast(text: error?.localizedDescription ?? "", color: errorRed)
                    }
                }
                
            }else {
                let number = Double(self.cardNumber.text?.replacingOccurrences(of: "  ", with: "") ?? "0")!
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                myVC.shipmentData = self.shipmentData
                self.navigationController?.pushViewController(myVC, animated: true)
                
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == cardNumber){
            self.cardNumberView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == cardName){
            self.cardNameView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == expiryMonth){
            self.expiryMonthView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == expiryYear){
            self.expiryYearView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == cvvText){
            self.cardCVVView.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == cardNumber){
            self.stringGetCreditCardType(string: self.cardNumber.text!.replacingOccurrences(of: " ", with: ""))
        }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == cardNumber){
            let currentCharacterCount = self.cardNumber.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.cardNumber.text = self.cardNumber.text?.applyPatternOnNumbers(pattern: "####  ####  ####  ####", replacmentCharacter: "#")
            return newLength <= 22
        }else if(textField == expiryMonth) {
            let currentCharacterCount = self.expiryMonth.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if(newLength == 2){
                self.expiryMonth.text! += string
                self.expiryYear.becomeFirstResponder()
            }
            return newLength <= 2
        }else if(textField == expiryYear) {
            let currentCharacterCount = self.expiryYear.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if(newLength == 4){
                self.expiryYear.text! += string
                self.cvvText.becomeFirstResponder()
            }
            return newLength <= 4
        }else if(textField == cvvText) {
            let currentCharacterCount = self.cvvText.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 3
        }else {
            return true
        }
    }
    
    func stringGetCreditCardType(string:String) -> String{
        let regVisa = NSPredicate(format:"SELF MATCHES %@", "^4[0-9]{12}(?:[0-9]{3})?$")
        let regMaster = NSPredicate(format:"SELF MATCHES %@", "^5[1-5][0-9]{14}$")
        let regExpress = NSPredicate(format:"SELF MATCHES %@", "^3[47][0-9]{13}$")
        let regDiners = NSPredicate(format:"SELF MATCHES %@", "^3(?:0[0-5]|[68][0-9])[0-9]{11}$")
        let regDiscover = NSPredicate(format:"SELF MATCHES %@", "^6(?:011|5[0-9]{2})[0-9]{12}$")
        let regJCB = NSPredicate(format:"SELF MATCHES %@", "^(?:2131|1800|35\\d{3})\\d{11}$")
        
        if (regVisa.evaluate(with: string)){
            
            self.visaImage.image = UIImage(named: "visa")
            
            self.visaImage.isHidden = false
            isCardInvalid = false
            return "VISA"
        }else if (regMaster.evaluate(with: string)){
            
            self.visaImage.image = UIImage(named: "mastercard")
            
            self.visaImage.isHidden = false
            isCardInvalid = false
            return "MASTER"
        }else  if (regExpress.evaluate(with: string)){
            
            self.visaImage.image = #imageLiteral(resourceName: "american-express")
            
            self.visaImage.isHidden = false
            isCardInvalid = false
            return "AEXPRESS"
        }else if (regDiners.evaluate(with: string)){
            
            self.visaImage.image = #imageLiteral(resourceName: "dinners-club")
            
            self.visaImage.isHidden = false
            isCardInvalid = false
            return "DINERS"
        }else if (regDiscover.evaluate(with: string)){
            
            self.visaImage.image = #imageLiteral(resourceName: "discover")
            
            self.visaImage.isHidden = false
            isCardInvalid = false
            return "DISCOVERS"
        }else if (regJCB.evaluate(with: string)){
            
            self.visaImage.image = #imageLiteral(resourceName: "jcb")
            
            self.visaImage.isHidden = false
            isCardInvalid = false
            return "JCB"
        }else{
            //self.cardTypeButton.setTitle("", for: .normal)
            self.visaImage.image = nil
            
            self.visaImage.isHidden = true
            isCardInvalid = true
            return "invalid"
        }
    }
    
    //MARK: IBActions
    @IBAction func tickAction(_ sender: Any) {
        if(tickImage.image == #imageLiteral(resourceName: "GreyBox")){
            tickImage.image = #imageLiteral(resourceName: "GreyTick")
        }else {
            tickImage.image = #imageLiteral(resourceName: "GreyBox")
        }
    }
}
