//
//  DriverDisputesVC.swift
//  SYM
//
//  Created by AM on 25/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class DriverDisputesVC: UIViewController {
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView == self.tableView){
            if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height) && !isLoadingList){
                if self.redirection == 2 {
                    self.isLoadingList = true
                    self.loadMoreItemsForList()
                }
            }
        }
    }
    
    func loadMoreItemsForList(){
        currentPage += 1
        self.getData()
    }
    
    //Mark: IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    
    
    var disputetData = [GetDisputedShipmentResponse]()
    var refreshControl = UIRefreshControl()
    var getUserShipment = [GetUserShipmentData]()
    var currentPage = 1
    var isLoadingList = false
    var redirection = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        if redirection == 1 {
            getDisputedShipments()
        } else {
            getData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavigationBar("back", nil)
        if self.redirection == 1 {
            setNavTitle(title: "Disputes")
        } else {
            setNavTitle(title: "Completed Shipment")
        }
    }
    
    func getData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_CUSTOMER_ALL_SHIPMENTS + "?offset=" + "\(self.currentPage)&status=6" , method: .get, parameter: nil, objectClass: GetUserShipmentResponse.self, requestCode: U_CUSTOMER_ALL_SHIPMENTS, userToken: nil) { (responseData) in
            
            if(self.getUserShipment.count == 0 || self.currentPage == 1 ){
                self.getUserShipment = responseData.response.data
                self.isLoadingList = false
            } else if responseData.response.data.count > 0{
                for val in responseData.response.data {
                    self.getUserShipment.append(val)
                }
                self.isLoadingList = false
            }
            
            if(responseData.response.data.count == 0){
                if self.currentPage > 1 {
                    self.currentPage -= 1
                }
            }
            self.tableView.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func getDisputedShipments(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_DISPUTE_SHIPMENT, method: .get, parameter: nil, objectClass: GetDisputedShipment.self, requestCode: U_GET_DISPUTE_SHIPMENT, userToken: nil) { (response) in
            self.disputetData = response.response
            self.tableView.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    @objc func refresh() {
        self.refreshControl.endRefreshing()
        self.getDisputedShipments()
    }
    
}

extension DriverDisputesVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.redirection == 1 {
            if(self.disputetData.count == 0){
                self.noDataLabel.isHidden = false
            }else{
                self.noDataLabel.isHidden = true
            }
            return self.disputetData.count ?? 0
        } else {
            if(self.getUserShipment.count == 0){
                self.noDataLabel.isHidden = false
            }else{
                self.noDataLabel.isHidden = true
            }
            return self.getUserShipment.count ?? 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShipmentAllTableViewCell", for: indexPath) as! ShipmentAllTableViewCell
        if self.redirection == 1 {
            let data = disputetData[indexPath.row]
            cell.lblSource.text = "\(data.from_name ?? "")"
            cell.lblDestination.text = "\(data.to_name ?? "")"
            let date = data.created_at ?? "0"
            cell.lblDate.text = String(date.prefix(10))
            cell.dropDate.text = "Date: \(convertTimestampToDate(Int(data.dropoff_date ?? 0), to: "dd-MM-yyy"))"
            cell.startDate.text = "Date: \(convertTimestampToDate(Int(data.pickup_date ?? 0), to: "dd-MM-yyy"))"
            cell.lblCurrentStatus.text = "Shipment is disputed."
            cell.lblDistance.text = (data.distance ?? "0") + " miles"
        } else {
            let data = getUserShipment[indexPath.row]
            cell.lblSource.text = "\(data.from_name ?? "")"
            cell.lblDestination.text = "\(data.to_name ?? "")"
            cell.lblDate.text = "\(convertTimestampToDate(Int(data.created_date ?? 0) ?? 0, to: "dd-MM-yyy"))"
            cell.dropDate.text = "Date: \(convertTimestampToDate(Int(data.dropoff_date ?? 0), to: "dd-MM-yyy"))"
            cell.startDate.text = "Date: \(convertTimestampToDate(Int(data.pickup_date ?? 0), to: "dd-MM-yyy"))"
            cell.lblCurrentStatus.text = "Shipment is completed."
            cell.lblDistance.text = (data.distance ?? "0") + " miles"
            
        }
        
        cell.shipmentButton = {
            if Singleton.shared.userType.role == 1 {
                let sSVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
                if self.redirection == 1 {
                    sSVC.id = self.disputetData[indexPath.item].shipment_id ?? 0
                } else {
                    sSVC.id = self.getUserShipment[indexPath.row].id ?? 0
                }
                self.navigationController?.pushViewController(sSVC, animated: true)
            } else {
                
                if self.redirection == 1 {
                    let sSVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverSingleShipmentVC") as! DriverSingleShipmentVC
                    sSVC.id = self.disputetData[indexPath.item].shipment_id ?? 0
                    self.navigationController?.pushViewController(sSVC, animated: true)
                }
                
            }
        }
        
        return cell
    }
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        let sSVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
    
    //        if (Singleton.shared.userType.role == 1){
    //            if self.redirection == 1 {
    //        sSVC.id = self.disputetData[indexPath.item].shipment_id ?? 0
    //            } else {
    //                sSVC.id = self.getUserShipment[indexPath.row].id ?? 0
    //            }
    //        self.navigationController?.pushViewController(sSVC, animated: true)
    //        }
    
    //        let sSVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
    //        sSVC.id = data.id ?? 0
    //        self.navigationController?.pushViewController(sSVC, animated: true)
    //    }
}

