//
//  MyOrdersViewController.swift
//  SYM
//
//  Created by qw on 29/10/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
import SDWebImage

class MyOrdersViewController: UIViewController ,UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.searchTextField {
            let text =  (self.searchTextField.text ?? "") + string

            self.searchData = []
                self.searchData = self.orderData.filter{
                    return ($0.order_items[0].name ?? "").lowercased().contains(text)
                }
                                  if let char = string.cString(using: String.Encoding.utf8) {
                                              let isBackSpace = strcmp(char, "\\b")
                                              if (isBackSpace == -92) {
                                                  if text.count == 1 {
                                                      self.searchData = self.orderData
                                                  }
                                              }
                                      }
                
                self.orderTable.reloadData()
            return true
        }
        return true
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var orderTable: UITableView!
    @IBOutlet weak var noOrderFound: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var startDate: DesignableUITextField!
    @IBOutlet weak var endDate: DesignableUITextField!
    
    
    var orderData = [OrderResponse]()
    var searchData = [OrderResponse]()
    var currentPicker = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavTitle(title: "Order History")
        self.setNavigationBar("back", nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePicker), name: NSNotification.Name(N_Date_Picker), object: nil)
    }
    
    @objc func handlePicker(){
        if(self.currentPicker == 1){
            self.startDate.text = K_SELECTED_DATE
        }else {
            self.endDate.text = K_SELECTED_DATE
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.getMyOrders()
    }
    
    func getMyOrders(){
        ActivityIndicator.show(view: self.view)
        var url = String()
        if startDate.text != "" && startDate.text != "" {
            url = U_BASE + U_GET_ORDERS + "?orders_from=\(startDate.text ?? "")&orders_to=\(endDate.text ?? "")"
        } else {
            url = U_BASE + U_GET_ORDERS
        }
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetOrders.self, requestCode: U_GET_ORDERS, userToken: nil) { (response) in
            self.orderData = response.response
            self.searchData = response.response
            self.orderTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func filterAction(_ sender: Any) {
        self.popUpView.isHidden = false
    }
    
    @IBAction func clearAction(_ sender: Any) {
        self.startDate.text = ""
        self.endDate.text = ""
        self.searchTextField.text = ""
        self.popUpView.isHidden = true
    }
    
    @IBAction func applyAction(_ sender: Any) {
        self.popUpView.isHidden = true
        getMyOrders()
    }
    
    @IBAction func openPicker(_ sender: UIButton) {
            self.currentPicker = sender.tag
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
            myVC.picker_type = 1
            myVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    
}

extension MyOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.searchData.count == 0){
            self.noOrderFound.isHidden = false
        }else {
            self.noOrderFound.isHidden = true
        }
        return self.searchData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableCell") as! CartTableCell
        let val = self.searchData[indexPath.row]
        if(val.order_items.count > 0){
        cell.cartImage.sd_setImage(with: URL(string:U_IMAGE + (val.order_items[0].image ?? "")), placeholderImage: nil)
        cell.productName.text  = val.order_items[0].name
        cell.productSize.text = "Quantity: \(val.order_items[0].qty_ordered ?? 0)"
        }
        cell.productPrice.text = "$" + (val.grand_total ?? "0")
        cell.status.text = val.status
        let date = self.convertDateToTimestamp(val.created_at ?? "",to: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        cell.date.text = self.convertTimestampToDate(date, to: "dd MMM, yy")
        cell.rateButton={
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverSupportVC") as! DriverSupportVC
            myVC.isRateProduct = true
            myVC.orderDetail = val
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        return cell
    }
}
