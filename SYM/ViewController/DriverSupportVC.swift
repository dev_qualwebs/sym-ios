//
//  DriverSupportVC.swift
//  SYM
//
//  Created by AM on 25/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage
import MapKit

class DriverSupportVC: UIViewController, UITextViewDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var emailAddress: DesignableUITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var emailView: UIStackView!
    @IBOutlet weak var supportView: View!
    @IBOutlet weak var nameTextField: DesignableUITextField!
    
    @IBOutlet weak var nameView: UIStackView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: DesignableUILabel!
    @IBOutlet weak var productRating: CosmosView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var rating: CosmosView!
    
    var isRateProduct = false
    var orderDetail = OrderResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar("back", nil)
        if(self.isRateProduct){
            self.nameView.isHidden = true
            self.emailView.isHidden = true
            self.ratingView.isHidden = false
            self.supportView.isHidden = false
        setNavTitle(title: "Review Product")
        self.textView.text = "Please write product review here"
            self.productImage.sd_setImage(with: URL(string:U_IMAGE + (self.orderDetail.order_items[0].image ?? "")), placeholderImage: nil)
            self.productName.text  = self.orderDetail.order_items[0].name
        }else {
            self.nameView.isHidden = false
            self.nameView.isUserInteractionEnabled = true
            self.emailView.isUserInteractionEnabled = true
            self.emailView.isHidden = false
            self.supportView.isHidden = true
            
            setNavTitle(title: "Support")
            self.emailAddress.text = Singleton.shared.userType.email ?? ""
            self.nameTextField.text = (Singleton.shared.userType.first_name ?? "") + " " + (Singleton.shared.userType.last_name ?? "")
            self.textView.text = "Write your query"
        }
        self.textView.textColor = .lightGray
        self.textView.delegate = self
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(self.textView.text == "Write your query" || self.textView.text == "Please write product review here"){
            self.textView.text = ""
            self.textView.textColor = .black
        }else {
            self.textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(self.textView.text == ""){
            self.textView.text = isRateProduct ? "Please write product review here":"Write your query"
            self.textView.textColor = .lightGray
        }else {
            self.textView.textColor = .black
        }
    }
    
    //MARK: IBActions
    @IBAction func submitAction(_ sender: Any) {
        
        if self.isRateProduct {
            if(self.rating.rating == 0 && self.isRateProduct){
                Singleton.shared.showToast(text: "Please give rating", color: errorRed)
                return
            }
        } else {
            if self.nameTextField.text?.isEmpty ?? true{
                Singleton.shared.showToast(text: "Enter your name", color: errorRed)
                return
            } else  if self.emailAddress.text?.isEmpty ?? true{
                Singleton.shared.showToast(text: "Enter your email address", color: errorRed)
                return
            }
        }
        
        if(self.textView.text!.isEmpty || (self.textView.text == "Write your query") || (self.textView.text == "Please write product review here")){
            Singleton.shared.showToast(text: "Please write your query", color: errorRed)
        }else{
            
            ActivityIndicator.show(view: self.view)
            let name = (Singleton.shared.userDetail.first_name ?? "") + " " + (Singleton.shared.userDetail.last_name ?? "")
            var url = String()
            var param = [String:Any]()
            if(self.isRateProduct){
                url = U_BASE +  U_ORDER_RATING
                param = [
                    "rating": self.rating.rating,
                    "review":self.textView.text,
                    "product_id": self.orderDetail.order_items[0].product_id,
                    "order_id": self.orderDetail.id
               ]
            }else {
                url = U_BASE +  U_CONTACT_US
                param = [
                   "email":self.emailAddress.text ?? "",
                   "message":self.textView.text ?? "",
                   "full_name": self.nameTextField.text ?? ""
               ]
            }
            SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter:param, objectClass: Response.self, requestCode: url, userToken: nil) { (response) in
                Singleton.shared.showToast(text: "Your query sent successfully.", color: successGreen)
                ActivityIndicator.hide()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
}
