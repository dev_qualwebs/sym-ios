//
//  EcommerceHomeViewController.swift
//  SYM
//
//  Created by qw on 18/09/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit


class EcommerceHomeViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var categoryTable: ContentSizedTableView!
    @IBOutlet weak var imageCollection: UICollectionView!
    
    var imageData = [String]()
    var categoryData = [CategoryResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryTable.estimatedRowHeight = 60
         categoryTable.rowHeight = UITableView.automaticDimension
        self.setNavTitle(title: "Ecommerce Store")
        self.setNavigationBar("back", "cart")
        NotificationCenter.default.addObserver(self, selector: #selector(self.openCategory(_:)), name: NSNotification.Name(N_OPEN_CATEGORY), object: nil)
        self.getCategoryData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APICallingViewController.shared.getCartData { (data) in
            self.setNavigationBar("back", "cart")
        }
    }
    
    
    @objc func openCategory(_ notif: Notification) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "EcommerceCategoryViewController") as! EcommerceCategoryViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    func getCategoryData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_PRODUCT_CATEGORY, method: .get, parameter: nil, objectClass: GetEcommerceCategories.self, requestCode: U_GET_PRODUCT_CATEGORY, userToken: nil) { (response) in
            self.categoryData = response.response
            for val in self.categoryData {
                self.imageData.append(val.category_image ?? "")
            }
            self.categoryTable.reloadData()
            self.imageCollection.reloadData()
            ActivityIndicator.hide()
        }
    }
}

extension EcommerceHomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCell", for: indexPath) as! AttachmentCell
        cell.attachmentImage.sd_setImage(with: URL(string:self.imageData[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "ecommerce"))
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 210)
    }
    
}

extension EcommerceHomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EcommerceTableCell") as! EcommerceTableCell
        let val = self.categoryData[indexPath.row]
        cell.categoryName.text = val.category_name
        cell.subcategories = val.subcategories
        cell.submenuTable.reloadData()
        cell.cellSelect = {
            if(cell.isExpand){
                cell.arrowImage.transform = CGAffineTransform(rotationAngle:  CGFloat(Double.pi*2))
                cell.isExpand = false
            }else {
                  cell.arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
                cell.isExpand = true
            }
            cell.submenuTable.reloadData()
            self.categoryTable.beginUpdates()
            self.categoryTable.endUpdates()
        }
        return cell
     }
    
}

class EcommerceTableCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    //MARK: IBOutlets
    @IBOutlet weak var categoryName: DesignableUILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var submenuTable: UITableView!
    
    var cellSelect:(() -> Void)? = nil
    var isExpand = false
    var subcategories = [SubCategoryResponse]()
    
    override func awakeFromNib() {
        submenuTable.delegate = self
        submenuTable.dataSource = self
        submenuTable.estimatedRowHeight = 30
        submenuTable.rowHeight = UITableView.automaticDimension
        super.awakeFromNib()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.isExpand){
            return self.subcategories.count
        }else {
          return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell
        cell.labelSideMenu.text = self.subcategories[indexPath.row].category_name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name(N_OPEN_CATEGORY), object: nil)
    }
    
    //MARK: IBActions
    @IBAction func collapseAction(_ sender: Any) {
        if let cellSelect = self.cellSelect {
            cellSelect()
        }
    }
    
}
