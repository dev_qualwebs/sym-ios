//
//  DriverSingleShipmentVC.swift
//  SYM
//
//  Created by AM on 19/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import MapKit
import ImageSlideshow


protocol ReloadBankData {
    func getBankData()
    func reloadFeatureData()
}

class DriverSingleShipmentVC: UIViewController,ReloadBankData {
    
    func reloadFeatureData(){
        getData()
    }
    
    func getBankData(){
        getAccountDetail()
    }
    
    //Mark: IBOUTLETS
    @IBOutlet weak var timeLeftLabel: DesignableUILabel!
    @IBOutlet weak var bottomTimeConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblDateTime: DesignableUILabel!
    @IBOutlet weak var lblSource: DesignableUILabel!
    @IBOutlet weak var lblDestination: DesignableUILabel!
    @IBOutlet weak var lblRouteDistance: DesignableUILabel!
    @IBOutlet weak var lblEstPrice: DesignableUILabel!
    @IBOutlet weak var lblCategory: DesignableUILabel!
    @IBOutlet weak var lblWeight: DesignableUILabel!
    @IBOutlet weak var lblAttachments: DesignableUILabel!
    @IBOutlet weak var attachmentCollection: UICollectionView!
    @IBOutlet weak var lblDscrptionbyClient: DesignableUILabel!
    @IBOutlet weak var placeBidView: UIView!
    @IBOutlet weak var securityCheckView: UIView!
    @IBOutlet weak var bidOrSecurityBtn: CustomButton!
    @IBOutlet weak var lblBidDetails: DesignableUILabel!
    @IBOutlet weak var userMessage: UITextView!
    @IBOutlet weak var userBidPrice: DesignableUITextField!
    @IBOutlet weak var stackForPlaceBid: UIStackView!
    @IBOutlet weak var viewForAcceptBid: View!
    @IBOutlet weak var headingText: DesignableUILabel!
    @IBOutlet weak var otherBidTable: ContentSizedTableView!
    @IBOutlet weak var viewBidAwarded: View!
    @IBOutlet weak var otherBidStack: UIStackView!
    @IBOutlet weak var yourPrice: DesignableUILabel!
    @IBOutlet weak var yourMessage: DesignableUILabel!
    @IBOutlet weak var labelBidSponsored: DesignableUILabel!
    @IBOutlet weak var awardBidImage: ImageView!
    
    @IBOutlet weak var pickupDate: DesignableUILabel!
    @IBOutlet weak var dropoffDate: DesignableUILabel!
    @IBOutlet weak var subcategoryNames: DesignableUILabel!
    @IBOutlet weak var noAttachment: DesignableUILabel!
    @IBOutlet weak var disputeButton: UIButton!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var disputeTextView: UITextView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var rejectedButton: CustomButton!
    @IBOutlet weak var estimatedPriceLabel: DesignableUILabel!
    @IBOutlet weak var fullImageHideShow: UIView!
    @IBOutlet weak var singleFullImage: ImageView!
    @IBOutlet weak var paymentAlertBox: UIView!
    @IBOutlet weak var paymentAmountLabel: DesignableUILabel!
    @IBOutlet weak var rejectedBidView: UIView!
    @IBOutlet weak var updateBidButtonVIew: CustomButton!
    @IBOutlet weak var attachmentCollectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var biddingClosedView: UIView!
    @IBOutlet weak var biddingCLosedLabel: UITextView!
    @IBOutlet weak var viewMapButtonView: UIView!
    @IBOutlet weak var viewMapHeight: NSLayoutConstraint!
    
    //Mark: Properties
    var id:Int?
    var shipmentData = GetDriverSingleShipment()
    var shipmentStatus = Int()
    var stripeDetail:[StripeResponse]?
    var remainingTime = Int()
    var countTimer : Timer!
    var biddingClosed = false
    var paymentAlertType = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StripeAccountViewController.bankDelegate = self
        self.disputeButton.isHidden = true
        self.disputeTextView.layer.borderColor = UIColor.lightGray.cgColor
        self.disputeTextView.layer.borderWidth = 1
        self.userMessage.delegate = self
        PaymentMethodVC.FeatureDelegate = self
        setNavTitle(title: "Single Shipment")
        setNavigationBar("back", nil)
        self.startButton.isHidden = true
    }
    
    func getAccountDetail(){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_RETRIEVE_STRIPE_ACCOUNT, method: .post, parameter: nil, objectClass: GetStripeResponse.self, requestCode: U_RETRIEVE_STRIPE_ACCOUNT, userToken: nil) { (response) in
            self.stripeDetail = response.response
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.subcategoryNames.text = ""
        getAccountDetail()
        getData()
        getCards()
    }
    
    func startTimer() {
        countTimer = Timer.scheduledTimer(timeInterval: 1 , target: self, selector: #selector(changeTitle), userInfo: nil, repeats: true)
    }
    
    @objc func changeTitle()
    {
        if(remainingTime > 0){
            remainingTime -= 1
            timeLeftLabel.isHidden = false
            bottomTimeConstraint.constant = 10
            let (d,h,m,s) = secondsToHoursMinutesSeconds(seconds: remainingTime)
            if(d >= 1){
                if(d == 3){
                    timeLeftLabel.text = "BIDDING ENDS IN : \(d) days"
                } else if d < 3 {
                    timeLeftLabel.text = "BIDDING ENDS IN : \(d) days, \(h) hr, \(m) min, \(s) sec"
                } else {
                    bottomTimeConstraint.constant = 0
                    timeLeftLabel.isHidden = true
                }
            } else {
                timeLeftLabel.text = "BIDDING ENDS IN : \(h) hr, \(m) min, \(s) sec"
            }
        } else {
            countTimer.invalidate()
            bottomTimeConstraint.constant = 0
            self.biddingClosed = true
            timeLeftLabel.text = "BIDDING CLOSED"
        }
    }
    
    func secondsToHoursMinutesSeconds(seconds: Int) -> (Int,Int, Int, Int) {
        let days = (seconds / 86400)
        let hours = ((seconds % 86400) / 3600)
        let minutes = ((seconds % 3600) / 60)
        let seconds = ((seconds % 3600) % 60)
        return (days,hours,minutes,seconds)
    }
    
    func getCards(){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_DRIVER_SAVED_CARDS, method: .post, parameter: nil, objectClass: GetCard.self, requestCode: U_GET_DRIVER_SAVED_CARDS, userToken: nil) { (response) in
            Singleton.shared.savedCardData = response.response
        }
    }
    
    
    func getData(){
        self.userBidPrice.resignFirstResponder()
        self.userMessage.resignFirstResponder()
        self.rejectedBidView.isHidden = true
        ActivityIndicator.show(view: self.view)
        let param:[String:Any] = ["shipment_id": id ?? 0]
        SessionManager.shared.methodForApiCalling(url: U_BASE + (Singleton.shared.userType.role! == 1 ?  U_CUSTOMER_SINGLE_SHIPMENT:U_DRIVER_SINGLE_SHIPMENTS), method: .post, parameter: param, objectClass: GetDriverSingleShipmentResponse.self, requestCode: U_DRIVER_SINGLE_SHIPMENTS, userToken: nil) { (responseData) in
            
            self.shipmentData = responseData.response
            
            if(self.shipmentData.lookingFor == 1){
                self.shipmentStatus = self.shipmentData.driver_status ?? 0
            }else if(self.shipmentData.lookingFor == 2){
                self.shipmentStatus = self.shipmentData.loader_status ?? 0
            }else if(self.shipmentData.lookingFor == 3){
                self.shipmentStatus = self.shipmentData.driver_status ?? 0
            }else{
                self.shipmentStatus = self.shipmentData.driver_status ?? 0
            }
            var ShowTimer = true
            
            if((self.shipmentData.own_bids?.count ?? 0) > 0){
                if((self.shipmentData.own_bids?[0].driver_accepted == 1) || (self.shipmentData.own_bids?[0].customer_accepted == 1)){
                    self.bottomTimeConstraint.constant = 0
                    self.timeLeftLabel.isHidden = true
                    ShowTimer = false
                }
                
                if((self.shipmentData.own_bids?[0].driver_accepted == 2) || (self.shipmentData.own_bids?[0].customer_accepted == 2)){
                    self.bottomTimeConstraint.constant = 0
                    self.timeLeftLabel.isHidden = true
                    ShowTimer = false
                }
            }
            
            var shipmentAwarded = false
            for i in 0..<(responseData.response.other_bids?.count ?? 0){
                if(responseData.response.other_bids?[i].customer_accepted == 1 || responseData.response.other_bids?[i].driver_accepted == 1){
                    if(responseData.response.other_bids?[i].customer_accepted != 2 && responseData.response.other_bids?[i].driver_accepted != 2){
                        shipmentAwarded = true
                    }
                }
            }
            
            self.remainingTime = self.shipmentData.remaining_time ?? 0
            
            let (d,h,m,s) = self.secondsToHoursMinutesSeconds(seconds: self.remainingTime)
            if(!shipmentAwarded){
                if(self.remainingTime > 0 && ShowTimer){
                    self.timeLeftLabel.isHidden = false
                    self.bottomTimeConstraint.constant = 10
                    if(d >= 1){
                        if(d>3){
                            self.timeLeftLabel.text = "BIDDING ENDS IN : \(d)+ days"
                            self.bottomTimeConstraint.constant = 0
                            self.timeLeftLabel.isHidden = true
                        } else if(d == 3){
                            self.timeLeftLabel.text = "BIDDING ENDS IN : \(d) days"
                        } else if d < 3 {
                            self.startTimer()
                        } else {
                            self.bottomTimeConstraint.constant = 0
                            self.timeLeftLabel.isHidden = true
                        }
                    } else {
                        self.startTimer()
                    }
                } else {
                    self.bottomTimeConstraint.constant = 0
                    self.timeLeftLabel.text = "BIDDING CLOSED"
                }
            } else {
                self.bottomTimeConstraint.constant = 0
                self.timeLeftLabel.isHidden = true
            }
            
            self.attachmentCollection.delegate = self
            self.attachmentCollection.dataSource = self
            self.attachmentCollection.reloadData()
            
            self.otherBidTable.delegate = self
            self.otherBidTable.dataSource = self
            if((responseData.response.other_bids?.count ?? 0) > 0){
                self.otherBidStack.isHidden = false
            }else {
                self.otherBidStack.isHidden = true
            }
            let pickDate = Date(timeIntervalSince1970: TimeInterval((responseData.response.pickup_date ?? 0)))
            self.lblDateTime.text = "Posted by \((responseData.response.customer_name?.first_name ?? "") + " " + (responseData.response.customer_name?.last_name ?? "")) on: \(self.convertTimestampToDate(responseData.response.pickup_date ?? 0, to: "EEE, dd-MM-yy, hh:mm a"))"
            self.lblSource.text = responseData.response.awarded_shipment == 0 ? "\(responseData.response.from_city ?? "")"  : "\(responseData.response.from_name ?? "")"
            self.lblDestination.text = responseData.response.awarded_shipment == 0 ? "\(responseData.response.to_city ?? "")"  : "\(responseData.response.to_name ?? "")"
            self.lblRouteDistance.text = "\(responseData.response.distance ?? "") miles"
            if self.shipmentData.price == nil {
                self.lblEstPrice.isHidden = true
                self.estimatedPriceLabel.isHidden = true
            } else {
                self.lblEstPrice.text = "$\(responseData.response.price ?? "0")"
                self.lblEstPrice.isHidden = false
                self.estimatedPriceLabel.isHidden = false
            }
            self.lblCategory.text = "\(responseData.response.category_name?.category ?? "")"
            self.lblWeight.text = "\(responseData.response.weight ?? 0)"
            self.lblDscrptionbyClient.text = responseData.response.description
            self.lblAttachments.text = "\(responseData.response.image_list?.count ?? 0)"
            if((responseData.response.image_list?.count ?? 0) > 0){
                self.noAttachment.isHidden = true
            }else {
                self.noAttachment.isHidden = false
            }
            
            for i in 0..<responseData.response.subcategory_name.count {
                if(i == (responseData.response.subcategory_name.count - 1)){
                    self.subcategoryNames.text = (self.subcategoryNames.text ?? "") +  (responseData.response.subcategory_name[i].category ?? "")
                } else{
                    self.subcategoryNames.text = (self.subcategoryNames.text ?? "") +  (responseData.response.subcategory_name[i].category ?? "") + ", "
                }
            }
            self.pickupDate.text = "\(self.convertTimestampToDate(responseData.response.pickup_date ?? 0, to: "EEE, dd-MMM-yy, h:mm a"))"
            self.dropoffDate.text = "\(self.convertTimestampToDate(responseData.response.dropoff_date ?? 0, to: "EEE, dd-MMM-yy, h:mm a"))"
            
            
            if(shipmentAwarded == false){
                self.viewMapButtonView.isHidden = false
                self.viewMapHeight.constant = 30
            } else {
                self.viewMapButtonView.isHidden = true
                self.viewMapHeight.constant = 0
            }
            
            
            if responseData.response.security_check == 0{
                self.bidOrSecurityBtn.setTitle("Start security check", for: .normal)
                self.placeBidView.isHidden = true
                self.headingText.text = "Start security check"
                self.stackForPlaceBid.isHidden = false
                self.securityCheckView.isHidden = false
                self.bidOrSecurityBtn.isHidden = false
                self.updateBidButtonVIew.isHidden = true
                self.biddingClosedView.isHidden = true
            } else if responseData.response.security_check == 1{
                self.lblBidDetails.text = "Your request is in progress, after verification from admin you will be able to post a bid."
                self.headingText.text = "Your security request is in progress"
                self.placeBidView.isHidden = true
                self.stackForPlaceBid.isHidden = false
                self.securityCheckView.isHidden = false
                self.bidOrSecurityBtn.isHidden = true
                self.updateBidButtonVIew.isHidden = true
                self.biddingClosedView.isHidden = true
            } else if(self.biddingClosed && (responseData.response.own_bids?.count ?? 0) == 0){
                self.biddingClosedView.isHidden = false
                self.rejectedBidView.isHidden = true
                self.stackForPlaceBid.isHidden = true
                self.viewForAcceptBid.isHidden = true
                self.viewBidAwarded.isHidden = true
                self.otherBidStack.isHidden = true
                self.startButton.isHidden = true
                self.disputeButton.isHidden = true
                self.rejectedButton.isHidden = true
                
                if(shipmentAwarded){
                    self.biddingClosedView.isHidden = false
                    self.rejectedBidView.isHidden = true
                    self.stackForPlaceBid.isHidden = true
                    self.viewForAcceptBid.isHidden = true
                    self.viewBidAwarded.isHidden = true
                    self.otherBidStack.isHidden = true
                    self.startButton.isHidden = true
                    self.disputeButton.isHidden = true
                    self.rejectedButton.isHidden = true
                    self.biddingClosedView.backgroundColor = blueColor
                    self.headingText.text = "Opps, You have missed this time"
                    self.bottomTimeConstraint.constant = 0
                    self.timeLeftLabel.isHidden = true
                    self.biddingCLosedLabel.text = "Oops, You have missed this time, No worries.\nPlace bid on other available shipments. "
                }
                
            } else {
                self.biddingClosedView.isHidden = true
                if((responseData.response.own_bids?.count ?? 0) > 0){
                    if(shipmentAwarded){
                        self.biddingClosedView.isHidden = false
                        self.rejectedBidView.isHidden = true
                        self.stackForPlaceBid.isHidden = true
                        self.viewForAcceptBid.isHidden = true
                        self.viewBidAwarded.isHidden = true
                        self.otherBidStack.isHidden = true
                        self.startButton.isHidden = true
                        self.disputeButton.isHidden = true
                        self.rejectedButton.isHidden = true
                        self.biddingClosedView.backgroundColor = blueColor
                        self.bottomTimeConstraint.constant = 0
                        self.timeLeftLabel.isHidden = true
                        self.headingText.text = "Opps, You have missed this time"
                        self.biddingCLosedLabel.text = "Oops, You have missed this time, No worries.\nPlace bid on other available shipments. "
                    } else if(responseData.response.own_bids?[0].customer_accepted == 2 && (self.shipmentStatus == 2)){
                        self.bidOrSecurityBtn.setTitle("Update Your Bid", for: .normal)
                        self.rejectedBidView.isHidden = false
                        self.bidOrSecurityBtn.isHidden = true
                        self.userBidPrice.text = "\(responseData.response.own_bids?[0].amount ?? "")"
                        self.userBidPrice.isUserInteractionEnabled = false
                        self.userMessage.isUserInteractionEnabled = false
                        self.userMessage.textColor = .black
                        self.userMessage.text = responseData.response.own_bids?[0].message
                        self.headingText.text = "Place a Bid for shipment below"
                        self.stackForPlaceBid.isHidden = true
                        self.viewForAcceptBid.isHidden = true
                        self.viewBidAwarded.isHidden = true
                        self.disputeButton.isHidden = true
                        self.updateBidButtonVIew.isHidden = true
                    } else if(responseData.response.own_bids?[0].sponsored == 0 && (self.shipmentStatus < 3)){
                        
                        if responseData.response.own_bids?[0].driver_accepted != 2 {
                            self.bidOrSecurityBtn.setTitle("Click to feature your bid", for: .normal)
                            self.bidOrSecurityBtn.isHidden = false
                            self.userBidPrice.text = "\(responseData.response.own_bids?[0].amount ?? "")"
                            self.userBidPrice.isUserInteractionEnabled = true
                            self.userMessage.isUserInteractionEnabled = true
                            self.userMessage.textColor = .black
                            self.userMessage.text =  responseData.response.own_bids?[0].message
                            self.headingText.text = "Place a Bid for shipment below"
                            self.stackForPlaceBid.isHidden = false
                            self.viewForAcceptBid.isHidden = true
                            self.viewBidAwarded.isHidden = true
                            self.disputeButton.isHidden = true
                            self.updateBidButtonVIew.isHidden = false
                            
                            if(self.shipmentStatus == 2 && responseData.response.own_bids?[0].customer_accepted == 1){
                                self.stackForPlaceBid.isHidden = true
                                self.viewForAcceptBid.isHidden = false
                                self.viewBidAwarded.isHidden = true
                                self.disputeButton.isHidden = true
                            }
                            
                            if(responseData.response.own_bids?[0].customer_accepted == 2){
                                self.bidOrSecurityBtn.setTitle("Update Your Bid", for: .normal)
                                self.rejectedBidView.isHidden = false
                                self.bidOrSecurityBtn.isHidden = true
                                self.userBidPrice.text = "\(responseData.response.own_bids?[0].amount ?? "")"
                                self.userBidPrice.isUserInteractionEnabled = false
                                self.userMessage.isUserInteractionEnabled = false
                                self.userMessage.textColor = .black
                                self.userMessage.text = responseData.response.own_bids?[0].message
                                self.headingText.text = "Place a Bid for shipment below"
                                self.stackForPlaceBid.isHidden = true
                                self.viewForAcceptBid.isHidden = true
                                self.viewBidAwarded.isHidden = true
                                self.disputeButton.isHidden = true
                                self.updateBidButtonVIew.isHidden = true
                            }
                        } else {
                            self.headingText.text = "You rejected this shipment"
                            self.stackForPlaceBid.isHidden = true
                            self.viewForAcceptBid.isHidden = true
                            self.viewBidAwarded.isHidden = true
                            self.labelBidSponsored.isHidden = true
                            self.otherBidStack.isHidden = true
                            self.disputeButton.isHidden = true
                            self.rejectedButton.isHidden = false
                            self.bottomTimeConstraint.constant = 0
                            self.timeLeftLabel.isHidden = true
                        }
                        
                    } else if(responseData.response.own_bids?[0].sponsored == 1 && (self.shipmentStatus  < 3)){
                        if responseData.response.own_bids?[0].driver_accepted != 2 {
                            self.headingText.text = "Awaiting for Customer to accept Bid"
                            self.stackForPlaceBid.isHidden = true
                            self.viewForAcceptBid.isHidden = true
                            self.viewBidAwarded.isHidden = false
                            self.yourPrice.text = "\(responseData.response.own_bids?[0].amount ?? "")"
                            self.labelBidSponsored.isHidden = false
                            self.awardBidImage.isHidden = true
                            self.disputeButton.isHidden = true
                            self.yourMessage.text = responseData.response.own_bids?[0].message ?? ""
                            self.updateBidButtonVIew.isHidden = true
                        } else {
                            self.headingText.text = "You rejected this shipment"
                            self.stackForPlaceBid.isHidden = true
                            self.viewForAcceptBid.isHidden = true
                            self.viewBidAwarded.isHidden = true
                            self.labelBidSponsored.isHidden = true
                            self.otherBidStack.isHidden = true
                            self.disputeButton.isHidden = true
                            self.rejectedButton.isHidden = false
                            self.updateBidButtonVIew.isHidden = true
                            self.bottomTimeConstraint.constant = 0
                            self.timeLeftLabel.isHidden = true
                        }
                        
                        
                        if(self.shipmentStatus == 2 && responseData.response.own_bids?[0].customer_accepted == 1){
                            self.stackForPlaceBid.isHidden = true
                            self.viewForAcceptBid.isHidden = false
                            self.viewBidAwarded.isHidden = true
                            self.disputeButton.isHidden = true
                        }
                    }
                    else if(responseData.response.own_bids?[0].customer_accepted == 0 && (self.shipmentStatus  == 3)){
                        self.bidOrSecurityBtn.setTitle("Click to feature your bid", for: .normal)
                        self.bidOrSecurityBtn.isHidden = false
                        self.userBidPrice.text = "\(responseData.response.own_bids?[0].amount ?? "")"
                        self.userBidPrice.isUserInteractionEnabled = true
                        self.userMessage.isUserInteractionEnabled = true
                        self.userMessage.textColor = .black
                        self.userMessage.text =  responseData.response.own_bids?[0].message
                        self.headingText.text = "Place a Bid for shipment below"
                        self.stackForPlaceBid.isHidden = false
                        self.viewForAcceptBid.isHidden = true
                        self.viewBidAwarded.isHidden = true
                        self.disputeButton.isHidden = true
                        self.updateBidButtonVIew.isHidden = false
                    }else if(responseData.response.own_bids?[0].driver_accepted == 2 && (self.shipmentStatus == 3)){
                        self.headingText.text = "Awaiting for Customer to accept Bid"
                        self.stackForPlaceBid.isHidden = true
                        self.viewForAcceptBid.isHidden = true
                        self.viewBidAwarded.isHidden = true
                        self.labelBidSponsored.isHidden = true
                        self.otherBidStack.isHidden = true
                        self.disputeButton.isHidden = true
                        self.rejectedButton.isHidden = false
                        self.updateBidButtonVIew.isHidden = true
                    } else if(self.shipmentStatus == 3 && responseData.response.own_bids?[0].customer_accepted == 1){
                        self.headingText.text = "Customer awarded your bid for shipment below"
                        self.stackForPlaceBid.isHidden = true
                        self.viewForAcceptBid.isHidden = false
                        self.viewBidAwarded.isHidden = true
                        self.disputeButton.isHidden = true
                        self.updateBidButtonVIew.isHidden = true
                        self.bottomTimeConstraint.constant = 0
                        self.timeLeftLabel.isHidden = true
                    } else if(self.shipmentStatus == 4){
                        if (self.shipmentData.pickup_date ?? 0) > Int(Date().onlyDate?.timeIntervalSince1970 ?? 0){
                            self.headingText.text = "Start the shipment"
                            self.startButton.isHidden = false
                        }else {
                            self.headingText.text = "Shipment is expired"
                            self.startButton.isHidden = true
                        }
                        self.stackForPlaceBid.isHidden = true
                        self.viewForAcceptBid.isHidden = true
                        self.viewBidAwarded.isHidden = false
                        self.yourPrice.text = "\(responseData.response.own_bids?[0].amount ?? "")"
                        self.awardBidImage.isHidden = true
                        self.yourMessage.text = responseData.response.own_bids?[0].message ?? ""
                        self.updateBidButtonVIew.isHidden = true
                        self.disputeButton.isHidden = true
                        self.bottomTimeConstraint.constant = 0
                        self.timeLeftLabel.isHidden = true
                    }else if(self.shipmentStatus == 5){
                        self.headingText.text = "Shipment is in progress"
                        self.stackForPlaceBid.isHidden = true
                        self.viewForAcceptBid.isHidden = true
                        self.viewBidAwarded.isHidden = false
                        self.yourPrice.text = "\(responseData.response.own_bids?[0].amount ?? "")"
                        self.awardBidImage.isHidden = true
                        self.disputeButton.isHidden = true
                        self.yourMessage.text = responseData.response.own_bids?[0].message ?? ""
                        self.updateBidButtonVIew.isHidden = true
                        self.bottomTimeConstraint.constant = 0
                        self.timeLeftLabel.isHidden = true
                    }else if(self.shipmentStatus == 6){
                        self.headingText.text = "Shipment Completed"
                        self.stackForPlaceBid.isHidden = true
                        self.viewForAcceptBid.isHidden = true
                        self.viewBidAwarded.isHidden = false
                        self.disputeButton.isHidden = true
                        self.yourPrice.text = "\(responseData.response.own_bids?[0].amount ?? "")"
                        self.awardBidImage.isHidden = true
                        self.yourMessage.text = responseData.response.own_bids?[0].message ?? ""
                        self.updateBidButtonVIew.isHidden = true
                        self.bottomTimeConstraint.constant = 0
                        self.timeLeftLabel.isHidden = true
                    }else {
                        self.headingText.text = "Shipment details below"
                        self.stackForPlaceBid.isHidden = true
                        self.viewForAcceptBid.isHidden = true
                        self.viewBidAwarded.isHidden = true
                        self.disputeButton.isHidden = true
                        self.yourPrice.text = "\(responseData.response.own_bids?[0].amount ?? "")"
                        self.awardBidImage.isHidden = true
                        self.yourMessage.text = responseData.response.own_bids?[0].message ?? ""
                        self.updateBidButtonVIew.isHidden = true
                        self.bottomTimeConstraint.constant = 0
                        self.timeLeftLabel.isHidden = true
                    }
                    
                    if(shipmentAwarded){
                        self.biddingClosedView.isHidden = false
                        self.rejectedBidView.isHidden = true
                        self.stackForPlaceBid.isHidden = true
                        self.viewForAcceptBid.isHidden = true
                        self.viewBidAwarded.isHidden = true
                        self.otherBidStack.isHidden = true
                        self.startButton.isHidden = true
                        self.disputeButton.isHidden = true
                        self.rejectedButton.isHidden = true
                        self.biddingClosedView.backgroundColor = blueColor
                        self.bottomTimeConstraint.constant = 0
                        self.timeLeftLabel.isHidden = true
                        self.headingText.text = "Opps, You have missed this time"
                        self.biddingCLosedLabel.text = "Opps, You have missed this time, No worries.\nPlace bid on other available shipments. "
                    }
                }
                else {
                    if responseData.response.security_check == 0{
                        self.bidOrSecurityBtn.setTitle("Start security check", for: .normal)
                        self.placeBidView.isHidden = true
                        self.stackForPlaceBid.isHidden = false
                        self.securityCheckView.isHidden = false
                        self.biddingClosedView.isHidden = true
                        self.bidOrSecurityBtn.isHidden = false
                        self.updateBidButtonVIew.isHidden = true
                        self.headingText.text = "Start security check"
                    } else if responseData.response.security_check == 1{
                        self.lblBidDetails.text = "Your request is in progress, after verification from admin you will be able to post a bid."
                        self.placeBidView.isHidden = true
                        self.stackForPlaceBid.isHidden = false
                        self.securityCheckView.isHidden = false
                        self.bidOrSecurityBtn.isHidden = true
                        self.updateBidButtonVIew.isHidden = true
                        self.biddingClosedView.isHidden = true
                        self.headingText.text = "Your security request is in progress"
                    }
                    else if responseData.response.security_check == 2{
                        self.updateBidButtonVIew.isHidden = true
                        self.biddingClosedView.isHidden = true
                        self.headingText.text = "Place a Bid for shipment below"
                        self.bidOrSecurityBtn.setTitle("Send", for: .normal)
                        if (self.shipmentData.pickup_date ?? 0) > Int(Date().onlyDate?.timeIntervalSince1970 ?? 0){
                            self.placeBidView.isHidden = false
                        }else { self.placeBidView.isHidden = true
                            self.bidOrSecurityBtn.isHidden = true
                        }
                        self.stackForPlaceBid.isHidden = false
                        self.securityCheckView.isHidden = true
                        self.bidOrSecurityBtn.isHidden = false
                    }
                    
                    if(shipmentAwarded){
                        self.biddingClosedView.isHidden = false
                        self.rejectedBidView.isHidden = true
                        self.stackForPlaceBid.isHidden = true
                        self.viewForAcceptBid.isHidden = true
                        self.viewBidAwarded.isHidden = true
                        self.otherBidStack.isHidden = true
                        self.startButton.isHidden = true
                        self.disputeButton.isHidden = true
                        self.rejectedButton.isHidden = true
                        self.biddingClosedView.backgroundColor = blueColor
                        self.bottomTimeConstraint.constant = 0
                        self.timeLeftLabel.isHidden = true
                        self.headingText.text = "Opps, You have missed this time"
                        self.biddingCLosedLabel.text = "Opps, You have missed this time, No worries.\nPlace bid on other available shipments. "
                    }
                }
            }
            
            if(self.shipmentStatus == 7){
                self.headingText.text = "Shipment disputed!"
                self.headingText.textColor = .red
            }
            
            if(self.shipmentStatus == 8){
                self.headingText.text = "Shipment cancelled! The user has requested for Refund."
                self.headingText.textColor = .red
                
                self.biddingClosedView.isHidden = true
                self.rejectedBidView.isHidden = true
                self.stackForPlaceBid.isHidden = true
                self.viewForAcceptBid.isHidden = true
                self.viewBidAwarded.isHidden = true
                self.otherBidStack.isHidden = true
                self.startButton.isHidden = true
                self.disputeButton.isHidden = true
                self.rejectedButton.isHidden = true
                self.bottomTimeConstraint.constant = 0
                self.timeLeftLabel.isHidden = true
            }
            
            ActivityIndicator.hide()
        }
    }
    
    func callBidAcceptApi(userResponse: Int){
        let param:[String:Any] = [
            "shipment_id":self.shipmentData.id ?? 0,
            "driver_response":userResponse
        ]
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_ACCEPT_BID_DRIVER, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ACCEPT_BID_DRIVER, userToken:nil) { (response) in
            ActivityIndicator.hide()
            self.getData()
            self.paymentAlertBox.isHidden = true
            Singleton.shared.showToast(text: response.message ?? "", color: successGreen)
        }
    }
    
    //MARK:IBActions
    
    
  
    
    
    
    
    @IBAction func updateRejectedViewAction(_ sender: Any) {
        self.bidOrSecurityBtn.isHidden = false
        self.userBidPrice.text = ""
        self.userBidPrice.isUserInteractionEnabled = true
        self.userMessage.isUserInteractionEnabled = true
        self.userMessage.textColor = .lightGray
        self.userMessage.text = "Your Message"
        self.rejectedBidView.isHidden = true
        self.stackForPlaceBid.isHidden = false
    }
    
    
    @IBAction func paymentAlertOkAction(_ sender: Any) {
        if(paymentAlertType == 1){
            if Singleton.shared.savedCardData.count > 0 {
                self.callBidAcceptApi(userResponse: 1)
            } else {
                self.paymentAlertBox.isHidden = true
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }} else {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentMethodVC") as! PaymentMethodVC
                self.paymentAlertBox.isHidden = true
                myVC.shipmentData = self.shipmentData
                myVC.isNavigationFromSideMenu = K_FEATURE_BID_NAVIGATION
                self.navigationController?.pushViewController(myVC, animated: true)
            }
    }
    
    
    @IBAction func updateBidAction(_ sender: Any) {
        let param : [String:Any] = [
            "bid_id": self.shipmentData.own_bids?[0].id,
            "message":self.userMessage.text,
            "placingAs": "1",
            "amount":self.userBidPrice.text,
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_EDIT_POSTED_BID, method: .post, parameter: param, objectClass: SuccessResponse.self , requestCode: U_EDIT_POSTED_BID, userToken: nil) { (response) in
            if response.message != "" {
                Singleton.shared.showToast(text: response.message ?? "", color: successGreen)
            }
            self.getData()
        }
    }
    
    
    @IBAction func paymentAlertCancelAction(_ sender: Any) {
        self.paymentAlertBox.isHidden = true
    }
    
    @IBAction func cancelImageAction(_ sender: Any) {
        self.fullImageHideShow.isHidden = true
    }
    
    
    @IBAction func viewOtherBidsAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OthersBidViewController") as! OthersBidViewController
        myVC.other_bids = self.shipmentData.other_bids ?? []
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func acceptAction(_ sender: Any) {
        self.callBidAcceptApi(userResponse: 1)
    }
    
    @IBAction func rejectAction(_ sender: Any) {
        self.callBidAcceptApi(userResponse: 2)
    }
    
    @IBAction func okAction(_ sender: Any) {
        if(self.disputeTextView.text == ""){
            Singleton.shared.showToast(text: "Enter reason for Dispute", color: errorRed)
        }else {
            self.popupView.isHidden = true
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "shipment_id" : self.shipmentData.id,
                "user_id" :Singleton.shared.userType.user_id,
                "reason" : self.disputeTextView.text ?? ""
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DISPUTE_SHIPMENT, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_DISPUTE_SHIPMENT, userToken: nil) { (response) in
                self.disputeTextView.text = ""
                ActivityIndicator.hide()
                Singleton.shared.showToast(text: "Successfully Disputed Job", color: successGreen)
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
    @IBAction func disputeAction(_ sender: Any) {
        self.disputeTextView.text = ""
        self.popupView.isHidden = false
    }
    
    @IBAction func startShipmentAction(_ sender: Any) {
        if (self.shipmentData.pickup_date ?? 0) > Int(Date().onlyDate?.timeIntervalSince1970 ?? 0){
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DRIVER_START_SHIPMENT + "\(self.shipmentData.id ?? 0)&startingAs=\(self.shipmentData.lookingFor ?? 1)", method: .get, parameter: nil, objectClass: Response.self, requestCode: U_DRIVER_START_SHIPMENT, userToken: nil) { (response) in
                ActivityIndicator.hide()
                Singleton.shared.progressShipment = []
                Singleton.shared.showToast(text: "Shipment started Successfully", color: successGreen)
                self.getData()
                self.startButton.isHidden = true
                self.headingText.text = "Shipment is in progress"
            }
        } else {
            Singleton.shared.showToast(text: "Unable to start shipment of the past.", color: errorRed)
        }
    }
    
    @IBAction func doneBtnPressed(_ sender: Any) {
        if bidOrSecurityBtn.titleLabel?.text == "Start security check"{
            let myVC = storyboard!.instantiateViewController(withIdentifier: "DriverSecurityCheckVC") as! DriverSecurityCheckVC
            self.navigationController?.pushViewController(myVC, animated: true)
        }else if bidOrSecurityBtn.titleLabel?.text == "Click to feature your bid"{
            self.paymentAlertType = 2
            self.paymentAmountLabel.text = "To feature your bid, you will have to pay $\(self.shipmentData.sponser_amount ?? "0")"
            self.paymentAlertBox.isHidden = false
            return
        } else if(bidOrSecurityBtn.titleLabel?.text == "Update Your Bid"){
            
            if(self.userBidPrice.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter bid price", color: errorRed)
            }else if(self.userMessage.text == "Your Message"){
                Singleton.shared.showToast(text: "Enter your message", color: errorRed)
            }else {
                if (self.shipmentData.pickup_date ?? 0) > Int(Date().onlyDate?.timeIntervalSince1970 ?? 0){
                    ActivityIndicator.show(view: self.view)
                    let param: [String:Any] = [
                        "bid_id": self.shipmentData.own_bids?[0].id,
                        "amount":self.userBidPrice.text,
                        "message":self.userMessage.text,
                    ]
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_REJECTED_BID, method: .post, parameter: param, objectClass: Response.self, requestCode: U_UPDATE_REJECTED_BID, userToken: nil) { (response) in
                        Singleton.shared.showToast(text: "Bid updated Successfully", color: successGreen)
                        self.getData()
                        ActivityIndicator.hide()
                    }
                } else {
                    Singleton.shared.showToast(text: "Unable to update bid for past date or time.", color: errorRed)
                }
            }
        } else{
            if(self.shipmentData.security_check == 2){
                if(self.userBidPrice.text!.isEmpty){
                    Singleton.shared.showToast(text: "Enter bid price", color: errorRed)
                }else if(self.userMessage.text == "Your Message"){
                    Singleton.shared.showToast(text: "Enter your message", color: errorRed)
                }else {
                    if(self.stripeDetail?.count ?? 0 > 0){
                        if (self.shipmentData.pickup_date ?? 0) > Int(Date().onlyDate?.timeIntervalSince1970 ?? 0){
                            ActivityIndicator.show(view: self.view)
                            let param: [String:Any] = [
                                "shipment_id": self.shipmentData.id,
                                "amount":self.userBidPrice.text,
                                "message":self.userMessage.text,
                                "placingAs": (Singleton.shared.userType.role ?? 2) - 1
                            ]
                            SessionManager.shared.methodForApiCalling(url: U_BASE + U_POST_BID_DRIVER, method: .post, parameter: param, objectClass: Response.self, requestCode: U_POST_BID_DRIVER, userToken: nil) { (response) in
                                Singleton.shared.showToast(text: "Bid Placed Successfully", color: successGreen)
                                self.getData()
                                ActivityIndicator.hide()
                            }
                        } else {
                            Singleton.shared.showToast(text: "Cannot place bids for Past date or time.", color: errorRed)
                        }
                        
                    }else {
                        let eVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverProfileViewController") as! DriverProfileViewController
                        K_COMING_DIRECT_AFTER_SIGNUP_DRIVER = 2
                        self.navigationController?.pushViewController(eVC, animated: true)
                        Singleton.shared.showToast(text: "Please update your personal and bank details first.", color: errorRed)
                    }
                }
            }
        }
    }
    
    @IBAction func viewMapAction(_ sender: Any) {
        let myVC = storyboard!.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        myVC.pickUpLatitude = self.shipmentData.from_latitude ?? ""
        myVC.pickUpLongitude = self.shipmentData.from_longitude ?? ""
        myVC.dropOffLatitude = self.shipmentData.to_latitude ?? ""
        myVC.dropOffLongitude = self.shipmentData.to_longitude ?? ""
        myVC.pickup_from = self.shipmentData.awarded_shipment == 0 ? self.shipmentData.from_city ?? "" : self.shipmentData.from_name ?? ""
        myVC.pickup_to = self.shipmentData.awarded_shipment == 0 ? self.shipmentData.to_city ?? "" : self.shipmentData.to_name ?? ""
        if(self.shipmentData.lookingFor == 1){
            myVC.shipmentStatus = self.shipmentData.driver_status ?? 0
        }else if(self.shipmentData.lookingFor == 2){
            myVC.shipmentStatus = self.shipmentData.loader_status ?? 0
        }else if(self.shipmentData.lookingFor == 3){
            myVC.shipmentStatus = self.shipmentData.driver_status ?? 0
        }else{
            myVC.shipmentStatus = self.shipmentData.driver_status ?? 0
        }
        let val = self.shipmentData
        if (val.customer_details?.count ?? 0) > 0 {
            myVC.CustomerData = val.customer_details?[0] ?? TypeOfUser()
        }
        myVC.redirection = 2
        myVC.shipmentDetail = GetAllCommonShipment(id: val.id, user_id: val.user_id, category_id: val.category_id, description: val.description, from_name: val.from_name, from_latitude: val.from_latitude, from_longitude: val.from_longitude, from_city: val.from_city, from_state: val.from_state, from_zipcode: val.from_zipcode, to_name: val.to_name, to_latitude: val.to_latitude, to_longitude: val.to_longitude, to_state: val.to_state, to_city: val.to_city, to_zipcode: val.to_zipcode, pickup_date: val.pickup_date, dropoff_date: val.dropoff_date, price: val.price, multiple_item: val.multiple_item, set_budget: val.set_budget, distance: val.distance, weight: val.weight, driver_status: val.driver_status, loader_status: val.loader_status, lookingFor: val.lookingFor, customer_id: val.customer_id, first_name: val.customer_name?.first_name, last_name: val.customer_name?.last_name,customer_accepted:(val.own_bids?.count ?? 0) > 0 ? (val.own_bids?[0].customer_accepted ?? 0) : nil, driver_accepted:(val.own_bids?.count ?? 0) > 0 ? (val.own_bids?[0].driver_accepted ?? 0) : nil, driver_id: val.driver_id, image_list: val.image_list)
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}

extension DriverSingleShipmentVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(self.userMessage.text == "Your Message"){
            self.userMessage.text = ""
            self.userMessage.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(self.userMessage.text == ""){
            self.userMessage.text = "Your Message"
            self.userMessage.textColor = .lightGray
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.attachmentCollectionHeightConstraint.constant = (self.shipmentData.image_list?.count ?? 0) > 0 ? 100 : 0
        return self.shipmentData.image_list?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCell", for: indexPath) as! AttachmentCell
        cell.attachmentImage.sd_setImage(with: URL(string:U_IMAGE + (self.shipmentData.image_list?[indexPath.row].image ?? "").replacingOccurrences(of: " ", with: "%20")), placeholderImage: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageSlider = ImageSlideshow()
        var input = [SDWebImageSource]()
        for val in 0..<(self.shipmentData.image_list?.count ?? 0) {
            input.append(SDWebImageSource(urlString: U_IMAGE + (self.shipmentData.image_list?[val].image ?? "").replacingOccurrences(of: " ", with: "%20"))!)
        }
        imageSlider.setImageInputs(input)
        let fullScreenController = imageSlider.presentFullScreenController(from: self)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.shipmentData.other_bids?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserSingleShipmentCell", for: indexPath) as! UserSingleShipmentCell
        let val = self.shipmentData.other_bids?[indexPath.row]
        cell.userImage.sd_setImage(with: URL(string:U_IMAGE + (val?.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        cell.username.text = (val?.first_name ?? "") + " "  + (val?.last_name ?? "")
        cell.bidValue.text = "$\(val?.amount ?? "")"
        if(val?.driver?.role == 1){
            cell.userOccupation.text = "Driver"
        }else if(val?.driver?.role == 2){
            cell.userOccupation.text = "Loader"
        }else if(val?.driver?.role == 3){
            cell.userOccupation.text = "Driver & Loader"
        }
        return cell
    }
}
