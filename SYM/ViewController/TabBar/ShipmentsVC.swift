//
//  ShipmentsVC.swift
//  SYM
//
//  Created by AM on 19/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import CarbonKit
import RAMAnimatedTabBarController

class ShipmentsVC: UIViewController, CarbonTabSwipeNavigationDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var draftTable: UITableView!
//    @IBOutlet weak var titleBar: RAMAnimatedTabBarItem!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    
    let refreshControl = UIRefreshControl()
    var draftData = [GetDraftResponse]()
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    
//    - (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
//        return YES;
//    }
    //paste above in CarbonTabSwipeSegmentedControl.m file
    
//    With iOS 13 UISegmentedControl is scrollable. That's why you guys should override gestureRecognizerShouldBegin method inside CarbonTabSwipeSegmentedControl.m file and return YES.
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        draftTable.estimatedRowHeight = 55
        draftTable.rowHeight = UITableView.automaticDimension
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        draftTable.addSubview(refreshControl)
        
        if(Singleton.shared.userType.role == 1){
            self.getDraftData()
            
        }else {
            let items = ["All", "Recommended", "Active","Won","In Progress","Completed","Disputed"]
            self.carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
            self.carbonTabSwipeNavigation.insert(intoRootViewController: self)
            carbonTabSwipeNavigation.setNormalColor(.lightGray)
            carbonTabSwipeNavigation.setSelectedColor(brownColor,  font: UIFont.systemFont(ofSize: 15, weight: .medium))
            carbonTabSwipeNavigation.setIndicatorColor(brownColor)
            
        }
    }
   
    override func viewDidAppear(_ animated: Bool) {
        
        if(Singleton.shared.userType.role == 1){
            self.setNavTitle(title: "Draft")
        }else {
            self.setNavTitle(title: "Shipments")
            self.carbonTabSwipeNavigation.setCurrentTabIndex(UInt(K_CURRENT_CATEGORY), withAnimation: false)
        }
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.getDraftData()
        refreshControl.endRefreshing()
    }
    
    func getDraftData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_DRAFT_SHIPMENT, method:.get, parameter: nil, objectClass: GetDraft.self, requestCode: U_GET_DRAFT_SHIPMENT, userToken: nil) { (response) in
            self.draftData = response.response
            self.draftTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
        K_CURRENT_CATEGORY = Int(index)
        print("working")
        print(K_CURRENT_CATEGORY)
    }
    
    
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        K_CURRENT_CATEGORY = Int(index)
        print(K_CURRENT_CATEGORY)
        return storyboard!.instantiateViewController(withIdentifier: "ShipmentAllVC")
    }
}

extension ShipmentsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.draftData.count == 0){
            self.noDataLabel.isHidden = false
        }else {
            self.noDataLabel.isHidden = true
        }
        return self.draftData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DriverMessagesTableCell") as! DriverMessagesTableCell
        let val = self.draftData[indexPath.row]
        if val.lookingFor == nil {
            cell.username.text = "No data available"
        } else if val.lookingFor == 1 {
            cell.username.text = "Looking for driver for the shipment"
        } else if val.lookingFor == 2 {
            cell.username.text = "Looking for loader for the shipment"
        } else if val.lookingFor == 3 {
            cell.username.text = "Looking for driver and loader for the shipment"
        }
        
        cell.userMessage.text = val.description ?? "No data available"
        return  cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! PageViewController
        let data = self.draftData[indexPath.row]
        
        var subcategory = [Int]()
        for val in data.draft_subcategories {
            subcategory.append(val.id ?? 0)
        }
        K_DRAFT_OR_EDIT = 2
        K_DRIVER_REHIRE_ID = 0
        var imageArray = [String]()
        for i in 0..<(data.draft_images?.count ?? 0){
            imageArray.append(data.draft_images?[i].image ?? "")
        }
        Singleton.shared.postYourShipment = GetPostYourShipmentData(id: data.id ?? 0, category_id: data.category_id ?? 0, discription: data.description ?? "", from_name: data.from_name ?? "", from_latitude: data.from_latitude ?? "", from_longitude: data.from_longitude ?? "", from_city: data.from_city ?? "", from_state: "\(data.from_state ?? 0)", from_zipcode: "\(data.from_zipcode ?? 0)", to_name: data.to_name ?? "", to_latitude: data.to_latitude ?? "", to_longitude: data.to_longitude ?? "", to_city: data.to_city ?? "", to_state: "\(data.to_state ?? 0)", to_zipcode: "\(data.to_zipcode ?? 0)", lookingFor: data.lookingFor ?? 0, sub_category_id: subcategory, set_budget: data.set_budget ?? 0, multiple_item: "0", distance: "\(data.distance ?? "0")", weight: Double(data.weight ?? 0), price: data.price ?? "0", image: imageArray, pickup_date: data.pickup_date ?? 0, dropoff_date: data.dropoff_date ?? 0)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

//extension RAMAnimatedTabBarItem {
//    convenience init(title: String, image: UIImage?, tag: Int, animation: RAMItemAnimation, selectedColor: UIColor, unselectedColor: UIColor) {
//        self.init(title: title, image: image, tag: 0)
//        animation.iconSelectedColor = selectedColor
//        animation.textSelectedColor = selectedColor
//        self.animation = animation
//        self.textColor = unselectedColor
//        self.iconColor = unselectedColor
//    }
//}
