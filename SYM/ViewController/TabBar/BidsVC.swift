//
//  BidsVC.swift
//  SYM
//
//  Created by AM on 19/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class BidsVC: UIViewController {
    
    //Marks: IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var shipmentView: StepProgressView!
    @IBOutlet weak var updateAmountTextfield: DesignableUITextField!
    @IBOutlet weak var updateMessageTextField: DesignableUITextField!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var confirmDeletePopupView: UIStackView!
    @IBOutlet weak var updateBidLabel: DesignableUILabel!
    
    var bidData = PostedBidResponse()
    var refreshControl = UIRefreshControl()
    var filterBidData = [PostedBid]()
    var selectedBid = PostedBid()
    var isRejectedBid = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchField.delegate = self
        self.tableView.estimatedRowHeight = 190
        self.tableView.rowHeight = UITableView.automaticDimension
        self.getDriverBids()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        
        
        self.shipmentView.viewShipment={
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavTitle(title: "Bids")
        self.shipmentView.showAnimatingDots()
        self.shipmentView.showHideView={
            
            if(Singleton.shared.progressShipment?.count == 0){
                self.shipmentView.isHidden = true
            }else{
                for val in Singleton.shared.progressShipment!{
                    if(val.driver_status == 5){
                        self.shipmentView.isHidden = false
                    }
                }
            }
        }
    }
    
    @objc func refresh() {
        self.refreshControl.endRefreshing()
        getDriverBids()
    }
    
    func getDriverBids(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_DRIVER_POSTED_BID, method: .get, parameter: nil, objectClass:GetPostedBid.self , requestCode: U_GET_DRIVER_POSTED_BID, userToken: nil) { (response) in
            self.bidData = response.response
            self.filterBidData = self.bidData.data ?? []
            self.tableView.reloadData()
            ActivityIndicator.hide()
        }
    }
    @IBAction func deleteBidOkayAction(_ sender: Any) {
        //  ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_EDIT_POSTED_BID, method: .post, parameter: ["bid_id": self.selectedBid.id,"amount":self.updateAmountTextfield.text], objectClass: SuccessResponse.self , requestCode: U_EDIT_POSTED_BID, userToken: nil) { (response) in
            self.confirmDeletePopupView.isHidden = true
            self.getDriverBids()
            //  ActivityIndicator.hide()
        }
        
    }
    
    @IBAction func deleteBidCancelAction(_ sender: Any) {
        self.confirmDeletePopupView.isHidden = true
    }
    
    @IBAction func updatePopupHideShowAction(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
    @IBAction func updateBidAction(_ sender: Any) {
        if(self.isRejectedBid){
            ActivityIndicator.show(view: self.view)
            let param: [String:Any] = [
                "bid_id": self.selectedBid.id,
                "amount":self.updateAmountTextfield.text,
                "message":self.updateMessageTextField.text,
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_REJECTED_BID, method: .post, parameter: param, objectClass: Response.self, requestCode: U_UPDATE_REJECTED_BID, userToken: nil) { (response) in
                Singleton.shared.showToast(text: "Bid updated Successfully", color: successGreen)
                self.popupView.isHidden = true
                self.getDriverBids()
                ActivityIndicator.hide()
            }
        } else {
            let param : [String:Any] = [
                "bid_id": self.selectedBid.id,
                "message": self.updateMessageTextField.text,
                "placingAs": "1",
                "amount":self.updateAmountTextfield.text
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_EDIT_POSTED_BID, method: .post, parameter: param, objectClass: SuccessResponse.self , requestCode: U_EDIT_POSTED_BID, userToken: nil) { (response) in
                if response.message != "" {
                    Singleton.shared.showToast(text: response.message ?? "", color: errorRed)
                }
                self.popupView.isHidden = true
                self.getDriverBids()
            }
        }
    }
}


extension BidsVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.filterBidData.count > 0 {
            self.noDataLabel.isHidden = true
        } else {
            self.noDataLabel.isHidden = false
        }
        return self.filterBidData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BidsTableViewCell", for: indexPath) as! BidsTableViewCell
        let val = self.filterBidData[indexPath.row]
        if(val.status == 2){
            cell.editView.isHidden = false
            cell.deleteView.isHidden = false
            cell.editView.backgroundColor = primaryColor
            cell.deleteView.backgroundColor = brownColor
            
            if(val.driver_accepted == 2){
                cell.bidStatus.text = "Rejected by you"
            } else if(val.customer_accepted == 2){
                cell.bidStatus.text = "Rejected by customer"
            } else {
                cell.bidStatus.text = "Bid Placed"
            }
            
        }else if(val.status == 1){
            cell.editView.isHidden = false
            cell.deleteView.isHidden = false
            cell.editView.backgroundColor = primaryColor
            cell.deleteView.backgroundColor = brownColor
            
            
            if(val.driver_accepted == 2) {
                cell.bidStatus.text = "Rejected by you"
            } else if (val.customer_accepted == 2){
                cell.bidStatus.text = "Rejected by customer"
            }
            else {
                cell.bidStatus.text = "Open For Bid"
            }
            
        }else if(val.status == 3){
            cell.editView.isHidden = true
            cell.deleteView.isHidden = true
            cell.editView.backgroundColor = primaryColor
            cell.deleteView.backgroundColor = brownColor
            cell.bidStatus.text = "Accepted By Customer"
            
            if(val.customer_accepted == 1){
                cell.bidStatus.text = "Accepted By Customer"
            } else {
                cell.bidStatus.text = "Shipment awarded to other driver."
                }
            
            if(val.driver_accepted == 2) {
                cell.bidStatus.text = "Rejected by you"
            } else if (val.customer_accepted == 2){
                cell.bidStatus.text = "Rejected by customer"
            }
            
        }else if(val.status == 4){
            cell.editView.backgroundColor = UIColor.lightGray
            cell.deleteView.backgroundColor = UIColor.lightGray
            cell.editView.isHidden = true
            cell.deleteView.isHidden = true
            cell.bidStatus.text = "Shipment is awarded to you."
            
            if(val.customer_accepted == 1){
                cell.bidStatus.text = "Shipment awarded to you."
                
                if(val.driver_accepted == 1){
                    cell.bidStatus.text = "Ready to start the shipment"
                }
                
                if(val.driver_accepted == 2){
                    cell.bidStatus.text = "Rejected by you"
                }
                
            } else {
                cell.bidStatus.text = "Shipment awarded to other driver."
                }
            
        }else if(val.status == 5){
            cell.editView.isHidden = true
            cell.deleteView.isHidden = true
            cell.editView.backgroundColor = UIColor.lightGray
            cell.deleteView.backgroundColor = UIColor.lightGray
            cell.bidStatus.text = "Shipment is started."
            
            if(val.customer_accepted == 1 && val.driver_accepted == 1){
                cell.bidStatus.text = "Shipment is started."
            } else {
                cell.bidStatus.text = "Shipment started by another driver."
                }
            
        }else if(val.status == 6){
            cell.editView.isHidden = true
            cell.deleteView.isHidden = true
            cell.editView.backgroundColor = UIColor.lightGray
            cell.deleteView.backgroundColor = UIColor.lightGray
            cell.bidStatus.text = "Shipment is completed."
            
            if(val.customer_accepted == 1 && val.driver_accepted == 1){
                cell.bidStatus.text = "Shipment is completed."
            } else {
                cell.bidStatus.text = "Shipment completed by another driver."
                }
        } else {
            cell.editView.isHidden = true
            cell.deleteView.isHidden = true
            cell.editView.backgroundColor = UIColor.lightGray
            cell.deleteView.backgroundColor = UIColor.lightGray
        }
        
        cell.bidAmount.text = "$\(val.amount ?? "")"
        cell.bidDate.text = self.convertTimestampToDate(val.date ?? 0, to: "dd MMM YYYY")
        cell.fromCity.text = val.shipment_detail?.from_city
        cell.toCity.text = val.shipment_detail?.to_city
        cell.message.text = val.message
        
        cell.editButton = {
            if val.status == 2 || val.status == 1 || val.status == 3 {
                
                if(val.customer_accepted == 2 && val.status == 2){
                    self.updateBidLabel.text = "Update rejected bid amount"
                    self.isRejectedBid = true
                } else {
                    self.updateBidLabel.text = "Update bid amount"
                    self.isRejectedBid = false
                }
                
                self.selectedBid = self.bidData.data[indexPath.row]
                self.updateAmountTextfield.text = val.amount ?? "Enter amount"
                self.updateMessageTextField.text = val.message ?? "Write your message"
                self.popupView.isHidden = false
            }
        }
        
        cell.deleteButton = {
            if val.status == 2 || val.status == 1 || val.status == 3 {
                self.selectedBid = self.bidData.data[indexPath.row]
                self.confirmDeletePopupView.isHidden = false
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard!.instantiateViewController(withIdentifier: "DriverSingleShipmentVC") as! DriverSingleShipmentVC
        myVC.id = self.bidData.data[indexPath.row].shipment_detail?.id ?? 0
        self.navigationController!.pushViewController(myVC, animated: true)
    }
}

extension BidsVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let searchText = (self.searchField.text ?? "") + string
        self.filterBidData = []
        for val in self.bidData.data {
            if (val.shipment_detail?.from_city ?? "").lowercased().contains(searchText.lowercased()) || (val.shipment_detail?.to_city ?? "").lowercased().contains(searchText.lowercased()) {
                self.filterBidData.append(val)
            }
        }
        
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                if searchText.count == 1 {
                    self.filterBidData = self.bidData.data
                }
            }
        }
        
        self.tableView.reloadData()
        return true
    }
}

class BidsTableViewCell: UITableViewCell{
    //MARK: IBOutlets
    @IBOutlet weak var bidStatus: DesignableUILabel!
    @IBOutlet weak var bidAmount: DesignableUILabel!
    @IBOutlet weak var bidDate: DesignableUILabel!
    @IBOutlet weak var fromCity: DesignableUILabel!
    @IBOutlet weak var toCity: DesignableUILabel!
    @IBOutlet weak var message: DesignableUILabel!
    
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var deleteView: UIView!
    
    var editButton:(()-> Void)? = nil
    var deleteButton:(()-> Void)? = nil
    
    @IBAction func editAction(_ sender: Any) {
        if let editButton = self.editButton {
            editButton()
        }
    }
    
    @IBAction func trashAction(_ sender: Any) {
        if let deleteButton = self.deleteButton {
            deleteButton()
        }
    }
}
