//
//  DashBoardVC.swift
//  SYM
//
//  Created by AM on 18/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialBottomNavigation


class DashBoardVC: UITabBarController,MDCBottomNavigationBarDelegate {
    
    var isFirstTime = true
    let bottomNavBar = MDCBottomNavigationBar()
    var dashboardViewControllers: [UIViewController] = []
    let tabBarItem1 = UITabBarItem( title: "Home", image: UIImage(named: "home(5)"), tag: 0 )
    let tabBarItem2 = UITabBarItem( title: "Shipment",   image: UIImage(named: "tracking-1"), tag: 1 )
    let tabBarItem3 = UITabBarItem( title: "Messages", image: UIImage(named: "email(5)"), tag: 2 )
    let tabBarItem4 = UITabBarItem( title: "Rate/Review", image: UIImage(named: "star"), tag: 3 )
    let tabBarItem5 = UITabBarItem( title: "Bids", image: UIImage(named: "auction(1)"), tag: 4 )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarItem.accessibilityValue = "10 unread emails"
        dashboardViewControllers = [self.newColoredViewController(controller: "DriverHomeVC"),
                                          self.newColoredViewController(controller: "ShipmentsVC"),
                                          self.newColoredViewController(controller: "DriverMessagesVC"),
                                          self.newColoredViewController(controller: "RateAndReviewVC"),
                                    self.newColoredViewController(controller: "BidsVC")]
        self.viewControllers = dashboardViewControllers
        self.bottomNavBar.unselectedItemTintColor = .lightGray
        
        self.bottomNavBar.selectedItemTintColor = primaryColor
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeTab(_:)), name: NSNotification.Name(N_CHANGE_CURRENT_TAB), object: nil)
        self.setNavigationBar("menu", nil)
        self.setNavTitle(title: "Menu")
        NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_CURRENT_TAB), object: nil,userInfo: ["index":self.selectedIndex])
        self.view.isUserInteractionEnabled = true
    }
    
    private func newColoredViewController(controller: String)->UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controller)
    }
    
    func bottomNavigationBar(_ bottomNavigationBar: MDCBottomNavigationBar, didSelect item: UITabBarItem) {
            print("did select item \(item.tag)")
        self.selectedViewController = self.viewControllers![item.tag]
        }
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.isUserInteractionEnabled = true
    }

    override func viewWillAppear(_ animated: Bool) {
        bottomNavBar.items = [ tabBarItem1, tabBarItem2, tabBarItem3 ,tabBarItem4 , tabBarItem5]
        if isFirstTime == true {
            self.selectedIndex = 1
            self.bottomNavBar.selectedItem = self.bottomNavBar.items[1]
            self.selectedIndex = 0
            self.bottomNavBar.selectedItem = self.bottomNavBar.items[0]
            self.isFirstTime = false
        }
        self.navigationController?.isNavigationBarHidden = false
        self.view.isUserInteractionEnabled = true
    }

    @objc func changeTab(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_CHANGE_CURRENT_TAB), object: nil)
        if let index = notif.userInfo?["index"] as? Int{
            if index < self.bottomNavBar.items.count {
            self.selectedIndex = index
            self.bottomNavBar.selectedItem = self.bottomNavBar.items[index]
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeTab(_:)), name: NSNotification.Name(N_CHANGE_CURRENT_TAB), object: nil)
    }
    
    
    init()
     {
         super.init(nibName: nil, bundle: nil)
         commonBottomNavigationTypicalUseSwiftExampleInit()
     }

     @available(*, unavailable)
     required init?(coder aDecoder: NSCoder)
     {
         super.init(coder: aDecoder)
         commonBottomNavigationTypicalUseSwiftExampleInit()
     }

     // Bottom Bar Customization
     func commonBottomNavigationTypicalUseSwiftExampleInit()
     {
         view.backgroundColor = .lightGray
         view.addSubview(bottomNavBar)

         // Always show bottom navigation bar item titles.
         bottomNavBar.titleVisibility = .selected
         bottomNavBar.alignment = .justifiedAdjacentTitles
//         bottomNavBar.elevation = ShadowElevation(rawValue: 10)

         // Cluster and center the bottom navigation bar items.
         bottomNavBar.alignment = .centered

         // Add items to the bottom navigation bar.

         // Select a bottom navigation bar item.
         bottomNavBar.selectedItem = tabBarItem1;
         bottomNavBar.delegate = self
     }


     override func viewWillLayoutSubviews()
     {
         super.viewWillLayoutSubviews()
         layoutBottomNavBar()
     }

     #if swift(>=3.2)
     @available(iOS 11, *)
     override func viewSafeAreaInsetsDidChange()
     {
         super.viewSafeAreaInsetsDidChange()
         layoutBottomNavBar()
     }
     #endif

     // Setting Bottom Bar
     func layoutBottomNavBar()
     {
         let size = bottomNavBar.sizeThatFits(view.bounds.size)
         let bottomNavBarFrame = CGRect( x: 0,
                                         y: view.bounds.height - size.height,
                                         width: size.width,
                                         height: size.height)
         bottomNavBar.frame = bottomNavBarFrame
     }

 }
