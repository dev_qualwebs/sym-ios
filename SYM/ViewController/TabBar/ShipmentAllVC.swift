//
//  ShipmentAllVC.swift
//  SYM
//
//  Created by AM on 19/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class ShipmentAllVC: UIViewController, ApplyFilter {
    
    func applyShipmentFilter(val: [String : Any]) {
        self.param = val
        if(K_CURRENT_CATEGORY != 6){
            self.getData(id: K_CURRENT_CATEGORY)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView == self.tableView){
            if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && (scrollView.contentOffset.y > 50) && !isLoadingList){
                self.isLoadingList = true
                self.loadMoreItemsForList()
            }
        }
    }
    
    func loadMoreItemsForList(){
        pagination += 1
        if(K_CURRENT_CATEGORY != 6){
            self.getData(id: K_CURRENT_CATEGORY)}
    }
    
    
    //Mark: IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    @IBOutlet weak var progressView: StepProgressView!
    @IBOutlet weak var popupLabel: DesignableUILabel!

    @IBOutlet weak var popUpView: UIView!
    
    
    //Mark: Properties

    var shipmentData = [GetAllCommonShipment]()
    var refreshControl = UIRefreshControl()
    var isLoadingList = false
    var pagination = 1
    var param = [
        "category":0,
        "city": 0,
        "dropoff_end_date":"",
        "dropoff_start_date":"",
        "offset":"1",
        "pickup_end_date":"",
        "pickup_start_date":"",
        "source_city":"",
        "dropoff_city":"",
        "transport_date":"Invalid date",
        "destination_city": "",
        "destination_state": "",
        "flag": "3",
        "read_status": "",
        "source_state": "",
        "totalShipment": 0,
    ] as! [String:Any]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 270
        self.tableView.rowHeight = UITableView.automaticDimension
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        self.progressView.viewShipment={
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavigationBar("menu", nil)
        setNavTitle(title: "Shipments")
        if(Singleton.shared.userType.role != 1){
            self.progressView.showAnimatingDots()
            self.progressView.showHideView={
                if(Singleton.shared.progressShipment?.count == 0){
                    self.progressView.isHidden = true
                }else{
                    for val in Singleton.shared.progressShipment!{
                        if(val.driver_status == 5){
                            self.progressView.isHidden = false
                        }
                    }
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        switch K_CURRENT_CATEGORY {
        case 0:
            self.getData(id: 0)
            break
        case 1:
            if((Singleton.shared.recommendedShipment?.count ?? 0) == 0){
                self.getData(id: 1)
            } else {
                self.shipmentData = Singleton.shared.recommendedShipment ?? []
                tableView.reloadData()
            }
            break
        case 2:
            if((Singleton.shared.activeShipment?.count ?? 0) == 0){
                self.getData(id: 2)
            }else {
                self.shipmentData = Singleton.shared.activeShipment ?? []
                tableView.reloadData()
            }
            break
        case 3:
            if((Singleton.shared.wonShipment?.count ?? 0) == 0){
                self.getData(id: 3)
            }else {
                self.shipmentData = Singleton.shared.wonShipment ?? []
                tableView.reloadData()
            }
            break
        case 4:
            if((Singleton.shared.progressShipment?.count ?? 0) == 0){
                self.getData(id: 4)
            }else {
                self.shipmentData = Singleton.shared.progressShipment ?? []
                tableView.reloadData()
            }
            break
        case 5:
            if((Singleton.shared.completeShipment?.count ?? 0) == 0){
                self.getData(id: 5)
            }else {
                self.shipmentData = Singleton.shared.completeShipment ?? []
                tableView.reloadData()
            }
            break
        case 6:
            if((Singleton.shared.disputedShipment?.count ?? 0) == 0){
                self.getDisputedData()
            }else {
                self.shipmentData = Singleton.shared.disputedShipment ?? []
                tableView.reloadData()
            }
            break
        default:
            break
        }
    }
    
    @objc func refresh() {
        self.refreshControl.endRefreshing()
        if(K_CURRENT_CATEGORY != 6){
            self.getData(id: K_CURRENT_CATEGORY)
        } else {
            getDisputedData()
        }
    }
    
    
    func getDisputedData(){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_DISPUTE_PROVIDER_SHIPMENT, method: .get, parameter: nil, objectClass: GetAllDisputedShipmentData.self, requestCode: U_DISPUTE_PROVIDER_SHIPMENT, userToken: nil) { (responseData) in
            self.shipmentData = responseData.response ?? []
            self.tableView.reloadData()
        }
    }
    
    func secondsToHoursMinutesSeconds(seconds: Int) -> (Int,Int, Int, Int) {
        let days = (seconds / 86400)
        let hours = ((seconds % 86400) / 3600)
        let minutes = ((seconds % 3600) / 60)
        let seconds = ((seconds % 3600) % 60)
        return (days,hours,minutes,seconds)
    }
    
    
    func getData(id: Int){
        ActivityIndicator.show(view: self.view)
        self.param["flag"] = id
        self.param["offset"] = "\(pagination)"
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_COMMON_SHIPMENTS, method: .post, parameter: self.param, objectClass: GetAllCommonShipmentResponse.self, requestCode: U_COMMON_SHIPMENTS, userToken: nil) { (responseData) in
            
            if(self.shipmentData.count == 0 || self.pagination == 1 ){
                self.shipmentData = responseData.response?.data ?? []
                self.isLoadingList = false
            } else if (responseData.response?.data?.count ?? 0) > 0{
                for i in 0..<(responseData.response?.data?.count ?? 0) {
                    self.shipmentData.append(responseData.response?.data?[i] ?? GetAllCommonShipment())
                }
                self.isLoadingList = false
            }
            
            if((responseData.response?.data?.count ?? 0) == 0){
                if self.pagination > 1 {
                    self.pagination -= 1
                }
                self.isLoadingList = false
            }
            
            
            switch id {
            case 0:
                Singleton.shared.allShipment = self.shipmentData
                //   self.shipmentData = Singleton.shared.allShipment
                self.tableView.reloadData()
                break
            case 1:
                Singleton.shared.recommendedShipment = self.shipmentData
                //   self.shipmentData = Singleton.shared.recommendedShipment ?? []
                self.tableView.reloadData()
                break
            case 2:
                Singleton.shared.activeShipment = self.shipmentData
                // self.shipmentData = Singleton.shared.activeShipment ?? []
                self.tableView.reloadData()
                break
            case 3:
                Singleton.shared.wonShipment = self.shipmentData
                // self.shipmentData = Singleton.shared.wonShipment ?? []
                self.tableView.reloadData()
                break
            case 4:
                Singleton.shared.progressShipment = self.shipmentData
                //  self.shipmentData = Singleton.shared.progressShipment ?? []
                self.tableView.reloadData()
                break
            case 5:
                Singleton.shared.completeShipment = self.shipmentData
                //  self.shipmentData = Singleton.shared.completeShipment ?? []
                self.tableView.reloadData()
                break
            default:
                break
            }
            ActivityIndicator.hide()
        }
    }
    
    
    @IBAction func popupCancelAction(_ sender: Any) {
        self.popUpView.isHidden = true
    }
    
    
    
    @IBAction func filterBtnPressed(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverShipmentFilterVC") as! DriverShipmentFilterVC
        myVC.filterDelegate = self
        myVC.param = self.param
        self.present(myVC, animated: true, completion: nil)
    }
}


extension ShipmentAllVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.shipmentData.count == 0){
            self.noDataLabel.isHidden = false
        }else{
            self.noDataLabel.isHidden = true
        }
        return self.shipmentData.count ?? 0
    }
    
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShipmentAllTableViewCell", for: indexPath) as! ShipmentAllTableViewCell
        let data = shipmentData[indexPath.row]
        var shipmentStatus = Int()
        if(data.lookingFor == 1){
            shipmentStatus = data.driver_status ?? 0
        }else if(data.lookingFor == 2){
            shipmentStatus = data.loader_status ?? 0
        }else if(data.lookingFor == 3){
            shipmentStatus = data.driver_status ?? 0
        }else{
            shipmentStatus = data.driver_status ?? 0
        }
        let chatId =  "\(data.user_id ?? 0)" + "_" + "\(data.id ?? 0)" + "_" + "\(Singleton.shared.userType.user_id ?? 0)"
        ref.child("messages/\(chatId)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists(){
                cell.messageBtn.backgroundColor = greenColor
                cell.messageBtn.isUserInteractionEnabled = true
                cell.messageBtn.isHidden = false
            }else{
                cell.messageBtn.backgroundColor = .lightGray
                cell.messageBtn.isUserInteractionEnabled = false
                cell.messageBtn.isHidden = true
            }
        })
        
        if(data.awarded_to == nil){
        if(data.shipment_title != nil && data.shipment_title != ""){
            cell.awardedToLabel.text = data.shipment_title ?? ""
            cell.awardedToLabel.isHidden = false
        } else {
            cell.awardedToLabel.isHidden = true
        }
        } else {
            cell.awardedToLabel.text = "Awarded to \(data.awarded_to?.first_name ?? "")  \(data.awarded_to?.last_name ?? "")"
            cell.awardedToLabel.isHidden = false
        }
        
        let dropDate = Date(timeIntervalSince1970: TimeInterval((data.dropoff_date ?? 0)))
        let pickDate = Date(timeIntervalSince1970: TimeInterval((data.pickup_date ?? 0)))
        
        cell.lblSource.text = data.awarded_shipment == 0 ? "\(data.from_city ?? "")" : "\(data.from_name ?? "")"
        cell.lblDestination.text = data.awarded_shipment == 0 ? "\(data.to_city ?? "")" : "\(data.to_name ?? "")"
        cell.startDate.text = "Date: " + "\(convertTimestampToDate(Int(data.pickup_date ?? 0), to: "dd-MM-yyy"))"
        cell.dropDate.text = "Date: " + "\(convertTimestampToDate(Int(data.dropoff_date ?? 0), to: "dd-MM-yyy"))"
        
        cell.remainingTime = data.remaining_time ?? 0
        
        let remainingTime = data.remaining_time ?? 0
        
        let (d,h,m,s) = self.secondsToHoursMinutesSeconds(seconds: remainingTime)
       
        if(K_CURRENT_CATEGORY == 0 || K_CURRENT_CATEGORY == 1){
        if(remainingTime > 0){
            cell.tomeLeftLabel.isHidden = false
        if(d >= 1){
            if(d == 3){
            cell.tomeLeftLabel.text = "BIDDING ENDS IN : \(d) days"
            } else if d < 3 {
                cell.startTimer()
            } else {
                cell.tomeLeftLabel.isHidden = true
            }
        } else {
            cell.startTimer()
        }
        } else {
            cell.tomeLeftLabel.text = "BIDDING CLOSED"
        }
        } else {
            cell.tomeLeftLabel.isHidden = true
        }
        
        cell.awardedView.isHidden = true
        
        if((data.own_bids?.count ?? 0) > 0) {
            if(data.own_bids?[0].driver_accepted == 1 || data.own_bids?[0].customer_accepted == 1){
                cell.awardedView.isHidden = false
                cell.tomeLeftLabel.isHidden = true
            }
            
            if(data.own_bids?[0].driver_accepted == 2 || data.own_bids?[0].customer_accepted == 2){
                cell.awardedView.isHidden = true
                cell.tomeLeftLabel.isHidden = true
            }
        }
        
        if(K_CURRENT_CATEGORY == 6){
            cell.tomeLeftLabel.text = "Dispute reason : - \(data.reason ?? "")"
            cell.tomeLeftLabel.isHidden = false
            if cell.tomeLeftLabel.calculateMaxLines() > 1 {
               cell.tomeLeftLabel.numberOfLines = 1
               cell.viewMoreButton.isHidden = false
            } else {
                cell.viewMoreButton.isHidden = true
            }
        }
        
        cell.shipmentAwardedOther = false
        
        if(Singleton.shared.userDetail.role != 1){
            if(K_CURRENT_CATEGORY != 6){
                if(shipmentStatus == 2){
                    if(data.own_bids?.count ?? 0 > 0){
                        if(data.own_bids?[0].driver_accepted == 2) {
                            cell.lblCurrentStatus.text = "Rejected by you"
                        } else if (data.own_bids?[0].customer_accepted == 2){
                            cell.lblCurrentStatus.text = "Rejected by customer"
                        }
                        else {
                            cell.lblCurrentStatus.text = K_CURRENT_CATEGORY == 2 ? "Under review" : "Bid Placed"
                        }
                    }else{
                        cell.lblCurrentStatus.text = K_CURRENT_CATEGORY == 2 ? "Under review" : "Open For Bid"
                    }
                }
                else if(shipmentStatus == 1){
                    cell.lblCurrentStatus.text = "Open For Bid"
                    if(data.own_bids?.count ?? 0 > 0){
                        if(data.own_bids?[0].driver_accepted == 2) {
                            cell.lblCurrentStatus.text = "Rejected by you"
                        } else if (data.own_bids?[0].customer_accepted == 2){
                            cell.lblCurrentStatus.text = "Rejected by customer"
                        }
                        else {
                            cell.lblCurrentStatus.text = K_CURRENT_CATEGORY == 2 ? "Under review" : "Bid Placed"
                        }
                    }
                }else if(shipmentStatus == 3){
                    if(data.own_bids?.count ?? 0 > 0){
                        if(data.own_bids?[0].customer_accepted == 1){
                            cell.lblCurrentStatus.text = "Accepted By Customer"
                        } else {
                            cell.shipmentAwardedOther = true
                            cell.tomeLeftLabel.isHidden = true
                            cell.lblCurrentStatus.text = "Shipment awarded to other driver."
                            }
                        
                        if(data.own_bids?[0].driver_accepted == 2) {
                            cell.lblCurrentStatus.text = "Rejected by you"
                        } else if (data.own_bids?[0].customer_accepted == 2){
                            cell.lblCurrentStatus.text = "Rejected by customer"
                        }
                    } else {
                        cell.shipmentAwardedOther = true
                        cell.tomeLeftLabel.isHidden = true
                    cell.lblCurrentStatus.text = "Shipment awarded to other driver."
                    }
                } else if(shipmentStatus == 4){
                    if(data.own_bids?.count ?? 0 > 0){
                        if(data.own_bids?[0].customer_accepted == 1){
                            cell.lblCurrentStatus.text = "Shipment awarded to you."
                            
                            if(data.own_bids?[0].driver_accepted == 1){
                                cell.lblCurrentStatus.text = "Ready to start the shipment"
                            }
                            
                            if(data.own_bids?[0].driver_accepted == 2){
                                cell.lblCurrentStatus.text = "Rejected by you"
                            }
                            
                            if(data.own_bids?[0].customer_accepted == 2){
                                cell.lblCurrentStatus.text = "Rejected by customer"
                            }
                            
                        } else {
                            cell.shipmentAwardedOther = true
                            cell.tomeLeftLabel.isHidden = true
                            cell.lblCurrentStatus.text = "Shipment awarded to other driver."
                            }
                    } else {
                        cell.shipmentAwardedOther = true
                        cell.tomeLeftLabel.isHidden = true
                    cell.lblCurrentStatus.text = "Shipment awarded to other driver."
                    }
                }else if(shipmentStatus == 5){
                    if(data.own_bids?.count ?? 0 > 0){
                        if(data.own_bids?[0].customer_accepted == 1 && data.own_bids?[0].driver_accepted == 1){
                            cell.lblCurrentStatus.text = "Shipment is started."
                        } else {
                            cell.shipmentAwardedOther = true
                            cell.tomeLeftLabel.isHidden = true
                            cell.lblCurrentStatus.text = "Shipment started by another driver."
                            }
                    } else {
                        cell.shipmentAwardedOther = true
                        cell.tomeLeftLabel.isHidden = true
                    cell.lblCurrentStatus.text = "Shipment started by another driver."
                    }
                }else if(shipmentStatus == 6){
                    if(data.own_bids?.count ?? 0 > 0){
                        if(data.own_bids?[0].customer_accepted == 1 && data.own_bids?[0].driver_accepted == 1){
                            cell.lblCurrentStatus.text = "Shipment is completed."
                        } else {
                            cell.shipmentAwardedOther = true
                            cell.tomeLeftLabel.isHidden = true
                            cell.lblCurrentStatus.text = "Shipment completed by another driver."
                            }
                    } else {
                        cell.shipmentAwardedOther = true
                        cell.tomeLeftLabel.isHidden = true
                    cell.lblCurrentStatus.text = "Shipment completed by another driver."
                    }
                }}else {
                    cell.lblCurrentStatus.text = " Shipment marked as disputed."
                }
            
            
            if(shipmentStatus == 8){
                cell.lblCurrentStatus.text = "Shipment cancelled! User requested refund."
            }
        }
        
        cell.startButton = {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DRIVER_START_SHIPMENT + "\(data.id ?? 0)&startingAs=\(data.lookingFor ?? 1)", method: .get, parameter: nil, objectClass: Response.self, requestCode: U_DRIVER_START_SHIPMENT, userToken: nil) { (response) in
                self.refresh()
                ActivityIndicator.hide()
                Singleton.shared.progressShipment = []
                Singleton.shared.showToast(text: "Shipment started Successfully", color: successGreen)
            }
        }
        
        cell.messageButton = {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            myVC.jobDetail = ChatDetail(sender_id: Singleton.shared.userType.user_id ?? 0, receiver_id: data.user_id ?? 0, receiver_name: (data.first_name ?? "") + " " +  (data.last_name ?? ""), sender_name:(Singleton.shared.userType.first_name ?? "") + " " + (Singleton.shared.userType.last_name ?? "") , sender_image: (Singleton.shared.userType.profile_image ?? ""), receiver_image: "", shipment_id: data.id ?? 0)
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        
        cell.shipmentButton = {
            let myVC = self.storyboard!.instantiateViewController(withIdentifier: "DriverSingleShipmentVC") as! DriverSingleShipmentVC
            if(K_CURRENT_CATEGORY != 6){
                myVC.id = self.shipmentData[indexPath.row].id ?? 0
            } else {
                myVC.id = self.shipmentData[indexPath.row].shipment_id ?? 0
            }
            myVC.biddingClosed = cell.tomeLeftLabel.text == "BIDDING CLOSED" ? true : false
            self.navigationController!.pushViewController(myVC, animated: true)
        }
        
        cell.viewButton = {
            self.popupLabel.text = "\(data.reason ?? "")"
            self.popUpView.isHidden = false
        }
        
        cell.lblDistance.text = (data.distance ?? "0") + " miles"
        return cell
    }
    
}

class ShipmentAllTableViewCell: UITableViewCell{
    
    @IBOutlet weak var tomeLeftLabel: UILabel!
    @IBOutlet weak var lblSource: DesignableUILabel!
    @IBOutlet weak var lblDestination: DesignableUILabel!
    @IBOutlet weak var lblDate: DesignableUILabel!
    @IBOutlet weak var lblCurrentStatus: DesignableUILabel!
    @IBOutlet weak var lblDistance: DesignableUILabel!
    @IBOutlet weak var messageBtn: CustomButton!
    @IBOutlet weak var startShipmentButton: CustomButton!
    @IBOutlet weak var startDate: DesignableUILabel!
    @IBOutlet weak var dropDate: DesignableUILabel!
    @IBOutlet weak var awardedView: UIView!
    @IBOutlet weak var awardedToLabel: UILabel!
    @IBOutlet weak var viewMoreButton: UIButton!
    
    
    var messageButton:(()-> Void)? = nil
    var shipmentButton:(()-> Void)? = nil
    var startButton:(()-> Void)? = nil
    var viewButton:(()-> Void)? = nil
    
    var countTimer:Timer!
    var remainingTime = Int()
    var shipmentAwardedOther = false
    
    //MARK: IBActions
    @IBAction func messageAction(_ sender: Any) {
        if let messageButton = self.messageButton {
            messageButton()
        }
    }
    
    @IBAction func shipmentAction(_ sender: Any) {
        if let shipmentButton = self.shipmentButton {
            shipmentButton()
        }
    }
    
    @IBAction func startAction(_ sender: Any) {
        if let startButton = self.startButton {
            startButton()
        }
    }
    
    @IBAction func viewMoreAction(_ sender: Any) {
        if let viewButton = viewButton {
            viewButton()
        }
    }
    
    
    
    func startTimer() {
        countTimer = Timer.scheduledTimer(timeInterval: 1 , target: self, selector: #selector(changeTitle), userInfo: nil, repeats: true)
    }
    
    @objc func changeTitle()
    {
        if(!shipmentAwardedOther){
        if(remainingTime > 0){
        remainingTime -= 1
        let (d,h,m,s) = secondsToHoursMinutesSeconds(seconds: remainingTime)
            if(d >= 1){
                if(d == 3){
                tomeLeftLabel.text = "BIDDING ENDS IN : \(d) days"
                } else if d < 3 {
                    tomeLeftLabel.text = "BIDDING ENDS IN : \(d) days, \(h) hr, \(m) min, \(s) sec"
                } else {
                    tomeLeftLabel.isHidden = true
                }
            } else {
                tomeLeftLabel.text = "BIDDING ENDS IN : \(h) hr, \(m) min, \(s) sec"
            }
        } else {
            tomeLeftLabel.text = "BIDDING CLOSED"
            countTimer.invalidate()
        }
        } else {
            tomeLeftLabel.isHidden = true
        }
    }
    
    func secondsToHoursMinutesSeconds(seconds: Int) -> (Int,Int, Int, Int) {
        let days = (seconds / 86400)
        let hours = ((seconds % 86400) / 3600)
        let minutes = ((seconds % 3600) / 60)
        let seconds = ((seconds % 3600) % 60)
        return (days,hours,minutes,seconds)
    }
    
    override func prepareForReuse() {
            super.prepareForReuse()
            self.countTimer?.invalidate()
            self.countTimer = nil
        }
}
