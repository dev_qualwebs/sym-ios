//
//  CancelledShipmentViewController.swift
//  SYM
//
//  Created by Sagar Pandit on 20/09/22.
//  Copyright © 2022 AM. All rights reserved.
//

import UIKit

class CancelledShipmentViewController: UIViewController {

    @IBOutlet weak var cancelledTable: ContentSizedTableView!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    
    var cancelledData = [CancelledData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
        self.setNavTitle(title: "Cancelled Shipments")
        self.setNavigationBar("back", nil)
        self.getCancelledData()
    }
    
    func getCancelledData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CANCELLED_SHIPMENT, method: .get, parameter: nil, objectClass: CanclledShipmentResponse.self, requestCode: U_GET_CANCELLED_SHIPMENT, userToken: nil) { (response) in
            self.cancelledData = response.response?.data ?? []
            self.cancelledTable.reloadData()
            ActivityIndicator.hide()
        }
    }

}


extension CancelledShipmentViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.noDataLabel.isHidden = self.cancelledData.count > 0 ? true : false
        return self.cancelledData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserDashBoardTableCell", for: indexPath) as! UserDashBoardTableCell
        let data = cancelledData[indexPath.row]
        var shipmentStatus = Int()
        if(data.lookingFor == 1){
            shipmentStatus = data.driver_status ?? 0
        }else if(data.lookingFor == 2){
            shipmentStatus = data.loader_status ?? 0
        }else if(data.lookingFor == 3){
            shipmentStatus = data.driver_status ?? 0
        }else{
            shipmentStatus = data.driver_status ?? 0
        }
        
        cell.awardedView.isHidden = true
        
        cell.shipmentTitleView.isHidden = data.shipment_title == nil ? true : false
        
        if(data.shipment_title != nil){
            cell.shipmentTitle.text = data.shipment_title ?? ""
        }
        
        cell.lblSource.text = "\(data.from_name ?? "")"
        cell.startDate.text = "Date: " + (self.convertTimestampToDate(data.pickup_date ?? 0, to: "dd-MM-yyyy"))
        cell.endDate.text = "Date: " + (self.convertTimestampToDate(data.dropoff_date ?? 0, to: "dd-MM-yyyy"))
        
        cell.postedDate.text = self.convertTimestampToDate(data.created_date ?? 0, to: "EEE, dd-MMM-yy")
        
        cell.distance.text = "\(data.distance ?? "0") miles"
        cell.rehireButtonView.isHidden = true
     
        cell.currentStatus.text = "Cancelled"
        cell.editShipmentButton.setTitle("Repost Shipment", for: .normal)
        cell.editShipmentButton.isHidden = false
        
        cell.lblDestination.text = "\(data.to_name ?? "")"
        cell.lblTtlBids.text = "\(data.driver_bids_count ?? 0)"
        cell.shipmentButton = {
            selectedShipment = indexPath.row
            let sSVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
            sSVC.id = data.id ?? 0
            sSVC.isCancelled = true
            self.navigationController?.pushViewController(sSVC, animated: true)
        }
        
        cell.editButton = {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! PageViewController
            var imageData = [String]()
            for val in data.image_list!{
                imageData.append(val.image ?? "")
            }
            var subcategory = [Int]()
            for val in data.subcategory_name {
                subcategory.append(val.id ?? 0)
            }
            K_DRAFT_OR_EDIT = 1
            K_DRIVER_REHIRE_ID = 0
            K_IS_REPOST_CANCELLED = 1
            Singleton.shared.postYourShipment = GetPostYourShipmentData(id: data.id ?? 0, category_id: data.category_id ?? 0, discription: data.description ?? "", from_name: data.from_name ?? "", from_latitude: data.from_latitude ?? "", from_longitude: data.from_longitude ?? "", from_city: data.from_city ?? "", from_state: data.from_state ?? "0", from_zipcode: "\(data.from_zipcode ?? 0)", to_name: data.to_name ?? "", to_latitude: data.to_latitude ?? "", to_longitude: data.to_longitude ?? "", to_city: data.to_city ?? "", to_state: data.to_state ?? "0", to_zipcode: "\(data.to_zipcode ?? 0)", lookingFor: data.lookingFor ?? 0, sub_category_id: subcategory, set_budget: data.set_budget ?? 0, multiple_item: "0", distance: "\(data.distance ?? "0")", weight: Double(data.weight ?? "0.0") ?? 0.0, price: data.price ?? "0", image: imageData, pickup_date: 0, dropoff_date: 0)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        return cell
    }
}
