//
//  StripeAccountViewController.swift
//  SYM
//
//  Created by qw on 07/09/20.
//  Copyright © 2020 AM. All rights reserved.
//


import UIKit
import CoreLocation
import GooglePlaces

class StripeAccountViewController: UIViewController,UITextFieldDelegate {
    //MARK:IBOutlets
    
    @IBOutlet weak var cardNumber: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var state: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var postalCode: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var submitButton: CustomButton!
    @IBOutlet weak var routingNumber: UITextField!
    @IBOutlet weak var detailStack: UIStackView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var routingNumberView: UIView!
    @IBOutlet weak var accountNumber: DesignableUILabel!
    @IBOutlet weak var bankName: DesignableUILabel!
    @IBOutlet weak var branchName: DesignableUILabel!
    @IBOutlet weak var routeNumber: DesignableUILabel!
    @IBOutlet weak var editView: View!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var ssnNumber: UITextField!
    @IBOutlet weak var businessTaxId: UITextField!
    @IBOutlet weak var imagePath: UILabel!
    
    
    var stripeDetail:[StripeResponse]?
    var accountNo = ""
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    var isNavigationFromSignup = false
    var param = [String:Any]()
    var number = String()
    static var bankDelegate : ReloadBankData? = nil
    let picker = UIImagePickerController()
    var imageName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cardNumber.delegate = self
        self.routingNumber.delegate = self
        self.setNavTitle(title: "My Bank")
        self.setNavigationBar("back", nil)
        if(self.isNavigationFromSignup){
            self.cardNumber.text = ""
            self.address.text  = ""
            self.state.text = ""
            self.city.text = ""
            self.postalCode.text = ""
            self.routingNumber.text = ""
            self.editView.isHidden = false
            self.mainView.isHidden = true
            self.routingNumberView.isHidden = false
            self.submitButton.isHidden = false
            self.detailStack.isUserInteractionEnabled = true
        } else{
            self.editView.isHidden = true
            self.mainView.isHidden = false
            self.getAccountDetail()
        }
    }
    
    func getAccountDetail(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_RETRIEVE_STRIPE_ACCOUNT, method: .post, parameter: nil, objectClass: GetStripeResponse.self, requestCode: U_RETRIEVE_STRIPE_ACCOUNT, userToken: nil) { (response) in
            self.stripeDetail = response.response
            
            if(((self.stripeDetail?.count ?? 0) > 0)){
                self.cardNumber.text = "XXXXXXXXXXXX" + (self.stripeDetail?[0].external_accounts?.data?[0].last4 ?? "")
                self.accountNumber.text = self.stripeDetail?[0].external_accounts?.data?[0]
                self.bankName.text = "Bank   : " + (self.stripeDetail?[0].external_accounts?.data?[0].bank_name ?? "")
                self.branchName.text = "Branch   : " + (self.stripeDetail?[0].individual?.address?.city ?? "")
                self.routeNumber.text = "Routing   : " + (self.stripeDetail?[0].external_accounts?.data?[0].routing_number ?? "")
                self.routingNumber.text = self.stripeDetail?[0].external_accounts?.data?[0].routing_number ?? ""
                self.country.text = self.stripeDetail?[0].individual?.address?.country
                self.state.text = self.stripeDetail?[0].individual?.address?.state
                self.postalCode.text = self.stripeDetail?[0].individual?.address?.postal_code
                self.city.text = self.stripeDetail?[0].individual?.address?.city
                self.address.text = self.stripeDetail?[0].individual?.address?.line1
                self.submitButton.setTitle("UPDATE", for: .normal)
                self.detailStack.isUserInteractionEnabled = false
                self.submitButton.isHidden = true
            } else {
//                self.routingNumberView.isHidden = false
//                self.detailStack.isUserInteractionEnabled = true
                self.submitButton.setTitle("SUBMIT", for: .normal)
//                self.submitButton.isHidden = false
                self.cardNumber.text = ""
                self.address.text  = ""
                self.state.text = ""
                self.city.text = ""
                self.postalCode.text = ""
                self.routingNumber.text = ""
                self.editView.isHidden = false
                self.mainView.isHidden = true
                self.routingNumberView.isHidden = false
                self.submitButton.isHidden = false
                self.detailStack.isUserInteractionEnabled = true
            }
            ActivityIndicator.hide()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField==cardNumber){
            let currentCharacterCount = self.cardNumber.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.cardNumber.text = self.cardNumber.text?.applyPatternOnNumbers(pattern: "##### : ### : #############", replacmentCharacter: "#")
            return newLength <= 27
        }
        return true
    }
    
    func latLong(lat: Double,long: Double)  {
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: lat , longitude: long)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            print("Response GeoLocation : \(placemarks)")
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // Country
            if let country = placeMark.addressDictionary!["Country"] as? String {
                self.country.text = "US"
                if let city = placeMark.addressDictionary!["City"] as? String {
                    self.city.text = city
                    
                    // State
                    if let state = placeMark.addressDictionary!["State"] as? String{
                        self.state.text = state
                        
                        if let street = placeMark.addressDictionary!["Street"] as? String{
                            print("Street :- \(street)")
                            let str = street
                            let streetNumber = str.components(
                                separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                            print("streetNumber :- \(streetNumber)" as Any)
                            
                            // ZIP
                            if let zip = placeMark.addressDictionary!["ZIP"] as? String{
                                self.postalCode.text = zip.replacingOccurrences(of: " ", with: "")
                                // Location name
                                if let locationName = placeMark?.addressDictionary?["Name"] as? String {
                                    print("Location Name :- \(locationName)")
                                    // Street address
                                    if let thoroughfare = placeMark?.addressDictionary!["Thoroughfare"] as? NSString {
                                        self.address.text = "\(thoroughfare)"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    
    func uploadImage(){
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
        }
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK:IBActions
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addressButtonAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue:UInt(GMSPlaceField.name.rawValue) |
                                                  UInt(GMSPlaceField.placeID.rawValue) |
                                                  UInt(GMSPlaceField.coordinate.rawValue) |
                                                  GMSPlaceField.addressComponents.rawValue |
                                                  GMSPlaceField.formattedAddress.rawValue)
        // autocompleteController.placeFields = .formattedAddress
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        filter.country = "US"
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        autocompleteController.modalPresentationStyle = .overFullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func editAccountAction(_ sender: Any) {
        let alert = UIAlertController(title: "Edit Account Detail", message: "Are you sure?", preferredStyle: .alert)
        
        let yesBtn = UIAlertAction(title:"Yes", style: .default) { (UIAlertAction) in
            self.cardNumber.text = ""
            if (self.stripeDetail?.count ?? 0) > 0 {
                self.state.text = self.stripeDetail?[0].individual?.address?.state ?? ""
                self.city.text = self.stripeDetail?[0].individual?.address?.city ?? ""
                self.postalCode.text = self.stripeDetail?[0].individual?.address?.postal_code ?? ""
                self.routingNumber.text = self.stripeDetail?[0].external_accounts?.data?[0].routing_number ?? ""
                self.cardNumber.text = "XXXXXXXXXXXX" + (self.stripeDetail?[0].external_accounts?.data?[0].last4 ?? "")
            }
            self.editView.isHidden = false
            self.mainView.isHidden = true
            self.routingNumberView.isHidden = false
            self.submitButton.isHidden = false
            self.detailStack.isUserInteractionEnabled = true
        }
        let noBtn = UIAlertAction(title: "No", style: .default){
            (UIAlertAction) in
            self.editView.isHidden = true
            self.mainView.isHidden = false
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(yesBtn)
        alert.addAction(noBtn)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func cancelEdit(_ sender: Any) {
        self.editView.isHidden = true
        self.mainView.isHidden = false
    }
    
    
    @IBAction func infoAction(_ sender: Any) {
        self.popupView.isHidden = false
    }
    
    @IBAction func hideCardPopup(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
    @IBAction func addImageAction(_ sender: Any) {
        self.picker.delegate = self
        self.uploadImage()
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if(routingNumber.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter your Routing Number", color: errorRed)
        }else if(cardNumber.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter your Account Number", color: errorRed)
        }else if(ssnNumber.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter your SSN Number", color: errorRed)
        }else if(businessTaxId.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter your Business Tax ID", color: errorRed)
        }else if(country.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter your Country", color: errorRed)
        }else if(state.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter your State", color: errorRed)
        }else if(city.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter your City", color: errorRed)
        }else if(postalCode.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter your Postal Code", color: errorRed)
        }else if(address.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter your Address", color: errorRed)
        }else if(imagePath.text!.isEmpty){
            Singleton.shared.showToast(text: "Upload document", color: errorRed)
        }else {
            var account = self.cardNumber.text ?? ""
            account = account.replacingOccurrences(of: ":", with: "")
            account = account.replacingOccurrences(of: " ", with: "")
            
            ActivityIndicator.show(view: self.view)
            if(self.isNavigationFromSignup){
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_SIGNUP, method: .post, parameter: param, objectClass: Login.self, requestCode: U_SIGNUP, userToken: nil) { (responseData) in
                    UserDefaults.standard.set(responseData.response.token ?? "", forKey: UD_TOKEN)
                    Singleton.shared.userType.role = responseData.response.data.role ?? 0
                    Singleton.shared.userType.first_name = responseData.response.data.first_name
                    Singleton.shared.userType.phone = responseData.response.data.phone ?? ""
                    Singleton.shared.userType.username = responseData.response.data.username
                    Singleton.shared.userType.last_name = responseData.response.data.last_name
                    Singleton.shared.userType.user_id = responseData.response.data.id
                    Singleton.shared.userType.email = responseData.response.data.email
                    Singleton.shared.userType.profile_image = responseData.response.data.profile_image
                    Singleton.shared.saveTypeOfUser(data: Singleton.shared.userType)
                    UserDefaults.standard.removeObject(forKey: UD_SHIPMENT_DATA)
                    
                    
                    let someParam:[String:Any] = [
                        "account_number": account,
                        "bank_city" :self.city.text ?? "",
                        "bank_country" : self.country.text ?? "",
                        "line1" : self.address.text ?? "",
                        "line2": "",
                        "routing_number": self.routingNumber.text ?? "",
                        "bank_postal_code": self.postalCode.text ?? "",
                        "bank_state" : self.state.text ?? "",
                        "ssn": self.ssnNumber.text ?? "",
                        "business_tax_id": self.businessTaxId.text ?? "",
                        "document_file_path": self.imagePath.text ?? ""
                    ]
                    
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_CREATE_STRIPE_ACCOUNT, method: .post, parameter: someParam, objectClass: Response.self, requestCode: U_CREATE_STRIPE_ACCOUNT, userToken: nil) { (response) in
                        ActivityIndicator.hide()
                        self.submitButton.isHidden = true
                        self.routingNumberView.isHidden = true
                        self.detailStack.isUserInteractionEnabled = false
                        self.editView.isHidden = true
                        self.mainView.isHidden = false
                        Singleton.shared.showToast(text: "Successfully created Account", color: successGreen)
                        
                        if responseData.response.data.role ?? 0 == 1{
                            let myVC = self.storyboard!.instantiateViewController(withIdentifier: "UserTabViewController") as! UserTabViewController
                            self.navigationController?.pushViewController(myVC, animated: true)
                        }
                        else {
//                            let myVC = self.storyboard!.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                            let myVC = self.storyboard!.instantiateViewController(withIdentifier: "DriverSecurityCheckVC") as! DriverSecurityCheckVC
                            myVC.isComingDirect = true
                            self.navigationController?.pushViewController(myVC, animated: true)
                        }
                    }
                }
                
            }else {
                let param:[String:Any] = [
                    "account_number": account,
                    "bank_city" :self.city.text ?? "",
                    "bank_country" : self.country.text ?? "",
                    "line1" : self.address.text ?? "",
                    "line2": "",
                    "routing_number": self.routingNumber.text ?? "",
                    "bank_postal_code": self.postalCode.text ?? "",
                    "bank_state" : self.state.text ?? "",
                    "ssn": self.ssnNumber.text ?? "",
                    "business_tax_id": self.businessTaxId.text ?? "",
                    "document_file_path": self.imagePath.text ?? ""
                ]
                
                if(self.submitButton.titleLabel?.text == "SUBMIT"){
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_CREATE_STRIPE_ACCOUNT, method: .post, parameter: param, objectClass: Response.self, requestCode: U_CREATE_STRIPE_ACCOUNT, userToken: nil) { (response) in
                        ActivityIndicator.hide()
                        self.submitButton.isHidden = true
                        self.routingNumberView.isHidden = true
                        self.detailStack.isUserInteractionEnabled = false
                        self.editView.isHidden = true
                        self.mainView.isHidden = false
                        self.getAccountDetail()
                        if(K_COMING_DIRECT_AFTER_SIGNUP_DRIVER == 1){
                            K_COMING_DIRECT_AFTER_SIGNUP_DRIVER = 0
//                            let myVC = self.storyboard!.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                            let myVC = self.storyboard!.instantiateViewController(withIdentifier: "DriverSecurityCheckVC") as! DriverSecurityCheckVC
                            myVC.isComingDirect = true
                            self.navigationController?.pushViewController(myVC, animated: true)
                        } else if(K_COMING_DIRECT_AFTER_SIGNUP_DRIVER == 2) {
                            StripeAccountViewController.bankDelegate?.getBankData()
                            self.navigationController?.popViewController(animated: true)
                        }
                        Singleton.shared.showToast(text: "Bank account added successfully", color: successGreen)
                    }
                }else {
                    let param:[String:Any] = [
                        "account_number": account,
                        "city" :self.city.text ?? "",
                        "country" : self.country.text ?? "",
                        "line1" : self.address.text ?? "",
                        "line2": "",
                        "routing_number": self.routingNumber.text ?? "",
                        "postal_code": self.postalCode.text ?? "",
                        "state" : self.state.text ?? "",
                        "ssn": self.ssnNumber.text ?? "",
                        "business_tax_id": self.businessTaxId.text ?? "",
                        "document_file_path": self.imagePath.text ?? ""
                    ]
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_STRIPE_ACCOUNT, method: .post, parameter: param, objectClass: Response.self, requestCode: U_UPDATE_STRIPE_ACCOUNT, userToken: nil) { (response) in
                        ActivityIndicator.hide()
                        self.routeNumber.text = self.routingNumber.text
                        self.branchName.text = self.city.text
                        self.routingNumberView.isHidden = true
                        self.submitButton.isHidden = true
                        self.detailStack.isUserInteractionEnabled = false
                        self.editView.isHidden = true
                        self.mainView.isHidden = false
                        self.getAccountDetail()
                        UserDefaults.standard.setValue(self.cardNumber.text ?? "", forKey: "card_number")
                        Singleton.shared.showToast(text: "Stripe account updated successfully", color: successGreen)
                    }
                }
            }
        }
    }
    
}

extension StripeAccountViewController: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        //  self.address.text = place.formattedAddress
        self.latitude = place.coordinate.latitude
        self.longitude = place.coordinate.longitude
        self.latLong(lat: Double(self.latitude), long:Double(self.longitude))
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}


extension StripeAccountViewController: UIImagePickerControllerDelegate ,UINavigationControllerDelegate  {
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        let imageData:NSData = selectedImage.pngData()! as NSData
        
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if let cropImage = selectedImage as? UIImage {
            let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
            let params = [
                "file": data!
            ] as [String : Any]
            
            // HTTP Request for upload Picture
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.makeMultipartRequest(url: U_BASE + U_UPLOAD_IMAGE, fileData: data!, param: params, objectClass: UploadImage.self, fileName: imageName) { (path) in
                var url = (path as! UploadImage).response.path ?? ""
                self.imagePath.text = url
                ActivityIndicator.hide()
            }
        }
        picker.dismiss(animated: true)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    func presentCameraSettings() {
        let alertController = UIAlertController(title: "Allow Camera to take photos",
                                                message: "Camera access is denied",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default){
            _ in
        })
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })
        self.present(alertController, animated: true)
    }
    
}
