//
//  EcommerceCategoryViewController.swift
//  SYM
//
//  Created by qw on 18/09/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class EcommerceCategoryViewController: UIViewController, SelectFromPicker {
    func selectedPickerData(val: String, pos: Int) {
        if(pos == 0){
            self.selectedCategory = 0
        }else {
        self.selectedCategory = self.categoryData[pos-1].id ?? 0
        }
      //  self.filterText.text = val
        self.getProductList(text: "")
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var categoryCollection: UICollectionView!
    @IBOutlet weak var searchField: UITextField!
   // @IBOutlet weak var filterText: DesignableUITextField!
    @IBOutlet weak var noDataFoundLabel: DesignableUILabel!
    
    var productData = [GetProductResponse]()
    var categoryData = [CategoryResponse]()
    var selectedCategory = Int()
    let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        categoryCollection.addSubview(refreshControl)
        
        self.searchField.delegate = self
        self.setNavTitle(title: "Moving Resources")
        self.setNavigationBar("back","none")
        self.getCategoryData()
        self.getProductList(text:"")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APICallingViewController.shared.getCartData { (data) in
            self.setNavigationBar("back", "none")
        }
    }
    
    
    @objc func refresh(_ sender: AnyObject) {
        self.getProductList(text: "")
    }

    
    func getProductList(text:String){
        ///text != "" ? ActivityIndicator.show(view: self.view):ActivityIndicator.hide()
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_PRODUCT + "?search=\(text)&price_sort=2&category_id=\(self.selectedCategory)", method: .get, parameter: nil, objectClass: GetProducts.self, requestCode: U_GET_PRODUCT, userToken: nil) { (response) in
            self.productData = response.response
            self.categoryCollection.reloadData()
            self.refreshControl.endRefreshing()
          //  ActivityIndicator.hide()
        }
    }
    
    func getCategoryData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_PRODUCT_CATEGORY, method: .get, parameter: nil, objectClass: GetEcommerceCategories.self, requestCode: U_GET_PRODUCT_CATEGORY, userToken: nil) { (response) in
            self.categoryData = response.response
            ActivityIndicator.hide()
        }
    }

    //MARK: IBActions
    @IBAction func filterAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        for val in self.categoryData{
            myVC.pickerData.append(val.category_name ?? "")
        }
        myVC.pickerData.insert("All Categories", at: 0)
        myVC.headingLabel = "Select Category"
        if(self.categoryData.count > 0){
         self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
}

extension EcommerceCategoryViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.noDataFoundLabel.isHidden = self.productData.count > 0 ? true : false
        return self.productData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionCell
        let val = self.productData[indexPath.row]
        cell.productName.text = val.product_name
        if((val.product_images?.count ?? 0) != 0){
        var image = val.product_images?[0].image ?? ""
            image = (val.product_images?[0].image ?? "").replacingOccurrences(of: " ", with: "%20")
            // for travelocity and test image issue
        cell.productImage.sd_setImage(with: URL(string:U_IMAGE + image), placeholderImage: #imageLiteral(resourceName: "ecommerce"))
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width/2)-15, height:250)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.openUrl(urlStr: self.productData[indexPath.row].product_link ?? "")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let a = self.searchField.text?.dropLast()
        let searchText = string == "\n" ? String(a!):((self.searchField.text ?? "") + string)
        self.getProductList(text: searchText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")
        return true
    }
}



class CategoryCollectionCell: UICollectionViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var productPrice: DesignableUILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: DesignableUILabel!
    @IBOutlet weak var productDesc: DesignableUILabel!
}
