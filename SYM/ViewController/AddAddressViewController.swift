//
//  AddAddressViewController.swift
//  ITreat
//
//  Created by qw on 12/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import GooglePlaces
import MapKit

protocol AddressAdded {
    func reloadAddressTable()
}

class AddAddressViewController: UIViewController {
    //MARK: IBOutlets
   
    @IBOutlet weak var saveButton: CustomButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var fullName: SkyFloatingLabelTextField!
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var address1: SkyFloatingLabelTextField!
    @IBOutlet weak var address2: SkyFloatingLabelTextField!
    @IBOutlet weak var countryName: SkyFloatingLabelTextField!
    @IBOutlet weak var cityName: SkyFloatingLabelTextField!
    @IBOutlet weak var postalCode: SkyFloatingLabelTextField!
    @IBOutlet weak var stateName: SkyFloatingLabelTextField!
    @IBOutlet weak var mobileNumber: SkyFloatingLabelTextField!
    
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    var addressDelegate: AddressAdded? = nil
    var addressDetail = GetAddressResponse()
    var isEditAddress = false
    var addressType = 1
    let regionRadius: Double = 1000

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(isEditAddress){
        self.handleValues()
         self.setNavTitle(title: "Update Address")
         self.saveButton.setTitle("Update Address", for: .normal)
        }else {
         self.setNavTitle(title: "Add Address")
         self.saveButton.setTitle("Save Address", for: .normal)
        }
        
        self.setNavigationBar("back", nil)
        self.initView()
    }
    
    func handleValues(){
        self.latitude = CLLocationDegrees(exactly: Double(addressDetail.latitude ?? "0")!)!
        self.longitude = CLLocationDegrees(exactly: Double(addressDetail.longitude ?? "0")!)!
        self.fullName.text = addressDetail.name
        self.address1.text = addressDetail.address
        self.address2.text = addressDetail.address2
        self.countryName.text = addressDetail.country
        self.stateName.text = addressDetail.state
        self.cityName.text = addressDetail.city
        self.postalCode.text = "\(addressDetail.postcode ?? 0)"
        self.mobileNumber.text = addressDetail.phone
        self.email.text = addressDetail.email
    }
    
    func initView(){
        self.mobileNumber.delegate = self
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func pickAddress1(_ sender: Any) {
          addressType = 1
          address1.resignFirstResponder()
          let acController = GMSAutocompleteViewController()
          let filter = GMSAutocompleteFilter()
          filter.type = .establishment
          filter.country = "USA"
          acController.autocompleteFilter = filter
          acController.delegate = self as! GMSAutocompleteViewControllerDelegate
          present(acController, animated: true, completion: nil)
      }
    
    @IBAction func pickAddress2(_ sender: Any) {
        addressType = 2
        address2.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        filter.country = "USA"
        acController.autocompleteFilter = filter
        acController.delegate = self as! GMSAutocompleteViewControllerDelegate
        present(acController, animated: true, completion: nil)
    }
    
    @IBAction func saveAddressAction(_ sender: Any) {
        if(address1.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter address", color: errorRed)
        }else {
            ActivityIndicator.show(view: self.view)
            var number = self.mobileNumber.text
            number =  number?.replacingOccurrences(of: "+1 | ", with: "")
            let param:[String:Any] = [
                "id":addressDetail.id,
                "name":self.fullName.text,
                "email":self.email.text,
                "address": self.address1.text,
                "address2": self.address2.text,
                "country": self.countryName.text,
                "state": self.stateName.text,
                "city": self.cityName.text,
                "postcode": self.postalCode.text,
                "phone": self.mobileNumber.text,
                "latitude":self.latitude,
                "longitude":self.longitude
            ]
            if(self.isEditAddress){
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_SHIPPING_ADDRESS, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_UPDATE_SHIPPING_ADDRESS, userToken: nil) { (response) in
                    Singleton.shared.showToast(text: "Address updated successfully", color: successGreen)
                    self.addressDelegate?.reloadAddressTable()
                    ActivityIndicator.hide()
                    self.navigationController?.popViewController(animated: true)
                }
                
            }else {
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_SHIPPING_ADDRESS, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_SHIPPING_ADDRESS, userToken: nil) { (response) in
                    Singleton.shared.showToast(text: "Address added successfully", color: successGreen)
                    self.addressDelegate?.reloadAddressTable()
                    ActivityIndicator.hide()
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
        }
    }
}

extension AddAddressViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == mobileNumber) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 15
        }else {
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == mobileNumber){
            if(textField.text == ""){
                self.mobileNumber.text = "+1 | "
            }
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if(textField == mobileNumber){
            if(textField.text!.count < 7){
                self.mobileNumber.text = ""
            }
        }
    }
}

extension AddAddressViewController: GMSAutocompleteViewControllerDelegate{
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        Singleton.shared.showToast(text: error.localizedDescription, color: errorRed)
        
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if addressType == 1{
            address1.text = place.formattedAddress
            self.latitude = place.coordinate.latitude
            self.longitude = place.coordinate.longitude
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
            geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
                guard let placeMark = placemarks?.first else { return }
                if let locationName = placeMark.subAdministrativeArea {
                    self.cityName.text = locationName
                }
                
                if let country = placeMark.country {
                    self.countryName.text = country
                }
                
                if let zipCode = placeMark.postalCode {
                    self.postalCode.text = zipCode
                }
                if let state = placeMark.administrativeArea {
                    let pickState = Singleton.shared.stateNames.filter{
                        $0.code == state
                    }
                    if(pickState.count > 0){
                        self.stateName.text = pickState[0].code
                    }
                }
            }
            dismiss(animated: true, completion: nil)
        }else if addressType == 2{
            address2.text = place.formattedAddress
            dismiss(animated: true, completion: nil)
        }
    }
    
    func showPinPoint(){
        let annotation = MKPointAnnotation()
        annotation.title = "Address"
        annotation.coordinate = CLLocationCoordinate2D(latitude:self.latitude, longitude: self.longitude)
        self.centerMapOnUserLocation(coordinate: annotation.coordinate)
    }
    
    func centerMapOnUserLocation(coordinate:CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegion(center: coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
}
