//
//  UserMapViewController.swift
//  SYM
//
//  Created by qw on 05/05/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
import Alamofire
import MapKit

class UserMapViewController: UIViewController {
    //Mark: IBOUTLETS
    @IBOutlet weak var pickUpCityTxtField: DesignableUILabel!
    @IBOutlet weak var drpOffCityTxtField: DesignableUILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mainView: View!
    @IBOutlet weak var playPauseButton: CustomButton!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var disputeTextView: UITextView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var controlsStackView: UIStackView!
    @IBOutlet weak var playPauseHeight: NSLayoutConstraint!
    
    var myRoute : MKRoute?
    var directionsRequest = MKDirections.Request()
    var placemarks = [MKMapItem]()
    var pickUpLatitude = String()
    var pickUpLongitude = String()
    var dropOffLatitude = String()
    var dropOffLongitude = String()
    var pickup_from = String()
    var pickup_to = String()
    var shipmentDetail = GetAllCommonShipment()
    var oldLocation = CLLocation()
    var locationManager: CLLocationManager!
    
    var userLocation = CustomPointAnnotation()
    var userAnnotation = CustomPointAnnotation()
    var endAnnotation = CustomPointAnnotation()
    var accuracyRangeCircle: MKCircle?
    var polyline: MKPolyline?
    var isZooming: Bool?
    var isBlockingAutoZoom: Bool?
    var zoomBlockingTimer: Timer?
    var didInitialZoom: Bool?
    var startingPoint:CLLocation?
    var driverData = GetDriverProfileDetails()
    var CustomerData = TypeOfUser()
    var redirection = 1
    var shipmentStatus = Int()
    
    var countTimer:Timer!
    var counter = 0
    var isTimerRunning = false
    var timerLoc = CLLocation()
    var isRedirectFromProgressView = false
    var showUserCar = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.showsUserLocation = false
            if self.driverData.id == nil {
                self.messageView.isHidden = true
                self.callView.isHidden = true
                self.cancelView.isHidden = true
                self.playPauseButton.isHidden = true
                self.playPauseHeight.constant = 0
            } else {
                self.messageView.isHidden = false
                self.callView.isHidden = false
                self.cancelView.isHidden = false
                self.playPauseButton.isHidden = true
                self.playPauseHeight.constant = 0
            }
        
        self.disputeTextView.layer.cornerRadius = 5
        self.disputeTextView.layer.borderWidth = 1
        self.disputeTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        setNavTitle(title: "Map View")
        setNavigationBar("back", nil)
        if(pickup_from != ""){
            self.pickUpCityTxtField.text = "From: \(self.pickup_from ?? "")"
            self.drpOffCityTxtField.text = "To: \(self.pickup_to ?? "")"
            
            self.handleMapView()
        }else {
            if((Singleton.shared.progressShipment?.count ?? 0) > 0){
                for val in Singleton.shared.progressShipment! {
                    if(val.driver_status == 5){
                        self.shipmentDetail = val
                    }
                }
                if(self.shipmentDetail.id == nil){
                    self.mainView.isHidden = true
                }else {
                    self.mainView.isHidden = false
                    self.pickUpCityTxtField.text
                    = "From: \(self.shipmentDetail.from_city ?? self.shipmentDetail.from_state ?? "")"
                    self.drpOffCityTxtField.text = "To: \(self.shipmentDetail.to_city ?? self.shipmentDetail.to_state ?? "")"
                    self.dropOffLatitude = self.shipmentDetail.to_latitude ?? ""
                    self.dropOffLongitude = self.shipmentDetail.to_longitude ?? ""
                    self.pickUpLatitude = self.shipmentDetail.from_latitude ?? ""
                    self.pickUpLongitude = self.shipmentDetail.from_longitude ?? ""
                    self.shipmentDetail.driver_id = Singleton.shared.userType.user_id ?? 0
                    if(self.shipmentDetail.lookingFor == 1){
                        self.shipmentStatus = self.shipmentDetail.driver_status ?? 0
                    }else if(self.shipmentDetail.lookingFor == 2){
                        self.shipmentStatus = self.shipmentDetail.loader_status ?? 0
                    }else if(self.shipmentDetail.lookingFor == 3){
                        self.shipmentStatus = self.shipmentDetail.driver_status ?? 0
                    }else{
                        self.shipmentStatus = self.shipmentDetail.driver_status ?? 0
                    }
                    self.handleMapView()
                }
                
            }}
        self.drawPolyLineRoute()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.addPathObserver()
    }
    
    func addPathObserver() {
        ref.child("tracking/").observe(.childChanged, with: { (snapshot) in
            if let realtimeFeedData = snapshot.value as? [String:Any] {
                if(self.shipmentDetail.id == Int(snapshot.key) && self.shipmentStatus >= 4 && self.shipmentStatus < 6){
                        self.showUserCar = true
                        self.userLocation.imageName = "pin (1)-1"
                        self.endAnnotation.coordinate = CLLocationCoordinate2D(latitude: realtimeFeedData["cLat"] as? Double ?? 0.0 , longitude: realtimeFeedData["cLng"] as? Double ?? 0.0)
                        self.endAnnotation.imageName = "car"
                        self.mapView.addAnnotation(self.endAnnotation)
                        self.drawUpdatedpolyline(userloc: CLLocationCoordinate2D(latitude: realtimeFeedData["cLat"] as? Double ?? 0.0 , longitude: realtimeFeedData["cLng"] as? Double ?? 0.0))
                }
            }
        })
    }
    
    //MARK:IBActions
    @IBAction func messageAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            myVC.jobDetail = ChatDetail(sender_id: Singleton.shared.userType.user_id, receiver_id: self.driverData.id, receiver_name:  ((self.driverData.first_name ?? "") + " " + (self.driverData.last_name ?? "")), sender_name: (Singleton.shared.userType.first_name ?? "") + " " + (Singleton.shared.userType.last_name ?? "") , sender_image:   (Singleton.shared.userType.profile_image ?? ""), receiver_image:
                                            self.driverData.profile_image ?? "", shipment_id: shipmentDetail.id ?? 0)
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func callAction(_ sender: Any) {
        var phone = String()
            phone = self.driverData.phone ?? ""
        self.callNumber(phoneNumber: phone)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.popupView.isHidden = false
    }
    
    @IBAction func playPauseAction(_ sender: Any) {
        if(playPauseButton.titleLabel?.text == "PAUSE"){
            self.playPauseButton.setTitle("PLAY", for: .normal)
        }else {
            self.playPauseButton.setTitle("PAUSE", for: .normal)
        }
    }
    
    @IBAction func okAction(_ sender: Any) {
        if(self.disputeTextView.text == ""){
            Singleton.shared.showToast(text: "Enter reason for Dispute", color: errorRed)
        }else {
            self.popupView.isHidden = true
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "shipment_id" : self.shipmentDetail.id ?? 0,
                "user_id" :Singleton.shared.userType.user_id ?? 0,
                "reason" : self.disputeTextView.text ?? ""
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DISPUTE_SHIPMENT, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_DISPUTE_SHIPMENT, userToken: nil) { (response) in
                self.disputeTextView.text = ""
                ActivityIndicator.hide()
                Singleton.shared.showToast(text: "Successfully Disputed Job", color: successGreen)
                Singleton.shared.progressShipment = []
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
    
    @IBAction func closePopup(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
}

extension UserMapViewController: CLLocationManagerDelegate,MKMapViewDelegate {
    func drawPolyLineRoute(){
        var coordinateArray = [CLLocationCoordinate2D]()
        coordinateArray.append(CLLocationCoordinate2D(latitude: Double(self.pickUpLatitude ?? "0")!, longitude: Double(self.pickUpLongitude ?? "0")!))
        coordinateArray.append(CLLocationCoordinate2D(latitude: Double(self.dropOffLatitude ?? "0")!, longitude: Double(self.dropOffLongitude ?? "0")!))
        self.zoomTo(location: CLLocation(latitude: coordinateArray[0].latitude, longitude: coordinateArray[0].longitude))
        
        placemarks = []
        let arrayPlace = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Double(self.pickUpLatitude)!, longitude: Double(self.pickUpLongitude)!))
        let arrayPlaceDrop = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Double(self.dropOffLatitude)!, longitude: Double(self.dropOffLongitude)!))
        placemarks.append(MKMapItem(placemark: arrayPlace))
        placemarks.append(MKMapItem(placemark: arrayPlaceDrop))
        
        directionsRequest.transportType = MKDirectionsTransportType.automobile
        for (k, item) in placemarks.enumerated() {
            if k < (placemarks.count - 1) {
                directionsRequest.source = item
                directionsRequest.destination = placemarks[k+1]
                
                let directions = MKDirections(request: directionsRequest)
                directions.calculate { (response:MKDirections.Response!, error: Error!) -> Void in
                    if error == nil {
                        self.myRoute = response.routes[0] as? MKRoute
                        let geodesic:MKPolyline = self.myRoute!.polyline
                        self.mapView.addOverlay(geodesic)
                    }
                }
            }
        }
    }
    
    func drawUpdatedpolyline(userloc:CLLocationCoordinate2D){
        
        var coordinateArray = [CLLocationCoordinate2D]()
        coordinateArray.append(userloc)
        coordinateArray.append(CLLocationCoordinate2D(latitude: Double(self.dropOffLatitude ?? "0")!, longitude: Double(self.dropOffLongitude ?? "0")!))
        
        self.zoomTo(location: CLLocation(latitude: userloc.latitude, longitude: userloc.longitude))
        self.mapView.removeOverlays(self.mapView.overlays)
        placemarks = []
        let arrayPlace = MKPlacemark(coordinate: userloc)
        let arrayPlaceDrop = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Double(self.dropOffLatitude)!, longitude: Double(self.dropOffLongitude)!))
        placemarks.append(MKMapItem(placemark: arrayPlace))
        placemarks.append(MKMapItem(placemark: arrayPlaceDrop))
        
        directionsRequest.transportType = MKDirectionsTransportType.automobile
        for (k, item) in placemarks.enumerated() {
            if k < (placemarks.count - 1) {
                directionsRequest.source = item
                directionsRequest.destination = placemarks[k+1]
                
                let directions = MKDirections(request: directionsRequest)
                directions.calculate { (response:MKDirections.Response!, error: Error!) -> Void in
                    if error == nil {
                        self.myRoute = response.routes[0] as? MKRoute
                        let geodesic:MKPolyline = self.myRoute!.polyline
                        self.mapView.addOverlay(geodesic)
                    }
                }
            }
        }
    }
    
    
    func handleMapView() {
        mapView.delegate = self
        self.accuracyRangeCircle = MKCircle(center: CLLocationCoordinate2D.init(latitude: locationManager.location?.coordinate.latitude ?? 0, longitude: locationManager.location?.coordinate.longitude ?? 0), radius: 150)
        self.didInitialZoom = false
        if self.shipmentStatus >= 4 && self.shipmentStatus < 6{
            NotificationCenter.default.addObserver(self, selector: #selector(self.updateMap(_:)), name: Notification.Name(rawValue:"didUpdateLocation"), object: nil)
        }
    }
    
    @objc func updateMap(_ notification: NSNotification){
        if let userInfo = notification.userInfo{
            updatePolylines()
            if let newLocation = userInfo["location"] as? CLLocation{
                zoomTo(location: newLocation)
            }
        }
    }
    
    func startTimer() {
        self.countTimer = Timer.scheduledTimer(timeInterval: 1 , target: self, selector: #selector(changeTitle), userInfo: nil, repeats: true)
        isTimerRunning = true
        
    }
    
    @objc func changeTitle()
    {
        if counter != 10
        {
            counter += 1
        }  else {
            drawUpdatedpolyline(userloc: CLLocationCoordinate2D(latitude: timerLoc.coordinate.latitude, longitude: timerLoc.coordinate.longitude))
            ref.child("tracking/\(shipmentDetail.id ?? 0)").setValue([
                "cLat" : timerLoc.coordinate.latitude,
                "cLng" : timerLoc.coordinate.longitude,
                "destLat" : self.dropOffLatitude,
                "destLng" : self.dropOffLongitude,
                "shipmentID" : self.shipmentDetail.id,
                "srcLat" : self.pickUpLatitude,
                "srcLng" : self.pickUpLongitude,
            ])
            counter = 0
            countTimer.invalidate()
            self.isTimerRunning = false
            
        }
    }
    
    func updatePolylines(){
        var coordinateArray = [CLLocationCoordinate2D]()
        
        for loc in Singleton.shared.locationDataArray{
            coordinateArray.append(loc.coordinate)
        }
        
        self.mapView.clearsContextBeforeDrawing = true
        let last = Singleton.shared.locationDataArray.last
        if(self.startingPoint != last){
            let distance = self.startingPoint?.distance(from: last!)
            let miles = (distance ?? 0)/1600
        }
    }
    
    func zoomTo(location: CLLocation){
        if self.didInitialZoom == false{
            let coordinate = location.coordinate
            let newDistance = CLLocation(latitude: Double(self.pickUpLatitude ?? "0.0")!, longitude: Double(self.pickUpLongitude ?? "0.0")!).distance(from: CLLocation(latitude: Double(self.dropOffLatitude ?? "0.0")!, longitude: Double(self.dropOffLongitude ?? "0.0")!))
            
            let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 1.8 * newDistance, longitudinalMeters: 1.8 * newDistance)
            let adjustRegion = self.mapView.regionThatFits(region)
            self.mapView.setRegion(adjustRegion, animated: false)
            self.didInitialZoom = true
        }
        
        if self.isBlockingAutoZoom == false{
            self.isZooming = true
            self.mapView.setCenter(location.coordinate, animated: true)
        }
        
        var accuracyRadius = 50.0
        if location.horizontalAccuracy > 0{
            if location.horizontalAccuracy > accuracyRadius{
                accuracyRadius = location.horizontalAccuracy
            }
        }
        self.mapView.removeOverlay(self.accuracyRangeCircle!)
        self.accuracyRangeCircle = MKCircle(center: location.coordinate, radius: accuracyRadius as CLLocationDistance)
        self.mapView.addOverlay(self.accuracyRangeCircle!)
        
        if self.userAnnotation != nil{
            self.mapView.removeAnnotation(self.userAnnotation)
        }
        if self.shipmentStatus >= 4 && self.shipmentStatus < 6 {
            if self.dropOffLatitude != nil && self.dropOffLongitude != nil {
                self.userAnnotation.coordinate = CLLocationCoordinate2D(latitude: Double(self.dropOffLatitude)!, longitude: Double(self.dropOffLongitude)!)
            }
        } else {
            if self.endAnnotation != nil{
                self.mapView.removeAnnotation(self.endAnnotation)
            }
            self.userAnnotation.coordinate = location.coordinate
            if self.dropOffLatitude != nil && self.dropOffLongitude != nil {
                self.endAnnotation.coordinate = CLLocationCoordinate2D(latitude: Double(self.dropOffLatitude)!, longitude: Double(self.dropOffLongitude)!)
            }
            self.endAnnotation.imageName = "redPin"
            self.mapView.addAnnotation(self.endAnnotation)
        }
        
            self.userAnnotation.imageName = "pin (1)-1"

        
        self.mapView.addAnnotation(self.userAnnotation)
       
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let newLocation = locations.last{
            
            var locationAdded: Bool
            locationAdded = filterAndAddLocation(newLocation)
            
            if locationAdded{
                if self.shipmentStatus >= 4 && self.shipmentStatus < 6 {
                    if isTimerRunning == false {
                        timerLoc = newLocation
                        Singleton.shared.locationDataArray.append(newLocation)
                        NotificationCenter.default.post(name: NSNotification.Name("didUpdateLocation"), object: nil,userInfo: ["location":locations.last])
                        startTimer()
                    }
                }
                
            }
        }
    }
    
    func filterAndAddLocation(_ location: CLLocation) -> Bool{
        let age = location.timestamp.timeIntervalSinceNow
        
        if age > 10 {
            return false
        }
        
        if location.horizontalAccuracy < 0 {
            return false
        }
        
        if location.horizontalAccuracy > 100{
            return false
        }
        
        return true
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let routePolyline = overlay as? MKPolyline {
            let renderer = MKPolylineRenderer(polyline: routePolyline)
            renderer.strokeColor = primaryColor
            renderer.lineWidth = 7
            return renderer
        }
        
        return MKOverlayRenderer()
    }
    
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            if(self.mapView.showsUserLocation){
            let identifier = "UserAnnotation"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if annotationView != nil{
                annotationView!.annotation = annotation
            }else{
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            }
            annotationView!.canShowCallout = false
            annotationView!.image = #imageLiteral(resourceName: "car")
                return annotationView
            } else {
                return nil
            }
           
        } else {
            let identifier = "UserAnnotation"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if annotationView != nil{
                annotationView!.annotation = annotation
            }else{
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            }
            annotationView!.canShowCallout = false
            
            let cpa = annotation as! CustomPointAnnotation
            annotationView!.image = UIImage(named:cpa.imageName ?? "")
            return annotationView
        }
    }
    
    
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        if self.isZooming == true{
            self.isZooming = false
            self.isBlockingAutoZoom = false
        }else{
            self.isBlockingAutoZoom = true
            if let timer = self.zoomBlockingTimer{
                if timer.isValid{
                    timer.invalidate()
                }
            }
            self.zoomBlockingTimer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false, block: { (Timer) in
                self.zoomBlockingTimer = nil
                self.isBlockingAutoZoom = false;
            })
        }
    }
    
//    func hadleLocationUsage(){
//        var status = CLLocationManager.authorizationStatus()
//        locationManager = CLLocationManager()
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.pausesLocationUpdatesAutomatically = false
//        locationManager.allowsBackgroundLocationUpdates = true
//        locationManager.startMonitoringSignificantLocationChanges()
//        locationManager.distanceFilter = 10
//        locationManager.activityType = .fitness
//        locationManager.delegate = self
//        if status == .notDetermined || status == .denied || status == .authorizedWhenInUse {
//            // present an alert indicating location authorization required
//            // and offer to take the user to Settings for the app via
//            // UIApplication -openUrl: and UIApplicationOpenSettingsURLString
//            locationManager.requestAlwaysAuthorization()
//            locationManager.requestWhenInUseAuthorization()
//        }
//        locationManager.startUpdatingLocation()
//        locationManager.startUpdatingHeading()
//    }
}
