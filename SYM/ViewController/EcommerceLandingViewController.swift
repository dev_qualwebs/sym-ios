//
//  EcommerceLandingViewController.swift
//  SYM
//
//  Created by qw on 18/09/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class EcommerceLandingViewController: UIViewController {
    //MARK: IBOutlets
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "EcommerceCategoryViewController") as! EcommerceCategoryViewController
            self.navigationController?.pushViewController(myVC, animated: true)
            self.navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
                if vc.isKind(of: EcommerceLandingViewController.self) {
                    return true
                } else {
                    return false
                }
            })
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

}
