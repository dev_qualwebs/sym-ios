//
//  MyCartViewController.swift
//  SYM
//
//  Created by qw on 23/10/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
import iOSDropDown
import Cosmos

class MyCartViewController: UIViewController {
    //MARK: IBOUtlets
    @IBOutlet weak var cartTable: UITableView!
    @IBOutlet weak var totalPrice: DesignableUILabel!
    @IBOutlet weak var discountPrice: DesignableUILabel!
    @IBOutlet weak var taxPrice: DesignableUILabel!
    @IBOutlet weak var subtotalPrice: DesignableUILabel!
    @IBOutlet weak var total: DesignableUILabel!
    @IBOutlet weak var shipmentAddress: UILabel!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    

    var cartData = CartData()
    var addressData = [GetAddressResponse]()
    var shippingAddress = GetAddressResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavTitle(title: "My Cart")
        self.setNavigationBar("back", "mycart")
        self.cartTable.tableFooterView = UIView()
        self.getShipmentAddress()
        self.getCartData()
        APICallingViewController.shared.getShippingAddress { (response) in
            
        }
    }
    
    func getCartData(){
        Singleton.shared.cartData = CartData()
        APICallingViewController.shared.getCartData { (val) in
            self.cartData = val
            self.totalPrice.text = "$" + (self.cartData.cart_detail.sub_total ?? "")
            self.discountPrice.text = "-$" + (self.cartData.cart_detail.discount_amount ?? "")
            self.taxPrice.text = "$" + (self.cartData.cart_detail.tax_total ?? "")
            self.subtotalPrice.text = "$" + (self.cartData.cart_detail.sub_total ?? "")
            self.total.text = "$" + (self.cartData.cart_detail.grand_total ?? "")
            self.cartTable.reloadData()
        }
    }
    
    func getShipmentAddress(){
           ActivityIndicator.show(view: self.view)
           SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SHIPPING_ADDRESS, method: .get, parameter: nil, objectClass: GetShippingAddress.self, requestCode: U_GET_SHIPPING_ADDRESS, userToken: nil) { (response) in
               self.addressData = response.response
            if(self.addressData.count > 0){
                self.shippingAddress = self.addressData[0]
            }
               ActivityIndicator.hide()
           }
       }
    
    func removeItem(id:Int){
        let alert = UIAlertController(title: "Add Item to cart", message: nil, preferredStyle: .alert)
        let actionOne = UIAlertAction(title: "Yes", style: .default) { (action) in
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "cart_item_id": id,
            ]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE+U_DELETE_CART, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_DELETE_CART, userToken: nil) { (response) in
                Singleton.shared.showToast(text:response.message ?? "", color: successGreen)
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
                self.navigationController?.pushViewController(myVC, animated: true)
                self.navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
                    if vc.isKind(of: MyCartViewController.self) {
                        return false
                    } else {
                        return true
                    }
                })
            }
        }
        
        let actionTwo = UIAlertAction(title: "No", style: .default) { (action) in
            
        }
        
        alert.addAction(actionTwo)
        alert.addAction(actionOne)
        self.present(alert, animated: true, completion: nil)
    }
    

    //MARK: IBActions
    @IBAction func placeOrderAction(_ sender: Any) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "CheckoutViewController") as! CheckoutViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func changeAddressAction(_ sender: Any) {
        
        
    }
}

extension MyCartViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.cartData.cart_items.count == 0){
            self.noDataLabel.isHidden = false
        }else {
            self.noDataLabel.isHidden = true
        }
        return self.cartData.cart_items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableCell") as! CartTableCell
        let val = self.cartData.cart_items[indexPath.row]
//        for i in 0..<(val.quantity ?? 0) {
//            self.quantityDropdown.optionArray.append("\(i+1)")
//        }
        cell.quantity.optionArray = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"]
        cell.quantity.optionImageArray=["1","2","3","4","5","6","7","8","9","10"]
        cell.quantity.didSelect { (string, a, id) in
            ActivityIndicator.show(view: self.view)
            let param:[String:Any]=[
                "cart_item_id":val.id,
              "quantity":string
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_CART, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_UPDATE_CART, userToken: nil) { (response) in
                ActivityIndicator.hide()
                self.getCartData()
            }
        }
        if(val.product_images.count > 0){
         cell.cartImage.sd_setImage(with: URL(string:U_IMAGE + (val.product_images[0].image ?? "")), placeholderImage: #imageLiteral(resourceName: "ecommerce"))
        }
        cell.quantity.text = "\(val.quantity ?? 0)"
        cell.productName.text = val.product_name
        cell.productSize.text = val.description
        cell.productPrice.text = "$" + (val.price ?? "")
        
        
        cell.removeButton={
            ActivityIndicator.show(view: self.view)
            let param:[String:Any]=[
                "cart_item_id":val.id,
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_CART, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_DELETE_CART, userToken: nil) { (response) in
                ActivityIndicator.hide()
                self.getCartData()
            }
        }
        
        
        return cell
    }
}


class CartTableCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var cartImage: UIImageView!
    @IBOutlet weak var quantity: DropDown!
    @IBOutlet weak var productName: DesignableUILabel!
    @IBOutlet weak var productSize: DesignableUILabel!
    @IBOutlet weak var productPrice: DesignableUILabel!
    @IBOutlet weak var date: DesignableUILabel!
    @IBOutlet weak var status: DesignableUILabel!
    @IBOutlet weak var ratingView: UIView!
    
    
    var removeButton:(()-> Void)? = nil
    var rateButton:(()-> Void)? = nil
    
    //MARK: IBActions
    @IBAction func removeAction(_ sender: Any) {
        if let removeButton = self.removeButton{
            removeButton()
        }
    }
    
    @IBAction func rateAction(_ sender: Any) {
        if let rateButton = self.rateButton{
            rateButton()
        }
    }
    
}
