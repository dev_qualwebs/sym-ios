//
//  DriverMessagesVC.swift
//  SYM
//
//  Created by AM on 22/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import iOSDropDown


protocol ReloadData {
    func reloadData()
}

class DriverMessagesVC: UIViewController , SelectFromPicker,ReloadData {
    
    func reloadData(){
        self.messageTable.reloadData()
    }
    
    func selectedPickerData(val: String, pos: Int) {
        self.sortbyField.text = val
        if currentPicker == 1 {
        self.getMessages(id: self.shipmentData[pos].id ?? 0)
        }
    }
    
    
    //Mark: IBOUTLETS
    @IBOutlet weak var messageTable: UITableView!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    @IBOutlet weak var sortbyField: DesignableUITextField!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var progressView: StepProgressView!
    
    
    var shipmentData = [GetDisputedShipmentResponse]()
    var messsageData = [MessageResponse]()
    var driverMessageData = [DriverMessageResponse]()
    var sortByData = [String]()
    var currentPicker = Int()
    var selectedId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ChatViewController.messageListDelegate = self
        messageTable.estimatedRowHeight = 80
        messageTable.rowHeight = UITableView.automaticDimension
        
        
        self.filterView.isHidden = Singleton.shared.userType.role == 1 ? false : true
        self.progressView.viewShipment={
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.navigationController?.pushViewController(myVC, animated: true)
         }
    }
    
    override func viewWillAppear(_ animated: Bool) {
      setNavTitle(title: "Messages")
        self.getCustomerShipments()
        if(Singleton.shared.userType.role != 1){
        self.progressView.showAnimatingDots()
        self.progressView.showHideView={
            if(Singleton.shared.progressShipment?.count == 0){
                self.progressView.isHidden = true
            }else{
                for val in Singleton.shared.progressShipment!{
                    if(val.driver_status == 5){
                        self.progressView.isHidden = false
                    }
                }
            }
        }
        }
    }
    
    func getCustomerShipments(){
        ActivityIndicator.show(view: self.view)
        var url = String()
        if(Singleton.shared.userType.role == 1){
            currentPicker = 1
            url = U_BASE + U_GET_CUSTOMER_SHIPMENT
            SessionManager.shared.methodForApiCalling(url: url, method:.get, parameter: nil, objectClass: GetDisputedShipment.self, requestCode: url, userToken: nil) { (response) in
                ActivityIndicator.hide()
                self.shipmentData = response.response
                self.sortByData = []
                for val in self.shipmentData{
                    self.sortByData.append((val.from_city ?? "") + "-" + (val.to_city ?? ""))
                }
                if(self.shipmentData.count > 0){
                    self.getMessages(id: self.shipmentData[0].id ?? 0)
                    self.sortbyField.text = (self.shipmentData[0].from_city ?? "") + "-" + (self.shipmentData[0].to_city ?? "")
                }
                
            }
        } else {
            currentPicker = 2
            url = U_BASE + U_GET_DRIVER_SHIPMENT
            SessionManager.shared.methodForApiCalling(url: url, method:.post, parameter: nil, objectClass: GetDriverMessage.self, requestCode: url, userToken: nil) { (response) in
                ActivityIndicator.hide()
                self.driverMessageData = response.response
                self.sortByData = []
                for val in self.driverMessageData{
                    self.sortByData.append((val.shipment?.from_city ?? "") + "-" + (val.shipment?.to_city ?? ""))
                }
                if(self.driverMessageData.count > 0){
                   // self.getMessages(id: self.shipmentData[0].id ?? 0)
                    self.sortbyField.text = (self.driverMessageData[0].shipment?.from_city ?? "") + "-" + (self.driverMessageData[0].shipment?.to_city ?? "")
                }
                self.messageTable.reloadData()
            }
        }
    }
    
    func getMessages(id: Int){
        ActivityIndicator.show(view: self.view)
        let param:[String:Any] = [
            "shipment_id":id
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CHAT_LIST, method: .post, parameter: param, objectClass: GetMessage.self, requestCode: U_GET_CHAT_LIST, userToken: nil) { (response) in
            self.messsageData = response.response
            self.messageTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    @IBAction func sortByAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.headingLabel = "Filter"
        myVC.pickerData = self.sortByData ?? []
        if self.sortByData.count > 0 {
        self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    
}

extension DriverMessagesVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(Singleton.shared.userType.role == 1){
            if(self.messsageData.count == 0){
                self.noDataLabel.isHidden = false
            }else {
                self.noDataLabel.isHidden = true
            }
            return self.messsageData.count
        }else {
            if(self.driverMessageData.count == 0){
                self.noDataLabel.isHidden = false
            }else {
                self.noDataLabel.isHidden = true
            }
            return self.driverMessageData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DriverMessagesTableCell", for: indexPath) as! DriverMessagesTableCell
        if(Singleton.shared.userType.role == 1){
        let val = self.messsageData[indexPath.row]
        cell.userImage.sd_setImage(with: URL(string:U_IMAGE + (val.driver?.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "placeholder"))
            
            if val.amount != nil {
            cell.userMessage.text = "$" + (val.amount ?? "0")
                cell.userMessage.isHidden = false
            } else {
                cell.userMessage.isHidden = true
            }
            if(self.messsageData[indexPath.row].read_status == 0){
            cell.isUnreadImage.isHidden = false
            } else {
            cell.isUnreadImage.isHidden = true
            }
        cell.username.text = (val.driver?.first_name ?? "") + " " + (val.driver?.last_name ?? "")
        }else {
            let val = self.driverMessageData[indexPath.row]
            cell.userImage.sd_setImage(with: URL(string:U_IMAGE + (val.shipment?.customer_info?.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "placeholder"))
            let currentDate = val.shipment?.created_at ?? "000000000000"
            var firstChar = currentDate.index(currentDate.startIndex, offsetBy: 10)
            let date = currentDate.prefix(upTo: firstChar)
            let timeStamp = convertDateToTimestamp(String(date), to: "YYYY-MM-dd")
            if(self.driverMessageData[indexPath.row].read_status == 0){
            cell.isUnreadImage.isHidden = false
            } else {
            cell.isUnreadImage.isHidden = true
            }
            cell.userMessage.text = "Shipment on " + convertTimestampToDate( timeStamp , to: "d MMM")
            cell.username.text = (val.shipment?.customer_info?.first_name ?? "") + " " + (val.shipment?.customer_info?.last_name ?? "")
            if val.shipment?.price != nil {
            cell.date.text = "$" + (val.shipment?.price ?? "0")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        if(Singleton.shared.userType.role == 1){
            myVC.customerAccepted = self.messsageData[indexPath.row].customer_accpeted ?? 0
            myVC.driverAccepted = self.messsageData[indexPath.row].driver_accepted ?? 0
            
            self.messsageData[indexPath.row].read_status = 1
            let val = self.messsageData[indexPath.row]
            myVC.status = self.messsageData[indexPath.row].status ?? 0
        myVC.jobDetail = ChatDetail(sender_id: Singleton.shared.userType.user_id ?? 0,receiver_id: val.driver_id ?? 0, receiver_name: (val.driver?.first_name ?? "") + " " + (val.driver?.last_name ?? ""),sender_name: (Singleton.shared.userType.first_name ?? "") + " " +  (Singleton.shared.userType.last_name ?? ""), sender_image: (Singleton.shared.userType.profile_image ?? ""), receiver_image: val.driver?.profile_image, shipment_id: val.shipment_id ?? 0)
        } else {
            self.driverMessageData[indexPath.row].read_status = 1
            let val = self.driverMessageData[indexPath.row]
            myVC.jobDetail = ChatDetail(sender_id:val.shipment?.customer_info?.id ?? 0,receiver_id: Singleton.shared.userType.user_id ?? 0, receiver_name: (Singleton.shared.userType.first_name ?? "") + " " +  (Singleton.shared.userType.last_name ?? "") ,sender_name:(val.shipment?.customer_info?.first_name ?? "") + " " + (val.shipment?.customer_info?.last_name ?? ""), sender_image:val.shipment?.customer_info?.profile_image ?? "" , receiver_image:(Singleton.shared.userType.profile_image ?? ""), shipment_id: val.shipment_id ?? 0)
        }
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}


class DriverMessagesTableCell: UITableViewCell{
    //MARK: IBOutlets
    
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var username: DesignableUILabel!
    @IBOutlet weak var userMessage: DesignableUILabel!
    @IBOutlet weak var date: DesignableUILabel!
    @IBOutlet weak var price: DesignableUILabel!
    @IBOutlet weak var isUnreadImage: UIImageView!
    
}
