    
    import UIKit
    import UserNotifications
    import SDWebImage
    import FirebaseStorage
    import Firebase
    import IQKeyboardManagerSwift
    
    class ChatViewController: UIViewController {
        
        //MARK:IBOutlets
        @IBOutlet weak var chatTable: UITableView!
        @IBOutlet weak var userImage: ImageView!
        @IBOutlet weak var textView: UITextView!
        @IBOutlet weak var lblUserName: UILabel!
        @IBOutlet weak var occupation: DesignableUILabel!
        @IBOutlet weak var noDataLabel: DesignableUILabel!
        @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
        @IBOutlet weak var rehireButtonView: UIView!
        
        var chatData = [ChatResponse]()
        var userId = Int()
        var driverId  = Int()
        var chatId = String()
        let picker = UIImagePickerController()
        var imageName = ""
        var imageData  = Data()
        var jobDetail : ChatDetail?
        var senderID = Int()
        var receiverID = Int()
        var senderName = String()
        var receiverName = String()
        var postedByMe = false
        var isAdminChat = false
        var urlPhoto = String()
        var isFiratTime = true
        var status = 0
        var customerAccepted = 0
        var driverAccepted = 0
        static var messageListDelegate : ReloadData? = nil
        
        override func viewDidLoad() {
            super.viewDidLoad()
            NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
            IQKeyboardManager.shared.enable = false
            picker.delegate = self
            self.textView.text = "Type your message..."
            self.textView.textColor = .lightGray
            self.textView.delegate = self
            if(isAdminChat){
                userImage.image = #imageLiteral(resourceName: "CATLEY_LAKEMAN-Russell")
                self.lblUserName.text =  "Admin"
                self.occupation.text = "Admin"
                self.senderID =  jobDetail?.sender_id ?? 0
                self.receiverID = jobDetail?.receiver_id ?? 0
                self.senderName = jobDetail?.sender_name ?? ""
                self.receiverName = self.jobDetail?.receiver_name ?? ""
                self.chatId = "\(self.jobDetail?.sender_id ?? 0)" + "_" + "\(self.jobDetail?.receiver_id ?? 0)"
                self.addChatObserver()
            }else {
                if(jobDetail?.shipment_id != nil){
                    if(Singleton.shared.userType.user_id == self.jobDetail?.sender_id){
                        userImage.sd_setImage(with:
                                                    URL(string: U_IMAGE + (self.jobDetail?.receiver_image ?? "")), placeholderImage: #imageLiteral(resourceName: "placeholder"))
                        self.lblUserName.text = jobDetail?.receiver_name ?? ""
                        self.senderID =  jobDetail?.sender_id ?? 0
                        self.receiverID = jobDetail?.receiver_id ?? 0
                        self.senderName = jobDetail?.sender_name ?? ""
                        self.receiverName = self.jobDetail?.receiver_name ?? ""
                    }else {
                        userImage.sd_setImage(with:URL(string: U_IMAGE + (self.jobDetail?.sender_image ?? "")), placeholderImage:#imageLiteral(resourceName: "placeholder"))
                        self.lblUserName.text = jobDetail?.sender_name ?? ""
                        self.senderID = jobDetail?.receiver_id ?? 0
                        self.receiverID = jobDetail?.sender_id ?? 0
                        self.senderName = jobDetail?.receiver_name ?? ""
                        self.receiverName = self.jobDetail?.sender_name ?? ""
                    }
                    
                    if(Singleton.shared.userType.role == 1){
                        if(Singleton.shared.userType.user_id == self.jobDetail?.sender_id){
                            self.driverId = self.jobDetail?.receiver_id ?? 0
                        } else {
                            self.driverId = self.jobDetail?.sender_id ?? 0
                        }
                        self.chatId = "\(self.jobDetail?.sender_id ?? 0)" + "_" + "\(self.jobDetail?.shipment_id ?? 0)" + "_" + "\(self.jobDetail?.receiver_id ?? 0)"
                    } else {
                        if(Singleton.shared.userType.user_id == self.jobDetail?.sender_id){
                        self.chatId  = "\(self.jobDetail?.receiver_id ?? 0)" + "_" + "\(self.jobDetail?.shipment_id ?? 0)" + "_" + "\(self.jobDetail?.sender_id ?? 0)"
                        } else {
                            self.chatId  = "\(self.jobDetail?.sender_id ?? 0)" + "_" + "\(self.jobDetail?.shipment_id ?? 0)" + "_" + "\(self.jobDetail?.receiver_id ?? 0)"
                        }
                    }
                    
                    self.addChatObserver()
                    self.updateReadStatus(recieverId: self.receiverID)
                }
            }
            
        }
        
        override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.setNavigationBarHidden(true, animated: false)
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            ChatViewController.messageListDelegate?.reloadData()
            NotificationCenter.default.removeObserver(self)
            IQKeyboardManager.shared.enable = true
            self.navigationController?.isNavigationBarHidden = false
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
        
        func updateReadStatus(recieverId: Int) {
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_CHAT_MESSAGE, method: .post, parameter: ["receiver_id":receiverID,"shipment_id":self.jobDetail?.shipment_id ?? 0], objectClass: GetChatMessageShipmentResponse.self, requestCode: U_UPDATE_CHAT_MESSAGE, userToken: nil) { (response) in
                if((response.response?.shipment_details?.count ?? 0) > 0){
                if(response.response?.shipment_details?[0].lookingFor == 1){
                    self.status = response.response?.shipment_details?[0].driver_status ?? 0
                } else if (response.response?.shipment_details?[0].lookingFor == 2){
                    self.status = response.response?.shipment_details?[0].loader_status ?? 0
                } else {
                    self.status = response.response?.shipment_details?[0].driver_status ?? 0
                }
                }
                
                if(self.status != 7 && Singleton.shared.userDetail.role == 1){
                   if(self.customerAccepted == 1 || self.driverAccepted == 1){
                self.rehireButtonView.isHidden = false
                }else {
                    self.rehireButtonView.isHidden = true
                }
                }
            }
        }
        
        func scrollToIndex(){
            if self.chatData.count > 0 {
              let index = IndexPath(row: self.chatData.count - 1, section: 0)
            self.chatTable.scrollToRow(at:index, at: .bottom, animated: true)
            }
        }
        
        
        @objc func handleKeyboardNotification(_ notification: Notification) {

          if let userInfo = notification.userInfo {

              let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue

              let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification

              bottomConstraint.constant = isKeyboardShowing ? keyboardFrame!.height : 0
              
            
             
              UIView.animate(withDuration: 0.5, animations: { () -> Void in
                  self.view.layoutIfNeeded()
              })
              //  self.scrollToIndex()
          }
      }
        
        func addChatObserver() {
            ref.child("messages/" + "\(self.chatId)").observe(.childAdded, with: { (snapshot) in
                if(Singleton.shared.userType.user_id !=  self.senderID || self.isFiratTime){
                if let myVal = snapshot.value as? [String:Any]{
                    self.chatData.append(ChatResponse(id: myVal["id"] as? String, message: myVal["message"] as? String, read_satus: myVal["read_status"] as? Int, receiver_id: myVal["receiver_id"] as? Int, sender_id: myVal["sender_id"] as? Int, time: myVal["time"] as? Int, type: myVal["type"] as? Int))
                    self.chatTable.beginUpdates()
                    self.chatTable.insertRows(at: [IndexPath(row: self.chatData.count - 1, section: 0)], with: .bottom)
                    self.chatTable.endUpdates()
                    self.chatTable.scrollToRow(at: IndexPath(row: self.chatData.count - 1, section: 0), at: .bottom, animated: false)
                }
                    
                }
            })
        }
        
        func uplaodUserImage() {
            self.isFiratTime = false
            ActivityIndicator.show(view: self.view)

                    let time =  Int(Date().timeIntervalSince1970)
                    if(self.isAdminChat){
                                        let dbRef = ref.child("messages/" + self.chatId).childByAutoId().setValue([
                                            "id": self.chatId,
                                            "time": time,
                                            "sender_id": self.senderID,
                                            "receiver_id": self.receiverID,
                                            "message": urlPhoto,
                                            "type": 2,
                                            "read_status": 1,
                                        ])
                    }else {
                        self.sendSingleMessage(recieverId: self.senderID, message: urlPhoto)
                        ActivityIndicator.hide()
                    }
        }
        
        func scheduleNotifications(title:String,msg: String) {
            let notificationContent = UNMutableNotificationContent()
            notificationContent.title = title
            notificationContent.body = msg
            notificationContent.sound = UNNotificationSound.default
            
            // Deliver the notification in five seconds.
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.01, repeats: false)
            // Schedule the notification.
            let request = UNNotificationRequest(identifier: "FiveSecond", content: notificationContent, trigger: trigger)
            let center = UNUserNotificationCenter.current()
            center.add(request) { (error: Error?) in
                if let theError = error {
                    print(theError)
                }
            }
        }
        
        func sendSingleMessage(recieverId: Int, message: String) {
            let param = [
                "message":message,
                "receiver_id":receiverID,
                "shipment_id":self.jobDetail?.shipment_id
            ] as? [String:Any]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_CHAT_MESSAGE, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ADD_CHAT_MESSAGE, userToken: nil) { (response) in
                
            }
        }
        
        
        @IBAction func rehireAction(_ sender: Any) {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! PageViewController
            Singleton.shared.postYourShipment = GetPostYourShipmentData()
            K_DRAFT_OR_EDIT = 2
            K_DRIVER_REHIRE_ID = self.driverId
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        
        
        //MARK: IBAction
        @IBAction func sendAction(_ sender: Any) {
            if(self.textView.text == "" || self.textView.text == "Type your message..."){
                return
            }
            
            self.isFiratTime = false
            if !(self.textView.text!.isEmpty && self.jobDetail?.shipment_id != nil){
                self.textView.resignFirstResponder()
                let id = self.chatId
                let time =  Int(Date().timeIntervalSince1970)
                if(self.isAdminChat && !self.textView.text!.isReallyEmpty){
                            let dbRef = ref.child("messages/" + self.chatId).childByAutoId().setValue([
                                "id": self.chatId,
                                "time": time,
                                "sender_id": self.senderID,
                                "receiver_id": self.receiverID,
                                "message": self.textView.text,
                                "type": 1,
                                "read_status": 1,
                            ])
                }else if(!self.textView.text!.isReallyEmpty) {
                self.sendSingleMessage(recieverId: self.senderID, message: self.textView.text)
                }
                self.chatData.append(ChatResponse(id: self.chatId, message: self.textView.text, read_satus: 1, receiver_id: self.receiverID, sender_id: self.senderID, time: Int(Date().timeIntervalSince1970), type: 1))
                
//                self.textView.text = ""
                self.textView.text = "Type your message..."
                self.textView.textColor = .lightGray
                self.textView.resignFirstResponder()
                self.chatTable.beginUpdates()
                self.chatTable.insertRows(at: [IndexPath(row: self.chatData.count - 1, section: 0)], with: .bottom)
                self.chatTable.endUpdates()
                self.chatTable.scrollToRow(at: IndexPath(row: self.chatData.count - 1, section: 0), at: .bottom, animated: false)
            }
        }
        
        
        @IBAction func attachmentAction(_ sender: Any) {
            let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as! UIView
                popoverController.sourceRect = (sender as? AnyObject)!.bounds
            }
            self.present(alert, animated: true, completion: nil)
        }
        
        
        
        @IBAction func backBtnPressed(_ sender: Any) {
            self.back()
        }
        
    }

extension ChatViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.scrollToIndex()
        
        if(textView.text == "Type your message..."){
            self.textView.text = ""
            self.textView.textColor = .black
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == ""){
            self.textView.text = "Type your message..."
            self.textView.textColor = .lightGray
        }
    }
    
    
}
    
    extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            self.noDataLabel.isHidden = self.chatData.count == 0 ? false : true
            return self.chatData.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let data = self.chatData[indexPath.row]
            var cell = ChatViewCell()
            if(Singleton.shared.userType.user_id == data.sender_id){
                cell = tableView.dequeueReusableCell(withIdentifier: "ChatViewCell2") as! ChatViewCell
            }else {
                cell = tableView.dequeueReusableCell(withIdentifier: "ChatViewCell1") as! ChatViewCell
                
            }
            
            if(data.type == 2){
                cell.messageTextView.isHidden = true
                cell.messageText.isHidden = true
                cell.uploadedImage.isHidden  = false
                cell.imageWidth.constant = 250
                if let profileURL = data.message as? String {
                    cell.uploadedImage.sd_setImage(with: URL(string:profileURL), placeholderImage: UIImage(named: ""))
                }
                else {
                    print("profileURL is nil")
                }
                
            }else {
                cell.messageTextView.isHidden = false
                cell.messageText.isHidden = false
                cell.uploadedImage.isHidden  = true
               
                
                
                cell.messageText.text = data.message
                var rect: CGRect = cell.messageText.frame
                rect.size = (cell.messageText.text?.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: cell.messageText.font.fontName , size: cell.messageText.font.pointSize)!]))! //Calculate as per label font
                cell.labelWidth.constant = rect.width
                cell.imageWidth.constant = rect.width
            }
            
            cell.messageText.text = data.message
            
            cell.messageTime.text = self.convertTimestampToDate(data.time ?? 0, to: "h:mm a, MMM dd")
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let cell = tableView.cellForRow(at: indexPath) as! ChatViewCell
            if(self.chatData[indexPath.row].type == 2){
                let imageView = UIImageView()
                let newImageView = UIImageView()
                if let profileURL = self.chatData[indexPath.row].message as? String {
                    newImageView.sd_setImage(with: URL(string:profileURL), placeholderImage: UIImage(named: ""))
                }
                newImageView.frame = UIScreen.main.bounds
                newImageView.backgroundColor = .black
                newImageView.contentMode = .scaleAspectFit
                newImageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
                newImageView.addGestureRecognizer(tap)
                self.view.addSubview(newImageView)
                self.navigationController?.isNavigationBarHidden = true
                self.tabBarController?.tabBar.isHidden = true
            }
        }
        
        @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
            self.navigationController?.isNavigationBarHidden = true
            self.tabBarController?.tabBar.isHidden = true
            sender.view?.removeFromSuperview()
        }
    }
    
    extension ChatViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
        
        
        func imagePickerController(_ picker: UIImagePickerController,
                                   didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
            guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
            
            let imageData:NSData = selectedImage.pngData()! as NSData
            
            let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
            if let cropImage = selectedImage as? UIImage {
                let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
                let params = [
                    "file": data!
                ] as [String : Any]
            
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.makeMultipartRequest(url: U_BASE + U_UPLOAD_IMAGE, fileData: data!, param: params, objectClass: UploadImage.self, fileName: imageName) { (path) in
                    var url = (path as! UploadImage).response.path ?? ""
                    self.urlPhoto = url
                    self.uplaodUserImage()
                    ActivityIndicator.hide()
                }
            }
           
            self.dismiss(animated: true, completion: nil)
        }
        
        func openCamera()
        {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
            {
                picker.sourceType = UIImagePickerController.SourceType.camera
                picker.allowsEditing = true
                self.present(picker, animated: true, completion: nil)
            }
            else
            {
                let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        func openGallary()
        {
            picker.sourceType = UIImagePickerController.SourceType.photoLibrary
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        
    }
    
    
    
    class ChatViewCell: UITableViewCell {
        //MARK:IBOutlets
        @IBOutlet weak var messageTime: UILabel!
        @IBOutlet weak var messageText: UILabel!
        @IBOutlet weak var uploadedImage: UIImageView!
        @IBOutlet weak var messageTextView: UIView!
        @IBOutlet weak var imageWidth: NSLayoutConstraint!
        @IBOutlet weak var labelWidth: NSLayoutConstraint!
        
        
        
    }
    
