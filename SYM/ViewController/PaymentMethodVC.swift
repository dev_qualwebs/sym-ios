//
//  PaymentMethodVC.swift
//  SYM
//
//  Created by AM on 22/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class PaymentMethodVC: UIViewController,AddCard {
    
    //MARK: IBOutlets
    @IBOutlet weak var cardTable: ContentSizedTableView!
    @IBOutlet weak var addNewView: UIView!
    @IBOutlet weak var dashView: UIView!
    @IBOutlet weak var dashSubview: UIView!
    @IBOutlet weak var proceedButton: CustomButton!
    @IBOutlet weak var popupView: UIStackView!
    @IBOutlet weak var popupText: DesignableUILabel!
    @IBOutlet weak var proceedButtonHeight: NSLayoutConstraint!
    
    var selectedPopup = Int()
    var cardData = [GetCardResponse]()
    var selectedId = String()
    var shipmentData = GetDriverSingleShipment()
    var isNavigationFromSideMenu = K_SIDE_MENU
    var popupId = String()
    static var FeatureDelegate : ReloadBankData? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavTitle(title: "Select Payment Card")
        self.setNavigationBar("back", "none")
        if(isNavigationFromSideMenu == K_SIDE_MENU){
            self.proceedButton.alpha = 0
            self.proceedButtonHeight.constant = 0
        }else if(isNavigationFromSideMenu == K_CONFIRM_ORDER){
            self.proceedButton.setTitle("Select", for: .normal)
            self.proceedButtonHeight.constant = 45
        }
        self.cardTable.tableFooterView = UIView()
        self.getCards()
    }
    
    func getCards(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_DRIVER_SAVED_CARDS, method: .post, parameter: nil, objectClass: GetCard.self, requestCode: U_GET_DRIVER_SAVED_CARDS, userToken: nil) { (response) in
            Singleton.shared.savedCardData = response.response
            self.cardData = response.response
            
            for i in 0..<self.cardData.count{
                if(self.cardData[i].is_default == 1){
                    self.selectedId = "\(self.cardData[i].id ?? 0)"
                }
            }
            
            self.cardTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func showPopup(title: String, msg: String, id: Int) {
        
        //           let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardPopupViewController") as! RewardPopupViewController
        //          myVC.modalPresentationStyle = .overFullScreen
        //          myVC.confirmationText = "Are you sure? \n You're removing a card."
        //          self.selectedId = id
        //          myVC.isConfirmationViewHidden = false
        //          myVC.confirmationDelegate = self
        //          self.navigationController?.present(myVC, animated: false, completion: nil)
    }
    
    func addCard() {
        self.getCards()
    }
    
    //MARK: IBActions
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
        myVC.cardDelegate = self
        myVC.shipmentData = self.shipmentData
        myVC.isAddCard = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func popupOkayAction(_ sender: Any) {
        if self.selectedPopup == 1 {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_CARD, method: .post, parameter: ["card_id":popupId], objectClass: Response.self, requestCode: U_DELETE_CARD, userToken: nil) { (response) in
                self.getCards()
                self.cardTable.reloadData()
                ActivityIndicator.hide()
            }
        } else if self.selectedPopup == 2{
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SET_CARD_AS_DEFAULT, method: .post, parameter: ["card_id":popupId], objectClass: Response.self, requestCode: U_SET_CARD_AS_DEFAULT, userToken: nil) { (response) in
                self.getCards()
                self.cardTable.reloadData()
                ActivityIndicator.hide()
            }
        }
        self.popupView.isHidden = true
    }
    
    @IBAction func popupCancelAction(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
    @IBAction func proceedAction(_ sender: Any) {
        if(self.selectedId == "" || self.selectedId == nil){
            Singleton.shared.showToast(text: "Select card to pay", color: errorRed)
        }else{
            if(isNavigationFromSideMenu == K_SIDE_MENU){
                
            }else if(isNavigationFromSideMenu == K_CONFIRM_ORDER){
                let card = self.cardData.filter{
                    "\(($0.id ?? 0))" == self.selectedId
                }[0]
                NotificationCenter.default.post(name: NSNotification.Name(N_SELECT_CARD), object:card)
                self.dismiss(animated: true, completion: nil)
            }else {
                
                var paymentDetail = GetCardResponse()
                paymentDetail = self.cardData.filter{
                    "\(($0.id ?? 0))" == self.selectedId
                }[0]
                
                ActivityIndicator.show(view: self.view)
                var param = [String:Any]()
                if(paymentDetail.id == nil){
                    param = [
                        "card_flag": 0,
                        "bid_id":self.shipmentData.own_bids?[0].id ?? 0,
                        "card_number": paymentDetail.card_number ?? 0,
                        "month": paymentDetail.expiry_month ?? "",
                        "year": paymentDetail.expiry_year ?? "",
                        "cvc":paymentDetail.customer ?? "",
                        "amount":shipmentData.sponser_amount ?? 0,
                    ]
                } else {
                    param = [
                        "card_flag": 1,
                        "card_id":paymentDetail.id ?? "",
                        "stripe_customer_id": paymentDetail.stripe_customer_id,
                        "bid_id":self.shipmentData.own_bids?[0].id,
                        "amount":self.shipmentData.sponser_amount ?? 0,
                    ]
                }
                
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_FEATURE_BID_DRIVER, method: .post, parameter: param, objectClass: Response.self, requestCode: U_FEATURE_BID_DRIVER, userToken: nil) { (response) in
                    ActivityIndicator.hide()
                    PaymentMethodVC.FeatureDelegate?.reloadFeatureData()
                    self.navigationController?.popViewController(animated: true)
                    Singleton.shared.showToast(text: "Successfully featured bid.", color: successGreen)
                }
                
                
                
                
            }
            
        }
    }
}

extension PaymentMethodVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.cardData.count == 0){
            self.dashView.isHidden = false
            self.cardTable.isHidden = true
        }else {
            self.dashView.isHidden = true
            self.cardTable.isHidden = false
        }
        return self.cardData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddCardCell") as! AddCardCell
        let val = self.cardData[indexPath.row]
        switch val.card_type?.lowercased() {
        case "visa":
            //cell.visaImage.image = UIImage(named: "myvisa")
            cell.masterOrVisa.text = "VISA"
            break
        case "mastercard":
            // cell.visaImage.image = UIImage(named: "mastercard")
            cell.masterOrVisa.text = "MasterCard"
            break
            
        case "jcb":
            //            cell.visaImage.image = UIImage(named: "jcb")
            cell.masterOrVisa.text = "jcb"
            break
        case "discover":
            cell.visaImage.image = UIImage(named: "discover")
            cell.masterOrVisa.text = "Discover"
            break
        case "UNKNOWN":
            //            cell.visaImage.image = nil
            cell.masterOrVisa.text = ""
            break
        case "amex":
            //cell.visaImage.image = UIImage(named: "american-express")
            cell.masterOrVisa.text = "American-Expersion"
            break
        default:
            // cell.visaImage.image = nil
            cell.masterOrVisa.text = ""
        }
        cell.viewRemoveButton.isHidden = val.is_default == 0 ? false : true
        cell.defaultView.isHidden = false
        cell.defaultImage.isHidden = val.is_default == 1 ? false : true
        
        cell.cardNumber.text = "XXXX XXXX " + "\(val.masked_card_number ?? ""   )"
        cell.cardHolderName.text = val.name
        
        cell.expiryDate.text = "\(val.expiry_month ?? "")" + "/" + "\(val.expiry_year ?? "")"
        
        if(selectedId == "\(val.id ?? 0)"){
            cell.mainView.borderWidth = 2
            cell.mainView.borderColor = primaryColor
        } else {
            cell.mainView.borderWidth = 0
        }
        
        cell.removeCard = {
            self.popupId = "\(val.id ?? 0)"
            self.selectedPopup = 1
            self.popupText.text = "Are you sure you want to remove this card?"
            self.popupView.isHidden = false
        }
        
        cell.defaultButton = {
            if val.is_default == 0 {
                self.popupId = "\(val.id ?? 0)"
                self.selectedPopup = 2
                self.popupText.text = "Are you sure you want to make this as a default card?"
                self.popupView.isHidden = false
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedId = "\(self.cardData[indexPath.row].id ?? 0)"
        self.cardTable.reloadData()
    }
    
}


class AddCardCell: UITableViewCell{
    //MARK:IBOutlets
    @IBOutlet weak var cardNumber: DesignableUILabel!
    @IBOutlet weak var cardHolderName: UILabel!
    @IBOutlet weak var expiryDate: UILabel!
    @IBOutlet weak var visaImage: UIImageView!
    @IBOutlet weak var tickButton: UIButton!
    @IBOutlet weak var viewRemoveButton: UIView!
    @IBOutlet weak var defaultImage: UIImageView!
    @IBOutlet weak var defaultView: UIView!
    @IBOutlet weak var masterOrVisa: DesignableUILabel!
    @IBOutlet weak var mainView: View!
    
    var removeCard:(()-> Void)? = nil
    var defaultButton:(()-> Void)? = nil
    
    @IBAction func setDefaultAction(_ sender: Any) {
        if let defaultButton = self.defaultButton{
            defaultButton()
        }
    }
    
    
    @IBAction func removeAction(_ sender: Any) {
        if let removeCard = self.removeCard{
            removeCard()
        }
    }
}

