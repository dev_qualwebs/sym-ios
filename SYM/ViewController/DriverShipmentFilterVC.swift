//
//  DriverShipmentFilterVC.swift
//  SYM
//
//  Created by AM on 04/12/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import iOSDropDown

protocol ApplyFilter {
    func applyShipmentFilter(val:[String:Any])
}

class DriverShipmentFilterVC: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var zipCode: DesignableUITextField!
    @IBOutlet weak var pickupDate: DesignableUITextField!
    @IBOutlet weak var categoryDropdown: DropDown!
    @IBOutlet weak var pickupState: DropDown!
    @IBOutlet weak var dropoffDate: DesignableUITextField!
    @IBOutlet weak var dropOffState: DropDown!
    @IBOutlet weak var pickupCity: DropDown!
    @IBOutlet weak var dropoffCity: DropDown!
    @IBOutlet weak var destinationEndDate: DesignableUITextField!
    @IBOutlet weak var pickupEndDate: DesignableUITextField!
    
    var selectedDate = Int()
    var selectedCategory = GetCategories()
    var currentPicker = Int()
    var filterDelegate: ApplyFilter? = nil
    var param = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.zipCode.delegate = self
        self.initializeView()
    }
    
    @objc func dateAdd(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_Date_Picker), object: nil)
        if(self.currentPicker == 1){
            self.pickupDate.text = "\(K_SELECTED_DATE)"
        }else if(self.currentPicker == 2){
            self.dropoffDate.text = "\(K_SELECTED_DATE)"
        }else if(self.currentPicker == 3){
            self.pickupEndDate.text = "\(K_SELECTED_DATE)"
        }else if(self.currentPicker == 4){
            self.destinationEndDate.text = "\(K_SELECTED_DATE)"
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(dateAdd(_:)), name: NSNotification.Name(N_Date_Picker), object: nil)
    }
    
    func initializeView(){
        for val in Singleton.shared.categoryData{
            self.categoryDropdown.optionArray.append(val.category ?? "")
        }
        
        self.categoryDropdown.didSelect { (value,id, index) in
            self.selectedCategory = Singleton.shared.categoryData[id]
        }
        
        self.categoryDropdown.text = param["category"] as? String
        self.pickupDate.text = param["pickup_start_date"] as? String
        self.dropoffDate.text = param["dropoff_end_date"] as? String
        self.pickupState.text = param["source_state"] as? String
        self.dropOffState.text = param["destination_state"] as? String
        self.pickupCity.text = param["source_city"] as? String
        self.dropoffCity.text = param["dropoff_city"] as? String
        self.zipCode.text = param["zipcode"] as? String
        
        self.pickupEndDate.text = param["pickup_end_date"] as? String
        self.dropoffCity.text = param["destination_city"] as? String
        self.destinationEndDate.text = param["dropoff_end_date"] as? String
        
        for val in Singleton.shared.categoryData{
            if(val.id == param["category"] as? Int){
                self.categoryDropdown.text = val.category
                self.selectedCategory = val
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(dateAdd(_:)), name: NSNotification.Name(N_Date_Picker), object: nil)
        for val in Singleton.shared.stateNames {
            self.pickupState.optionArray.append(val.state ?? "")
            self.dropOffState.optionArray.append(val.state ?? "")
        }
        
        for val in Singleton.shared.cityNames {
            self.pickupCity.optionArray.append(val.from_city ?? "")
            self.dropoffCity.optionArray.append(val.from_city ?? "")
        }
    }
    
    
    
    
    @IBAction func dismissBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dateAction(_ sender: UIButton) {
        self.currentPicker = sender.tag
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        myVC.picker_type = 1
        myVC.modalPresentationStyle = .overFullScreen
        
        self.present(myVC, animated: true, completion: nil)
     }
    
    @IBAction func clearAction(_ sender: Any) {
        self.categoryDropdown.text = ""
        self.pickupDate.text = ""
        self.dropoffDate.text = ""
        self.pickupState.text = ""
        self.pickupEndDate.text = ""
        self.destinationEndDate.text = ""
        self.dropOffState.text = ""
        self.pickupCity.text = ""
        self.dropoffCity.text = ""
        self.zipCode.text = ""
        self.selectedCategory = GetCategories()
    }
    
    @IBAction func filterAction(_ sender: Any) {
        let val:[String:Any] = [
           // "category":self.selectedCategory.id ?? 0,
            "pickup_start_date":self.pickupDate.text ?? "",
            "dropoff_start_date":self.dropoffDate.text ?? "",
            "source_state":self.pickupState.text ?? "",
            "destination_state":self.dropOffState.text ?? "",
            "source_city":self.pickupCity.text ?? "",
            "destination_city":self.dropoffCity.text ?? "",
            "pickup_end_date" : self.pickupEndDate.text ?? "",
            "dropoff_end_date" : self.destinationEndDate.text ?? "",
           // "zipcode":self.zipCode.text ?? "",
            "flag":K_CURRENT_CATEGORY,
            "location":"",
            "offset":"1",
        ]
        self.filterDelegate?.applyShipmentFilter(val: val)
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension DriverShipmentFilterVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         if (textField == zipCode) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 6
        }else {
            return true
        }
    }

}
