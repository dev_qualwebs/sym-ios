//
//  RatingViewController.swift
//  SYM
//
//  Created by qw on 20/07/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
import Cosmos

class RatingViewController: UIViewController, UITextViewDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var comment: UITextView!
    @IBOutlet weak var yesButton: CustomButton!
    @IBOutlet weak var noButton: CustomButton!
    
    var shipmentId  = Int()
    var shipmentData = GetUserShipmentData()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        self.ratingView.rating = 0
        self.setNavTitle(title: "Rate Driver")
        self.setNavigationBar("back", nil)
        self.comment.text = "Your comment"
        self.comment.delegate = self
        self.getSingleShipments()
        
    }
    
    
    func getSingleShipments(){
        ActivityIndicator.show(view: self.view)
        let param:[String:Any] = ["shipment_id":shipmentId]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_CUSTOMER_SINGLE_SHIPMENT, method: .post, parameter: param, objectClass: CustomerSingleShipment.self, requestCode: U_CUSTOMER_SINGLE_SHIPMENT, userToken: nil) { (response) in
            self.shipmentData = response.response
        }
    }
    
    //MARK: IBActions
    @IBAction func submitAction(_ sender: Any) {
        if(self.comment.text == "Your comment"){
            Singleton.shared.showToast(text: "Plese write some comment", color: errorRed)
        }else{
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "given_to":self.shipmentData.completed_driver_id ?? 0,
                "shipment_id":self.shipmentId,
                "review":self.comment.text ?? "",
                "rating":self.ratingView.rating
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_RATE_USER, method: .post, parameter: param, objectClass: Response.self, requestCode: U_RATE_USER, userToken: nil) { (response) in
                ActivityIndicator.hide()
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "UserTabViewController") as! UserTabViewController
                self.navigationController?.pushViewController(myVC, animated: true)
                Singleton.shared.showToast(text: "Shipment rated successfully.", color: successGreen)
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Your comment"){
            self.comment.text = ""
            self.comment.textColor = .black
        }else{
            self.comment.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == ""){
            self.comment.text = "Your comment"
            self.comment.textColor = .lightGray
        }else{
            self.comment.textColor = .black
        }
    }
}
