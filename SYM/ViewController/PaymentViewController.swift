//
//  PaymentViewController.swift
//  SYM
//
//  Created by qw on 07/05/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var sessionExpireBtn: CustomButton!
    @IBOutlet weak var lblDateTime: DesignableUILabel!
    @IBOutlet weak var lblSource: DesignableUILabel!
    @IBOutlet weak var lblDestination: DesignableUILabel!
    @IBOutlet weak var lblRouteDistance: DesignableUILabel!
    @IBOutlet weak var lblEstPrice: DesignableUILabel!
    @IBOutlet weak var lblCategory: DesignableUILabel!
    @IBOutlet weak var lblWeight: DesignableUILabel!
    @IBOutlet weak var lblAttachments: DesignableUILabel!
    @IBOutlet weak var attachmentCollection: UICollectionView!
    @IBOutlet weak var lblDscrptionbyClient: DesignableUILabel!
    @IBOutlet weak var viewForExpire: UIView!
    @IBOutlet weak var paymentButton: CustomButton!
    
    
    var shipmentData = GetDriverSingleShipment()
    var paymentDetail = GetCardResponse()
    var transactionDetail:StartTransactionResponse?
    var sessionTime = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavTitle(title: "Pay for Shipment")
        self.setNavigationBar("back", nil)
        if((self.shipmentData.driver_status ?? 0) > 2){
            self.paymentButton.setTitle("Pay", for: .normal)
            self.viewForExpire.isHidden = true
            //  self.startTransactionApi()
        }else {
            self.viewForExpire.isHidden = true
            // self.startTransactionApi()
        }
        self.handleView()
        
    }
    
    func handleView(){
        self.attachmentCollection.delegate = self
        self.attachmentCollection.dataSource = self
        self.attachmentCollection.reloadData()
        self.lblDateTime.text = "\(self.convertTimestampToDate(shipmentData.pickup_date ?? 0, to: "EEE, dd-MM-yy, hh:mm a"))"
        self.lblSource.text = "\(shipmentData.from_name ?? "")"
        self.lblDestination.text = "\(shipmentData.to_name ?? "")"
        self.lblRouteDistance.text = "\(shipmentData.distance ?? "")"
        self.lblEstPrice.text = "$\(shipmentData.price ?? "0")"
        self.lblCategory.text = "\(shipmentData.category_name?.category ?? "")"
        self.lblDscrptionbyClient.text = shipmentData.description
        self.lblWeight.text = "\(shipmentData.weight ?? 0)"
        
    }
    
    func startTransactionApi(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_DRIVER_START_TRANSACTION, method: .post, parameter: ["shipment_id": self.shipmentData.id ?? 0], objectClass: StartTransaction.self, requestCode: U_DRIVER_START_TRANSACTION, userToken: nil) { (response) in
            ActivityIndicator.hide()
            self.transactionDetail = response.response
            self.callSessionApi(id:response.response?.transaction?.transaction_id ?? "")
        }
    }
    
    func callSessionApi(id: String){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_DRIVER_CHECK_SESSION, method: .post, parameter: ["transaction_id":id], objectClass: StartTransaction.self, requestCode: U_DRIVER_CHECK_SESSION, userToken: nil) { (response) in
            ActivityIndicator.hide()
            self.sessionTime = self.calculateTimeDifference(date1:Int(Date().timeIntervalSince1970), date2: response.response?.expiry_time ?? 0)
            
            Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.startTimer), userInfo: nil, repeats: true)
            
            self.startTimer()
        }
    }
    
    func callAcceptBidApi(){
        let param:[String:Any] = [
            "shipment_id":self.shipmentData.id,
            "driver_id":Singleton.shared.userType.user_id,
            "driver_response":1
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_ACCEPT_BID_DRIVER, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ACCEPT_BID_DRIVER, userToken:nil) { (response) in
            Router.DriverHomeVC()
            Singleton.shared.showToast(text: "Successfully accepted shipment.", color: successGreen)
        }
        
    }
    
    func callPaymentApi(){
        if(self.sessionTime == 0){
            Singleton.shared.showToast(text: "Your session expires", color: errorRed)
        }else {
            ActivityIndicator.show(view: self.view)
            var param = [String:Any]()
            if(self.paymentDetail.id == nil){
                param = [
                    "customer_id":"",
                    "status_key":3,
                    "transaction_id":self.transactionDetail?.transaction?.transaction_id,
                    "card_number":self.paymentDetail.card_number ?? 0,
                    "month":self.paymentDetail.expiry_month ?? "",
                    "year":self.paymentDetail.expiry_year ?? "",
                    "cvc": self.paymentDetail.customer ?? "0" ,
                    "amount":self.shipmentData.sponser_amount ?? 0,
                ]
            }else {
                param = [
                    "customer_id":self.paymentDetail.customer ?? "",
                    "status_key":3,
                    "transaction_id":self.transactionDetail?.transaction?.transaction_id,
                    "amount":self.shipmentData.sponser_amount ?? 0,
                ]
            }
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_PAYMENT_API, method: .post, parameter: param, objectClass: Response.self, requestCode: U_PAYMENT_API, userToken: nil) { (response) in
                ActivityIndicator.hide()
                self.callAcceptBidApi()
                
            }
        }
    }
    
    func callFeatureBidAPI(){
        ActivityIndicator.show(view: self.view)
        var param = [String:Any]()
        if(self.paymentDetail.id == nil){
            param = [
                "card_flag": 0,
                "bid_id":self.shipmentData.own_bids?[0].id,
                "card_number": self.paymentDetail.card_number ?? 0,
                "month": self.paymentDetail.expiry_month ?? "",
                "year": self.paymentDetail.expiry_year ?? "",
                "cvc":self.paymentDetail.customer ?? "",
                "amount":self.shipmentData.sponser_amount ?? 0,
            ]
        }else {
            param = [
                "card_flag": 1,
                "card_id":self.paymentDetail.id ?? "",
                "stripe_customer_id": self.paymentDetail.customer,
                "bid_id":self.shipmentData.own_bids?[0].id,                "amount":self.shipmentData.sponser_amount ?? 0,
            ]
        }
        
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_FEATURE_BID_DRIVER, method: .post, parameter: param, objectClass: Response.self, requestCode: U_FEATURE_BID_DRIVER, userToken: nil) { (response) in
            ActivityIndicator.hide()
            Router.DriverHomeVC()
            Singleton.shared.showToast(text: "Successfully featured bid.", color: successGreen)
        }
        
    }
    
    @objc func startTimer(){
        if self.sessionTime > 0 {
            self.sessionTime = self.sessionTime - 1
            self.sessionExpireBtn.setTitle("Your session will expire in: \(self.sessionTime)", for: .normal)
            self.sessionExpireBtn.layoutIfNeeded()
            UIView.setAnimationsEnabled(false)
        }else if(self.sessionTime == 0){
            self.sessionExpireBtn.setTitle("Click to reset session", for: .normal)
            self.sessionExpireBtn.layoutIfNeeded()
            UIView.setAnimationsEnabled(false)
        }
    }
    
    //MARK:IBActions
    @IBAction func callSessionApi(_ sender: Any) {
        if(self.sessionExpireBtn.titleLabel!.text == "Click to reset session"){
            self.sessionExpireBtn.setTitle("Your session will expire in: 0", for: .normal)
            self.callSessionApi(id: self.transactionDetail?.transaction?.transaction_id ?? "")
        }
    }
    
    @IBAction func viewMapAction(_ sender: Any) {
        let myVC = storyboard!.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        myVC.pickUpLatitude = self.shipmentData.from_latitude ?? ""
        myVC.pickUpLongitude = self.shipmentData.from_longitude ?? ""
        myVC.dropOffLatitude = self.shipmentData.to_latitude ?? ""
        myVC.dropOffLongitude = self.shipmentData.to_longitude ?? ""
        myVC.pickup_from = self.shipmentData.from_name ?? ""
        myVC.pickup_to = self.shipmentData.to_name ?? ""
        if(self.shipmentData.lookingFor == 1){
            myVC.shipmentStatus = self.shipmentData.driver_status ?? 0
        }else if(self.shipmentData.lookingFor == 2){
            myVC.shipmentStatus = self.shipmentData.loader_status ?? 0
        }else if(self.shipmentData.lookingFor == 3){
            myVC.shipmentStatus = self.shipmentData.driver_status ?? 0
        }else{
            myVC.shipmentStatus = self.shipmentData.driver_status ?? 0
        }
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func fetureBIdAction(_ sender: Any) {
        if(paymentButton.titleLabel!.text == "Pay"){
            self.callPaymentApi()
        }else {
            self.callFeatureBidAPI()
        }
        
    }
}

extension PaymentViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.shipmentData.image_list?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCell", for: indexPath) as! AttachmentCell
        cell.attachmentImage.sd_setImage(with: URL(string:U_IMAGE + (self.shipmentData.image_list?[indexPath.row].image ?? "")), placeholderImage: nil)
        return cell
    }
    
}
