//
//  TransactionViewController.swift
//  SYM
//
//  Created by Sagar Pandit on 26/05/21.
//  Copyright © 2021 AM. All rights reserved.
//

import UIKit

class TransactionViewController: UIViewController {
   //MARK: IBOutlets
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var transactionTable: UITableView!
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var transactionFrom: DesignableUITextField!
    @IBOutlet weak var transactionTo: DesignableUITextField!
    
    var transactionData = [TransactionResponse]()
    var currentPicker = 1
    var param = [String:Any]()
    var timer = Timer()
    var timeStarted = 0
    var searchTransactionText = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavTitle(title: "Transactions")
        self.setNavigationBar("back", "none")
        
        self.transactionTable.estimatedRowHeight = 100
        self.transactionTable.rowHeight = UITableView.automaticDimension
        
        self.searchField.delegate = self
        self.getTransactionData(text: "")
        self.transactionFrom.placeholder = "Select Date"
        self.transactionTo.placeholder = "Select Date"
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePicker), name: NSNotification.Name(N_Date_Picker), object: nil)
    }
    
    @objc func handlePicker(){
        if(self.currentPicker == 1){
            self.transactionFrom.text = K_SELECTED_DATE
        }else {
            self.transactionTo.text = K_SELECTED_DATE
        }
    }
    
    @objc func timerAction(){
        if timeStarted <= 5 {
        timeStarted += 1
        print(timeStarted)
            if self.timeStarted == 5 {
            timer.invalidate()
            self.getTransactionData(text: searchTransactionText)
        }
        } else {
            timer.invalidate()
        }
        
        }
    
    func getTransactionData(text: String?){
        ActivityIndicator.show(view: self.view)
        var url = ""
        if(Singleton.shared.userType.role == 1){
            url = U_BASE + U_GET_CUSTOMER_TRANSACTION
        }else {
            url = U_BASE + U_GET_DRIVER_TRANSACTION
        }
//        let startDate = self.convertDateToTimestamp(self.transactionFrom.text ?? "", to: K_CUSTOM_YYYY_MM_DD)
//        let endDate = self.convertDateToTimestamp(self.transactionTo.text ?? "", to: K_CUSTOM_YYYY_MM_DD)
        
        let param:[String:Any] = [
            "search":self.searchField.text ?? "",
            "start_date":self.transactionFrom.text,
            "end_date": self.transactionTo.text 
        ]
        SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: param, objectClass: GetTransactions.self, requestCode: url, userToken: nil) { (response) in
            self.transactionData = response.response
            self.transactionTable.reloadData()
        }
    }
    
    //MARK: IBActions
    @IBAction func filterAction(_ sender: Any) {
        self.popupView.isHidden = false
    }
    
    @IBAction func applyAction(_ sender: Any) {
        self.popupView.isHidden = true
        self.searchField.text = ""
        self.getTransactionData(text: "")
    }
    
    @IBAction func openPicker(_ sender: UIButton) {
        self.currentPicker = sender.tag
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        myVC.picker_type = 1
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func clearAction(_ sender: Any) {
        self.popupView.isHidden = true
        self.searchField.text = ""
        self.transactionFrom.text = ""
        self.transactionTo.text = ""
        self.getTransactionData(text: "")
    }
}

extension TransactionViewController: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.searchTransactionText = (self.searchField.text ?? "") + string
        self.timeStarted = 0
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.transactionData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DriverMessagesTableCell") as! DriverMessagesTableCell
        let val = self.transactionData[indexPath.row]
      //  cell.username.text = val.transaction[0].transaction_id ?? ""
        cell.username.text = (val.from_city ?? "") + " - " + (val.to_city ?? "")
        
        if(val.payment_flag == 2){
            cell.userMessage.text = "Full payment transferred"
        }else{
            cell.userMessage.text = "50% Transferred"
        }
        if val.transaction.count > 0 {
            cell.date.text = self.convertTimestampToDate(Int(val.transaction[val.transaction.count - 1].created_date ?? "0")!, to: "dd MMM, yyyy")
           
            cell.price.text = "$\(val.transaction[val.transaction.count - 1].amount ?? "")"
           
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TransactionDetailViewController") as! TransactionDetailViewController
        myVC.transactionData = self.transactionData[indexPath.row]
        myVC.id = self.transactionData[indexPath.row].id ?? 0
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
}
