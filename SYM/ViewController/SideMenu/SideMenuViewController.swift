//
//  SideMenuViewController.swift
//  SYM
//
//  Created by AM on 18/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SDWebImage

class SideMenuViewController: UIViewController {
    
    //Mark: IBOUTLETS
    @IBOutlet weak var tableViewMenu: UITableView!
    @IBOutlet weak var lblUserName: DesignableUILabel!
    @IBOutlet weak var lblEmail: DesignableUILabel!
    @IBOutlet weak var userImgView: ImageView!
    
    //Mark: Properties
    var arrayMenu = [MenuObject]()
    var type = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let image = (Singleton.shared.userType.profile_image ?? "")
        
        self.userImgView.sd_setImage(with: URL(string: U_IMAGE + image), placeholderImage: #imageLiteral(resourceName: "AppIcon"))
        
        lblUserName.text = "\(Singleton.shared.userType.first_name ?? "") \(Singleton.shared.userType.last_name ?? "")"
        lblEmail.text = "\(Singleton.shared.userType.email ?? "")"
        self.tableViewMenu.isUserInteractionEnabled = true
        
        // MenuObject(image: #imageLiteral(resourceName: "home-dispute") , name: "Disputes"),
        //        MenuObject(image: #imageLiteral(resourceName: "tracking") , name: "Moving Resouces"),MenuObject(image: #imageLiteral(resourceName: "tracking") , name: "Moving Resouces"),
        if Singleton.shared.userType.role as? Int == 1{
            arrayMenu = [MenuObject(image: UIImage(named: "") , name: "Profile"),MenuObject(image: #imageLiteral(resourceName: "tracking") , name: "Post your Move"), MenuObject(image: #imageLiteral(resourceName: "payment_cards") , name: "Manage Cards"),MenuObject(image: #imageLiteral(resourceName: "ringing") , name: "Notifications"),MenuObject(image: #imageLiteral(resourceName: "home-dispute") , name: "Disputes"),MenuObject(image: #imageLiteral(resourceName: "completed_shipment") , name: "Completed Shipment"),MenuObject(image: #imageLiteral(resourceName: "cancelled_shipment") , name: "Cancelled Shipments"),
                         MenuObject(image: #imageLiteral(resourceName: "transaction-1") , name: "Transactions"),
                         MenuObject(image: #imageLiteral(resourceName: "support"), name: "Support"),
                         MenuObject(image: #imageLiteral(resourceName: "terms"), name: "Terms of Service"), MenuObject(image: #imageLiteral(resourceName: "policy"), name: "Policy"),MenuObject(image: #imageLiteral(resourceName: "share(6)") , name: "Refer App"), MenuObject(image: #imageLiteral(resourceName: "logout(2)"), name: "Log Out")]
            tableViewMenu.reloadData()
        }
        else {
            arrayMenu =  [MenuObject(image: UIImage(named: "") , name: "Profile"), MenuObject(image: #imageLiteral(resourceName: "payment_cards") , name: "Manage Cards"),MenuObject(image: #imageLiteral(resourceName: "ringing") , name: "Notifications"),
                          MenuObject(image: #imageLiteral(resourceName: "transaction-1") , name: "Transactions"), MenuObject(image: #imageLiteral(resourceName: "support"), name: "Support"), MenuObject(image: #imageLiteral(resourceName: "terms"), name: "Terms of Service"), MenuObject(image: #imageLiteral(resourceName: "policy"), name: "Policy"),MenuObject(image: #imageLiteral(resourceName: "share(6)") , name: "Refer App"), MenuObject(image: #imageLiteral(resourceName: "logout(2)"), name: "Log Out")]
            UIView.performWithoutAnimation {
                tableViewMenu.reloadData()
            }
        }
    }
    
    @IBAction func editProfileBtnPressed(_ sender: Any) {
        // let eVC = storyboard!.instantiateViewController(withIdentifier: "EditProfileVC")
        // self.navigationController?.pushViewController(eVC, animated: true)
    }
}

extension SideMenuViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell", for: indexPath) as! SideMenuTableViewCell
        if(indexPath.row == 0){
            let imageIcon = (Singleton.shared.userType.profile_image ?? "")
            cell.cellImage.sd_setImage(with: URL(string: U_IMAGE + imageIcon), placeholderImage: #imageLiteral(resourceName: "AppIcon"))
            cell.cellImage.contentMode = .scaleAspectFit
            cell.cellImage.circle = true
            
        }else{
            cell.cellImage.contentMode = .scaleAspectFit
            cell.cellImage.circle = false
            cell.cellImage.image = arrayMenu[indexPath.row].image ?? UIImage(named: "")
            cell.cellImage.tintColor = .black
        }
        
        cell.labelSideMenu.text = arrayMenu[indexPath.row].name!
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableViewMenu.isUserInteractionEnabled = false
        let selectedCell = tableView.cellForRow(at: indexPath) as? SideMenuTableViewCell
        switch (arrayMenu[indexPath.row]).name {
            
        case "Post your Move":
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! PageViewController
            Singleton.shared.postYourShipment = GetPostYourShipmentData()
            K_DRAFT_OR_EDIT = 2
            K_DRIVER_REHIRE_ID = 0
            self.navigationController?.pushViewController(controller, animated: true)
            self.tableViewMenu.isUserInteractionEnabled = true
            break
        case "Profile":
            if(Singleton.shared.userType.role == 1){
                let eVC = storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
                self.tableViewMenu.isUserInteractionEnabled = true
                self.navigationController?.pushViewController(eVC, animated: true)
            }else {
                let eVC = storyboard?.instantiateViewController(withIdentifier: "DriverProfileViewController") as! DriverProfileViewController
                self.tableViewMenu.isUserInteractionEnabled = true
                self.navigationController?.pushViewController(eVC, animated: true)
            }
            break
        case "Notifications":
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(controller, animated: true)
            self.tableViewMenu.isUserInteractionEnabled = true
            break
        case "Message":
            let controller = storyboard?.instantiateViewController(withIdentifier: "DriverMessagesVC") as! DriverMessagesVC
            self.navigationController?.pushViewController(controller, animated: true)
            self.tableViewMenu.isUserInteractionEnabled = true
            break
        case "Manage Cards":
            // if(Singleton.shared.userType.role == 1){
            let controller = storyboard?.instantiateViewController(withIdentifier: "PaymentMethodVC") as! PaymentMethodVC
            controller.isNavigationFromSideMenu = K_SIDE_MENU
            self.navigationController?.pushViewController(controller, animated: true)
            self.tableViewMenu.isUserInteractionEnabled = true
            //}
            //            else {
            //                let controller = storyboard?.instantiateViewController(withIdentifier: "PaymentTabViewController") as! PaymentTabViewController
            //                self.navigationController?.pushViewController(controller, animated: true)
            //                self.tableViewMenu.isUserInteractionEnabled = true
            //            }
            break
        case "Disputes":
            let controller = storyboard?.instantiateViewController(withIdentifier: "DriverDisputesVC") as! DriverDisputesVC
            self.navigationController?.pushViewController(controller, animated: true)
            self.tableViewMenu.isUserInteractionEnabled = true
            break
            
        case "Completed Shipment":
            let controller = storyboard?.instantiateViewController(withIdentifier: "DriverDisputesVC") as! DriverDisputesVC
            controller.redirection = 2
            self.navigationController?.pushViewController(controller, animated: true)
            self.tableViewMenu.isUserInteractionEnabled = true
            break
        case "Moving Resouces":
            //            let controller = storyboard?.instantiateViewController(withIdentifier: "EcommerceLandingViewController") as! EcommerceLandingViewController
            let controller = storyboard?.instantiateViewController(withIdentifier: "EcommerceCategoryViewController") as! EcommerceCategoryViewController
            self.navigationController?.pushViewController(controller, animated: true)
            self.tableViewMenu.isUserInteractionEnabled = true
            break
        case "Transactions":
            let controller = storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
            self.navigationController?.pushViewController(controller, animated: true)
            self.tableViewMenu.isUserInteractionEnabled = true
            break
        case "Shipping Address":
            let controller = storyboard?.instantiateViewController(withIdentifier: "ShippingAddressViewController") as! ShippingAddressViewController
            self.navigationController?.pushViewController(controller, animated: true)
            self.tableViewMenu.isUserInteractionEnabled = true
            break
        case "Order History":
            let controller = storyboard?.instantiateViewController(withIdentifier: "MyOrdersViewController") as! MyOrdersViewController
            self.navigationController?.pushViewController(controller, animated: true)
            self.tableViewMenu.isUserInteractionEnabled = true
            break
        case "Cancelled Shipments":
            let controller = storyboard?.instantiateViewController(withIdentifier: "CancelledShipmentViewController") as! CancelledShipmentViewController
            self.navigationController?.pushViewController(controller, animated: true)
            self.tableViewMenu.isUserInteractionEnabled = true
            break
        case "Chat with Admin":
            
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            myVC.jobDetail = ChatDetail(sender_id:Singleton.shared.userType.user_id ?? 0, receiver_id:  0, receiver_name: "Admin", sender_name:(Singleton.shared.userType.first_name ?? "") + " " + (Singleton.shared.userType.last_name ?? ""), sender_image: (Singleton.shared.userType.profile_image ?? ""), receiver_image:"" , shipment_id: nil)
            myVC.isAdminChat = true
            self.navigationController?.pushViewController(myVC, animated: true)
            
        case "Support":
            let controller = storyboard?.instantiateViewController(withIdentifier: "DriverSupportVC") as! DriverSupportVC
            self.navigationController?.pushViewController(controller, animated: true)
            self.tableViewMenu.isUserInteractionEnabled = true
            break
        case "Log Out":
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_LOGOUT, method: .get, parameter: nil, objectClass: Response.self, requestCode: U_LOGOUT, userToken: nil) { (response) in
            }
            UserDefaults.standard.removeObject(forKey: UD_TOKEN)
            UserDefaults.standard.removeObject(forKey: UD_ROLE)
            Singleton.shared.userType = TypeOfUser()
            Singleton.shared.postYourShipment = GetPostYourShipmentData()
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "EntryViewController") as! EntryViewController
            self.navigationController?.pushViewController(controller, animated: true)
            ActivityIndicator.hide()
            self.tableViewMenu.isUserInteractionEnabled = true
            break
        case "Policy":
            self.openUrl(urlStr: U_PRIVACY_POLICY)
            self.tableViewMenu.isUserInteractionEnabled = true
            break
        case "Terms of Service":
            self.openUrl(urlStr: U_TERMS_CONDITION)
            self.tableViewMenu.isUserInteractionEnabled = true
            break
        case "Refer App":
            if let urlStr = NSURL(string: "https://apps.apple.com/us/app/id1582036445") {
                let objectsToShare = [urlStr]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
                if UI_USER_INTERFACE_IDIOM() == .pad {
                    if let popup = activityVC.popoverPresentationController {
                        popup.sourceView = self.view
                        popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
                    }
                }
                activityVC.modalPresentationStyle = .overFullScreen
                self.present(activityVC, animated: true, completion: nil)
            }
            break
        default:
            self.tableViewMenu.isUserInteractionEnabled = true
            print("default")
            
        }
    }
}


class SideMenuTableViewCell: UITableViewCell{
    @IBOutlet weak var cellImage: ImageView!
    @IBOutlet weak var labelSideMenu: UILabel!
}
