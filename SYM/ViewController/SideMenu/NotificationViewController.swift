//
//  NotificationViewController.swift
//  SYM
//
//  Created by Sagar Pandit on 29/05/21.
//  Copyright © 2021 AM. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView == self.notificationTable){
            if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && (scrollView.contentOffset.y > 50) && !isLoadingList){
                self.isLoadingList = true
                self.loadMoreItemsForList()
            }
        }
    }
    
    func loadMoreItemsForList(){
        currentPage += 1
        self.getNotificationData()
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var noDatalabel: DesignableUILabel!
    @IBOutlet weak var notificationTable: UITableView!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    
    var notificationData = [Notifications]()
    var currentPage = 1
    var isLoadingList = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavTitle(title: "Notifications")
        self.setNavigationBar("back", "notif")
        self.getNotificationData()
    }
    
    func getNotificationData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_NOTIFICATION + "?page=" + "\(currentPage)", method: .get, parameter: nil, objectClass: GetNotificationsResponse.self, requestCode: U_GET_NOTIFICATION, userToken: nil) { (response) in
            
            if(self.notificationData.count == 0 || self.currentPage == 1 ){
                self.notificationData = response.response?.notification?.data ?? []
                self.isLoadingList = false
            } else if (response.response?.notification?.data?.count ?? 0) > 0{
                for i in 0..<(response.response?.notification?.data?.count ?? 0){
                    self.notificationData.append(response.response?.notification?.data?[i] ?? Notifications())
                }
                self.isLoadingList = false
            }
            if((response.response?.notification?.data?.count ?? 0) == 0){
                if self.currentPage > 1 {
                    self.currentPage -= 1
                }
                self.isLoadingList = false
            }
            
            self.notificationTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    
}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.notificationData.count == 0){
            self.noDatalabel.isHidden = false
        }else {
            self.noDatalabel.isHidden = true
        }
        
        return self.notificationData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DriverMessagesTableCell") as! DriverMessagesTableCell
        let val = self.notificationData[indexPath.row]
        cell.username.text = ""
        let timestamp = self.convertDateToTimestamp(val.created_at ?? "", to: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        cell.date.text = val.notification
        cell.userMessage.text = self.convertTimestampToDate(timestamp, to: "dd MMM YYYY")
        //        let notifDate = Date(timeIntervalSince1970 : TimeInterval(timestamp))
        //        cell.userMessage.text = notifDate.timeAgoDisplay()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let val = self.notificationData[indexPath.row]
       
        if val.type == 1 || val.type == 15 || val.type == 5 || val.type == 12 || val.type == 18 {
            if(Singleton.shared.userType.role == 1){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
            myVC.id = val.shipment_id ?? 0
            self.navigationController?.pushViewController(myVC, animated: true)
            } else {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverSingleShipmentVC") as! DriverSingleShipmentVC
                myVC.id = val.shipment_id
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
        else if val.type == 3 {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverBidsViewController") as! DriverBidsViewController
            myVC.shipmentData = GetUserShipmentData(id: val.shipment_id ?? 0)
            myVC.notificationRedirection = true
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        else if val.type == 17 {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            let val = self.notificationData[indexPath.row]
            if Singleton.shared.userType.role == 1 {
                myVC.jobDetail = ChatDetail(sender_id: Singleton.shared.userType.user_id ?? 0,receiver_id: val.sender_id ?? 0, receiver_name: (val.user_details?.first_name ?? "") + (val.user_details?.last_name ?? "") ,sender_name: (Singleton.shared.userType.first_name ?? "") + " " +  (Singleton.shared.userType.last_name ?? ""), sender_image: (Singleton.shared.userType.profile_image ?? ""), receiver_image: val.user_details?.profile_image ?? "", shipment_id: val.shipment_id ?? 0)
            } else {
                myVC.jobDetail = ChatDetail(sender_id: val.sender_id ?? 0,receiver_id: Singleton.shared.userType.user_id ?? 0, receiver_name: (Singleton.shared.userType.first_name ?? "") + " " +  (Singleton.shared.userType.last_name ?? "") ,sender_name:(val.user_details?.first_name ?? "") + (val.user_details?.last_name ?? ""), sender_image: val.user_details?.profile_image ?? "", receiver_image: (Singleton.shared.userType.profile_image ?? ""), shipment_id: val.shipment_id ?? 0)
            }
            self.navigationController?.pushViewController(myVC, animated: true)
        } else if val.type == 11 || val.type == 14 || val.type == 4 || val.type == 9 || val.type == 20{
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverSingleShipmentVC") as! DriverSingleShipmentVC
            myVC.id = val.shipment_id
            self.navigationController?.pushViewController(myVC, animated: true)
        } else if val.type == 13 {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RateAndReviewVC") as! RateAndReviewVC
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
}
