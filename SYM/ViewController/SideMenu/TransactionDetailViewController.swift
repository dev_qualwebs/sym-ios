//
//  TransactionDetailViewController.swift
//  SYM
//
//  Created by Sagar Pandit on 26/05/21.
//  Copyright © 2021 AM. All rights reserved.
//

import UIKit

class TransactionDetailViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var postedDate: DesignableUILabel!
    @IBOutlet weak var sourceAddress: DesignableUILabel!
    @IBOutlet weak var destinationAddress: DesignableUILabel!
    @IBOutlet weak var estimatedPrice: DesignableUILabel!
    @IBOutlet weak var categoryDetail: DesignableUILabel!
    @IBOutlet weak var awardedTo: DesignableUILabel!
    @IBOutlet weak var currentStatus: DesignableUILabel!
    @IBOutlet weak var totalBidsPlaced: DesignableUILabel!
    @IBOutlet weak var headingText: DesignableUILabel!
    @IBOutlet weak var pickupDate: DesignableUILabel!
    @IBOutlet weak var dropoffDate: DesignableUILabel!
    @IBOutlet weak var shipmentWeight: DesignableUILabel!
    @IBOutlet weak var attachmentCount: DesignableUILabel!
    @IBOutlet weak var subcategoryName: DesignableUILabel!
    @IBOutlet weak var rateDriverButton: UIButton!
    @IBOutlet weak var transactionDetail: ContentSizedTableView!
    @IBOutlet weak var totalDistance: DesignableUILabel!
    @IBOutlet weak var startDate: DesignableUILabel!
    @IBOutlet weak var endDate: DesignableUILabel!
    @IBOutlet weak var shipmentBy: DesignableUILabel!
    @IBOutlet weak var noDataFoundLabel: DesignableUILabel!
    
    
    var transactionData = TransactionResponse()
    var id = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavTitle(title: "Transaction Detail")
        self.setNavigationBar("back", "none")
        
        if(Singleton.shared.userType.role != 1){
        self.getSingleTransaction()
        } else {
            self.initializeView()
        }
    }
    
    func getSingleTransaction(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_DRIVER_SINGLE_TRANSACTION + "\(id)", method: .get, parameter: nil, objectClass: GetSingleTransactionResponse.self, requestCode: U_GET_DRIVER_SINGLE_TRANSACTION, userToken: nil) { (response) in
            ActivityIndicator.hide()
            if response.response != nil {
            self.transactionData = response.response ?? TransactionResponse()
            } else {
                Singleton.shared.showToast(text: "Error finding details", color: errorRed)
                self.navigationController?.popViewController(animated: true)
            }
            self.initializeView()
            self.transactionDetail.reloadData()
        }
        
    }
    
    func initializeView(){
        self.transactionDetail.reloadData()
        self.postedDate.text = "Posted on: " +  "\(self.convertTimestampToDate(self.transactionData.created_date ?? 0, to: "EEE, dd-MMM-yy, h:mm a"))"
        self.sourceAddress.text = self.transactionData.from_name
        self.destinationAddress.text = self.transactionData.to_name
        self.estimatedPrice.text = "#\(self.transactionData.id ?? 0)"
        let cat = Singleton.shared.categoryData.filter{
            $0.id == self.transactionData.category_id
        }
        if(cat.count > 0){
            self.categoryDetail.text = cat[0].category
        }
        self.shipmentBy.text = (self.transactionData.customer_details?.first_name ?? "") + (self.transactionData.customer_details?.last_name ?? "")
        self.totalDistance.text = (self.transactionData.distance ?? "0") + " miles"
        self.shipmentWeight.text = self.transactionData.category_name?.category ?? ""
        self.pickupDate.text = "$\(self.transactionData.amount ?? "")"
        self.dropoffDate.text = "$\(self.transactionData.commission_amount ?? "")"
        self.startDate.text = "Date: \(self.convertTimestampToDate(self.transactionData.pickup_date ?? 0, to: "EEE, dd-MMM-yy, h:mm a"))"
        self.endDate.text = "Date: \(self.convertTimestampToDate(self.transactionData.dropoff_date ?? 0, to: "EEE, dd-MMM-yy, h:mm a"))"
    }

}

extension TransactionDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

    self.noDataFoundLabel.isHidden = self.transactionData.transaction.count > 0 ? true : false
       
        return self.transactionData.transaction.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DriverMessagesTableCell") as! DriverMessagesTableCell
        let val = self.transactionData
        cell.username.text = val.transaction[indexPath.row].transaction_id ?? ""
        cell.date.text = self.convertTimestampToDate(Int(val.transaction[indexPath.row].created_date ?? "0")!, to: "dd MMM, yyyy")
        
        if(Singleton.shared.userType.role == 1){
        cell.price.text = "$\(val.transaction[indexPath.row].amount ?? "")"
        } else {
            if(val.transaction[indexPath.row].type == 2){
                cell.price.text = "$\(val.transaction[indexPath.row].driver_amount ?? "")"
            } else {
                cell.price.text = "$\(val.transaction[indexPath.row].amount ?? "")"
            }
        }
       
        if(val.transaction[indexPath.row].type == 1){
            cell.userMessage.text = "Featured Bid"
        } else if(val.transaction[indexPath.row].type == 2){
            cell.userMessage.text = "50% Amount Transferred (10% deducted as commission)"
        }else if(val.transaction[indexPath.row].type == 3){
            cell.userMessage.text = "Payment transaferred successfully"
        }
//            if val.transaction.count == 2 {
//                if indexPath.row == 0 {
//                    cell.userMessage.text = "50% Amount Transferred (Rest amount in process)"
//                } else {
//                }
//            }
        return cell
    }
    
    
}
