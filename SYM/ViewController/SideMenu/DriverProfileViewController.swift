//
//  DriverProfileViewController.swift
//  SYM
//
//  Created by Sagar Pandit on 28/05/21.
//  Copyright © 2021 AM. All rights reserved.
//

import UIKit
import CarbonKit


protocol ChangeProfileTab {
    func changeTab()
}

class DriverProfileViewController: UIViewController, CarbonTabSwipeNavigationDelegate,ChangeProfileTab {
    
    func changeTab(){
        carbonTabSwipeNavigation.setCurrentTabIndex(1, withAnimation: true)
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        if(index == 0){
            return storyboard!.instantiateViewController(withIdentifier: "EditProfileVC")
        }else {
            return storyboard!.instantiateViewController(withIdentifier: "StripeAccountViewController")
        }
        
    }
    
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(K_COMING_DIRECT_AFTER_SIGNUP_DRIVER == 1){
            self.navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
                if vc.isKind(of: SignUpVC.self) || vc.isKind(of: SignInVC.self) || vc.isKind(of: EntryViewController.self) || vc.isKind(of: DashBoardVC.self)
                  {
                   return true
                   }
                else
                   {
                    return false
                    }
                   })
        } else {
            self.navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
                if vc.isKind(of: SignUpVC.self) || vc.isKind(of: SignInVC.self) || vc.isKind(of: EntryViewController.self)
                  {
                   return true
                   }
                else
                   {
                    return false
                    }
                   })
        }
        
        setNavTitle(title: "Profile")
        if(K_COMING_DIRECT_AFTER_SIGNUP_DRIVER == 1){
        setNavigationBar("none", "none")
        } else {
        setNavigationBar("back", nil)
        }
       
        EditProfileVC.tabChangeDelegate = self
        let items = ["My Profile", "My Bank"]
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(self.view.frame.width/2, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(self.view.frame.width/2, forSegmentAt:1)
        carbonTabSwipeNavigation.setNormalColor(.lightGray)
        carbonTabSwipeNavigation.setSelectedColor(.black, font: UIFont.systemFont(ofSize: 15, weight: .medium))
        carbonTabSwipeNavigation.setIndicatorColor(brownColor)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
