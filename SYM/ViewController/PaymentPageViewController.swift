//
//  PaymentPageViewController.swift
//  SYM
//
//  Created by qw on 07/09/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

protocol PageViewScroll {
    func changeCurrentPage(page: Int)
}

class PaymentPageViewController: UIPageViewController {

    static var dataSource: UIPageViewControllerDataSource?
    var profileViewContorllers: [UIViewController] = []
    var pageControl = UIPageControl()
    static var pageDelegate: PageViewScroll? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleView()
        setControllers()
        self.dataSource = self
        self.delegate = self
        PaymentPageViewController.dataSource = self
        for view in self.view.subviews {
                   if let subView = view as? UIScrollView {
                           subView.isScrollEnabled = true
                   }
               }
    }
    
    func setControllers() {
        if let firstViewController = profileViewContorllers.first {
            setViewControllers([firstViewController],
                               direction: .reverse,
                               animated: false,
                               completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        PaymentPageViewController.dataSource = self
    }
    
    func handleView() {
      profileViewContorllers = [self.newColoredViewController(controller: "PaymentMethodVC"),
        self.newColoredViewController(controller: "StripeAccountViewController")]
    }
    
    func setControllerFirst() {
        if let firstViewController = profileViewContorllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
    }
    
    func setControllerFirstReverse() {
        if let firstViewController = profileViewContorllers.first {
            setViewControllers([firstViewController],
                               direction: .reverse,
                               animated: false,
                               completion: nil)
        }
    }
    
    func setControllerSecond() {
        setViewControllers([profileViewContorllers[1]], direction: .forward, animated: false, completion: nil)
    }
    
    func setControllerLast() {
        if let lastViewController = profileViewContorllers.last {
            setViewControllers([lastViewController],
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
    }
    
//    func currentControllerIndex(VC: UIViewController) {
//        if let viewControllerIndex = profileViewContorllers.index(of: VC) {
//            VisitorPageViewController.index_delegate?.getControllerIndex(index: viewControllerIndex)
//        }
//    }
    
    private func newColoredViewController(controller: String)->UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controller)
    }
}

extension PaymentPageViewController: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
      //  currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = profileViewContorllers.index(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard profileViewContorllers.count > previousIndex else {
            return nil
        }
        return profileViewContorllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        //currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = profileViewContorllers.index(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        guard profileViewContorllers.count != nextIndex else {
            return nil
        }
        guard profileViewContorllers.count > nextIndex else {
            return nil
        }
        return profileViewContorllers[nextIndex]
    }
    
}

extension PaymentPageViewController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
            self.pageControl.currentPage = profileViewContorllers.index(of: pageContentViewController)!
            PaymentPageViewController.self.pageDelegate?.changeCurrentPage(page:  profileViewContorllers.index(of: pageContentViewController)!)
    }
}
