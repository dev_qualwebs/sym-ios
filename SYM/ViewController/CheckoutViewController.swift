//
//  CheckoutViewController.swift
//  SYM
//
//  Created by qw on 19/09/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class CheckoutViewController: UIViewController, AddCard, AddressAdded {
    func addCard() {
        self.getCards()
    }
    
    func reloadAddressTable() {
        Singleton.shared.addressData = []
        APICallingViewController.shared.getShippingAddress { (address) in
            self.addressData = address
            self.addressTable.reloadData()
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var addressTable: ContentSizedTableView!
    @IBOutlet weak var paymentTable: ContentSizedTableView!
    
    @IBOutlet weak var totalPrice: DesignableUILabel!
    @IBOutlet weak var discountPrice: DesignableUILabel!
    @IBOutlet weak var taxPrice: DesignableUILabel!
    @IBOutlet weak var subtotalPrice: DesignableUILabel!
    @IBOutlet weak var total: DesignableUILabel!
    
    
    var addressData = [GetAddressResponse]()
    var cardData = [GetCardResponse]()
    var selectedAddress: Int?
    var selectedCard: GetCardResponse?


    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCards()
        self.addressTable.estimatedRowHeight = 80
        self.paymentTable.estimatedRowHeight = 80
        self.addressTable.tableFooterView = UIView()
        self.paymentTable.tableFooterView = UIView()
        self.addressTable.rowHeight = UITableView.automaticDimension
        self.paymentTable.rowHeight = UITableView.automaticDimension
        self.setNavTitle(title: "Checkout")
        self.setNavigationBar("back", nil)
        APICallingViewController.shared.getShippingAddress { (address) in
            self.addressData = address
            self.addressTable.reloadData()
        }
        
        self.totalPrice.text = "$" + (Singleton.shared.cartData.cart_detail.sub_total ?? "")
        self.discountPrice.text = "-$" + (Singleton.shared.cartData.cart_detail.discount_amount ?? "")
        self.taxPrice.text = "$" + (Singleton.shared.cartData.cart_detail.tax_total ?? "")
        self.subtotalPrice.text = "$" + (Singleton.shared.cartData.cart_detail.sub_total ?? "")
        self.total.text = "$" + (Singleton.shared.cartData.cart_detail.grand_total ?? "")
    }
    
    func getCards(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_DRIVER_SAVED_CARDS, method: .post, parameter: nil, objectClass: GetCard.self, requestCode: U_GET_DRIVER_SAVED_CARDS, userToken: nil) { (response) in
            self.cardData = response.response

            self.paymentTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBAction
    @IBAction func shippingAddressAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        myVC.addressDelegate = self
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
        myVC.cardDelegate = self
        myVC.isAddCard = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func orderAction(_ sender: Any) {
        if(self.selectedAddress == nil){
            Singleton.shared.showToast(text: "Select shipping address", color: errorRed)
        }else if(self.selectedCard?.id == nil){
            Singleton.shared.showToast(text: "Select payment card", color: errorRed)
        }else {
            ActivityIndicator.show(view: self.view)
                    let param:[String:Any] = [
                        "shipping_address_id":self.selectedAddress,
                        "card_id":self.selectedCard?.id,
                        "stripe_customer_id":self.selectedCard?.customer,
                      "card_flag":"1"
                    ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_PLACE_ORDER, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_PLACE_ORDER, userToken: nil) { (response) in
                ActivityIndicator.hide()
                Singleton.shared.showToast(text: response.message ?? "", color: successGreen)
                if(Singleton.shared.userType.role == 1){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "UserTabViewController") as! UserTabViewController
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else {
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }
        }
    }
    
}


extension CheckoutViewController: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == self.paymentTable){
            return self.cardData.count
        }else {
            return self.addressData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == self.paymentTable){
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableCell", for: indexPath) as! AddressTableCell
            let val = self.cardData[indexPath.row]
            if(val.id == self.selectedCard?.id){
                cell.tickImage.image = #imageLiteral(resourceName: "Ellipse")
            }else {
                cell.tickImage.image = #imageLiteral(resourceName: "Ellipse3")
            }
            cell.street.text =  (val.name ?? "")
            cell.userAddress.text = "xxxx xxxx \(val.masked_card_number ?? "")  \(val.expiry_month ?? "")/\(val.expiry_year ?? "")"
            cell.editView.isHidden = true
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableCell", for: indexPath) as! AddressTableCell
            let val = self.addressData[indexPath.row]
            if(val.id == self.selectedAddress){
                cell.tickImage.image = #imageLiteral(resourceName: "Ellipse")
            }else {
                cell.tickImage.image = #imageLiteral(resourceName: "Ellipse3")
            }

            cell.userAddress.text = val.address
            cell.deleteButton = {
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_REMOVE_SHIPPING_ADDRESS , method: .post, parameter: ["id": val.id], objectClass: SuccessResponse.self, requestCode: U_REMOVE_SHIPPING_ADDRESS, userToken: nil) { (response) in
                    Singleton.shared.showToast(text: response.message ?? "", color: successGreen)
                    ActivityIndicator.hide()
                    self.reloadAddressTable()
                }
            }
            
            cell.editButton = {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
                myVC.addressDelegate = self
                myVC.addressDetail = val
                myVC.isEditAddress = true
                self.navigationController?.pushViewController(myVC, animated: true)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView == self.paymentTable){
            self.selectedCard = self.cardData[indexPath.row]
            self.paymentTable.reloadData()
        }else{
            self.selectedAddress = self.addressData[indexPath.row].id
            self.addressTable.reloadData()
        }
    }
    
}
