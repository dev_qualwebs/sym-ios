//
//  StepProgressView.swift
//  SYM
//
//  Created by qw on 22/02/21.
//  Copyright © 2021 AM. All rights reserved.
//

import UIKit


public class StepProgressView: UIView {
    //MARK: IBOutlets
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var dotView: UIView!
    
    var viewShipment:(()->Void)? = nil
    var showHideView:(()->Void)? = nil
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("StepProgressView", owner: self, options: nil)
        contentView.fixInView(self)
        if(Singleton.shared.userType.role != 1){
            self.getCurrentShipment()
            self.showAnimatingDots()
        }
    }
    
    func showAnimatingDots() {
        let lay = CAReplicatorLayer()
        lay.frame = CGRect(x: 0, y: 2.5, width: 15, height: 7) //yPos == 12
        let circle = CALayer()
        circle.frame = CGRect(x: 0, y: 0, width: 7, height: 7)
        circle.cornerRadius = circle.frame.width / 2
        circle.backgroundColor = UIColor(red: 110/255.0, green: 110/255.0, blue: 110/255.0, alpha: 1).cgColor//lightGray.cgColor //UIColor.black.cgColor
        lay.addSublayer(circle)
        lay.instanceCount = 3
        lay.instanceTransform = CATransform3DMakeTranslation(10, 0, 0)
        let anim = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        anim.fromValue = 1.0
        anim.toValue = 0.2
        anim.duration = 1
        anim.repeatCount = .infinity
        circle.add(anim, forKey: nil)
        lay.instanceDelay = anim.duration / Double(lay.instanceCount)
        dotView.layer.addSublayer(lay)
    }
    
    func getCurrentShipment(){
      //  if((Singleton.shared.progressShipment?.count ?? 0) == 0){
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_COMMON_SHIPMENTS, method: .post, parameter: ["flag":4], objectClass: GetAllCommonShipmentResponse.self, requestCode: U_COMMON_SHIPMENTS, userToken: nil) { (responseData) in
                Singleton.shared.progressShipment =  responseData.response?.data
                if let showHideView = self.showHideView {
                    showHideView()
                }
            }
     //   }
    }
    
    //MARK: IBActions
    @IBAction func viewAction(_ sender: Any) {
        if let viewShipment = self.viewShipment {
            viewShipment()
        }
    }
    
}


extension UIView
{
    func fixInView(_ container: UIView!) -> Void{
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}

