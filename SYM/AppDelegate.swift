//
//  AppDelegate.swift
//  SYM
//
//  Created by AM on 18/11/19.
//  Copyright © 2019 AM. All rights reserved.


import UIKit
import Firebase
import GooglePlaces
import IQKeyboardManagerSwift
import GoogleMaps
import FirebaseMessaging
import UserNotifications
import FirebaseStorage
import FirebaseDatabase
import Stripe

var storageRef: StorageReference!
var ref: DatabaseReference!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        handleGoogleMap()
        configureFirebase(application: application)
        IQKeyboardManager.shared.enable = true
        StripeAPI.defaultPublishableKey = "pk_test_LeD0E9gmKtpQHdwoOJKwyWvy"
       // pk_live_51Ik8daDihkn3sJOnJ6UIadBwWc3oDlnm5VqKyjpeuzRNgRycp9EH9ytz6UwiyZmDaJVV72AQuwdXtpLAYV11W1As00Q6ALuM76
        return true
    }
    
    func handleGoogleMap(){
        GMSPlacesClient.provideAPIKey("AIzaSyDdyinc_88KQdK0QufHq2qyc6hDkbmgV0s")
        GMSServices.provideAPIKey("AIzaSyDdyinc_88KQdK0QufHq2qyc6hDkbmgV0s")
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func configureFirebase(application: UIApplication){
        FirebaseApp.configure()
        ref = Database.database().reference()
        storageRef = Storage.storage().reference()
        Messaging.messaging().delegate = self
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    //This function work when we tap on notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        if let data = userInfo as? [String:Any] {
            // if ((data["gcm.notification.type"] as! String) == "7") {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.handleUserNotfication(info: userInfo)
            }
            // }
        }
    }
    
    func handleUserNotfication(info:[AnyHashable:Any]){
        if let token = UserDefaults.standard.value(forKey: UD_TOKEN) as? String {
            if let data = info as? [String:Any] {
                var initialViewController = UIViewController()
                let navigationController = self.window?.rootViewController as! UINavigationController
                let stringToData = Data((data["gcm.notification.data"] as! String).utf8)
                var jobData = StartTransactionDetail()
                let decoder = JSONDecoder()
                do {
                    jobData = try decoder.decode(StartTransactionDetail.self, from: stringToData)
                }catch {
                    print(error.localizedDescription)
                }
                let currentVC =  UIApplication.getTopViewController(base: ViewController())
                switch(jobData.type){
                    
                    //     'POST_SHIPMENT'|| 'POST_SHIPMENT_WITH_REGISTRATION', 1
                case 1:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
                    
                    (initialViewController as! SingleShipmentVC).id = jobData.shipment_id ?? 0
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                    //'DRIVER_REGISTERED', 2
                case 2:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
                    
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                    //'BID_PLACED', 3
                case 3:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
                    (initialViewController as! SingleShipmentVC).id = jobData.shipment_id ?? 0
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                    //Customer accepted driver bid  'BID_ACTION', 4
                case 4:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "DriverSingleShipmentVC") as! DriverSingleShipmentVC
                    (initialViewController as! DriverSingleShipmentVC).id = jobData.shipment_id ?? 0
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                    //'SHIPMENT_ACTION', 5
                case 5:
                    if(Singleton.shared.userType.role == 1){
                        initialViewController = storyboard.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
                        (initialViewController as! SingleShipmentVC).id = jobData.shipment_id ?? 0
                    }else {
                        initialViewController = storyboard.instantiateViewController(withIdentifier: "DriverSingleShipmentVC") as! DriverSingleShipmentVC
                        (initialViewController as! SingleShipmentVC).id = jobData.shipment_id ?? 0
                    }
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                    //       'LOADER_REGISTERED', 7 || 'DRIVER_LOADER_REGISTERED', 8
                case 7,8:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                    
                    //Driver pay to feature bid on Shipment || 'PAY_FOR_FEATURE_BID', 9
                case 9:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
                    (initialViewController as! SingleShipmentVC).id = jobData.shipment_id ?? 0
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                    //Driver Accepted the shipment
                case 10:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
                    (initialViewController as! SingleShipmentVC).id = jobData.shipment_id ?? 0
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                    // 'COMPLETE_SHIPMENT_DRIVER', 11
                case 11:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "DriverSingleShipmentVC") as! DriverSingleShipmentVC
                    (initialViewController as! DriverSingleShipmentVC).id = jobData.shipment_id ?? 0
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                    //Driver Completed the shipment || 'COMPLETE_SHIPMENT_USER', 12
                case 12:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "DriverSingleShipmentVC") as! DriverSingleShipmentVC
                    (initialViewController as! DriverSingleShipmentVC).id = jobData.shipment_id ?? 0
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                    // 'RATE_REVIEW', 13
                case 13:
                    if(Singleton.shared.userType.role == 1){
                        initialViewController = storyboard.instantiateViewController(withIdentifier: "UserTabViewController") as! UserTabViewController
                    }else {
                        initialViewController = storyboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                    }
                    
                    //   navigationController.pushViewController(initialViewController, animated: false)
                    NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_CURRENT_TAB), object: nil, userInfo: ["index":3])
                    break
                    //'START_SHIPMENT_DRIVER', 14
                case 14:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
                    
                    (initialViewController as! SingleShipmentVC).id = jobData.shipment_id ?? 0
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                    //   'START_SHIPMENT_USER', 15
                case 15:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
                    (initialViewController as! SingleShipmentVC).id = jobData.shipment_id ?? 0
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                    // 'PROFILE_VERIFICATION', 16
                case 16:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                case 18:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
                    (initialViewController as! SingleShipmentVC).id = jobData.shipment_id ?? 0
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                    //'NEW_CHAT_MESSAGE', 17
                case 17:
                    initialViewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                    (initialViewController as! ChatViewController).jobDetail = ChatDetail(sender_id: Singleton.shared.userType.user_id ?? 0,receiver_id: jobData.sender_id ?? 0, receiver_name: (jobData.sender_name ?? ""),sender_name: (Singleton.shared.userType.first_name ?? "") + " " +  (Singleton.shared.userType.last_name ?? ""), sender_image: (Singleton.shared.userType.profile_image ?? ""), receiver_image: jobData.profile_image ?? "", shipment_id: jobData.shipment_id ?? 0)
                    navigationController.pushViewController(initialViewController, animated: false)
                    break
                default:
                    break
                }
                
            }
            //        NotificationCenter.default.post(name: NSNotification.Name(N_NOTIFICATION_DATA),object: nil,userInfo: ["info": info])
        }
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
    
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("fcm token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: UD_FCM_TOKEN)
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("foreground remote notification")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let data = userInfo as? [String:Any] {
            // self.handleUserNotfication(info: userInfo)
            //  let center = UNUserNotificationCenter.current()
            // center.removeAllDeliveredNotifications()
        }
    }
}

extension UIApplication{
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
            
        } else if let tab = base as? UITabBarController {
            return getTopViewController(base: tab)
            
        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}
