//
//  SignUpVC.swift
//  SYM
//
//  Created by AM on 18/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import iOSDropDown
import GooglePlaces

class SignUpVC: UIViewController,SelectedCategories {
    
    //Mark: IBOUTLETS
    @IBOutlet weak var firstNameTxtField: UITextField!
    @IBOutlet weak var lastNameTxtField: DesignableUITextField!
    @IBOutlet weak var emailTxtField: DesignableUITextField!
    @IBOutlet weak var phoneTxtField: DesignableUITextField!
    @IBOutlet weak var passwordTxtField: DesignableUITextField!
    @IBOutlet weak var confirmPassTxtField: DesignableUITextField!
    @IBOutlet weak var becomeTxtField: DropDown!
    @IBOutlet weak var shipTxtField: DropDown!
    @IBOutlet weak var termsCnditionImgView: UIImageView!
    @IBOutlet weak var shipCityTxtField: UITextField!
    @IBOutlet weak var shipCityView: UIView!
    @IBOutlet weak var shipThingView: UIView!
    @IBOutlet weak var shipCityTable: DynamicHeightTableView!
    @IBOutlet weak var passwordEyeIcon: UIImageView!
    @IBOutlet weak var confirmEyeIcon: UIImageView!
    @IBOutlet weak var ShippingcollectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var signupButton: CustomButton!
    
    
    //Mark: Properties
    var become = Int()
    var selectedCategory = Set<Int>()
    var categoryWithName = [GetCategories]()
    var type = Int()
    var shipCity = [String]()
    var termsSelected = false
    var didSelectTime = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.phoneTxtField.delegate = self
        setNavTitle(title: "Sign up")
        setNavigationBar("back", "none")
        self.phoneTxtField.delegate = self
        becomeTxtField.optionArray = ["Driver","Loader","Both"]
        becomeTxtField.didSelect{(selectedText , index ,id) in
            self.becomeTxtField.text = "\(selectedText)"
            self.type = index + 1
            self.shipThingView.isHidden = false
            self.shipCityView.isHidden = false
        }
        self.shipCityTable.isHidden = true
        self.ShippingcollectionView.isHidden = true
        self.hideIndicator()
    }
    
    func showIndicator(){
        self.signupButton.setTitle("", for: .normal)
        self.indicatorView.isHidden = false
        self.indicatorView.color = .white
        self.indicatorView.startAnimating()
        self.signupButton.isUserInteractionEnabled = false
    }
    
    func hideIndicator(){
        self.signupButton.setTitle("SIGN UP", for: .normal)
        self.indicatorView.isHidden = true
        self.indicatorView.color = .white
        self.indicatorView.stopAnimating()
        self.signupButton.isUserInteractionEnabled = true
    }
    
    
    
    func currentSelectedCategories(id: Set<Int>, categories: [GetCategories]) {
        self.selectedCategory = id
        self.categoryWithName = categories
        self.shipTxtField.text = ""
        var count = 0
        for val in self.categoryWithName{
            count += 1
            self.shipTxtField.text = (self.shipTxtField.text ?? "") + (val.category ?? "") + (self.categoryWithName.count > count ? ", ":"")
        }
        self.ShippingcollectionView.reloadData()
    }
    
    func currentSelectedSubCategories(id: Set<Int>, categories: [GetSubCategories]) {
        
    }
    
    func callApi(param:[String:Any]){
        //delay for preventing multiple taps
        if Date().timeIntervalSince(self.didSelectTime) < 1.0 {
            self.didSelectTime = Date()
            return
        }
        
        self.didSelectTime = Date()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
            self.hideIndicator()
        }
        
        self.showIndicator()
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SIGNUP, method: .post, parameter: param, objectClass: Login.self, requestCode: U_SIGNUP, userToken: nil) { (responseData) in
            UserDefaults.standard.set(responseData.response.token ?? "", forKey: UD_TOKEN)
            Singleton.shared.userType.role = responseData.response.data.role ?? 0
            Singleton.shared.userType.first_name = responseData.response.data.first_name
            Singleton.shared.userType.phone = responseData.response.data.phone ?? ""
            Singleton.shared.userType.username = responseData.response.data.username
            Singleton.shared.userType.last_name = responseData.response.data.last_name
            Singleton.shared.userType.user_id = responseData.response.data.id
            Singleton.shared.userType.email = responseData.response.data.email
            Singleton.shared.userType.profile_image = responseData.response.data.profile_image
            Singleton.shared.saveTypeOfUser(data: Singleton.shared.userType)
            UserDefaults.standard.removeObject(forKey: UD_SHIPMENT_DATA)
            self.hideIndicator()
            
            if responseData.response.data.role ?? 0 == 1 {
                let myVC = self.storyboard!.instantiateViewController(withIdentifier: "UserTabViewController") as! UserTabViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
            else {
                let eVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverProfileViewController") as! DriverProfileViewController
                K_COMING_DIRECT_AFTER_SIGNUP_DRIVER = 1
                self.navigationController?.pushViewController(eVC, animated: true)
            }
        }
        
    }
    
    //MARK: IBActions
    
    @IBAction func confirmEyeAction(_ sender: Any) {
        if self.confirmEyeIcon.image == UIImage(named: "eye") {
            self.confirmEyeIcon.image = UIImage(named: "invisible")
            self.confirmPassTxtField.isSecureTextEntry = true
        } else {
            self.confirmEyeIcon.image = UIImage(named: "eye")
            self.confirmPassTxtField.isSecureTextEntry = false
        }
    }
    
    
    
    @IBAction func passwordEyeAction(_ sender: Any) {
        if self.passwordEyeIcon.image == UIImage(named: "eye") {
            self.passwordEyeIcon.image = UIImage(named: "invisible")
            self.passwordTxtField.isSecureTextEntry = true
        } else {
            self.passwordEyeIcon.image = UIImage(named: "eye")
            self.passwordTxtField.isSecureTextEntry = false
        }
    }
    
    
    @IBAction func shipCityTapped(_ sender: Any) {
        shipCityTxtField.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
        let filter = GMSAutocompleteFilter()
        filter.type = .city
        filter.country = "US"
        acController.autocompleteFilter = filter
        acController.delegate = self as! GMSAutocompleteViewControllerDelegate
        present(acController, animated: true, completion: nil)
    }
    
    
    
    @IBAction func signUpBtPressed(_ sender: Any) {
        
        if firstNameTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please Enter First Name", color: errorRed)
        }
        else if lastNameTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please Enter Last Name", color: errorRed)
        }
        else if emailTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please Enter Email", color: errorRed)
        }
        else if phoneTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please Enter Phone Number", color: errorRed)
        }
        else if passwordTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please Enter Password", color: errorRed)
        }
//        else if passwordTxtField.text!.count < 8{
//            Singleton.shared.showToast(text: "Password must be atleast 8 characters long")
//        }
        else if confirmPassTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please Enter Confirm Password", color: errorRed)
        }
        else if becomeTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please select what you want to become.", color: errorRed)
        }else {
            if self.selectedCategory.count == 0{
                Singleton.shared.showToast(text: "Please select what can you ship", color: errorRed)
            }
            else if self.shipCity.count == 0{
                Singleton.shared.showToast(text: "Please fill city", color: errorRed)
            }
            else if (!self.termsSelected) {
                Singleton.shared.showToast(text: "Please Read Terms and Conditions", color: errorRed)
            }else{
                let token = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) as? String
                var number = self.phoneTxtField.text ?? ""
                number = number.replacingOccurrences(of: "+1", with: "")
                number = number.replacingOccurrences(of: "(", with: "")
                number = number.replacingOccurrences(of: ")", with: "")
                number = number.replacingOccurrences(of: " ", with: "")
                number = number.replacingOccurrences(of: "-", with: "")
                let param:[String:Any] =
                [
                    "first_name":firstNameTxtField.text ?? "",
                    "last_name":lastNameTxtField.text ?? "",
                    "email":emailTxtField.text ?? "",
                    "password":passwordTxtField.text ?? "",
                    "password_confirmation":confirmPassTxtField.text ?? "",
                    "type":"\(type)",
                    "phone":number,
                    "loader_city":self.shipCity,
                    "ship":Array(self.selectedCategory),
                    "firebase_token":token ?? "",
                    "country_code": "+1",
                    "platform":2
                ]
                self.callApi(param: param)
            }
        }
    }
    
    
    @IBAction func shipDropDown(_ sender: Any) {
        if(Singleton.shared.categoryData.count > 0){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ShipCategoriesVC") as! ShipCategoriesVC
            myVC.categoryData = Singleton.shared.categoryData
            myVC.selectedCategory = self.selectedCategory
            myVC.headingText = "Select Category"
            myVC.categoryDelegate = self
            myVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @IBAction func termsAndConditionBtn(_ sender: Any) {
        if termsCnditionImgView.image == #imageLiteral(resourceName: "GreyBox"){
            termsCnditionImgView.image = #imageLiteral(resourceName: "GreyTick")
            self.termsSelected = true
        }
        else{
            termsCnditionImgView.image = #imageLiteral(resourceName: "GreyBox")
            self.termsSelected = false
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        let sIVC = storyboard!.instantiateViewController(withIdentifier: "SignInVC")
        self.navigationController?.pushViewController(sIVC, animated: true)
    }
    
    @IBAction func termsAndConditionAction(_ sender: Any) {
        self.openUrl(urlStr: U_TERMS_CONDITION)
    }
    
    @IBAction func privacyPolicyAction(_ sender: Any) {
        self.openUrl(urlStr: U_PRIVACY_POLICY)
    }
}

extension SignUpVC: GMSAutocompleteViewControllerDelegate{
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
            // Iterate through address components
            for component in place.addressComponents ?? [] {
                let types = component.types
                if(types.contains("administrative_area_level_1")){
                    // This component is the administrative area (e.g., city or state)
                    let cityName = component.name
                    if(!(self.shipCity.contains(cityName ?? ""))){
                        self.shipCity.append(cityName ?? "")
                    }
                    break
                }else {
                    if(types.contains("locality")){
                        // This component is the administrative area (e.g., city or state)
                        let cityName = component.name
                        if(!(self.shipCity.contains(cityName ?? ""))){
                            self.shipCity.append(cityName ?? "")
                        }
                        break
                    }
                }
            }
        dismiss(animated: true, completion: nil)
        DispatchQueue.main.async {
            self.ShippingcollectionView.reloadData()
            if self.shipCity.count == 0{
                self.ShippingcollectionView.isHidden = true
            }
            else{
                self.ShippingcollectionView.isHidden = false
            }
        }
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        Singleton.shared.showToast(text:error.localizedDescription, color: errorRed)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
}

extension SignUpVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shipCity.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShipCityTableCell", for: indexPath) as! ShipCityTableCell
        cell.lblShipCity.text = shipCity[indexPath.row]
        cell.viewCon = self
        cell.indexPath = indexPath
        return cell
    }
}

//extension SignUpVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if(shipCity.count % 2 == 0){
//            self.collectionViewHeight.constant = CGFloat((shipCity.count/2) * 40)
//        } else {
//            self.collectionViewHeight.constant =  CGFloat(((shipCity.count/2) + 1) * 40)
//        }
//        return shipCity.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShipCityCollectionCell", for: indexPath) as! ShipCityCollectionCell
//        cell.lblShipCity.text = shipCity[indexPath.row]
//        cell.viewCon = self
//        cell.indexPath = indexPath
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 40, height:35)
//    }
//
//}

extension SignUpVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(shipCity.count % 2 == 0){
            self.collectionViewHeight.constant = CGFloat((shipCity.count/2) * 50)
        } else {
            self.collectionViewHeight.constant =  CGFloat(((shipCity.count/2) + 1) * 50)
        }
        return self.shipCity.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CityCell", for: indexPath) as! CityCell
        cell.layer.cornerRadius = 10
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.cityName.text = self.shipCity[indexPath.row]
        cell.cancel = {
            self.shipCity.remove(at: indexPath.item)
            self.ShippingcollectionView.reloadData()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.ShippingcollectionView.frame.width/2)-5, height: 35)
    }
}

class ShipCityTableCell: UITableViewCell{
    
    @IBOutlet weak var lblShipCity: DesignableUILabel!
    var viewCon:SignUpVC?
    var indexPath:IndexPath?
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        viewCon!.shipCity.remove(at: indexPath!.row)
        viewCon?.shipCityTable.reloadData()
    }
}

class ShipCityCollectionCell: UICollectionViewCell{
    
    @IBOutlet weak var lblShipCity: DesignableUILabel!
    
    var viewCon:SignUpVC?
    var indexPath:IndexPath?
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        viewCon!.shipCity.remove(at: indexPath!.row)
        viewCon?.ShippingcollectionView.reloadData()
    }
    
}

extension SignUpVC : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == phoneTxtField) {
            if(textField.text == ""){
                //                self.phoneTxtField.text = "+1"
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == phoneTxtField) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.phoneTxtField.text = self.phoneTxtField.text?.applyPatternOnNumbers(pattern:"x (xxx) xxx-xxxxx", replacmentCharacter: "x")
            return newLength <= 17
        }else {
            return true
        }
    }
    
}
