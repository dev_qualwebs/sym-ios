//
//  SignInVC.swift
//  SYM
//
//  Created by AM on 18/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class SignInVC: UIViewController {
    
    //Mark: IBOUTLETS
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var eyeIcon: UIImageView!
    @IBOutlet weak var loginButton: CustomButton!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    var didSelectTime = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavTitle(title: "Sign in")
        setNavigationBar("back", "none")
        self.passwordTxtField.isSecureTextEntry = true
        self.hideIndicator()
    }
    
    //MARK:IBActions
    @IBAction func forgotAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func toggleEyeIcon(_ sender: Any) {
        if self.eyeIcon.image == UIImage(named: "eye") {
            self.eyeIcon.image = UIImage(named: "invisible")
            self.passwordTxtField.isSecureTextEntry = true
        } else {
            self.eyeIcon.image = UIImage(named: "eye")
            self.passwordTxtField.isSecureTextEntry = false
        }
    }
    
    func showIndicator(){
        self.loginButton.setTitle("", for: .normal)
        self.indicatorView.isHidden = false
        self.indicatorView.color = .white
        self.indicatorView.startAnimating()
        self.loginButton.isUserInteractionEnabled = false
    }
    
    func hideIndicator(){
        self.loginButton.setTitle("LOGIN", for: .normal)
        self.indicatorView.isHidden = true
        self.indicatorView.color = .white
        self.indicatorView.stopAnimating()
        self.loginButton.isUserInteractionEnabled = true
    }
    
    
    @IBAction func loginBtnPressed(_ sender: Any) {
        //delay for preventing multiple taps
         if Date().timeIntervalSince(self.didSelectTime) < 1.0 {
                 self.didSelectTime = Date()
                 return
             }
        
      self.didSelectTime = Date()
        
        if emailTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter Email", color: errorRed)
        }
        else if passwordTxtField.text!.isEmpty{
            Singleton.shared.showToast(text: "Please enter Password", color: errorRed)
        }
        else{
            let token = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) as? String
            let param :[String:Any] = [
                "email":emailTxtField.text ?? "",
                "password":passwordTxtField.text ?? "",
                "firebase_token":token ?? "",
                "platform":2,
                "type":"2",
            ]
            DispatchQueue.main.asyncAfter(wallDeadline: .now() + .seconds(5)) {
                self.hideIndicator()
            }
            self.showIndicator()
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_LOGIN, method: .post, parameter: param, objectClass: Login.self, requestCode: U_LOGIN, userToken: nil) { (responseData) in
                UserDefaults.standard.set(responseData.response.token ?? "", forKey: UD_TOKEN)
                self.hideIndicator()
                Singleton.shared.showToast(text: "Login Successfull", color: successGreen)
                Singleton.shared.userType.role = responseData.response.data.role ?? 0
                Singleton.shared.userType.first_name = responseData.response.data.first_name
                Singleton.shared.userType.phone = responseData.response.data.phone ?? ""
                Singleton.shared.userType.username = responseData.response.data.username
                Singleton.shared.userType.last_name = responseData.response.data.last_name
                Singleton.shared.userType.user_id = responseData.response.data.id
                Singleton.shared.userType.email = responseData.response.data.email
                Singleton.shared.userType.profile_image = responseData.response.data.profile_image
                Singleton.shared.saveTypeOfUser(data: Singleton.shared.userType)
                UserDefaults.standard.removeObject(forKey: UD_SHIPMENT_DATA)
                if responseData.status == 200{
                    if responseData.response.data.role == 1{
                        let myVC = self.storyboard!.instantiateViewController(withIdentifier: "UserTabViewController") as! UserTabViewController
                        self.navigationController?.pushViewController(myVC, animated: true)
                    }
                    else {
                        let myVC = self.storyboard!.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                        self.navigationController?.pushViewController(myVC, animated: true)
                    }
                }
            }
        }
        
    }
    
    @IBAction func signUpBtnPressed(_ sender: Any) {
        let sVC = self.storyboard!.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(sVC, animated: true)
    }
}
