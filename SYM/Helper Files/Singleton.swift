//
//  Singleton.swift
//  Agile Sports
//
//  Created by AM on 21/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import Foundation
import UIKit
import Toaster
import CoreLocation

class Singleton
{
    static let shared = Singleton()
    var userDetail = GetDriverProfileDetailsData()
    var categoryData = [GetCategories]()
    var stateNames = [GetStateResidenceDetails]()
    var cityNames = [GetStateResidenceDetails]()
    var postYourShipment = GetPostYourShipmentData()
    var userType = TypeOfUser()
    var allShipment : [GetAllCommonShipment]?
    var recommendedShipment : [GetAllCommonShipment]?
    var activeShipment : [GetAllCommonShipment]?
    var wonShipment : [GetAllCommonShipment]?
    var progressShipment : [GetAllCommonShipment]?
    var completeShipment : [GetAllCommonShipment]?
    var disputedShipment : [GetAllCommonShipment]?
    var ratingData = Ratings()
    var cartData = CartData()
    var addressData = [GetAddressResponse]()
    var locationDataArray = [CLLocation]()
    var savedCardData = [GetCardResponse]()
    var draftData = GetPostYourShipmentData()
    
    private init(){
        self.userType = getUserRole()
        self.postYourShipment = getShimentData()
        if(self.userType.role == 2){
          APICallingViewController.shared.getProfileData()
        }
        APICallingViewController.shared.stateResidenceData()
        APICallingViewController.shared.getCityData()
        APICallingViewController.shared.getCategory()
    }
    
    
    func getUserRole() -> TypeOfUser{
        if let savedPerson = UserDefaults.standard.object(forKey: UD_ROLE) as? Data{
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(TypeOfUser.self, from: savedPerson){
                return loadedPerson
            }
            return TypeOfUser()
        }
        else
        {
            return TypeOfUser()
        }
    }
    
    func saveTypeOfUser(data: TypeOfUser)
    {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(data)
        {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: UD_ROLE)
        }
    }
    
    func saveShipmentData(data:GetPostYourShipmentData,screen: String){
      let encoder = JSONEncoder()
        UserDefaults.standard.setValue(screen, forKey: UD_INITIAL_SCREEN)
        if let data = try? encoder.encode(data){
            UserDefaults.standard.setValue(data, forKey: UD_SHIPMENT_DATA)
            self.showToast(text: "Data Saved.", color: successGreen)
        }
        
    }
    
    
    func getShimentData() -> GetPostYourShipmentData{
        if let savedPerson = UserDefaults.standard.object(forKey: UD_SHIPMENT_DATA) as? Data{
            let decoder = JSONDecoder()
            if let data = try? decoder.decode(GetPostYourShipmentData.self, from: savedPerson){
                self.postYourShipment = data
                return data
            }
            return GetPostYourShipmentData()
        }
        else
        {
            return GetPostYourShipmentData()
        }
    }
    
    
    func showToast(text: String?, color: UIColor?){
        if (ToastCenter.default.currentToast?.text == text) {
             return
            }
        if(UIScreen.main.bounds.height > 700){
            ToastView.appearance().bottomOffsetPortrait = 110
        }else {
            ToastView.appearance().bottomOffsetPortrait = 70
        }
        
            ToastView.appearance().textColor = .white
        ToastView.appearance().backgroundColor = color
            ToastView.appearance().font = UIFont.systemFont(ofSize: 17)
           
            Toast(text: text, delay: 0, duration: 2).show()
            Toast(text: text, delay: 0, duration: 2).cancel()
    }
    
}

