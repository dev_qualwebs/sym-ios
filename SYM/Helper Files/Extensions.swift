//
//  Extemsions.swift
//
//  Created by Manisha  Sharma on 02/01/2019.
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import UIKit
import SideMenu
import GooglePlaces
//import SideMenu
import FlexibleSteppedProgressBar

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
    
}

extension UIViewController : FlexibleSteppedProgressBarDelegate{
    
    func removePreviousView(){
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if vc.isKind(of: SingleShipmentVC.self) || vc.isKind(of: DriverSingleShipmentVC.self) || vc.isKind(of: ChatViewController.self) || vc.isKind(of: EditProfileVC.self)  {
                return true
            }else {
                return false
            }
        })
    }
    
    func showAlert(title: String?, message: String?, action1Name: String?, action2Name: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: action1Name, style: .default, handler: nil))
        if action2Name != nil {
            alertController.addAction(UIAlertAction(title: action2Name, style: .default, handler: nil))
        }
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func callNumber(phoneNumber:String) {
           if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {
               let application:UIApplication = UIApplication.shared
               if (application.canOpenURL(phoneCallURL)) {
                   if #available(iOS 10.0, *) {
                       application.open(phoneCallURL, options: [:], completionHandler: nil)
                   } else {
                       // Fallback on earlier versions
                        application.openURL(phoneCallURL as URL)
                   }
               }
           }
       }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = primaryColor
       // navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.sizeToFit()
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        if #available(iOS 13.0, *) {
            let statusBar1 =  UIView()
            statusBar1.frame = UIApplication.shared.keyWindow?.windowScene?.statusBarManager!.statusBarFrame as! CGRect
            statusBar1.backgroundColor = primaryColor
            UIApplication.shared.keyWindow?.addSubview(statusBar1)
            
        } else {
            
            let statusBar1: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            statusBar1.backgroundColor = primaryColor
        }
        UINavigationBar.appearance().backgroundColor = primaryColor
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    func setNavigationBar(_ leftBarButton: String? = nil,_ rightBarButton: String? = nil) {
        self.setupNavigationBar()
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.barTintColor = primaryColor
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.tintColor = .white
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        var leftBarButtonItem: UIBarButtonItem?
        var rightBarButtonItem: UIBarButtonItem?
        
        if leftBarButton == "back" {
            var view  = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            view = UIImageView(image: #imageLiteral(resourceName: "back"))
            view.clipsToBounds = true
            leftBarButtonItem = UIBarButtonItem(image: view.image, style: .plain, target: self, action: #selector(self.back))
            
        }else if leftBarButton == "pageBack" {
            var view  = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            view = UIImageView(image: #imageLiteral(resourceName: "back"))
            view.clipsToBounds = true
            leftBarButtonItem = UIBarButtonItem(image: view.image, style: .plain, target: self, action: #selector(self.pageBack))
         
        }else if leftBarButton == "menu" {
            var view  = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 20))
            view.contentMode = .scaleToFill
//            view = UIImageView(image: #imageLiteral(resourceName: "menu-symbol-of-three-parallel-lines (1)"))
            view = UIImageView(image: #imageLiteral(resourceName: "more"))
            view.clipsToBounds = true
            leftBarButtonItem = UIBarButtonItem(image: view.image, style: .plain, target: self, action: #selector(self.menu))

            
        }else if leftBarButton == "none" {
            
        }
        if rightBarButton == "back" {
            var view  = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
//            view = UIImageView(image: #imageLiteral(resourceName: "sos"))
            view = UIImageView(image: #imageLiteral(resourceName: "left"))
            view.clipsToBounds = true

        }else if rightBarButton == "cart" {
           
            let badgeCount = UILabel(frame: CGRect(x: 22, y: -05, width: 20, height: 20))
            badgeCount.layer.borderColor = UIColor.clear.cgColor
            badgeCount.layer.borderWidth = 2
            badgeCount.layer.cornerRadius = badgeCount.bounds.size.height / 2
            badgeCount.textAlignment = .center
            badgeCount.layer.masksToBounds = true
            badgeCount.textColor = .white
            badgeCount.font = badgeCount.font.withSize(12)
            badgeCount.backgroundColor = greenColor
            badgeCount.text = "\(Singleton.shared.cartData.cart_items.count)"


            let view = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
            view.setBackgroundImage(UIImage(named: "shopping-cart"), for: .normal)
            view.addTarget(self, action: #selector(self.openCart), for: .touchUpInside)
            view.addSubview(badgeCount)
            rightBarButtonItem = UIBarButtonItem(customView: view)
        }else if rightBarButton == "notificationAndcart" {
            var view  = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            var noView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            view = UIImageView(image: #imageLiteral(resourceName: "shopping-cart-2"))
            noView = UIImageView(image: #imageLiteral(resourceName: "notification"))
            view.clipsToBounds = true
        } else if rightBarButton == "notification" {
            var view  = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 20))
            view.contentMode = .scaleToFill
            view = UIImageView(image: #imageLiteral(resourceName: "ringing-1"))
            view.clipsToBounds = true
            rightBarButtonItem = UIBarButtonItem(image: view.image, style: .plain, target: self, action: #selector(self.openNotification))
        }else if rightBarButton == "none" {
            
        } else if rightBarButton == nil {
            var view  = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 20))
            view.contentMode = .scaleToFill
            view = UIImageView(image: #imageLiteral(resourceName: "ringing-1"))
            view.clipsToBounds = true
            rightBarButtonItem = UIBarButtonItem(image: view.image, style: .plain, target: self, action: #selector(self.openNotification))
        }
        
        navigationItem.leftBarButtonItem = leftBarButtonItem
        //Set right bar button so title view comes in center
        navigationItem.rightBarButtonItem = rightBarButtonItem
        navigationItem.rightBarButtonItem?.tintColor = .white
        UINavigationBar.appearance().barTintColor = primaryColor
        UINavigationBar.appearance().tintColor = .white
    }
    
    public func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
            switch index {
            case 0: return "1"
            case 1: return "2"
            case 2: return "3"
            case 3: return "4"
            default: return "1"
            }
    }
    
    func setNavTitle(title: String?) {
        
        if tabBarController != nil {
            if title != nil {
                self.tabBarController?.navigationItem.titleView = nil
                self.tabBarController?.navigationItem.title = title
            } else {
                var view  = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
                view = UIImageView(image: #imageLiteral(resourceName: "drupp logo"))
                view.clipsToBounds = true
                self.tabBarController?.navigationItem.titleView = view
                let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
                navigationController?.navigationBar.titleTextAttributes = textAttributes
                self.tabBarController?.navigationItem.titleView?.backgroundColor = primaryColor
                self.tabBarController?.navigationItem.titleView?.tintColor = UIColor.white
                self.tabBarController?.navigationItem.titleView?.contentMode = .center
            }
        } else {
                if(title == "1" || title == "0" || title == "2" || title == "3" ){
                let progressBar = FlexibleSteppedProgressBar()
                let view = UIView()
                progressBar.frame = CGRect(x:2, y: 0, width: 220, height: 44)
                view.frame = CGRect(x:0, y: 0, width: 230, height: 44)
                view.backgroundColor = .clear
                progressBar.translatesAutoresizingMaskIntoConstraints = true
                
//                progressBar.frame = view.frame
                view.addSubview(progressBar)
            
                progressBar.numberOfPoints = 4
                progressBar.lineHeight = 5
                progressBar.radius = 15
                progressBar.progressRadius = 15
                progressBar.progressLineHeight = 5
                progressBar.backgroundColor = .clear
                progressBar.currentIndex = Int(title ?? "1")!
                progressBar.tintColor = offWhiteColor
                progressBar.centerLayerDarkBackgroundTextColor = .white
                progressBar.backgroundShapeColor = .lightText
                progressBar.currentSelectedCenterColor = brownColor
                progressBar.selectedBackgoundColor = brownColor
                progressBar.viewBackgroundColor = .clear
                progressBar.centerLayerTextColor = .white
                progressBar.selectedOuterCircleStrokeColor = brownColor

                progressBar.stepTextColor = .white
                progressBar.delegate = self
                view.clipsToBounds = true
                self.navigationItem.titleView = view
            }else if title != nil {
                self.navigationItem.titleView = nil
                self.navigationItem.title = title
            } else {
                self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "druppMenuLogo"))
                self.navigationItem.titleView?.contentMode = .scaleAspectFit
            }
        }
         UINavigationBar.appearance().barTintColor = primaryColor
        UINavigationBar.appearance().tintColor = .white
    }
    
    func calculateTimeDifference(date1: Int, date2: Int) -> Int{
        let d1 = Date(timeIntervalSince1970: TimeInterval(date1))
        let d2 = Date(timeIntervalSince1970: TimeInterval(date2))
        let diff = d2.timeIntervalSince(d1)
        return Int(diff)
    }
    
    @objc func openNotification(){
        let myVC = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @objc func back() {
        if let navController = navigationController {            
            navController.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func pageBack() {
        NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_CURRENT_INDEX), object: nil, userInfo: ["index":5])
    }
    
    @objc func openCart() {
        if(Singleton.shared.cartData.cart_items.count == 0){
            Singleton.shared.showToast(text: "Your cart is empty", color: errorRed)
        }else {
            let myVC = storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    @objc func menu() {
        SideMenuManager.default.menuFadeStatusBar = false
        guard let sideMenuNavController =  SideMenuManager.defaultManager.menuLeftNavigationController else {
            let sideMenuController = storyboard?.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            sideMenuController.type = "Driver"
            SideMenuManager.defaultManager.menuWidth = self.view.frame.width*0.75
            SideMenuManager.defaultManager.menuLeftNavigationController = UISideMenuNavigationController(rootViewController: sideMenuController)
            SideMenuManager.defaultManager.menuLeftNavigationController?.setNavigationBarHidden(true, animated: false)
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
            return
        }
        present(sideMenuNavController, animated: true, completion: nil)
    }
  
    
    func convertTimestampToDate(_ timestamp: Int, to format: String) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    func convertDateToTimestamp(_ date: String, to format: String) -> Int{
        let dfmatter = DateFormatter()
        dfmatter.dateFormat = format
        dfmatter.timeZone = TimeZone(abbreviation: "GMT-5")
        let date = dfmatter.date(from: date)
        var dateStamp:TimeInterval?
        if let myDate = date {
            dateStamp = myDate.timeIntervalSince1970
            let dateSt:Int = Int(dateStamp!)
            return dateSt
        }else {
            return Int(Date().timeIntervalSince1970)
        }
    }
    
    func openUrl(urlStr: String) {
        let url = URL(string: urlStr)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}



extension UIImageView {
    func changeTint(color: UIColor) {
        let templateImage =  self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension String {
    func withBoldText(text: String, font: UIFont? = nil) -> NSAttributedString {
        let _font = font ?? UIFont.systemFont(ofSize: 14, weight: .regular)
        let fullString = NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font: _font])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: _font.pointSize)]
        let range = (self as NSString).range(of: text)
        fullString.addAttributes(boldFontAttribute, range: range)
        return fullString
    }
    
    func applyPatternOnNumbers(pattern: String, replacmentCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.Index(encodedOffset: index)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacmentCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
}



final class ContentSizedTableView: UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}

extension Date {
    var onlyDate: Date? {
        get {
            let calender = Calendar.current
            var dateComponents = calender.dateComponents([.year, .month, .day], from: self)
            dateComponents.timeZone = NSTimeZone.system
            return calender.date(from: dateComponents)
        }
    }
}

extension String {
    var isReallyEmpty: Bool {
        return self.trimmingCharacters(in: .whitespaces).isEmpty
    }
}


extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}

extension Date {
func timeAgoDisplay() -> String {
 let secondsAgo = Double(Date().timeIntervalSince(self))

 let minute: Double = 60
 let hour: Double = 60 * minute
 let day: Double = 24 * hour
 let week: Double = 7 * day

 if secondsAgo < minute {
     return "Few momments ago"
 } else if secondsAgo < hour {
     return "Few minutes ago"
 } else if secondsAgo < day {
     return "\(Int(round(secondsAgo / hour))) hrs ago"
 } else if secondsAgo < week {
     return "\(Int(round(secondsAgo / day))) days ago"
 }

let dates = self
let dateFormatter = DateFormatter()
dateFormatter.dateFormat = "dd MMM YYYY"
return dateFormatter.string(from: dates)
}
}

//extension Date {
//        func timeAgoDisplay() -> String {
////            if #available(iOS 13.0, *) {
////                let formatter = RelativeDateTimeFormatter()
////                formatter.unitsStyle = .full
////                return formatter.localizedString(for: self, relativeTo: Date())
////            } else {
//                let calendar = Calendar.current
//                let minuteAgo = calendar.date(byAdding: .minute, value: -1, to: Date())!
//                let hourAgo = calendar.date(byAdding: .hour, value: -1, to: Date())!
//                let dayAgo = calendar.date(byAdding: .day, value: -1, to: Date())!
//                let weekAgo = calendar.date(byAdding: .day, value: -7, to: Date())!
//                if minuteAgo < self {
//                    let diff = Calendar.current.dateComponents([.second], from: self, to: Date()).second ?? 0
//                    return "Few moments ago"
//                } else if hourAgo < self {
//                    let diff = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute ?? 0
//                    return "Few minutes ago"
//                } else if dayAgo < self {
//                    let diff = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour ?? 0
//                    return "\(diff) hrs ago"
//                } else if weekAgo < self {
//                    let diff = Calendar.current.dateComponents([.day], from: self, to: Date()).day ?? 0
//                    return "\(diff) days ago"
//                }
//                let dates = self
//                let dateFormatter = DateFormatter()
//                dateFormatter.dateFormat = "dd MMM YYYY"
//                return dateFormatter.string(from: dates)
////            }
//
//        }
//    }
