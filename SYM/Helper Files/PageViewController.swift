//
//  PageViewController.swift
//
//  Created by User on 18/05/17.
//  Copyright © 2017 Qualwebs. All rights reserved.
//

import UIKit

protocol ControllerIndexDelegate {
    func getControllerIndex(index: Int)
}

class PageViewController: UIPageViewController{
    var pageControl: UIPageControl?
    var currentIndex = 0
    var transitionInProgress: Bool = false
    static var customDataSource : UIPageViewControllerDataSource?
    
    lazy var viewControllerList: [UIViewController] = {
        var sb = UIStoryboard(name:"Main",bundle:nil)
        return [sb.instantiateViewController(withIdentifier: "UserBasicInfoVC" ),
                sb.instantiateViewController(withIdentifier: "UserRouteVC"),sb.instantiateViewController(withIdentifier: "UserPhotosUploadVC"),sb.instantiateViewController(withIdentifier: "UserFinishingUpVC")]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavTitle(title: "\(self.currentIndex)")
        self.setNavigationBar("pageBack", "none")
       // self.dataSource = self
        self.pageControl?.currentPage = self.currentIndex
        self.delegate = self as? UIPageViewControllerDelegate
       // PageViewController.customDataSource = self
        if(self.currentIndex == 0){
            if let firstViewController = viewControllerList.first {
                self.setViewControllers([firstViewController],direction: .forward,
                                        animated:false,completion:nil)
            }
        }else if(self.currentIndex == 1){
            let firstViewController = viewControllerList[1]
            self.setViewControllers([firstViewController],direction: .forward,
                                    animated:false,completion:nil)
            
        }else if(self.currentIndex == 2){
            let firstViewController = viewControllerList[2]
            self.setViewControllers([firstViewController],direction: .forward,
                                    animated:false,completion:nil)
            
        }else if(self.currentIndex == 3){
            let firstViewController = viewControllerList[3]
            self.setViewControllers([firstViewController],direction: .forward,
                                    animated:false,completion:nil)
            
        }
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = true
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeCurrentIndex(_:)), name: NSNotification.Name(N_CHANGE_CURRENT_INDEX), object: nil)
    }
    
    
    
    @objc func changeCurrentIndex(_ notif: NSNotification){
        
        if let index = notif.userInfo?["index"] as? Int{
            if(index == 5){
                if(currentIndex != 0){
                    self.currentIndex = self.currentIndex - 1
                    self.pageControl?.currentPage = self.currentIndex
                    self.setNavTitle(title: "\(self.currentIndex)")
                    setViewControllers([viewControllerList[self.currentIndex]], direction: .reverse, animated: true, completion: nil)
                }else {
                    // Singleton.shared.postYourShipment = GetPostYourShipmentData()
                    self.navigationController?.popViewController(animated: true)
                }
            }else {
                self.pageControl?.currentPage = index
                self.currentIndex = index
                self.setNavTitle(title: "\(index)")
                setViewControllers([viewControllerList[index]], direction: .forward, animated: true, completion: nil)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     return [self.newColoredViewController(controller: "FindRideViewController"),
     self.newColoredViewController(controller: "OfferRideViewController")]
     // Pass the selected object to th                                   e new view controller.
     
     }
     */
    //    private(set) lazy
    private func newColoredViewController(controller: String)->UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controller)
    }
}

//extension PageViewController: UIPageViewControllerDataSource
//{
//    func pageViewController(_ pageViewController: UIPageViewController,
//                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
//        guard let vcIndex = viewControllerList.index(of:viewController)
//        else{return nil}
//        let previousIndex = vcIndex - 1
//        guard previousIndex >= 0 else{return nil}
//        guard viewControllerList.count > previousIndex else{return nil}
//        return viewControllerList[previousIndex]
//    }
//
//    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
//        guard let vcIndex = viewControllerList.index(of:viewController)
//        else{return nil}
//        let nextIndex = vcIndex + 1
//        guard viewControllerList.count != nextIndex else{return nil}
//        guard viewControllerList.count > nextIndex else{return nil}
//        return viewControllerList[nextIndex]
//    }
//}

extension PageViewController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl?.currentPage = viewControllerList.index(of: pageContentViewController)!
    }
}
