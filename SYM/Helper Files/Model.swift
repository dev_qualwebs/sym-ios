
import UIKit

struct SplashContent {
    var backgroundImage: String
    var icon: String?
    var title: String?
    var content: String?
}

struct SuccessResponse:Codable
{
    var message:String?
    var status:Int?
}

struct Response: Codable {
    var message: String?
    // var response: String?
    var status: Int?
}

struct GetChatMessageShipmentResponse: Codable {
    var message: String?
    var response: ChatMessageShipment?
    var status: Int?
}

struct ChatMessageShipment: Codable {
    var shipment_details : [GetDisputedShipmentResponse]?
}

struct UploadImage: Codable {
    var response = UploadImageResponse()
    var message: String?
    var status: Int?
}

struct UploadImageResponse: Codable {
    var path: String?
    var image: String?
}


struct SignupResponse : Codable {
    var response: SignUpDetails
    var message: String?
    var status:Int?
}

struct SignUpDetails: Codable{
    var id:Int?
    var username:String?
    var email:String?
    var phone:String?
    // var dob:String?
    var flat:Int?
    var profile_image:String?
    var verify:Int?
    var created_date:Int?
    var role:Int?
    var status:Int?
    var token:String?
    var first_name:String?
    var last_name:String?
}

struct ChatResponse: Codable {
    var id: String?
    var message: String?
    var read_satus: Int?
    var receiver_id: Int?
    var sender_id: Int?
    var time: Int?
    var type: Int?
}

struct ChatDetail: Codable{
    var sender_id: Int?
    var receiver_id: Int?
    var receiver_name: String?
    var sender_name: String?
    var sender_image: String?
    var receiver_image: String?
    var shipment_id: Int?
}



struct Login: Codable {
    var response = LoginData()
    var message: String?
    var status: Int?
}

struct LoginData: Codable {
    var data = LogInResponse()
    var token: String?
}

struct UserInfo: Codable {
    var token: String?
}

struct AddUser: Codable {
    var userName: String?
    var firstName: String?
    var lastName: String?
    var email: String?
    var image:String?
    var role: String?
    var roleType: Int?
    var roleId: Int?
    var userRole: Int?
}

struct MenuObject {
    
    var image:UIImage?
    var name: String?
    
    init(image: UIImage?, name: String?) {
        self.image = image
        self.name = name
    }
}

struct GetCategoriesResponse: Codable{
    var status_code:Int?
    var message:String?
    var response:[GetCategories]
}

struct GetCategories: Codable{
    var id:Int?
    var category:String?
    var parent_id:Int?
    var image_name:String?
    var active:String?
    var status:Int?
    var sub_categories:[GetSubCategories]?
}
struct GetSubCategories: Codable{
    var id:Int?
    var category:String?
    var parent_id:Int?
    var image_name:String?
    var active:String?
}

struct LogInResponse: Codable{
    //    var response:
    var id: Int?
    var email: String?
    var status:Int?
    var country_code: String?
    //var dob: Int?
    var message:String?
    var success:String?
    var username: String?
    var user_id:Int?
    var role:Int?
    var phone: String?
    var first_name:String?
    var last_name:String?
    var profile_image:String?
    var flag: Int?
    var verify: Int?
    var stripe_id: String?
    var card_last_four: String?
    var stripe_account_id: String?
    var created_at: String?
}

struct GetPostShipmentAndRegister: Codable{
    var response: PostShipmentAndRegister?
    var message: String?
    var status: Int?
}

struct PostShipmentAndRegister:Codable{
    var status_code:Int?
    var user_id:Int?
    var message:String?
    var error:String?
    var token:String?
    var phone: String?
    var username: String?
    var first_name:String?
    var last_name:String?
    var url:String?
}

struct GetAllCommonShipmentResponse: Codable{
    var status_code:Int?
    var message:String?
    var response:GetAllShipmentData?
}

struct GetAllShipmentData: Codable{
    var data:[GetAllCommonShipment]?
}

struct GetAllDisputedShipmentData: Codable{
    var status_code:Int?
    var message:String?
    var response:[GetAllCommonShipment]?
}

struct GetAllCommonShipment:Codable{
    var id:Int?
    var shipment_id : Int?
    var user_id: Int?
    var category_id:Int?
    var description:String?
    var reason : String?
    var from_name:String?
    var from_latitude:String?
    var from_longitude:String?
    var from_city:String?
    var from_state:String?
    var from_house_no : String?
    var from_building : String?
    var from_street : String?
    var to_house_no : String?
    var to_building : String?
    var to_street : String?
    var from_zipcode:Int?
    var to_name:String?
    var to_latitude:String?
    var to_longitude:String?
    var to_state:String?
    var to_city:String?
    var to_zipcode:Int?
    var pickup_date:Int?
    var dropoff_date:Int?
    var price:String?
    var multiple_item:Int?
    var set_budget:Int?
    var sub_category_id:[Int]?
    var shipment_title :  String?
    var distance:String?
    var weight:Double?
    var driver_status:Int?
    var loader_status:Int?
    var lookingFor:Int?
    var customer_id:Int?
    var awarded_shipment : Int?
    var first_name:String?
    var last_name:String?
    var category_name:GetShipmentCategoryName?
    var customer_accepted:Int?
    var driver_accepted:Int?
    var completed_driver_id: Int?
    var completed_loader_id: Int?
    var driver_completed_at: Int?
    var loader_completed_at: Int?
    var driver_id:Int?
    var image_list: [UploadImageResponse]?
    var own_bids:[AppliedBidDetail]?
    var awarded_to: AwardedDriver?
    var remaining_time : Int?
}

struct AwardedDriver:Codable {
    var id : Int?
    var username : String?
    var first_name: String?
    var last_name : String?
    var email : String?
    var profile_image : String?
    var status : Int?
    var flag : Int?
}


struct GetShipmentCategoryName:Codable{
    var id:Int?
    var category:String?
    var parent_id:Int?
    var image_name:String?
    var active:String?
}

struct GetDriverSingleShipmentResponse:Codable{
    var status_code:Int?
    var message:String?
    var response = GetDriverSingleShipment()
}

struct GetDriverSingleShipment:Codable{
    var id:Int?
    var user_id: Int?
    var category_id:Int?
    var description:String?
    var from_name:String?
    var from_latitude:String?
    var from_longitude:String?
    var from_city:String?
    var from_state:String?
    var from_zipcode:Int?
    var from_house_no : String?
    var from_building : String?
    var from_street : String?
    var reason:String?
    var to_house_no : String?
    var to_building : String?
    var awarded_shipment : Int?
    var to_street : String?
    var to_name:String?
    var to_latitude:String?
    var to_longitude:String?
    var to_state:String?
    var to_city:String?
    var to_zipcode:Int?
    var pickup_date:Int?
    var dropoff_date:Int?
    var price:String?
    var multiple_item:Int?
    var set_budget:Int?
    var sub_category_id:Int?
    var distance:String?
    var weight:Double?
    var driver_status:Int?
    var bid_status:Int?
    var loader_status:Int?
    var lookingFor:Int?
    var customer_id:Int?
    var security_check:Int?
    var pay_amount: Double?
    var awarded_status_driver: Int?
    var awarded_status_loader:Int?
    var rejection_reason: String?
    var completed: Int?
    var sponser_amount: String?
    var awarded_count: Int?
    var session_count: Int?
    // var expiry_time: Int?
    var selected_driver: String?
    var selected_loader: String?
    var own_bids:[AppliedBidDetail]?
    var other_bids:[AppliedBidDetail]?
    var image_list: [UploadImageResponse]?
    var subcategory_name = [GetSubCategories]()
    var customer_name:TypeOfUser?
    var driver_id:Int?
    var category_name:GetDriverSingleCategory?
    var customer_details:[TypeOfUser]?
    var remaining_time : Int?
    var refund_shipment_request : RefundRequest?
}

struct GetDriverSingleCategory:Codable {
    var category:String?
}

struct GetUserShipmentResponse:Codable{
    var status_code:Int?
    var message:String?
    var response = AllShipmentResponse()
}

struct CustomerSingleShipment: Codable {
    var response = GetUserShipmentData()
    var message: String?
    var status: Int?
}

struct AllShipmentResponse: Codable {
    var data = [GetUserShipmentData]()
    var total_shipments:Int?
    var total_completed_shipments:Int?
}

struct GetUserShipmentData:Codable{
    var id:Int?
    var is_driver_rated: Int?
    var shipment_title : String?
    var user_id:Int?
    var category_id:Int?
    var category_name:GetCategories?
    var description:String?
    var from_name:String?
    var from_latitude:String?
    var from_longitude:String?
    var from_city:String?
    var from_state:String?
    var from_zipcode:Int?
    var from_house_no : String?
    var from_building : String?
    var from_street : String?
    var to_house_no : String?
    var to_building : String?
    var to_street : String?
    var to_name:String?
    var to_latitude:String?
    var to_longitude:String?
    var to_state:String?
    var to_city:String?
    var to_zipcode:Int?
    var pickup_date:Int?
    var dropoff_date:Int?
    var price:String?
    var multiple_item:Int?
    var set_budget:Int?
    var distance:String?
    var weight:Double?
    var driver_status:Int?
    var loader_status:Int?
    var lookingFor:Int?
    var image_list: [UploadImageResponse]?
    var completed_driver_id:Int?
    var completed_loader_id:Int?
    var driver_completed_at:Int?
    var loader_completed_at:Int?
    var subcategory_name = [GetSubCategories]()
    var created_date:Int?
    var driver_bids_count:Int?
    var driver:TypeOfUser?
    var driver_bids: [AppliedBidDetail]?
    var customer_name:TypeOfUser?
    var awarded_count:Int?
    var driver_id:Int?
    var refund_shipment_request : RefundRequest?
}

struct RefundRequest : Codable {
    var id : Int?
    var shipment_id : Int?
    var user_id : Int?
    var bider_id : Int?
    var status : Int?
    var reason_by_user : String?
    var comment_by_admin : String?
    var created_at : String?
    var updated_at :String?
}



struct GetPostYourShipmentData: Codable{
    var id = Int()
    var category_id = Int()
    var discription = String()
    var from_name = String()
    var from_latitude = String()
    var from_longitude = String()
    var from_city = String()
    var from_state = String()
    var from_zipcode = String()
    var from_house_no = String()
    var from_building = String()
    var from_street = String()
    var to_house_no = String()
    var to_building = String()
    var to_street = String()
    var to_name = String()
    var to_latitude = String()
    var to_longitude = String()
    var to_city = String()
    var to_state = String()
    var to_zipcode = String()
    var lookingFor = Int()
    var sub_category_id = [Int]()
    var set_budget = Int()
    var multiple_item = String()
    var distance = String()
    var weight = Double()
    var price = String()
    var image : [String]?
    var pickup_date = Int()
    var dropoff_date = Int()
}

struct TypeOfUser:Codable{
    var user_id:Int?
    var id: Int?
    // var dob:Int?
    var phone: String?
    var username: String?
    var first_name:String?
    var last_name:String?
    var email:String?
    var address: String?
    var profile_image:String?
    var role:Int?
    var country_code: String?
    
}

struct GetDriverProfileResponse:Codable{
    var status_code:Int?
    var message:String?
    var response:[GetDriverProfileDetails]
}

struct GetDriverProfileDetails:Codable {
    var id:Int?
    var username:String?
    var first_name:String?
    var last_name:String?
    var email:String?
    var phone:String?
    // var dob:String?
    var role:Int?
    var flag:Int?
    var profile_image:String?
    var verify:Int?
    var status:Int?
    var created_date:Int?
    var updated_at:String?
    var address:String?
    var city:String?
}

struct GetSingleTransactionResponse : Codable {
    let response : TransactionResponse?
    let message : String?
    let status : Int?
}

struct GetStateResidenceResponse:Codable{
    var status_code:Int?
    var message:String?
    var response:[GetStateResidenceDetails]
}

struct GetStateResidenceDetails:Codable {
    var id:Int?
    var state:String?
    var code:String?
    var from_city: String?
}

struct GetSecurityOtpResponse:Codable{
    var status_code:Int?
    var message:String?
    //    var response:[GetSecurityOtpData]
    var response: GetSecurityOtpData?
}

struct GetSecurityOtpData:Codable{
    var id:Int?
    var token:Int?
}

struct GetDriverProfileDetailsResponse:Codable{
    var response = [GetDriverProfileDetailsData]()
    var message:String?
    var status:Int?
}

struct GetDriverProfileDetailsData:Codable{
    var id:Int?
    var username:String?
    var first_name:String?
    var last_name:String?
    var email:String?
    var phone:String?
    // var dob:Int?
    var role:Int?
    var flag:Int?
    var profile_image:String?
    var verify:Int?
    var status:Int?
    var created_date:Int?
    var address:String?
    var city:String?
    var state:Int?
    var zipcode:String?
    var experience:String?
    var business_name:String?
    var usdot_number:String?
    var ein_number:String?
    var description:String?
    var insurance_company:String?
    var ssn_number:String?
    var security_check:Int?
    var show_msg:Int?
    var rejection_msg:String?
    var driver_id:Int?
    var category = [GetCategories]()
    var loader_city = [String]()
    //    var security_status:Int?
    //    var security_status:String?
    var type:Int?
}

//struct GetDriverProfileCategory:Codable{
//    var id:Int?
//    var category:String?
//    var parent_id:Int?
//    var image_name:String?
//    var active:String?
//    var status:Int?
//}

struct GetBidSearchDetail : Codable {
    var response : [AppliedBidDetail]?
    var message : String?
    var status : Int?
}

struct AppliedBidDetail: Codable {
    var id: Int?
    var shipment_id: Int?
    var driver_id: Int?
    var date: Int?
    var amount: String?
    var sponsored: Int?
    var message:String?
    var first_name:String?
    var last_name: String?
    var profile_image: String?
    var customer_accepted: Int?
    var driver_accepted: Int?
    var placingAs: Int?
    var latest_flag: Int?
    var status: Int?
    var driver:GetDriverProfileDetails?
    var total_complete_shipment : Int?
}

struct GetEligibility: Codable{
    var response = GetEligibilityResponse()
    var message: String?
    var status:Int?
}

struct GetEligibilityResponse:Codable{
    var driver_eligible: Int?
    var loader_eligible: Int?
    var security_check: Int?
    var own_bid: Int?
    var sponsored: Int?
    var customer_accepted: Int?
    var driver_accepted: Int?
    var more_count=MoreEligibilityResponse()
}

struct MoreEligibilityResponse:Codable{
    var total:Int?
    var driver_count:Int?
    var loader_count: Int?
    var both_count:Int?
    
}

struct GetCard: Codable{
    var response = [GetCardResponse]()
    var message: String?
    var status: Int?
}

struct GetCardResponse: Codable{
    var id: Int?
    var user_id: Int?
    var stripe_token : String?
    var stripe_customer_id : String?
    var card_id : String?
    var charge_id : String?
    var customer: String?
    var card_number: Int?
    var expiry_month: String?
    var expiry_year: String?
    var card_type: String?
    var country: String?
    var masked_card_number: String?
    var name: String?
    var is_default: Int?
}

struct StartTransaction: Codable {
    var response:StartTransactionResponse?
    var message: String?
    var status: Int?
}

struct StartTransactionResponse: Codable {
    var transaction:StartTransactionDetail?
    var amount: String?
    var expiry_time: Int?
}

struct StartTransactionDetail: Codable {
    var user_id: Int?
    var transaction_id: String?
    var amount: String?
    var driver_loader_id: Int?
    var charge_id: String?
    var refund_id: String?
    var type: Int?
    var refund_status: Int?
    var amount_status: Int?
    var shipment_id: Int?
    var bid_id: Int?
    var status: Int?
    var start_duration:Int?
    var end_duration: Int?
    var flag: Int?
    var session_count: Int?
    var created_date: String?
    var transaction_date: String?
    var sender_id : Int?
    var sender_name : String?
    var profile_image : String?
    var driver_amount : String?
}

struct GetCustomerProfile: Codable {
    var response: TypeOfUser?
    var message:String?
    var status:Int?
}

struct GetRating: Codable {
    var response = Ratings()
    var message: String?
    var status: Int?
    
}

struct Ratings: Codable{
    var review_rating = [RatingResponse]()
    var total_rating: Int?
    
}

struct RatingResponse: Codable {
    var id:Int?
    var review:String?
    var rating: Int?
    var given_by: Int?
    var given_to: Int?
    var shipment_id: Int?
    var from_name: String?
    var to_name: String?
    var created_at: String?
    var user_details: LogInResponse?
}

struct GetDashboardData: Codable {
    var status_code: Int?
    var message: String?
    var response = DashboardData()
}

struct DashboardData: Codable {
    var total_shipment: Int?
    var total_distance: Double?
    var total_reviews: Int?
    var total_disputes: Int?
    var inprogress_shipment: Int?
    var latest_shipment = [GetAllCommonShipment]()
}

struct GetDisputedShipment: Codable {
    var response = [GetDisputedShipmentResponse]()
    var message:String?
    var status: Int?
}

struct GetDisputedShipmentResponse: Codable {
    var id: Int?
    var shipment_id: Int?
    var user_id: Int?
    var bider_id: Int?
    var reason: String?
    var category_id:Int?
    var category_name:GetCategories?
    var description:String?
    var from_name:String?
    var from_latitude:String?
    var from_longitude:String?
    var from_city:String?
    var from_state:String?
    var from_zipcode:Int?
    var to_name:String?
    var from_house_no : String?
    var from_building : String?
    var from_street : String?
    var to_house_no : String?
    var to_building : String?
    var to_street : String?
    var to_latitude:String?
    var to_longitude:String?
    var to_state:String?
    var to_city:String?
    var to_zipcode:Int?
    var pickup_date:Int?
    var dropoff_date:Int?
    var price:String?
    var multiple_item:Int?
    var created_at:String?
    var set_budget:Int?
    var distance:String?
    var weight:Double?
    var lookingFor: Int?
    var driver_status:Int?
    var loader_status:Int?
    var bider: TypeOfUser?
    var  owner: TypeOfUser?
    var customer_info:GetDriverProfileDetails?
}

struct GetEcommerceCategories: Codable {
    var response = [CategoryResponse]()
    var message: String?
    var status: Int?
}


struct CategoryResponse: Codable {
    var id: Int?
    var category_name: String?
    var category_image: String?
    var parent_id: Int?
    var subcategories = [SubCategoryResponse]()
}

struct SubCategoryResponse: Codable {
    var id: Int?
    var category_name: String?
    var category_image: String?
    var parent_id: Int?
}

struct GetProducts: Codable {
    var response  = [GetProductResponse]()
    var message: String?
    var status: Int?
}

struct GetProductResponse: Codable {
    var id: Int?
    var product_name: String?
    var description: String?
    var price: String?
    var discount: Int?
    var color: String?
    var brand: String?
    var size: Int?
    var quantity: Int?
    var avg_product_rating: String?
    var category_id: Int?
    var subcategory_id: Int?
    var product_link: String?
    var product_images:[ProductImage]?
    var category:SubCategoryResponse?
    var subcategory: SubCategoryResponse?
}

struct ProductImage: Codable {
    var id: Int?
    var product_id: Int?
    var image: String?
}

struct GetCart: Codable {
    var response = CartData()
    var message: String?
    var status: Int?
}

struct CartData: Codable {
    var cart_detail = CartDetails()
    var cart_items = [CartItems]()
}

struct CartDetails: Codable {
    var id: Int?
    var user_id: Int?
    var shipping_method: Int?
    var total_items: Int?
    var items_qty: Int?
    var grand_total: String?
    var base_grand_total: String?
    var sub_total: String?
    var base_sub_total: String?
    var tax_total: String?
    var base_tax_total: String?
    var discount_amount: String?
    var base_discount_amount: String?
}

struct CartItems: Codable {
    var id: Int?
    
    var product_id: Int?
    var cart_id: Int?
    var product_name: String?
    var quantity: Int?
    var price: String?
    var base_price: String?
    var total: String?
    var base_total: String?
    var tax_percent: String?
    var tax_amount: String?
    var base_tax_amount: String?
    var discount_percent: String?
    var discount_amount: String?
    var base_discount_amount: String?
    var description: String?
    var color: String?
    var brand: String?
    var size: String?
    var discount: Int?
    var product_images = [ProductImage]()
}

struct GetShippingAddress: Codable {
    var response = [GetAddressResponse]()
    var message: String?
    var status: Int?
}

struct GetAddressResponse: Codable {
    var id: Int?
    var name: String?
    var email: String?
    var address: String?
    var address2: String?
    var country: String?
    var state: String?
    var city: String?
    var phone: String?
    var postcode: Int?
    var is_default: Int?
    var user_id: Int?
    var latitude: String?
    var longitude: String?
    
}

struct GetMessage: Codable {
    var response = [MessageResponse]()
    var message: String?
    var status: Int?
}

struct MessageResponse: Codable {
    var id: Int?
    var shipment_id: Int?
    var driver_id: Int?
    var date: Int?
    var amount: String?
    var sponsored: Int?
    var customer_accpeted: Int?
    var driver_accepted: Int?
    var read_status : Int?
    var status: Int?
    var message: String?
    var created_at: String?
    var driver: GetDriverProfileDetails?
    var shipment: GetDisputedShipmentResponse?
}

struct GetDriverMessage: Codable {
    var response = [DriverMessageResponse]()
    var message: String?
    var status: Int?
}

struct DriverMessageResponse: Codable {
    var id: Int?
    var shipment_id: Int?
    var driver_id: Int?
    var date: Int?
    var read_status : Int?
    var amount: String?
    var sponsored: Int?
    var customer_accpeted: Int?
    var driver_accepted: Int?
    var status: Int?
    var message: MessageResponse?
    var driver: GetDriverProfileDetails?
    var shipment: GetDisputedShipmentResponse?
}

struct GetDraft: Codable {
    var response = [GetDraftResponse]()
    var message: String?
    var status: Int?
}

struct GetDraftResponse: Codable {
    var id: Int?
    var user_id: Int?
    var category_id: Int?
    var description: String?
    var from_name: String?
    var from_latitude: String?
    var from_longitude: String?
    var from_city: String?
    var from_state: Int?
    var from_zipcode: Int?
    var from_house_no : String?
    var from_building : String?
    var from_street : String?
    var to_house_no : String?
    var to_building : String?
    var to_street : String?
    var to_name: String?
    var to_latitude: String?
    var to_longitude: String?
    var to_city: String?
    var to_state: Int?
    var to_zipcode: Int?
    var pickup_date: Int?
    var dropoff_date: Int?
    var price: String?
    var set_budget: Int?
    var distance: String?
    var weight: Int?
    var weight_unit: Int?
    var total_size: Int?
    var size_unit: Int?
    var lookingFor: Int?
    var draft_images : [Draft_images]?
    var draft_subcategories = [DraftSubcategories]()
}

struct Draft_images : Codable {
    var id : Int?
    var draft_shipment_id : Int?
    var image : String?
    var created_at : String?
    var updated_at : String?
}

struct DraftSubcategories: Codable {
    var id: Int?
    var draft_shipment_id: Int?
    var subcategory_id: Int?
}

struct GetTransactions: Codable {
    var response = [TransactionResponse]()
    var message: String?
    var status: Int?
}

struct TransactionResponse: Codable {
    var id:Int?
    var user_id: Int?
    var category_id:Int?
    var description:String?
    var from_name:String?
    var from_latitude:String?
    var from_longitude:String?
    var from_city:String?
    var from_state:String?
    var from_zipcode:Int?
    var to_name:String?
    var from_house_no : String?
    var from_building : String?
    var from_street : String?
    var to_house_no : String?
    var to_building : String?
    var to_street : String?
    var to_latitude:String?
    var to_longitude:String?
    var to_state:String?
    var to_city:String?
    var to_zipcode:Int?
    var pickup_date:Int?
    var dropoff_date:Int?
    var price:String?
    var multiple_item:Int?
    var set_budget:Int?
    var sub_category_id:[Int]?
    var distance:String?
    var weight:Double?
    var driver_status:Int?
    var loader_status:Int?
    var lookingFor:Int?
    var customer_id:Int?
    var first_name:String?
    var last_name:String?
    var category_name:GetShipmentCategoryName?
    var customer_details: LogInResponse?
    var customer_accepted:Int?
    var driver_accepted:Int?
    var completed_driver_id: Int?
    var completed_loader_id: Int?
    var driver_completed_at: Int?
    var loader_completed_at: Int?
    var created_date: Int?
    var commission_percent: Int?
    var commission_amount: String?
    var shipment_amount:String?
    var award_shipment_flag:Int?
    var card_used:String?
    var weight_unit:Int?
    var total_size: Int?
    var size_unit:Int?
    var sponsored:Int?
    var amount: String?
    var transaction = [StartTransactionDetail]()
    var payment_flag : Int?
}


struct GetStripeResponse:Codable{
    var response: [StripeResponse]?
    var message:String?
    var status:Int?
}

struct StripeResponse: Codable{
    var external_accounts:ExternalAccount?
    var individual:IndividualData?
    var sin_snn_visibility: Int?
    //    let id : String?
    //    let object : String?
    //    let business_profile : Business_profile?
    //    let business_type : String?
    //    let capabilities : Capabilities?
    //    let charges_enabled : Bool?
    //    let company : Company?
    //    let country : String?
    //    let created : Int?
    //    let default_currency : String?
    //    let details_submitted : Bool?
    //    let email : String?
    //    let future_requirements : Future_requirements?
    //    let metadata : [String]?
    //    let payouts_enabled : Bool?
    //    let requirements : Requirements?
    //    let settings : Settings?
    //    let tos_acceptance : Tos_acceptance?
    //    let type : String?
}


struct ExternalAccount:Codable {
    var data:[StripeData]?
}

struct IndividualData:Codable{
    var address:IndividualAddress?
}

struct IndividualAddress:Codable{
    var city:String?
    var country:String?
    var line1: String?
    var line2: String?
    var postal_code:String?
    var state: String?
}


struct StripeData: Codable{
    var last4: String?
    var bank_name : String?
    var routing_number : String?
}


struct GetPostedBid: Codable {
    var response = PostedBidResponse()
    var message: String?
    var status: Int?
}

struct PostedBidResponse: Codable {
    var data = [PostedBid]()
    var total: Int?
}

struct PostedBid: Codable {
    var id: Int?
    var shipment_id: Int?
    var date: Int?
    var amount: String?
    var message: String?
    var customer_accepted: Int?
    var driver_accepted : Int?
    var placingAs: Int?
    var shipment_detail: ShipmentDetail?
    var status: Int?
}

struct ShipmentDetail: Codable {
    var id: Int?
    var from_city: String?
    var to_city: String?
    var driver_status: Int?
    var lookingFor: Int?
}

//struct GetNotifications: Codable {
//    var response = NotificationResponse()
//    var message: String?
//    var status: Int?
//
//}
//
//struct NotificationResponse: Codable {
//    var notification = Notifications()
//    var unread_count: Int?
//}
//
//struct Notifications: Codable {
//    var id: Int?
//    var user_id: Int?
//    var notification: String?
//    var read_status: Int?
//    var type: Int?
//    var sender_id: Int?
//    var created_at: String?
//    var shipment_id: Int?
//}

struct GetNotificationsResponse: Codable {
    var response : NotificationResponse?
    var message : String?
    var status : Int?
}

struct NotificationResponse: Codable {
    var notification : GetNotification?
    var unread_count: Int?
}

struct GetNotification : Codable {
    var current_page : Int?
    var data : [Notifications]?
    var first_page_url : String?
    var from : Int?
    var last_page : Int?
    var last_page_url : String?
    var next_page_url : String?
    var path : String?
    var per_page : Int?
    var prev_page_url : String?
    var to : Int?
    var total : Int?
}

struct Notifications: Codable {
    var id: Int?
    var user_id: Int?
    var notification: String?
    var read_status: Int?
    var type: Int?
    var sender_id: Int?
    var created_at: String?
    var read_at : Int?
    var shipment_id: Int?
    var updated_at : String?
    var user_details : TypeOfUser?
}



struct GetOrders: Codable {
    var response = [OrderResponse]()
    var message: String?
    var status: Int?
}

struct OrderResponse: Codable {
    var id: Int?
    var status: String?
    var shipping_method: Int?
    var total_item_count: Int?
    var total_qty_ordered: Int?
    var grand_total: String?
    var base_grand_total: String?
    var grand_total_refunded: String?
    var base_grand_total_refunded: String?
    var discount_amount: String?
    var base_discount_amount: String?
    var discount_refunded: String?
    var base_discount_refunded: String?
    var tax_amount: String?
    var base_tax_amount: String?
    var created_at: String?
    var user_id: Int?
    var order_items = [OrderItems]()
}

struct OrderItems: Codable {
    var id: Int?
    var name: String?
    var qty_ordered: Int?
    var rating:Int?
    var price: String?
    var base_price: String?
    var total: String?
    var base_total: String?
    var amount_refunded: String?
    var base_amount_refunded: String?
    var discount_percent: String?
    var discount_amount: String?
    var base_discount_amount: String?
    var discount_refunded: String?
    var base_discount_refunded: String?
    var tax_percent: String?
    var tax_amount: String?
    var base_tax_amount: String?
    var tax_amount_refunded: String?
    var product_id: Int?
    var order_id: Int?
    var image: String?
    var is_rated: Int?
    var product_images:[ProductImage]?
}


struct SaveDraftResponse : Codable {
    var response : SaveDraft?
    var message : String?
    var status : Int?
}

struct SaveDraft : Codable {
    var draft_shipment_id : Int?
}

struct GetSingleDraftResponse : Codable {
    var response : SingleDraft?
    var message : String?
    var status : Int?
}

struct SingleDraft : Codable {
    var id : Int?
    var user_id : Int?
    var category_id : Int?
    var description : String?
    var from_name : String?
    var from_latitude : String?
    var from_longitude : String?
    var from_city : String?
    var from_state : Int?
    var from_zipcode : Int?
    var from_house_no : String?
    var from_building : String?
    var from_street : String?
    var to_house_no : String?
    var to_building : String?
    var to_street : String?
    var to_name : String?
    var to_latitude : String?
    var to_longitude : String?
    var to_city : String?
    var to_state : Int?
    var to_zipcode : Int?
    var pickup_date : Int?
    var dropoff_date : Int?
    var price : String?
    var set_budget : Int?
    var distance : String?
    var weight : String?
    var weight_unit : String?
    var total_size : String?
    var size_unit : String?
    var lookingFor : Int?
    var created_at : String?
    var updated_at : String?
    var draft_images : [Draft_images]?
    var draft_subcategories : [DraftSubcategories]?
}

struct LiveTracking : Codable{
    var cLat : Double?
    var cLng : Double?
    var destLat : String?
    var destLng : String?
    var shipmentID : Int?
    var srcLat : String?
    var srcLng : String?
}


struct CanclledShipmentResponse : Codable {
    var response : ShipmentResponse?
    var message : String?
    var status : Int?
}

struct ShipmentResponse : Codable {
    var data : [CancelledData]?
    var total_shipments : Int?
}

struct CancelledData : Codable {
    var id : Int?
    var user_id : Int?
    var category_id : Int?
    var sub_category_id : String?
    var is_cancelled : Int?
    var shipment_title : String?
    var description : String?
    var from_name : String?
    var from_house_no : String?
    var from_building : String?
    var from_street : String?
    var to_house_no : String?
    var to_building : String?
    var to_street : String?
    var from_latitude : String?
    var from_longitude : String?
    var from_city : String?
    var from_state : String?
    var from_zipcode : Int?
    var to_name : String?
    var to_latitude : String?
    var to_longitude : String?
    var to_city : String?
    var to_state : String?
    var to_zipcode : Int?
    var pickup_date : Int?
    var dropoff_date : Int?
    var price : String?
    var multiple_item : Int?
    var set_budget : Int?
    var distance : String?
    var weight : String?
    var payment_flag : Int?
    var driver_status : Int?
    var loader_status : Int?
    var lookingFor : Int?
    var completed_driver_id : Int?
    var completed_loader_id : Int?
    var driver_completed_at : Int?
    var loader_completed_at : Int?
    var created_date : Int?
    var created_at : Int?
    var updated_at : String?
    var is_driver_rated : Int?
    var commission_percent : String?
    var commission_amount : String?
    var shipment_amount : String?
    var award_shipment_flag : Int?
    var card_used : String?
    var weight_unit : String?
    var total_size : String?
    var size_unit : Int?
    var driver_bids_count : Int?
    var image_list : [UploadImageResponse]?
    var category_name : Category_name?
    var subcategory_name = [Subcategory_name]()
    var driver_bids : [String]?
    var total_awarded : [String]?
    var awarded_count : Int?
    var awarded_person : [String]?
}


struct Category_name : Codable {
    var id : Int?
    var category : String?
    var parent_id : Int?
    var image_name : String?
    var active : String?
    var created_at : String?
    var updated_at : String?
}


struct Subcategory_name : Codable {
    var id : Int?
    var category : String?
    var parent_id : Int?
    var image_name : String?
    var active : String?
    var created_at : String?
    var updated_at : String?
    var laravel_through_laravel_through_key : Int?
}
