//
//  DatePickerVC.swift
//  SYM
//
//  Created by AM on 29/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class DatePickerVC: UIViewController {
    //Mark: IBOUTLETS
    @IBOutlet weak var datePicker: UIDatePicker!
    
    //Mark: Properties
    var picker_type = Int()
    var minDate : Date?
    var maxDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(minDate != nil){
            self.datePicker.minimumDate = minDate ?? Date()
        }
        
        if(maxDate != nil){
            self.datePicker.maximumDate = maxDate ?? Date()
        }
    }
   
    
//    @IBAction func datePick(_ sender: UIDatePicker) {
//        if picker_type == 1{
//            K_SELECTED_DATE = self.convertTimestampToDate(Int(self.datePicker.date.timeIntervalSince1970), to: "dd-MM-yyyy")
//            NotificationCenter.default.post(name: NSNotification.Name(N_Date_Picker), object: nil, userInfo: nil)
//        }
//        else if picker_type == 2{
//            K_SELECTED_DATE = self.convertTimestampToDate(Int(self.datePicker.date.timeIntervalSince1970), to: "dd-MM-yyyy")
//            NotificationCenter.default.post(name: NSNotification.Name(N_Date_Picker), object: nil, userInfo: nil)
//        }
//        else if picker_type == 3{
//            K_SELECTED_DATE = self.convertTimestampToDate(Int(self.datePicker.date.timeIntervalSince1970), to: "dd-MM-yyyy")
//            NotificationCenter.default.post(name: NSNotification.Name(N_Date_Picker), object: nil, userInfo: nil)
//        }
//    }
    
    @IBAction func doneBtnPressed(_ sender: Any) {
        if picker_type == 1{
            K_SELECTED_DATE = self.convertTimestampToDate(Int(self.datePicker.date.onlyDate!.timeIntervalSince1970), to: K_CUSTOM_YYYY_MM_DD)
            NotificationCenter.default.post(name: NSNotification.Name(N_Date_Picker), object: nil, userInfo: nil)
        }
        else if picker_type == 2{
            K_SELECTED_DATE = self.convertTimestampToDate(Int(self.datePicker.date.onlyDate!.timeIntervalSince1970), to: K_CUSTOM_YYYY_MM_DD)
            NotificationCenter.default.post(name: NSNotification.Name(N_Date_Picker), object: nil, userInfo: nil)
        }
        else if picker_type == 3{
            K_SELECTED_DATE = self.convertTimestampToDate(Int(self.datePicker.date.onlyDate!.timeIntervalSince1970), to: K_CUSTOM_YYYY_MM_DD) //"MM-dd-yyyy"
            NotificationCenter.default.post(name: NSNotification.Name(N_Date_Picker), object: nil, userInfo: nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
}
