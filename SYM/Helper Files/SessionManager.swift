import UIKit
import Alamofire

class SessionManager: NSObject {

    static var shared = SessionManager()

    var createWallet: Bool = true

    func methodForApiCalling<T: Codable>(url: String, method: HTTPMethod, parameter: Parameters?, objectClass: T.Type, requestCode: String, userToken: String?, completionHandler: @escaping (T) -> Void) {
        print("URL: \(url)")
        print("METHOD: \(method)")
        print("PARAMETERS: \(parameter)")
        print("TOKEN: \(getHeader(reqCode: requestCode, userToken: userToken))")
        AF.request(url, method: method, parameters: parameter, encoding: JSONEncoding.default, headers:  getHeader(reqCode: requestCode, userToken: userToken), interceptor: nil).responseString { (dataResponse) in

            let statusCode = dataResponse.response?.statusCode
            print("statusCode: ",dataResponse.response?.statusCode)
            print("dataResponse: \(dataResponse)")

            switch dataResponse.result {
            case .success(_):
                let object = self.convertDataToObject(response: dataResponse.data, T.self)
                let errorObject = self.convertDataToObject(response: dataResponse.data, Response.self)
                let loginObject = self.convertDataToObject(response: dataResponse.data, T.self)

                if (statusCode == 200 || statusCode == 201) && object != nil
                    //                    && (requestCode != U_VALIDATE_USER)
                {
                    completionHandler(object!)
                }
                else if statusCode == 401 {
                    Singleton.shared.showToast(text:errorObject?.message ?? "", color: errorRed)
                    
                }
                else if statusCode == 404  {
                    Singleton.shared.showToast(text:errorObject?.message ?? "", color: errorRed)
                } else {
                    Singleton.shared.showToast(text:errorObject?.message ?? "", color: errorRed)
                }
                ActivityIndicator.hide()
                break
            case .failure(_):
                ActivityIndicator.hide()
                let error = dataResponse.error?.localizedDescription
                if error == "The Internet connection appears to be offline." {
                    //Showing error message on alert
                    self.showAlert(msg: error)
                } else {
                    //Showing error message on alert
                    self.showAlert(msg: error)
                }
                break
            }
        }
    }

    private func showAlert(msg: String?) {
        UIApplication.shared.keyWindow?.rootViewController?.showAlert(title: "Error", message: msg, action1Name: "Ok", action2Name: nil)

    }
    
    
    func makeMultipartRequest<T: Codable>(url: String, fileData: Data, param: [String:Any], objectClass: T.Type, fileName: String, completionHandler: @escaping (T) -> Void) {
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in param {
                if key == "file_type" {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                } else {
                    multipartFormData.append(value as! Data, withName: key as! String, fileName: "image.png", mimeType: "image/png")
                }
            }

            },
        to: url, method: .post , headers: self.getHeader(reqCode: url, userToken: nil))
                .responseJSON(completionHandler: { (response) in
                    ActivityIndicator.hide()
                    print(response)
                    
                    if let err = response.error{
                        print(err)
                       // onError?(err)
                        return
                    }
                    print("Succesfully uploaded")
                    
                    let json = response.data
                    
                    if (json != nil)
                    {
                        
                        let object = self.convertDataToObject(response: json, T.self)
                        completionHandler(object!)
                    }
                    ActivityIndicator.hide()
                })
          

    }
    

    private func convertDataToObject<T: Codable>(response inData: Data?, _ object: T.Type) -> T? {
        if let data = inData {
            do {
                let decoder = JSONDecoder()
                let decoded = try decoder.decode(T.self, from: data)
                return decoded
            } catch {
                print(error)
            }
        }
        return nil
    }

    func getHeader(reqCode: String, userToken: String?) -> HTTPHeaders? {
        var token = UserDefaults.standard.string(forKey: UD_TOKEN)
        if (reqCode != U_LOGIN) || (reqCode != U_SIGNUP) || (reqCode != U_GET_CATEGORIES) || (reqCode != U_STATE_RESIDENCE){
            if(token == nil){
                return nil
            }else {
                return ["Authorization": "Bearer " + token!]
            }
        } else {
            return nil
        }
    }
}

