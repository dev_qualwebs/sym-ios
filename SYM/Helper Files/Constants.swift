//
//  Constants.swift
//
//  Created by Manisha  Sharma on 02/01/2019.
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import UIKit

//MARK: Constants
var primaryColor = UIColor(red: 0/255, green: 126/255, blue: 125/255, alpha: 1)//007e7d
var brownColor = UIColor(red: 128/255, green: 5/255, blue: 21/255, alpha: 1)//800515
var lightGreen = UIColor(red: 87/255, green: 163/255, blue: 171/255, alpha: 1)//57a3ab
var blueColor = UIColor(red: 2/255, green: 50/255, blue: 128/255, alpha: 1)//023280
var greenColor = UIColor(red: 0/255, green: 230/255, blue: 118/255, alpha: 1)//00E676
var redColor = UIColor(red: 232/255, green: 51/255, blue: 35/255, alpha: 1)//E83323

var successGreen = UIColor(red: 75/255, green: 181/255, blue: 67/255, alpha: 1)//4BB543
var errorRed = UIColor(red: 139/255, green: 0/255, blue: 0/255, alpha: 1)//8B0000

//// var yellowColor =   UIColor(red: 229/255, green: 178/255, blue: 69/255, alpha: 1)//E5B245
//var secondaryColor = UIColor(red: 219/255, green: 183/255, blue: 55/255, alpha: 1)
//var barBackgroundColor = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1)
//var backgroundColor = UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1)
var offWhiteColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1)
//
//Keys


//MARK: API Constants
//Base Url
//let U_BASE = "http://52.15.169.195/api-sym/api/"
let U_BASE = "https://admin.shareyourmove.com/api/"
//let U_BASE = "http://52.15.169.195/beta-api-sym/"
//let U_BASE = "https://beta-admin.shareyourmove.com/api/"
//let U_IMAGE = "http://52.15.169.195/api-sym/public/storage/"
let U_IMAGE = "https://admin.shareyourmove.com/public/storage/"
let U_UPLOAD_IMAGE = "customer/shipment/upload-image"

let U_SIGNUP = "sign-up"
let U_LOGIN = "login"
let U_LOGOUT = "logout"
let U_STATE_RESIDENCE = "state"
let U_GET_CITIES = "get-available-cities?type=1"
//var U_ALL_SHIPMENTS = "common/shipment/app-view/"
let U_EDIT_SHIPMENT = "customer/shipment/update-shipment-details"
let U_COMMON_SHIPMENTS = "common/shipment/view"
let U_DISPUTE_PROVIDER_SHIPMENT = "get-dispute-shipment"
let U_POST_BID_DRIVER = "common/shipment/post-bid"
let U_UPDATE_REJECTED_BID = "driver/shipment/edit-rejected-bid"
let U_RATE_USER = "add-rating-review"
let U_GET_RATINGS = "get-rating-review"
let U_DASHBOARD_DATA = "common/dashboard"
let U_ACCEPT_REJECT_BID_CUSTOMER = "customer/shipment/accept-bid"
let U_PAYMENT_API = "stripe-payment"
let U_CHECK_SHIPMENT_ELIGIBILITY = "common/shipment/check-eligibility"
let U_FORGOT_PASSWORD = "forget-password"
let U_RESET_PASSWORD = "reset-password"
let U_CONTACT_US = "contact-us"

let U_SEACRH_TRANSACTION_LIST = "customer/shipment/transaction-list"
let U_GET_SINGLE_TRANSACTION = "customer/shipment/single-transaction?shipment_id="
let U_GET_DRIVER_SINGLE_TRANSACTION = "driver/shipment/single-transaction?shipment_id="

let U_GET_CUSTOMER_TRANSACTION = "customer/shipment/transaction-list"
let U_GET_DRIVER_TRANSACTION = "driver/shipment/transaction-list"



let U_GET_CHAT_LIST = "get-chat-list"
let U_ADD_CHAT_MESSAGE = "add-chat-message"
let U_UPDATE_CHAT_MESSAGE = "get-chat-message"

let U_GET_CUSTOMER_PROFILE = "customer/profile"
let U_EDIT_CUSTOMER_PROFILE = "customer/profile-update"


let U_ADD_CARD = "add-my-card"
let U_DELETE_CARD = "delete-card"
let U_SET_CARD_AS_DEFAULT = "set-default-card"

let U_CREATE_STRIPE_ACCOUNT = "stripe/create-account"
let U_RETRIEVE_STRIPE_ACCOUNT = "stripe/retrieve-account"
let U_GET_STRIPE_ACCOUNT_DETAILS = "get-stripe-account-details"
let U_UPDATE_STRIPE_ACCOUNT = "stripe/update-account"
let U_GET_CANCELLED_SHIPMENT = "customer/shipment/cancelled-shipments"

let U_CUSTOMER_DISPUTE = "customer/shipment/dispute-shipment"
let U_ADD_NEW_SHIPMENT = "v2/shipments"
let U_GET_CATEGORIES = "customer/shipment/categories"
let U_CUSTOMER_ALL_SHIPMENTS = "customer/shipment/view"
let U_CUSTOMER_SINGLE_SHIPMENT = "customer/shipment/customer-single-shipment"
let U_DELETE_SHIPMENT_IMAGE = "customer/shipment/delete-image"
let U_CUSTOMER_SEARCH_BID = "customer/shipment/customer-search-bid"
let U_CUSTOMER_COMPLETE_SHIPMENT = "customer/shipment/shipment-completion?shipment_id="
let U_GET_CUSTOMER_SHIPMENT = "customer/shipment/customer-shipments"
let U_GET_DRIVER_SHIPMENT = "driver/shipment/shipment-chat-list"
let U_GET_NOTIFICATION = "get-notification-v2"

let U_REFUND_REQUEST = "customer/shipment/refund-shipment"

let U_EDIT_DRIVER_PROFILE = "common/update-profile"
let U_ACCEPT_BID_DRIVER = "driver/shipment/accept-bid"
let U_DRIVER_SINGLE_SHIPMENTS = "driver/shipment/single-shipment"
let U_PROFILE_DETAILS = "driver/profile-details"
let U_FEATURE_BID_DRIVER = "driver/shipment/pay-sponsor-amount"
let U_DRIVER_START_TRANSACTION = "driver/start-transaction"
let U_DRIVER_CHECK_SESSION = "driver/check-session"
let U_GET_DRIVER_SAVED_CARDS = "get-cards"
let U_DRIVER_START_SHIPMENT = "driver/shipment/shipment-start?shipment_id="
let U_DISPUTE_SHIPMENT = "driver/shipment/dispute-shipment"
let U_GET_DISPUTE_SHIPMENT = "get-dispute-shipment"
let U_GET_DRIVER_POSTED_BID = "common/shipment/posted-bid"
let U_EDIT_POSTED_BID = "driver/shipment/edit-posted-bid"
let U_DELETE_POSTED_BID = "driver/shipment/delete-posted-bid"

let U_SEND_OTP = "driver/shipment/send-otp"
let U_DRIVER_SECURITY_CHECK = "driver/security-check"

let U_GET_DRAFT_SHIPMENT = "customer/shipment/get-draft"
let U_SAVE_DRAFT_SHIPMENT = "customer/shipment/save-draft"
let U_GET_SINGLE_DRAFT = "customer/shipment/get-single-draft/"

//Ecommerce API
let U_GET_PRODUCT_CATEGORY = "products/categories/get"
let U_GET_PRODUCT = "products/get"
let U_GET_SINGLE_PRODUCT = "products/single/"
let U_ADD_TO_CART = "cart/add"
let U_GET_CART = "cart/get"
let U_UPDATE_CART = "cart/update"
let U_EMPTY_CART = "cart/empty"
let U_DELETE_CART = "cart/delete"
let U_PLACE_ORDER = "order/place"
let U_GET_ORDERS = "order/get"
let U_ADD_SHIPPING_ADDRESS = "shipping-address/add"
let U_GET_SHIPPING_ADDRESS = "shipping-address/get"
let U_UPDATE_SHIPPING_ADDRESS = "shipping-address/update"
let U_REMOVE_SHIPPING_ADDRESS = "shipping-address/remove"
let U_ORDER_RATING = "products/review/add"


//let U_PRIVACY_POLICY = "http://52.15.169.195/sym/privacy"
let U_PRIVACY_POLICY = "https://shareyourmove.com/privacy-policy"
//let U_TERMS_CONDITION = "http://52.15.169.195/sym/term-and-conditions"
let U_TERMS_CONDITION = "https://shareyourmove.com/term-and-conditions"
//Controller id's


//MARK: GOOGLE API Keys
var K_GOOGLE_API_KEY = "AIzaSyDdyinc_88KQdK0QufHq2qyc6hDkbmgV0s"

//MARK: Constants
var K_DRAFT_OR_EDIT = 1   //2 - draft

var K_IS_REPOST_CANCELLED = 0

var K_DRIVER_REHIRE_ID = 0


var K_COMING_DIRECT_AFTER_SIGNUP_DRIVER = 0

var K_SELECTED_DATE = ""
var K_CURRENT_CATEGORY = 0
var K_CUSTOM_YYYY_MM_DD = "yyyy-MM-dd"
var K_CURRENT_NAVIGATION = ""
let K_SIDE_MENU = "side_menu_navigation"
let K_FEATURE_BID_NAVIGATION = "feature_bid_navigation"
let K_CONFIRM_ORDER = "confirm_order_navigation"



//MARK: User Defaults
let UD_TOKEN = "access_token"
let UD_ROLE = "type-of-user"
let UD_FCM_TOKEN = "fcm_token"
let UD_INITIAL_SCREEN = "initial_screen"
let UD_SHIPMENT_DATA = "shipment_data"

//MARK: User Defaults Terminate

//MARK: Notification
let N_Date_Picker = "date-picker-post-shipment"
let N_OPEN_CATEGORY = "open_category_screen"
let N_SELECT_CARD = "select_card"
let N_CHANGE_CURRENT_INDEX = "change_current_index"
let N_CHANGE_CURRENT_TAB = "N_CHANGE_CURRENT_TAB"
let N_RELOAD_DASHBOARD_DATA = "N_RELOAD_DASHBOARD_DATA"
//MARK: (Notification) On RideNow Click You Can't change Source or Destination
