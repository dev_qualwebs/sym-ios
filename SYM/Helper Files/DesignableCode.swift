//
//  CustomButtom.swift
//
//  Created by Manisha  Sharma on 29/12/2018.
//  Copyright © 2018 Qualwebs. All rights reserved.
//

import UIKit

@IBDesignable
class CustomButton: UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return borderWidth
        } set {
            self.layer.borderWidth = newValue
        }
    }

    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
   
    
    @IBInspectable var fontSize: Int {
        get {
            return fontSize
        } set {
            self.titleLabel?.font = changeFont(val: newValue)
            
        }
    }

    
    @IBInspectable var circle: Bool {
        get {
            return circle
        } set {
            if newValue {
                self.layer.cornerRadius = self.frame.width/2
                self.clipsToBounds = true
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return cornerRadius
        } set {
            self.layer.cornerRadius = newValue
            self.clipsToBounds = true
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        } set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            self.layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        } set {
            self.layer.shadowOffset = newValue
        }
    }
}

@IBDesignable
class View: UIView {
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return borderWidth
        } set {
            self.layer.borderWidth = newValue
            
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var topCornorRound: CGFloat {
          get {
              return topCornorRound
          } set {
              self.clipsToBounds = false
              self.layer.cornerRadius = newValue
              self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
          }
      }
      
      @IBInspectable var bottomCornorRound: CGFloat {
          get {
              return topCornorRound
          } set {
              self.clipsToBounds = false
              self.layer.masksToBounds = false
              self.layer.cornerRadius = newValue
              self.layer.maskedCorners =  [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
          }
      }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return cornerRadius
        } set {
            self.layer.cornerRadius = newValue
            self.clipsToBounds = true
            self.layer.masksToBounds = false
        }
    }
    
    @IBInspectable var circle: Bool {
        get {
            return circle
        } set {
            if newValue {
                self.layer.cornerRadius = self.frame.width/2
                self.clipsToBounds = true
            }
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        } set {
            self.layer.shadowColor = newValue?.cgColor
            self.layer.masksToBounds = false
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            
            self.layer.shadowOpacity = newValue
            
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        } set {
            
            self.layer.shadowOffset = newValue
        }
    }
}

@IBDesignable
class ImageView: UIImageView {
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return borderWidth
        } set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        } set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            self.layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        } set {
            self.layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return cornerRadius
        } set {
            self.layer.cornerRadius = newValue
            self.clipsToBounds = true
        }
    }
    
    @IBInspectable var circle: Bool {
        get {
            return circle
        } set {
            if newValue {
                self.layer.cornerRadius = self.frame.width/2
                self.clipsToBounds = true
            }
        }
    }
    
    @IBInspectable var customTintColor: UIColor {
        get {
            return customTintColor
        } set {
            let templateImage =  self.image?.withRenderingMode(.alwaysTemplate)
            self.image = templateImage
            self.tintColor = newValue
        }
    }
}

@IBDesignable
class DesignableUITextField: UITextField {
    
    // Provides left padding for images
    
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return borderWidth
        } set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var fontSize: Int {
        get {
            return fontSize
        } set {
            self.font = changeFont(val: newValue)
            
        }
    }

    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.rightViewRect(forBounds: bounds)
        textRect.origin.x -= rightPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        } set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            self.layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        } set {
            self.layer.shadowOffset = newValue
        }
    }
  
    
    @IBInspectable var color: UIColor = primaryColor {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            leftView = imageView
        } else if let image = rightImage {
            rightViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            rightView = imageView
        } else {
            leftViewMode = UITextField.ViewMode.never
            leftView = nil
            rightViewMode = UITextField.ViewMode.never
            rightView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: UIColor.black])
    }
}


@IBDesignable
class DesignableUILabel: UILabel {
    
    // Provides left padding for images
    
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
   
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return borderWidth
        } set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var fontSize: Int {
        get {
            return fontSize
        } set {
            self.font = changeFont(val: newValue)
            
        }
    }

    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            self.layer.shadowOpacity = newValue
        }
    }
    
}


func changeFont(val: Int) -> UIFont {
    switch val {
    //For Main heading
    case 1:
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont.boldSystemFont(ofSize: 26)
        }
        else {
           return UIFont.boldSystemFont(ofSize: 24)
        }
        
    //FOR Main Title
    case 2:
         if UIDevice.current.userInterfaceIdiom == .pad {
             return UIFont.systemFont(ofSize: 24, weight: .semibold)
         }
         else {
            return UIFont.systemFont(ofSize: 21, weight: .semibold)
         }
    //FOR Main Sub-heading
    case 3:
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont.systemFont(ofSize: 19, weight: .semibold)
              }
              else {
            return UIFont.systemFont(ofSize: 17, weight: .semibold)
              }
    case 4:
         if UIDevice.current.userInterfaceIdiom == .pad {
                   return UIFont.systemFont(ofSize: 16, weight: .regular)
               }
               else {
                  return UIFont.systemFont(ofSize: 16, weight: .regular)
               }
    case 5:
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont.systemFont(ofSize: 14, weight: .regular)
        }
        else {
           return UIFont.systemFont(ofSize: 14, weight: .regular)
        }
    case 6:
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont.systemFont(ofSize: 13, weight: .regular)
        }
        else {
           return UIFont.systemFont(ofSize: 12, weight: .regular)
        }
    default:
     return UIFont.systemFont(ofSize: 17, weight: .regular)
    }
}
