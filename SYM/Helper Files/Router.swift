//
//  Router.swift
//  Diamonium
//
//  Created by Manisha  Sharma on 04/01/2019.
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import UIKit
import Foundation

class Router {

    static func launchSplash() {
      //  let splachScreen = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
      //  getRootViewController().pushViewController(splachScreen, animated: false)
    }

    static func mobileVC() {
        let mobileVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        getRootViewController().pushViewController(mobileVC, animated: false)
    }

    static func DriverHomeVC() {
        if(Singleton.shared.userType.role == 1){
            let mobileVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserTabViewController") as! UserTabViewController
            getRootViewController().pushViewController(mobileVC, animated: false)
        }else if(Singleton.shared.userType.role == 2){
            let mobileVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
            getRootViewController().pushViewController(mobileVC, animated: false)

        }else if(Singleton.shared.userType.role == 3){
            let mobileVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
            getRootViewController().pushViewController(mobileVC, animated: false)

        }else if(Singleton.shared.userType.role == 4){
          let mobileVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
          getRootViewController().pushViewController(mobileVC, animated: false)
        }
    }
    
    static func entryHomeVC() {
//        if let controller = UserDefaults.standard.value(forKey: UD_INITIAL_SCREEN) as? String{
//            let myVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PageViewController")
//            (myVC as! PageViewController).currentIndex = Int(controller)!
//            getRootViewController().pushViewController(myVC, animated: false)
//        }else {
        let mobileVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EntryViewController")
        getRootViewController().pushViewController(mobileVC, animated: false)
     //   }
    }

  
    static func getRootViewController() -> UINavigationController {
        let navController = UINavigationController()
        self.getWindow()?.rootViewController = navController
        return navController
    }

    static  func getWindow() -> UIWindow? {
        var window: UIWindow? = nil
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            window = appDelegate.window
        }
        return window
    }

   
}
