//
//  APICallingViewController.swift
//  SYM
//
//  Created by qw on 28/01/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class APICallingViewController: UIViewController {
    static var shared = APICallingViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    func getProfileData(){
        DispatchQueue.global(qos: .userInteractive).async {
            if let token = UserDefaults.standard.value(forKey: UD_TOKEN) as? String{
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_PROFILE_DETAILS, method: .get, parameter: nil, objectClass: GetDriverProfileDetailsResponse.self, requestCode: U_PROFILE_DETAILS, userToken: nil) { (response) in
                    Singleton.shared.userDetail = response.response[0]
                }
            }
        }
    }
    
    func getCategory(){
        DispatchQueue.global(qos: .userInteractive).async {
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CATEGORIES, method: .get, parameter: nil, objectClass: GetCategoriesResponse.self, requestCode: U_GET_CATEGORIES, userToken: nil) { (response) in
                Singleton.shared.categoryData = response.response
            }
        }
    }
    
    func getCartData(completionHandler: @escaping (CartData) -> Void){
        if(Singleton.shared.cartData.cart_items.count == 0){
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CART, method: .get, parameter: nil, objectClass: GetCart.self, requestCode: U_GET_CART, userToken: nil) { (response) in
                Singleton.shared.cartData = response.response
                completionHandler(response.response)
            }
        }else{
            completionHandler(Singleton.shared.cartData)
        }
        
    }
    
    func stateResidenceData(){
        DispatchQueue.global(qos: .userInteractive).async {
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_STATE_RESIDENCE, method: .get, parameter: nil, objectClass: GetStateResidenceResponse.self, requestCode: U_STATE_RESIDENCE, userToken: nil) { (response) in
                Singleton.shared.stateNames = response.response
            }
        }
    }
    
    func getShippingAddress(completionHandler: @escaping ([GetAddressResponse]) -> Void){
        if(Singleton.shared.addressData.count == 0){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SHIPPING_ADDRESS, method: .get, parameter: nil, objectClass: GetShippingAddress.self, requestCode: U_GET_SHIPPING_ADDRESS, userToken: nil) { (response) in
            Singleton.shared.addressData = response.response
            completionHandler(response.response)
        }
        }else {
            completionHandler(Singleton.shared.addressData)
        }
    }
    
    func getRatingData(offset:Int, sortBy:String ,completionHandler: @escaping (Ratings) -> Void){
        ActivityIndicator.show(view: self.view)
        if(Singleton.shared.ratingData.review_rating.count > 0){
            completionHandler(Singleton.shared.ratingData)
        }else {
            var path = String()
            if sortBy == "" {
                path = U_BASE + U_GET_RATINGS + "?offset=\(offset)"
            } else {
                path = U_BASE + U_GET_RATINGS + "?offset=\(offset)" + "&sort_by=" + sortBy
            }
            SessionManager.shared.methodForApiCalling(url: path, method: .get, parameter: nil, objectClass: GetRating.self, requestCode: U_GET_RATINGS, userToken: nil) { (response) in
                Singleton.shared.ratingData = response.response
                completionHandler(response.response)
            }
        }
    }
    
    func getCityData(){
      SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CITIES, method: .get, parameter: nil, objectClass: GetStateResidenceResponse.self, requestCode: U_GET_CITIES, userToken: nil) { (response) in
                Singleton.shared.cityNames = response.response
       }
    }
    
}
